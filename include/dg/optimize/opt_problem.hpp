#ifndef DG_OPT_PROBLEM_HPP
#define DG_OPT_PROBLEM_HPP

#include <Eigen/Dense>

namespace dg
{
  ///
  /// @brief Optimization problem
  ///
  template<class Real>
  struct opt_problem
  {
    typedef Real Scalar;
    typedef Eigen::Matrix<Real, Eigen::Dynamic, 1> TVector;

    virtual TVector operator()(const TVector& x) = 0;

    // TODO: approximate gradient?
    virtual TVector gradient(const TVector& x) = 0;

    virtual ~opt_problem() = default;
  };
}

#endif
