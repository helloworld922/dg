#ifndef DG_BFGS_HPP
#define DG_BFGS_HPP

#include <Eigen/Dense>

namespace dg
{
  ///
  /// @brief BFGS algorithm
  /// @tparam TProblem subclass of opt_problem<Real>
  ///
  template<class TProblem>
  struct bfgs
  {
    typedef typename TProblem::Scalar Scalar;
    typedef typename TProblem::TVector TVector;
    typedef Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic> TMatrix;

    TVector grad;
    TVector grad_next;
    TVector search_dir;

    // approx. inverse hessian
    TMatrix inv_hessian;
    TMatrix next_inv_hessian;

    ///
    /// @brief minimize problem, using initial guess x
    ///
    void minimize(TProblem& problem, TVector& x)
    {
      bool converged = false;
      while(!converged)
      {
        // find search direction
        grad = problem.gradient(x);
        search_dir = -inv_hessian * grad;
        // TODO: use line search to find step size
        Scalar step_size = 1;
        search_dir *= step_size;
        x += search_dir;
        grad_next = problem.gradient(x);
        auto y = grad_next - grad;
        Scalar sy = search_dir.transpose() * y;
        Scalar yBy = sy + y.transpose() * inv_hessian * y;
        next_inv_hessian.noalias() =
          inv_hessian +
          (sy + yBy) * (search_dir * search_dir.transpose()) / (sy * sy) -
          (inv_hessian * y * search_dir.transpose() +
            search_dir * y.transpose() * inv_hessian) /
            sy;
      }
    }
  };
}

#endif
