#ifndef DG_UNSTRUCTURED_PARTITION_HPP
#define DG_UNSTRUCTURED_PARTITION_HPP

#include "dg/utils/mpi_utils.hpp"

#include <parmetis.h>

#include <vector>

namespace dg
{
  namespace partitions
  {
    ///
    /// @brief Distributed unstructured mesh
    ///
    class unstructured
    {
      mpi::comm comm;
      
    };

    void mesh_dual(std::vector<idx_t>& elem_partition,
      std::vector<idx_t>& elem_starts, std::vector<idx_t>& elem_nodes,
      std::vector<idx_t>& xadj, std::vector<idx_t>& adjncy, idx_t ncommon,
      MPI_Comm comm);
  }
}

#endif
