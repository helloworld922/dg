#ifndef DG_STRUCTURED_PARTITION_HPP
#define DG_STRUCTURED_PARTITION_HPP

#include "dg/utils/mpi_utils.hpp"
#include "dg/utils/petsc_utils.hpp"

#include "dg/utils/indexer.hpp"

#include <vector>

namespace dg
{
  namespace partitions
  {
    ///
    /// @brief Mesh partitioning scheme representing a 1D structured mesh.
    /// TODO: generalize to work with N-D structured mesh
    ///
    struct structured
    {
      ///
      /// @brief Type of indexers into elements
      ///
      typedef dg::indexer<3, dg::layout_offset<>> int_type;
      
      ///
      /// @brief Type of indexers for boundary faces/ghosts
      ///
      typedef dg::indexer<1, dg::layout_offset<>> ext_type;

      ///
      /// @brief Indexer to the global vector of unknowns (not including
      /// external boundary nodes), sliced to the range owned.
      /// This allows translating a local index (input) to a global index
      /// (output)
      ///
      int_type global_idxr;

      ///
      /// @brief Indexer to the local vector of unknowns (not including
      /// external boundary nodes), sliced to the range owned.
      /// Useful for indexing into actual data arrays owned locally.
      ///
      int_type local_idxr;

      ///
      /// @brief Indexer which maps to the global mapping for the left domain
      /// boundary nodes. Left domain boundary nodes are owned by rank 0. Only
      /// has meaning in non-periodic meshes on rank 0.
      ///
      ext_type bc_L_global;
      
      ///
      /// @brief Indexer which maps to the global mapping for the right domain
      /// boundary nodes. Right domain boundary nodes are owned by rank nprocs -
      /// 1. Only has meaning in non-periodic meshes on rank nprocs - 1.
      ///
      ext_type bc_R_global;

      ///
      /// @brief Indexer which maps to the global mapping for the left patch face
      ///
      ext_type global_left;
      
      ///
      /// @brief Indexer which maps to the global mapping for the right patch
      /// face
      ///
      ext_type global_right;

      ///
      /// @brief Indexer which maps to the local left boundary face
      ///
      ext_type local_left;
      ///
      /// @brief Indexer which maps to the local right boundary face
      ///
      ext_type local_right;

      ///
      /// @brief TODO: what is this?
      ///
      size_t local_elem_start;
      ///
      /// @brief TODO: what is this?
      ///
      size_t local_elem_stop;

      /// @brief how many elements there are
      size_t nelems;
      /// @brief polynomial basis degree
      size_t degree;
      /// @brief Number of components
      size_t num_comps;

      /// @brief The MPI Rank number
      size_t rank;
      /// @brief Total number of processes this partition exists in
      size_t nprocs;

      /// @brief True if domain is periodic
      bool periodic;

      ///
      /// @brief Partitions a mesh with a uniform number of components in every
      /// element
      ///
      structured(size_t rank, size_t nprocs, size_t nelems, size_t degree,
        size_t num_comps, bool periodic);

      ///
      /// @brief Calculates the ghost indices
      ///
      std::vector<PetscInt> calc_ghost_idcs() const;

      ///
      /// @brief Allocate a vector and sets up the ghost nodes
      ///
      dg::petsc::vector alloc(MPI_Comm comm) const;
    };
  }
}
#endif
