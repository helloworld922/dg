#ifndef DG_CONTEXT_HPP
#define DG_CONTEXT_HPP

#include "utils/mpi_utils.hpp"
#include "utils/petsc_utils.hpp"

///
/// @brief The base DG namespace
///
namespace dg
{
  ///
  /// @brief A simulation context
  ///
  class context
  {
    ///
    /// @brief true if this context initialized MPI
    ///
    bool initialized_mpi_ = false;
    ///
    /// @brief true if this context initialized PETSc
    ///
    bool initialized_petsc_ = false;

    context() = default;

  public:
    ///
    /// @brief Primary communicator associated with this context
    ///
    mpi::comm comm;

    ///
    /// @brief Creates a context which tries to initialize MPI/PETSc if not
    /// previously initialized.
    /// Uses MPI_COMM_WORLD or PETSC_COMM_WORLD, depending on what has/has not
    /// been initialized
    ///
    context(int argn, char** argv, const char* petsc_db = nullptr,
      const char* petsc_help = nullptr,
      int thread_required = MPI_THREAD_FUNNELED);

    ///
    /// @brief Creates a context which uses a copy of the given MPI
    /// Communicator.
    /// MPI/PETSc must have been previously initialized.
    ///
    context(MPI_Comm c);

    context(const context& o) = delete;

    context& operator=(const context& o) = delete;

    ~context();
  };
}

#endif
