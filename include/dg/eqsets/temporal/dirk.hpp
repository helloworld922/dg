#ifndef DG_DIRK_HPP
#define DG_DIRK_HPP

#include "dg/utils/hslab.hpp"
#include "dg/utils/view_utils.hpp"
#include "dg/utils/compiler_support.hpp"

namespace dg
{
  namespace eqsets
  {
    ///
    /// @brief Backwards Euler
    ///
    template <size_t N = 1, class Real = double>
    class dirk1
    {
    public:
      ///
      /// @brief \f$\frac{1}{\Delta t}\f$
      ///
      Real idt;

      template <class Qs>
      bool advance_stage(Qs&& qs) noexcept
      {
        return true;
      }

      constexpr static size_t stage() noexcept
      {
        return 0;
      }

      constexpr static size_t num_stages() noexcept
      {
        return 1;
      }

      constexpr static size_t num_buffers() noexcept
      {
        // doesn't include residual vector
        return 2;
      }

      template <size_t S>
      constexpr static size_t stage_buffer() noexcept
      {
        return S;
      }

      constexpr static size_t stage_buffer(size_t s) noexcept
      {
        return s;
      }

      ///
      /// @brief Gets c for the given stage
      ///
      constexpr static Real c_coeff(size_t stage) noexcept
      {
        return 1;
      }

      template <class Q>
      auto source(const Q& q) const
      {
        auto qcurr = q.template get<0>();
        auto qprev = q.template get<1>();

        dg::shslab<double, N, 1> res(N);
        for (size_t i = 0; i < N; ++i)
        {
          res[i] = idt * (qprev[i] - qcurr[i]);
        }

        return res;
      }

      template <class Q, class D>
      void source_jac(const Q& q, double mult, D&& dst) const
      {
        for (size_t i = 0; i < N; ++i)
        {
          dst(i, i) += mult * idt;
        }
      }
    };

    ///
    /// @brief 2-stage, 2nd order stiffly accurate L-stable diagonally implicit
    /// Runge-Kutta method
    ///
    template <size_t N = 1, class Real = double>
    class dirk2
    {
      size_t stage_ = 0;

      constexpr static Real diag_coeff() noexcept
      {
        // 1/A_ii
        return 3.4142135623730950488;
      }

      constexpr static Real d10() noexcept
      {
        // D10 coefficient
        return -1.4142135623730950488;
      }

      constexpr static Real d11() noexcept
      {
        // D11 coefficient
        return 2.4142135623730950488;
      }

    public:
      ///
      /// @brief \f$\frac{1}{\Delta t}\f$
      ///
      Real idt;

      ///
      /// @brief Current RK stage
      ///
      size_t stage() const noexcept
      {
        return stage_;
      }

      constexpr static size_t num_stages() noexcept
      {
        return 2;
      }

      template <size_t S>
      constexpr static size_t stage_buffer() noexcept
      {
        return S;
      }

      constexpr static size_t stage_buffer(size_t s) noexcept
      {
        return s;
      }

      constexpr static size_t num_buffers() noexcept
      {
        return 3;
      }

      ///
      /// @brief Gets c for the given stage
      ///
      static Real c_coeff(size_t stage) noexcept
      {
        switch (stage)
        {
        case 0:
          return 0.292893218813452476;
        case 1:
          return 1;
        default:
          DG_UNREACHABLE;
        }
      }

      ///
      /// @brief Advance to the next stage
      /// @param qs view_sequence of all q buffers
      /// @return true if the stage advance finishes this timestep
      /// @tparam Qs
      ///
      template <class Qs>
      bool advance_stage(Qs&& qs) noexcept
      {
        switch (stage_)
        {
        case 0:
        {
          stage_ = 1;
          auto& q0 = dg::get<0>(qs);
          auto& q1 = dg::get<1>(qs);
          dg::util::nd_transform(
            [](auto&& q0_, auto&& q1_) { q1_ = q1_ * d11() + q0_ * d10(); }, q0,
            q1);
        }
          return false;
        case 1:
          stage_ = 0;
          // stiffly accurate, so no work required
          return true;
        default:
          DG_UNREACHABLE;
        }
      }

      template <class Q>
      auto source(const Q& q) const
      {
        auto qcurr = q.template get<0>();
        auto qprev = q.template get<1>();

        dg::shslab<double, N, 1> res(N);
        for (size_t i = 0; i < N; ++i)
        {
          res[i] = idt * diag_coeff() * (qprev[i] - qcurr[i]);
        }

        return res;
      }

      template <class Q, class D>
      void source_jac(const Q& q, double mult, D&& dst) const
      {
        for (size_t i = 0; i < N; ++i)
        {
          dst(i, i) += mult * diag_coeff() * idt;
        }
      }
    };

    ///
    /// @brief 4-stage, 3rd order stiffly accurate L-stable diagonally implicit
    /// Runge-Kutta method
    ///
    template <size_t N = 1, class Real = double>
    class dirk3
    {
      size_t stage_ = 0;

      constexpr static Real diag_coeff() noexcept
      {
        // 1/A_ii
        return 2;
      }

      constexpr static Real d10() noexcept
      {
        return 0.666666666666666666666666666666666666666666666666666666667;
      }

      constexpr static Real d11() noexcept
      {
        return 0.333333333333333333333333333333333333333333333333333333333;
      }

    public:
      ///
      /// @brief \f$\frac{1}{\Delta t}\f$
      ///
      Real idt;

      ///
      /// @brief Current RK stage
      ///
      size_t stage() const noexcept
      {
        return stage_;
      }

      constexpr static size_t num_stages() noexcept
      {
        return 4;
      }

      constexpr static size_t num_buffers() noexcept
      {
        // q1 and q2 buffers are re-purposed
        return 3;
      }

      ///
      /// @brief What buffer is expected to be used for a given stage
      ///
      template <size_t S>
      constexpr static size_t stage_buffer() noexcept
      {
        switch (S)
        {
        case 0:
          return 0;
        case 1:
          return 1;
        case 2:
          return 2;
        case 3:
          return 1;
        case 4:
          return 2;
        default:
          DG_UNREACHABLE;
        }
      }

      constexpr static size_t stage_buffer(size_t s) noexcept
      {
        switch (s)
        {
        case 0:
          return 0;
        case 1:
          return 1;
        case 2:
          return 2;
        case 3:
          return 1;
        case 4:
          return 2;
        default:
          DG_UNREACHABLE;
        }
      }

      ///
      /// @brief Gets c for the given stage
      ///
      static Real c_coeff(size_t stage) noexcept
      {
        switch (stage)
        {
        case 0:
        case 2:
          return 0.5;
        case 1:
          return 0.6666666666666666666666666666666666667;
        case 3:
          return 1;
        default:
          DG_UNREACHABLE;
        }
      }

      template <class Qs>
      bool advance_stage(Qs&& qs) noexcept
      {
        switch (stage_)
        {
        case 0:
        {
          stage_ = 1;
          auto& q0 = dg::get<0>(qs);
          auto& q1 = dg::get<1>(qs);
          dg::util::nd_transform(
            [](auto& q0, auto& q1) { q1 = q1 * d11() + q0 * d10(); }, q0, q1);
          return false;
        }
        case 1:
        {
          stage_ = 2;
          auto& q0 = dg::get<0>(qs);
          auto& q1 = dg::get<1>(qs);
          auto& q2 = dg::get<2>(qs);
          dg::util::nd_transform(
            [](auto& q0, auto& q1, auto& q2) { q2 = q2 - 4 * q1 + 4 * q0; }, q0,
            q1, q2);
          return false;
        }
        case 2:
        {
          stage_ = 3;
          auto& q0 = dg::get<0>(qs);
          auto& q2 = dg::get<2>(qs);
          // q1 is no longer used after this, re-purpose it's storage for q3
          auto& q3 = dg::get<1>(qs);
          dg::util::nd_transform(
            [](auto& q0, auto& q2, auto& q3) { q3 = q3 - 4 * q2 + 4 * q0; }, q0,
            q2, q3);
          return false;
        }
        case 3:
        {
          stage_ = 0;
          // stiffly accurate, so no work required
          return true;
        }
        default:
          DG_UNREACHABLE;
        }
      }

      template <class Q>
      auto source(const Q& q) const
      {
        auto qcurr = q.template get<0>();
        auto qprev = q.template get<1>();

        dg::shslab<double, N, 1> res(N);
        for (size_t i = 0; i < N; ++i)
        {
          res[i] = idt * diag_coeff() * (qprev[i] - qcurr[i]);
        }

        return res;
      }

      template <class Q, class D>
      void source_jac(const Q& q, double mult, D&& dst) const
      {
        for (size_t i = 0; i < N; ++i)
        {
          dst(i, i) += mult * diag_coeff() * idt;
        }
      }
    };
  } // namespace eqsets
} // namespace dg

#endif
