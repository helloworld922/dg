#ifndef DG_FIVE_MOMENT_FIELD_SOURCE_TCC
#define DG_FIVE_MOMENT_FIELD_SOURCE_TCC

namespace dg
{
  namespace eqsets
  {
    template <class Real>
    template <class M>
    auto field_source<Real>::source(const M& momentum) const
    {
      dg::shslab<Real, 3, 1> res(3);
      for (size_t i = 0; i < 3; ++i)
      {
        res[i] = norm_factor * momentum[i];
      }
      return res;
    }
  } // namespace eqsets
} // namespace dg

#endif
