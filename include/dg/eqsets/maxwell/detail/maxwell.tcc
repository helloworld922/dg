#ifndef DG_MAXWELL_TCC
#define DG_MAXWELL_TCC

#include <cmath>

namespace dg
{
  namespace eqsets
  {
    template <class Real>
    template <class Q>
    auto maxwell<1, Real>::int_flux(const Q& fields) const
    {
      auto res = dg::make_view_sequence(
        dg::shslab<Real, 3, 1>(3), dg::shslab<Real, 3, 1>(3));
      auto& efield = get<0>(fields);
      auto& bfield = get<1>(fields);
      auto& erhs = get<0>(res);
      auto& brhs = get<1>(res);

      erhs[0] = 0;
      erhs[1] = c0 * c0 * bfield[2];
      erhs[2] = -c0 * c0 * bfield[1];

      brhs[0] = 0;
      brhs[1] = -efield[2];
      brhs[2] = efield[1];
      return res;
    }

    template <class Real>
    template <class QOut, class QIn>
    auto maxwell<1, Real>::num_flux(
      const QOut& qout, const QIn& qin, Real normal) const
    {
      auto res = dg::make_view_sequence(
        dg::shslab<Real, 3, 1>(3), dg::shslab<Real, 3, 1>(3));
      auto& eout = get<0>(qout);
      auto& bout = get<1>(qout);
      auto& ein = get<0>(qin);
      auto& bin = get<1>(qin);
      auto& erhs = get<0>(res);
      auto& brhs = get<1>(res);
      erhs[0] = 0;
      erhs[1] =
        0.5 * c0 * (normal * c0 * (bin[2] + bout[2]) - (eout[1] - ein[1]));
      erhs[2] =
        0.5 * c0 * (normal * -c0 * (bin[1] + bout[1]) - (eout[2] - ein[2]));

      brhs[0] = 0;
      brhs[1] = 0.5 * (normal * (-ein[2] - eout[2]) - c0 * (bout[1] - bin[1]));
      brhs[2] = 0.5 * (normal * (ein[1] + eout[1]) - c0 * (bout[2] - bin[2]));
      return res;
    }

    template <class Real>
    template <class QOut, class QIn>
    auto maxwell<1, Real>::b2_num_flux(
      const QOut& qout, const QIn& qin, Real normal, Real gamma) const
    {
      auto res = dg::make_view_sequence(
        dg::shslab<Real, 3, 1>(3), dg::shslab<Real, 1, 1>(1));
      auto& eout = get<0>(get<2>(qout));
      auto& bout = get<1>(get<2>(qout));
      auto& qout_i = get<0>(qout);
      auto& qout_e = get<1>(qout);

      auto& ein = get<0>(get<2>(qin));
      auto& bin = get<1>(get<2>(qin));
      // auto& qin_i = get<0>(qin);
      // auto& qin_e = get<1>(qin);
      auto& brhs = get<0>(res);
      auto& b2rhs = get<1>(res);

      brhs[0] = 0;
      brhs[1] = 0.5 * (normal * (-ein[2] - eout[2]) - c0 * (bout[1] - bin[1]));
      brhs[2] = 0.5 * (normal * (ein[1] + eout[1]) - c0 * (bout[2] - bin[2]));

      // TODO: fix
      b2rhs[0] = 0;
#if 0

      auto be_out =
        0.5 * (bout[0] * bout[0] + bout[1] * bout[1] + bout[2] * bout[2]);
      auto be_in = 0.5 * (bin[0] * bin[0] + bin[1] * bin[1] + bin[2] * bin[2]);

      auto ee_out =
        0.5 * (eout[0] * eout[0] + eout[1] * eout[1] + eout[2] * eout[2]) /
        (c0 * c0);
      auto ee_in =
        0.5 * (ein[0] * ein[0] + ein[1] * ein[1] + ein[2] * ein[2]) / (c0 * c0);

      // poynting energy
      // TODO: penalty speed?
      auto energy_in = (ein[1] * bin[2] - ein[2] * bin[1]);
      auto energy_out = (eout[1] * bout[2] - eout[2] * bout[1]);
      b2rhs[0] = 0.5 * (normal * (energy_in + energy_out) -
                         c0 * (be_out + ee_out - (be_in + ee_in)));
#endif

      return res;
    }

    template <class Real>
    template <class Q>
    auto maxwell<2, Real>::int_flux(const Q& fields) const
    {
      auto res = dg::make_view_sequence(
        dg::shslab<Real, 6, 2>(3, 2), dg::shslab<Real, 6, 2>(3, 2));
      auto& efield = get<0>(fields);
      auto& bfield = get<1>(fields);
      auto& erhs = get<0>(res);
      auto& brhs = get<1>(res);

      erhs(0, 0) = 0;
      erhs(0, 1) = -c0 * c0 * bfield[2];
      erhs(1, 0) = c0 * c0 * bfield[2];
      erhs(1, 1) = 0;
      erhs(2, 0) = -c0 * c0 * bfield[1];
      erhs(2, 1) = c0 * c0 * bfield[0];

      brhs(0, 0) = 0;
      brhs(0, 1) = efield[2];
      brhs(1, 0) = -efield[2];
      brhs(1, 1) = 0;
      brhs(2, 0) = efield[1];
      brhs(2, 1) = -efield[0];
      return res;
    }

    template <class Real>
    template <class Q>
    auto maxwell<3, Real>::int_flux(const Q& fields) const
    {
      auto res = dg::make_view_sequence(
        dg::shslab<Real, 9, 3>(3, 3), dg::shslab<Real, 9, 3>(3, 3));
      auto& efield = get<0>(fields);
      auto& bfield = get<1>(fields);
      auto& erhs = get<0>(res);
      auto& brhs = get<1>(res);

      erhs(0, 0) = 0;
      erhs(0, 1) = -c0 * c0 * bfield[2];
      erhs(0, 2) = c0 * c0 * bfield[1];
      erhs(1, 0) = c0 * c0 * bfield[2];
      erhs(1, 1) = 0;
      erhs(1, 2) = -c0 * c0 * bfield[0];
      erhs(2, 0) = -c0 * c0 * bfield[1];
      erhs(2, 1) = c0 * c0 * bfield[0];
      erhs(2, 2) = 0;

      brhs(0, 0) = 0;
      brhs(0, 1) = efield[2];
      brhs(0, 2) = -efield[1];
      brhs(1, 0) = -efield[2];
      brhs(1, 1) = 0;
      brhs(1, 2) = efield[0];
      brhs(2, 0) = efield[1];
      brhs(2, 1) = -efield[0];
      brhs(2, 2) = 0;
      return res;
    }
  } // namespace eqsets
} // namespace dg

#endif
