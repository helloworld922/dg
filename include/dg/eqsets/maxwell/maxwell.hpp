#ifndef DG_MAXWELL_HPP
#define DG_MAXWELL_HPP

#include "dg/utils/view.hpp"
#include "dg/utils/hslab.hpp"

#include <cstddef>
#include <array>

namespace dg
{
  namespace eqsets
  {
    ///
    /// @brief Base class for standard Maxwell's Equations.
    /// @tparam NDims
    ///
    template <size_t NDims, class Real = double>
    class maxwell;

    ///
    /// @brief 1D specialization
    ///
    template <class Real>
    class maxwell<1, Real>
    {
    public:
      /// c0/v0 = skin_depth_norm * omega_p_norm
      Real c0;

      ///
      /// @brief Evaluate the internal flux function
      /// @param fields a view_sequence where the first view is the electric
      /// field, and the second view is the magnetic field.
      /// @return a view_sequence for the RHS of the efield and bfield
      ///
      template <class Q>
      auto int_flux(const Q& fields) const;

      ///
      /// @brief Evaluates the numerical flux function
      /// @param qout a view_sequence where the first view is the electric
      /// field, and the second view is the magnetic field.
      /// @param qin a view_sequence where the first view is the electric
      /// field, and the second view is the magnetic field.
      ///
      template <class QOut, class QIn>
      auto num_flux(const QOut& qout, const QIn& qin, Real normal) const;

      ///
      /// @brief Evaluates the numerical flux function for transition for two-fluid->MHD
      /// @param qout a view_sequence where the first view is the electric
      /// field, and the second view is the magnetic field.
      /// @param qin a view_sequence where the first view is the electric
      /// field, and the second view is the magnetic field.
      /// @return view_sequence, first entry is flux_B (Faraday's Law), second
      /// is B.flux_B (magnetic field energy flow)
      ///
      template <class QOut, class QIn>
      auto b2_num_flux(const QOut& qout, const QIn& qin, Real normal, Real gamma) const;
    };

    ///
    /// @brief 2D specialization
    ///
    template <class Real>
    class maxwell<2, Real> : protected maxwell<1, Real>
    {
      typedef maxwell<1, Real> type_1d;
    public:
      using type_1d::c0;
      
      ///
      /// @brief Evaluate the internal flux function
      ///
      template <class Q>
      auto int_flux(const Q& q) const;

      ///
      /// @brief Evaluates the numerical flux function
      ///
      template <class EIn, class BIn, class EOut, class BOut, class N>
      auto num_flux(const EOut& eout, const BOut& bout, const EIn& ein,
        const BIn& bin, const N& normal) const;
    };

    ///
    /// @brief 3D specialization
    ///
    template <class Real>
    class maxwell<3, Real> : protected maxwell<2, Real>
    {
      typedef maxwell<1, Real> type_1d;
      typedef maxwell<2, Real> type_2d;
    public:
      using type_1d::c0;
      
      ///
      /// @brief Evaluate the internal flux function
      ///
      template <class Q>
      auto int_flux(const Q& q) const;

      ///
      /// @brief Evaluates the numerical flux function
      ///
      template <class EIn, class BIn, class EOut, class BOut, class N>
      auto num_flux(const EOut& eout, const BOut& bout, const EIn& ein,
        const BIn& bin, const N& normal) const;
    };
  }
}

#include "detail/maxwell.tcc"

#endif
