#ifndef DG_FIELD_SOURCE_HPP
#define DG_FIELD_SOURCE_HPP

#include "dg/utils/hslab.hpp"

namespace dg
{
  namespace eqsets
  {
    ///
    /// @brief Multifluid E&M field source terms for Ampere's Law.
    ///
    template<class Real = double>
    class field_source
    {
    public:
      ///
      /// @brief normalization factor:
      /// \f[
      /// -(\omega_{p} \tau)^{2} \left(\frac{\delta_{p}}{L}\right)\left(\frac{Z_{s}}{A_{s}}\right)
      /// \f]
      ///
      Real norm_factor;

      ///
      /// @brief M species momentum vector
      ///
      template <class M>
      auto source(const M& momentum) const;
    };
  }
}

#include "detail/field_source.tcc"

#endif
