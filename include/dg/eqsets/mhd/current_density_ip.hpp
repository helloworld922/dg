#ifndef DG_MHD_CURRENT_DENSITY_IP_HPP
#define DG_MHD_CURRENT_DENSITY_IP_HPP

#include "current_density.hpp"

namespace dg
{
  namespace eqsets
  {
    ///
    /// @brief MHD current density calculation using interior penalty method
    /// @tparam NDims
    /// @tparam Real
    ///
    template <size_t NDims, class Real = double>
    class current_density_ip;

    /// @brief 1D specialization
    template<class Real>
    class current_density_ip<1, Real> : private current_density<1, Real>
    {
      typedef current_density<1, Real> super_type;
      
    public:
      using super_type::skin_depth_norm;

      using super_type::int_flux;

      ///
      /// @brief Computes the numerical flux
      /// @param qout B outside the interface
      /// @param qin B inside the interface
      /// @param normal outward pointing normal
      ///
      template <class QOut, class QIn>
      auto num_flux(const QOut& qout, const QIn& qin, Real normal) const;
    };
  }
}

#include "detail/current_density_ip.tcc"

#endif
