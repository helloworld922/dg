#ifndef DG_MHD_CURRENT_DENSITY_HPP
#define DG_MHD_CURRENT_DENSITY_HPP

#include "dg/utils/view.hpp"
#include "dg/utils/hslab.hpp"

#include <cstddef>
#include <array>

namespace dg
{
  namespace eqsets
  {
    ///
    /// @brief Base class for calculating MHD current density.
    /// This only implements the internal flux terms, subclasses extend this to
    /// the appropriate numerical flux.
    ///
    template<size_t NDims, class Real = double>
    class current_density;

    ///
    /// @brief 1D specialization
    ///
    template<class Real>
    class current_density<1, Real>
    {
    public:
      ///
      /// @brief skin depth
      ///
      Real skin_depth_norm;

      ///
      /// @brief Evaluate the internal flux function
      /// @param q magnetic field
      /// @return magnetic field RHS
      ///
      template<class Q>
      auto int_flux(const Q& q) const;
    };
  }
}

#include "detail/current_density.tcc"

#endif
