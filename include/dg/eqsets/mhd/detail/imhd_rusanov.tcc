#ifndef DG_IMHD_RUSANOV_TCC
#define DG_IMHD_RUSANOV_TCC

#include <cmath>

namespace dg
{
  namespace eqsets
  {
    template <class Real>
    template <class QOut, class QIn>
    auto imhd_rusanov<1, Real>::num_flux(
      const QOut& qout_, const QIn& qin_, Real normal) const
    {
      constexpr size_t rho = 0;
      constexpr size_t px = 1;
      constexpr size_t py = 2;
      constexpr size_t pz = 3;
      constexpr size_t e = 4;

      constexpr size_t x = 0;
      constexpr size_t y = 1;
      constexpr size_t z = 2;

      auto& qout = dg::get<0>(qout_);
      auto& bout = dg::get<1>(qout_);
      auto& qin = dg::get<0>(qin_);
      auto& bin = dg::get<1>(qin_);

      auto res = dg::make_view_sequence(
        dg::shslab<Real, 5, 1>(5), dg::shslab<Real, 3, 1>(3));

      auto& rq = dg::get<0>(res);
      auto& rb = dg::get<1>(res);

      auto inv_rho_out = 1 / qout[rho];
      auto inv_rho_in = 1 / qin[rho];

      auto ux_out = qout[px] * inv_rho_out;
      auto ux_in = qin[px] * inv_rho_in;

      auto ke_in = .5 * inv_rho_in *
                   (qin[px] * qin[px] + qin[py] * qin[py] + qin[pz] * qin[pz]);

      auto ke_out =
        .5 * inv_rho_out *
        (qout[px] * qout[px] + qout[py] * qout[py] + qout[pz] * qout[pz]);

      auto be_out =
        0.5 * (bout[x] * bout[x] + bout[y] * bout[y] + bout[z] * bout[z]);
      auto be_in = 0.5 * (bin[x] * bin[x] + bin[y] * bin[y] + bin[z] * bin[z]);

      auto P_out = (gamma - 1) * (qout[e] - ke_out - be_out);
      auto P_in = (gamma - 1) * (qin[e] - ke_in - be_in);

      // compute fastest wavespeed
      auto fms_out =
        fabs(ux_out) +
        sqrt(
          inv_rho_out * (be_out + gamma * P_out) +
          sqrt(pow(bout[x], 4) +
               2 * bout[x] * bout[x] *
                 (bout[y] * bout[y] + bout[z] * bout[z] - gamma * P_out) +
               pow(bout[y] * bout[y] + bout[z] * bout[z] + gamma * P_out, 2)));

      auto fms_in =
        fabs(ux_in) +
        sqrt(inv_rho_in * (be_in + gamma * P_in) +
             sqrt(pow(bin[x], 4) +
                  2 * bin[x] * bin[x] *
                    (bin[y] * bin[y] + bin[z] * bin[z] - gamma * P_in) +
                  pow(bin[y] * bin[y] + bin[z] * bin[z] + gamma * P_in, 2)));
      auto lambda = std::max(fms_in, fms_out);

      rq[0] =
        0.5 * (normal * (qin[px] + qout[px]) - lambda * (qout[rho] - qin[rho]));
      rq[1] = 0.5 * (normal * (qout[px] * ux_out + P_out + be_out -
                                bout[x] * bout[x] + qin[px] * ux_in + P_in +
                                be_in - bin[x] * bin[x]) -
                      lambda * (qout[px] - qin[px]));
      rq[2] = 0.5 * (normal * (qout[py] * ux_out - bout[x] * bout[y] +
                                qin[py] * ux_in - bin[x] * bin[y]) -
                      lambda * (qout[py] - qin[py]));
      rq[3] = 0.5 * (normal * (qout[pz] * ux_out - bout[x] * bout[z] +
                                qin[pz] * ux_in - bin[x] * bin[z]) -
                      lambda * (qout[pz] - qin[pz]));
      rq[4] =
        0.5 *
        (normal *
            ((qout[e] + P_out + be_out) * ux_out -
              inv_rho_out *
                (qout[px] * bout[x] + qout[py] * bout[y] + qout[pz] * bout[z]) *
                bout[x] +
              (qin[e] + P_in + be_in) * ux_in -
              inv_rho_in *
                (qin[px] * bin[x] + qin[py] * bin[y] + qin[pz] * bin[z]) *
                bin[x]) -
          lambda * (qout[e] - qin[e]));

      rb[0] = 0;
      rb[1] =
        0.5 *
        (normal * ((qout[px] * bout[y] - qout[py] * bout[x]) * inv_rho_out +
                    (qin[px] * bin[y] - qin[py] * bin[x]) * inv_rho_in) -
          lambda * (bout[y] - bin[y]));
      rb[2] =
        0.5 *
        (normal * ((qout[px] * bout[z] - qout[pz] * bout[x]) * inv_rho_out +
                    (qin[px] * bin[z] - qin[pz] * bin[x]) * inv_rho_in) -
          lambda * (bout[z] - bin[z]));
      return res;
    }
  }
}

#endif
