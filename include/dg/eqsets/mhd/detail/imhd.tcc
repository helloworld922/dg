#ifndef DG_IMHD_TCC
#define DG_IMHD_TCC

namespace dg
{
  namespace eqsets
  {

    template <class Real>
    template <class Q>
    auto imhd<1, Real>::compute_E(const Q& q_, Real inv_rho) const
    {
      constexpr size_t px = 1;
      constexpr size_t py = 2;
      constexpr size_t pz = 3;
      auto& q = dg::get<0>(q_);
      auto& b = dg::get<1>(q_);
      dg::shslab<Real, 3, 1> res(3);
      res[0] = -inv_rho * (q[py] * b[2] - q[pz] * b[1]);
      res[1] = -inv_rho * (q[pz] * b[0] - q[px] * b[2]);
      res[2] = -inv_rho * (q[px] * b[1] - q[py] * b[0]);
      return res;
    }
    
    template <class Real>
    template <class Q>
    auto imhd<1, Real>::int_flux(const Q& q_)
    {
      constexpr size_t rho = 0;
      constexpr size_t px = 1;
      constexpr size_t py = 2;
      constexpr size_t pz = 3;
      constexpr size_t e = 4;

      constexpr size_t x = 0;
      constexpr size_t y = 1;
      constexpr size_t z = 2;

      auto& q = dg::get<0>(q_);
      auto& B = dg::get<1>(q_);

      inv_rho = 1 / q[rho];

      Real ux = q[px] * inv_rho;
      Real be = 0.5 * (B[x] * B[x] + B[y] * B[y] + B[z] * B[z]);

      Real P =
        (gamma - 1) *
        (q[e] -
          0.5 * inv_rho * (q[px] * q[px] + q[py] * q[py] + q[pz] * q[pz]) - be);

      auto res = dg::make_view_sequence(
        dg::shslab<Real, 5, 1>(5), dg::shslab<Real, 3, 1>(3));

      auto& rq = dg::get<0>(res);
      auto& rb = dg::get<1>(res);

      rq[0] = q[px];
      rq[1] = q[px] * ux + P + be - B[x] * B[x];
      rq[2] = q[py] * ux - B[x] * B[y];
      rq[3] = q[pz] * ux - B[x] * B[z];
      rq[4] = (q[e] + P + be) * ux -
              inv_rho * (q[px] * B[x] + q[py] * B[y] + q[pz] * B[z]) * B[x];
      rb[0] = 0;
      rb[1] = inv_rho * (q[px] * B[y] - q[py] * B[x]);
      rb[2] = inv_rho * (q[px] * B[z] - q[pz] * B[x]);
      return res;
    }
  }
}

#endif
