#ifndef DG_MHD_CURRENT_DENSITY_TCC
#define DG_MHD_CURRENT_DENSITY_TCC

namespace dg
{
  namespace eqsets
  {
    template <class Real>
    template <class Q>
    auto current_density<1, Real>::int_flux(const Q& q) const
    {
      constexpr size_t Bx = 0;
      constexpr size_t By = 1;
      constexpr size_t Bz = 2;

      dg::shslab<Real, 3, 1> res(3);

      res[0] = 0;
      res[1] = -skin_depth_norm * q[Bz];
      res[2] = skin_depth_norm * q[By];
      
      return res;
    }
  }
}

#endif
