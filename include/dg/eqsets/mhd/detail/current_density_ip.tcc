#ifndef DG_MHD_CURRENT_DENSITY_IP_TCC
#define DG_MHD_CURRENT_DENSITY_IP_TCC

#include <cmath>

namespace dg
{
  namespace eqsets
  {
    template <class Real>
    template <class QOut, class QIn>
    auto current_density_ip<1, Real>::num_flux(
      const QOut& qout, const QIn& qin, Real normal) const
    {
      constexpr size_t Bx = 0;
      constexpr size_t By = 1;
      constexpr size_t Bz = 2;

      dg::shslab<Real, 3, 1> res(3);
      res[0] = 0;
      res[1] = -skin_depth_norm * 0.5 * (qout[Bz] + qin[Bz]) * normal;
      res[2] = skin_depth_norm * 0.5 * (qout[By] + qin[By]) * normal;
      return res;
    }
  }
}

#endif
