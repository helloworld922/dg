#ifndef DG_IMHD_RUSANOV_HPP
#define DG_IMHD_RUSANOV_HPP

#include "imhd.hpp"

namespace dg
{
  namespace eqsets
  {
    ///
    /// @brief Ideal MHD with a Rusanov numerical flux
    /// @tparam NDims
    /// @tparam Real
    ///
    template <size_t NDims, class Real = double>
    class imhd_rusanov;

    template<class Real>
    class imhd_rusanov<1, Real> : private imhd<1, Real>
    {
      typedef imhd<1, Real> super_type;
      
    public:
      using super_type::gamma;

      imhd_rusanov(Real gamma) : super_type(gamma)
      {}

      using super_type::int_flux;

      using super_type::compute_E;

      ///
      /// @brief Computes the numerical flux
      /// @param qout fluid variables outside the interface
      /// @param qin fluid variables inside the interface
      /// @param bout B field outside the interface
      /// @param bin B field inside the interface
      /// @param normal outward pointing normal
      ///
      template <class QOut, class QIn>
      auto num_flux(const QOut& qout, const QIn& qin, Real normal) const;
    };
  }
}

#include "detail/imhd_rusanov.tcc"

#endif
