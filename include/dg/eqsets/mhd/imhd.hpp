#ifndef DG_IMHD_HPP
#define DG_IMHD_HPP

#include "dg/utils/view.hpp"
#include "dg/utils/hslab.hpp"

#include <cstddef>
#include <array>

namespace dg
{
  namespace eqsets
  {
    ///
    /// @brief Base class for Ideal MHD system. This assumes a unified energy equation.
    /// This only implements the internal flux terms, subclasses extend this to
    /// the appropriate numerical flux.
    ///
    template<size_t NDims, class Real = double>
    class imhd;

    ///
    /// @brief 1D specialization
    ///
    template<class Real>
    class imhd<1, Real>
    {
    public:
      ///
      /// @brief gas gamma
      ///
      const Real gamma;

      imhd(Real gamma) : gamma(gamma)
      {
      }

      ///
      /// @brief Evaluate the internal flux function
      /// @param q Variable sequence, first view is MHD fluid variables, second
      /// view is magnetic field
      /// @return a view_sequence, first view is MHD fluid variables, second
      /// view is magnetic field
      ///
      template<class Q>
      auto int_flux(const Q& q);

      ///
      /// @brief Computes the electric field, given q
      ///
      template <class Q>
      auto compute_E(const Q& q, Real inv_rho) const;

    protected:
      /// @brief Cache of the last computed inverse mass density
      Real inv_rho;
    };
  }
}

#include "detail/imhd.tcc"

#endif
