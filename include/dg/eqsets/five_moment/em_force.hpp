#ifndef DG_FIVE_MOMENT_EM_FORCE_HPP
#define DG_FIVE_MOMENT_EM_FORCE_HPP

#include "dg/utils/view.hpp"
#include "dg/utils/hslab.hpp"

#include <cstddef>
#include <array>

namespace dg
{
  namespace eqsets
  {
    ///
    /// @brief E&M body force term for a five moment fluid.
    /// Expects the E&M fields to be E&B formulation.
    ///
    template <class Real = double>
    class five_moment_em_force
    {
    public:
      ///
      /// @brief normalization factor:
      /// \f[
      /// \frac{Z_s}{A_s} \left(\frac{L}{\delta_p}\right)
      /// \f]
      ///
      Real norm_factor;

      ///
      /// @brief Evaluates the internal flux function
      /// @param Q view sequence, first are fluid variables, second is E field,
      /// third is B field
      ///
      template <class Q>
      auto source(const Q& q) const;
    };
  } // namespace eqsets
} // namespace dg

#include "detail/em_force.tcc"

#endif
