#ifndef DG_EULER_RUSANOV_HPP
#define DG_EULER_RUSANOV_HPP

#include "euler.hpp"

namespace dg
{
  namespace eqsets
  {
    ///
    /// @brief Five moment Euler fluid Rusanov flux
    ///
    template <size_t NDims, class Real = double>
    class euler_rusanov;

    ///
    /// @brief 1D specialization
    ///
    template <class Real>
    class euler_rusanov<1, Real> : private euler<1, Real>
    {
      typedef euler<1, Real> super_type;

    public:
      using super_type::gamma;

      using super_type::int_flux;

      ///
      /// @brief Evaluates the numerical flux function
      ///
      template <class QOut, class QIn>
      std::array<Real, 5> num_flux(
        const QOut& qout, const QIn& qin, Real normal) const;
    };

    ///
    /// @brief 2D specialization
    ///
    template <class Real>
    class euler_rusanov<2, Real> : private euler<2, Real>
    {
      typedef euler<2, Real> super_type;

    public:
      using super_type::gamma;

      using super_type::int_flux;

      ///
      /// @brief Evaluates the numerical flux function
      ///
      template <class QOut, class QIn, class N>
      std::array<Real, 5> num_flux(
        const QOut& qout, const QIn& qin, const N& normal) const;
    };

    ///
    /// @brief 3D specialization
    ///
    template <class Real>
    class euler_rusanov<3, Real> : private euler<3, Real>
    {
      typedef euler<3, Real> super_type;

    public:
      using super_type::gamma;

      using super_type::int_flux;

      ///
      /// @brief Evaluates the numerical flux function
      ///
      template <class QOut, class QIn, class N>
      std::array<Real, 5> num_flux(
        const QOut& qout, const QIn& qin, const N& normal) const;
    };
  }
}

#include "detail/euler_rusanov.tcc"

#endif
