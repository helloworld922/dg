#ifndef DG_EULER_HPP
#define DG_EULER_HPP

#include "dg/utils/view.hpp"
#include "dg/utils/hslab.hpp"

#include <cstddef>
#include <array>

namespace dg
{
  namespace eqsets
  {
    ///
    /// @brief Base class for five moment Euler system.
    /// This only implements the internal flux terms, subclasses extend this to
    /// the appropriate numerical flux
    /// @tparam NDims
    ///
    template <size_t NDims, class Real = double>
    class euler;

    ///
    /// @brief 1D specialization
    ///
    template <class Real>
    class euler<1, Real>
    {
    public:
      ///
      /// @brief gas gamma
      ///
      Real gamma;

      ///
      /// @brief Evaluate the internal flux function
      ///
      template <class Q>
      std::array<Real, 5> int_flux(const Q& q);
      
    protected:
      /// @brief Cache of the last computed inverse mass density
      Real inv_rho;
    };

    ///
    /// @brief 2D specialization
    ///
    template <class Real>
    class euler<2, Real> : protected euler<1, Real>
    {
      typedef euler<1, Real> type_1d;
    public:
      using type_1d::gamma;

      ///
      /// @brief Evaluate the internal flux function
      ///
      template <class Q>
      dg::shslab<Real, 10, 2> int_flux(const Q& q);

    private:
      using type_1d::inv_rho;
    };

    ///
    /// @brief 3D specialization
    ///
    template <class Real>
    class euler<3, Real> : protected euler<2, Real>
    {
      typedef euler<1, Real> type_1d;
      typedef euler<2, Real> type_2d;
      
    public:
      using type_1d::gamma;

      ///
      /// @brief Evaluate the internal flux function
      ///
      template <class Q>
      dg::shslab<Real, 15, 2> int_flux(const Q& q);
      
    private:
      using type_1d::inv_rho;
    };
  }
}

#include "detail/euler.tcc"
#endif
