#ifndef DG_EULER_RUSANOV_TCC
#define DG_EULER_RUSANOV_TCC

#include <cmath>

namespace dg
{
  namespace eqsets
  {
    template <class Real>
    template <class QOut, class QIn>
    std::array<Real, 5> euler_rusanov<1, Real>::num_flux(
      const QOut& qout, const QIn& qin, Real normal) const
    {
      constexpr size_t rho = 0;
      constexpr size_t px = 1;
      constexpr size_t py = 2;
      constexpr size_t pz = 3;
      constexpr size_t e = 4;

      auto inv_rho_out = 1 / qout[rho];
      auto inv_rho_in = 1 / qin[rho];
      auto P_out =
        (gamma - 1) * (qout[e] - .5 * inv_rho_out *
                                   (qout[px] * qout[px] + qout[py] * qout[py] +
                                     qout[pz] * qout[pz]));
      auto P_in =
        (gamma - 1) * (qin[e] - .5 * inv_rho_in *
                                  (qin[px] * qin[px] + qin[py] * qin[py] +
                                    qin[pz] * qin[pz]));

      auto cs_out = sqrt(inv_rho_out * gamma * P_out);
      auto cs_in = sqrt(inv_rho_in * gamma * P_in);

      auto lambda = std::max(fabs(qout[px]) * inv_rho_out + cs_out,
        fabs(qin[px]) * inv_rho_in + cs_in);

      std::array<Real, 5> res = {
        0.5 * (normal * (qin[px] + qout[px]) - lambda * (qout[rho] - qin[rho])),
        0.5 * (normal * (inv_rho_in * qin[px] * qin[px] + P_in +
                          inv_rho_out * qout[px] * qout[px] + P_out) -
                lambda * (qout[px] - qin[px])),
        0.5 * (normal * (inv_rho_in * qin[px] * qin[py] +
                          inv_rho_out * qout[px] * qout[py]) -
                lambda * (qout[py] - qin[py])),
        0.5 * (normal * (inv_rho_in * qin[px] * qin[pz] +
                          inv_rho_out * qout[px] * qout[pz]) -
                lambda * (qout[pz] - qin[pz])),
        0.5 * (normal * (inv_rho_in * qin[px] * (qin[e] + P_in) +
                          inv_rho_out * qout[px] * (qout[e] + P_out)) -
                lambda * (qout[e] - qin[e]))};
      return res;
    }

    template <class Real>
    template <class QOut, class QIn, class N>
    std::array<Real, 5> euler_rusanov<2, Real>::num_flux(
      const QOut& qout, const QIn& qin, const N& normal) const
    {
      constexpr size_t rho = 0;
      constexpr size_t px = 1;
      constexpr size_t py = 2;
      constexpr size_t pz = 3;
      constexpr size_t e = 4;

      auto inv_rho_out = 1 / qout[rho];
      auto inv_rho_in = 1 / qin[rho];
      auto P_out =
        (gamma - 1) * (qout[e] - .5 * inv_rho_out *
                                   (qout[px] * qout[px] + qout[py] * qout[py] +
                                     qout[pz] * qout[pz]));
      auto P_in =
        (gamma - 1) * (qin[e] - .5 * inv_rho_in *
                                  (qin[px] * qin[px] + qin[py] * qin[py] +
                                    qin[pz] * qin[pz]));

      auto cs_out = sqrt(inv_rho_out * gamma * P_out);
      auto cs_in = sqrt(inv_rho_in * gamma * P_in);

      auto lambda = std::max(
        fabs(qout[px] * normal[0] + qout[py] * normal[1]) * inv_rho_out +
          cs_out,
        fabs(qin[px] * normal[0] + qin[py] * normal[1]) * inv_rho_in + cs_in);

      std::array<Real, 5> res = {0.5 * (normal[0] * (qin[px] + qout[px]) +
                                         normal[1] * (qin[py] + qout[py]) -
                                         lambda * (qout[rho] - qin[rho])),
        0.5 * (normal[0] * (inv_rho_in * qin[px] * qin[px] + P_in +
                             inv_rho_out * qout[px] * qout[px] + P_out) +
                normal[1] * (inv_rho_in * qin[px] * qin[py] +
                              inv_rho_out * qout[px] * qout[py]) -
                lambda * (qout[px] - qin[px])),
        0.5 * (normal[0] * (inv_rho_in * qin[px] * qin[py] +
                             inv_rho_out * qout[px] * qout[py]) +
                normal[1] * (inv_rho_in * qin[py] * qin[py] + P_in +
                              inv_rho_out * qout[py] * qout[py] + P_out) -
                lambda * (qout[py] - qin[py])),
        0.5 * (normal[0] * (inv_rho_in * qin[px] * qin[pz] +
                             inv_rho_out * qout[px] * qout[pz]) +
                normal[1] * (inv_rho_in * qin[py] * qin[pz] +
                              inv_rho_out * qout[py] * qout[pz]) -
                lambda * (qout[pz] - qin[pz])),
        0.5 * (normal[0] * (inv_rho_in * qin[px] * (qin[e] + P_in) +
                             inv_rho_out * qout[px] * (qout[e] + P_out)) +
                normal[1] * (inv_rho_in * qin[py] * (qin[e] + P_in) +
                              inv_rho_out * qout[py] * (qout[e] + P_out)) -
                lambda * (qout[e] - qin[e]))};
      return res;
    }
  } // namespace eqsets
} // namespace dg

#endif
