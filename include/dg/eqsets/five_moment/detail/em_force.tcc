#ifndef DG_FIVE_MOMENT_EM_FORCE_TCC
#define DG_FIVE_MOMENT_EM_FORCE_TCC

#include "dg/utils/hslab.hpp"

namespace dg
{
  namespace eqsets
  {
    template <class Real>
    template <class Q>
    auto five_moment_em_force<Real>::source(const Q& q) const
    {
      constexpr size_t rho = 0;
      constexpr size_t px = 1;
      constexpr size_t py = 2;
      constexpr size_t pz = 3;
      constexpr size_t e = 4;

      auto& fluid = dg::get<0>(q);
      auto& efield = dg::get<1>(q);
      auto& bfield = dg::get<2>(q);

      dg::shslab<Real, 4, 1> res(4);
      res[0] = norm_factor * (fluid[rho] * efield[0] + fluid[py] * bfield[2] -
                               fluid[pz] * bfield[1]);
      res[1] = norm_factor * (fluid[rho] * efield[1] + fluid[pz] * bfield[0] -
                               fluid[px] * bfield[2]);
      res[2] = norm_factor * (fluid[rho] * efield[2] + fluid[px] * bfield[1] -
                               fluid[py] * bfield[0]);
      res[3] = norm_factor * (fluid[px] * efield[0] + fluid[py] * efield[1] +
                               fluid[pz] * efield[2]);
      return res;
    }
  }
}

#endif
