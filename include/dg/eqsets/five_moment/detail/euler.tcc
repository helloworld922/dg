#ifndef DG_EULER_TCC
#define DG_EULER_TCC

namespace dg
{
  namespace eqsets
  {
    template <class Real>
    template <class Q>
    std::array<Real, 5> euler<1, Real>::int_flux(const Q& q)
    {
      constexpr size_t rho = 0;
      constexpr size_t px = 1;
      constexpr size_t py = 2;
      constexpr size_t pz = 3;
      constexpr size_t e = 4;

      inv_rho = 1 / q[rho];

      auto P =
        (gamma - 1) *
        (q[e] - static_cast<Real>(.5) *
                  (q[px] * q[px] + q[py] * q[py] + q[pz] * q[pz]) * inv_rho);
      std::array<Real, 5> res = {q[px], q[px] * q[px] * inv_rho + P,
        q[px] * q[py] * inv_rho, q[px] * q[pz] * inv_rho,
        (q[e] + P) * q[px] * inv_rho};
      return res;
    }

    template <class Real>
    template <class Q>
    dg::shslab<Real, 10, 2> euler<2, Real>::int_flux(const Q& q)
    {
      constexpr size_t rho = 0;
      constexpr size_t px = 1;
      constexpr size_t py = 2;
      constexpr size_t pz = 3;
      constexpr size_t e = 4;

      inv_rho = 1 / q[rho];

      auto P =
        (gamma - 1) *
        (q[e] - static_cast<Real>(.5) *
                  (q[px] * q[px] + q[py] * q[py] + q[pz] * q[pz]) * inv_rho);
      dg::shslab<Real, 10, 2> res(5, 2);
      res(0, 0) = q[px];
      res(0, 1) = q[py];
      res(1, 0) = q[px] * q[px] * inv_rho + P;
      res(1, 1) = q[px] * q[py] * inv_rho;
      res(2, 0) = q[px] * q[py] * inv_rho;
      res(2, 1) = q[py] * q[py] * inv_rho + P;
      res(3, 0) = q[px] * q[pz] * inv_rho;
      res(3, 1) = q[py] * q[pz] * inv_rho;
      res(4, 0) = (q[e] + P) * q[px] * inv_rho;
      res(4, 1) = (q[e] + P) * q[py] * inv_rho;
      return res;
    }

    template <class Real>
    template <class Q>
    dg::shslab<Real, 15, 2> euler<3, Real>::int_flux(const Q& q)
    {
      constexpr size_t rho = 0;
      constexpr size_t px = 1;
      constexpr size_t py = 2;
      constexpr size_t pz = 3;
      constexpr size_t e = 4;

      inv_rho = 1 / q[rho];

      auto P =
        (gamma - 1) *
        (q[e] - static_cast<Real>(.5) *
                  (q[px] * q[px] + q[py] * q[py] + q[pz] * q[pz]) * inv_rho);
      dg::shslab<Real, 15, 2> res(5, 3);
      res(0, 0) = q[px];
      res(0, 1) = q[py];
      res(0, 2) = q[pz];
      res(1, 0) = q[px] * q[px] * inv_rho + P;
      res(1, 1) = q[px] * q[py] * inv_rho;
      res(1, 2) = q[px] * q[pz] * inv_rho;
      res(2, 0) = q[px] * q[py] * inv_rho;
      res(2, 1) = q[py] * q[py] * inv_rho + P;
      res(2, 2) = q[py] * q[pz] * inv_rho;
      res(3, 0) = q[px] * q[pz] * inv_rho;
      res(3, 1) = q[py] * q[pz] * inv_rho;
      res(3, 2) = q[pz] * q[pz] * inv_rho + P;
      res(4, 0) = (q[e] + P) * q[px] * inv_rho;
      res(4, 1) = (q[e] + P) * q[py] * inv_rho;
      res(4, 2) = (q[e] + P) * q[pz] * inv_rho;
      return res;
    }
  } // namespace eqsets
} // namespace dg

#endif
