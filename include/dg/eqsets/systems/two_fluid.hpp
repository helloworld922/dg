#ifndef DG_TWO_FLUID_SYSTEM_HPP
#define DG_TWO_FLUID_SYSTEM_HPP

#include "dg/eqsets/five_moment/euler_rusanov.hpp"
#include "dg/eqsets/five_moment/em_force.hpp"
#include "dg/eqsets/maxwell/field_source.hpp"
#include "dg/eqsets/maxwell/maxwell.hpp"

namespace dg
{
  namespace systems
  {
    ///
    /// @brief Inviscid ideal two-fluid system (no collision terms)
    ///
    template <size_t NDims, class Real = double,
      class ENumFlux = eqsets::euler_rusanov<NDims, Real>>
    class ideal_two_fluid
    {
    public:
      /// @brief Maxwell's equations
      eqsets::maxwell<NDims, Real> msys;
      /// @brief ion current contribution to ampere's low
      eqsets::field_source<Real> ion_fsource;
      /// @brief electron current contribution to ampere's low
      eqsets::field_source<Real> elec_fsource;

      /// @brief Euler flux for ions
      ENumFlux ion_fluid;
      /// @brief Euler flux for electrons
      ENumFlux elec_fluid;
      /// @brief E&M body force on ion species
      eqsets::five_moment_em_force<Real> ion_em_force;
      /// @brief E&M body force on electron species
      eqsets::five_moment_em_force<Real> elec_em_force;

      /// @brief normalized plasma frequency
      const Real omega_p_norm;
      /// @brief normalized skin depth
      const Real skin_depth_norm;
      /// @brief ion gas gamma
      const Real gamma_i;
      /// @brief electron gas gamma
      const Real gamma_e;
      /// @brief normalized ion mass
      const Real Ai;
      /// @brief normalized ion charge
      const Real Zi;
      /// @brief normalized electron mass
      const Real Ae;
      /// @brief normalized electron charge
      const Real Ze;

      ///
      /// @param omega_p_norm
      /// @param skin_depth_norm
      /// @param gamma_i
      /// @param gamma_e
      /// @param Ai
      /// @param Zi
      /// @param Ae
      /// @param Ze
      ///
      ideal_two_fluid(Real omega_p_norm, Real skin_depth_norm, Real gamma_i,
        Real gamma_e, Real Ai, Real Zi, Real Ae, Real Ze)
        : omega_p_norm(omega_p_norm), skin_depth_norm(skin_depth_norm),
          gamma_i(gamma_i), gamma_e(gamma_e), Ai(Ai), Zi(Zi), Ae(Ae), Ze(Ze)
      {
        msys.c0 = skin_depth_norm * omega_p_norm;
        ion_fsource.norm_factor =
          -skin_depth_norm * omega_p_norm * omega_p_norm * Zi / Ai;
        elec_fsource.norm_factor =
          -skin_depth_norm * omega_p_norm * omega_p_norm * Ze / Ae;

        ion_fluid.gamma = gamma_i;
        elec_fluid.gamma = gamma_e;
        ion_em_force.norm_factor = Zi / (Ai * skin_depth_norm);
        elec_em_force.norm_factor = Ze / (Ae * skin_depth_norm);
      }

      ///
      /// @brief Evaluates the internal flux for the ideal two-fluid system
      /// @param q
      /// @tparam Q view_sequence
      ///
      template <class Q>
      auto int_flux(const Q& q);

      ///
      /// @brief Evaluates the numerical flux for the ideal two-fluid system
      /// @param qout
      /// @param qin
      /// @param normal
      /// @tparam QOut view_sequence
      /// @tparam QIn view_sequence
      /// @tparam N real
      ///
      template <class QOut, class QIn, class N>
      auto num_flux(const QOut& qout, const QIn& qin, const N& normal);

      ///
      /// @brief Evaluates the source terms for the ideal two-fluid system
      /// @param q
      /// @tparam Q view_sequence
      ///
      template <class Q>
      auto source(const Q& q);
    };
  } // namespace systems
} // namespace dg

#include "detail/two_fluid.tcc"

#endif
