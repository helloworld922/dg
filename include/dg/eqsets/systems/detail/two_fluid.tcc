#ifndef DG_TWO_FLUID_SYSTEM_TCC
#define DG_TWO_FLUID_SYSTEM_TCC

namespace dg
{
  namespace systems
  {
    template <size_t NDims, class Real, class ENumFlux>
    template <class Q>
    auto ideal_two_fluid<NDims, Real, ENumFlux>::int_flux(const Q& q)
    {
      return dg::make_view_sequence(ion_fluid.int_flux(dg::get<0>(q)),
        elec_fluid.int_flux(dg::get<1>(q)), msys.int_flux(dg::get<2>(q)));
    }

    template <size_t NDims, class Real, class ENumFlux>
    template <class QOut, class QIn, class N>
    auto ideal_two_fluid<NDims, Real, ENumFlux>::num_flux(
      const QOut& qout, const QIn& qin, const N& normal)
    {
      return dg::make_view_sequence(
        ion_fluid.num_flux(dg::get<0>(qout), dg::get<0>(qin), normal),
        elec_fluid.num_flux(dg::get<1>(qout), dg::get<1>(qin), normal),
        msys.num_flux(dg::get<2>(qout), dg::get<2>(qin), normal));
    }

    template <size_t NDims, class Real, class ENumFlux>
    template <class Q>
    auto ideal_two_fluid<NDims, Real, ENumFlux>::source(const Q& q)
    {
      return dg::make_view_sequence(ion_em_force.source(dg::get<0>(q)),
        elec_em_force.source(dg::get<1>(q)), ion_fsource.source(dg::get<2>(q)),
        elec_fsource.source(dg::get<3>(q)));
    }
  }
}

#endif
