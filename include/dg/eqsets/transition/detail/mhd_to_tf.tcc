#ifndef DG_MHD_TO_TF_TCC
#define DG_MHD_TO_TF_TCC

#include "dg/utils/hslab.hpp"

namespace dg
{
  namespace eqsets
  {
    template <class TFSys, class MHDSys>
    template <class QOut, class QIn, class N>
    auto mhd_to_tf<TFSys, MHDSys>::num_flux(
      const QOut& qout, const QIn& qin, const N& normal)
    {
      auto& q_mhd = dg::get<0>(dg::get<0>(qout));
      auto& qout_B = dg::get<1>(dg::get<0>(qout));
      auto& j_mhd = dg::get<1>(qout);

      typedef typename std::decay_t<decltype(q_mhd)>::value_type real_type;

      // convert MHD variables in qin to two-fluid variables
      dg::shslab<real_type, 5, 1> qout_ion(5);
      dg::shslab<real_type, 5, 1> qout_elec(5);
      constexpr size_t rho = 0;
      constexpr size_t px = 1;
      constexpr size_t py = 2;
      constexpr size_t pz = 3;
      constexpr size_t e = 4;
      constexpr size_t x = 0;
      constexpr size_t y = 1;
      constexpr size_t z = 2;

      auto inv_rho = 1 / q_mhd[rho];

      qout_ion[rho] = q_mhd[rho];
      qout_ion[px] = q_mhd[px];
      qout_ion[py] = q_mhd[py];
      qout_ion[pz] = q_mhd[pz];

      auto ne = ne_mult * q_mhd[rho];

      qout_elec[rho] = tf_sys.Ae * ne;

      qout_elec[px] = pe_mult * (tf_sys.Ai * j_mhd[x] - tf_sys.Zi * q_mhd[px]);
      qout_elec[py] = pe_mult * (tf_sys.Ai * j_mhd[y] - tf_sys.Zi * q_mhd[py]);
      qout_elec[pz] = pe_mult * (tf_sys.Ai * j_mhd[z] - tf_sys.Zi * q_mhd[pz]);

      // ideal MHD Gen. Ohm's law
      // TODO: generalize by call into MHDSys?
      auto qout_E = mhd_sys.compute_E(dg::get<0>(qout), inv_rho);

      // ion kinetic energy
      auto ke_i = 0.5 *
                  (qout_ion[px] * qout_ion[px] + qout_ion[py] * qout_ion[py] +
                    qout_ion[pz] * qout_ion[pz]) *
                  inv_rho;
      // electron kinetic energy
      auto ke_e =
        0.5 *
        (qout_elec[px] * qout_elec[px] + qout_elec[py] * qout_elec[py] +
          qout_elec[pz] * qout_elec[pz]) /
        qout_elec[rho];
      // magnetic field energy
      auto mag_energy = 0.5 * (qout_B[x] * qout_B[x] + qout_B[y] * qout_B[y] +
                                qout_B[z] * qout_B[z]);

      // compute T
      auto ni = q_mhd[rho] / tf_sys.Ai;
      auto T = (q_mhd[e] - ke_i - mag_energy) * (mhd_sys.gamma - 1) / (ne + ni);

      qout_ion[e] = ni * T / (tf_sys.gamma_i - 1) + ke_i;
      qout_elec[e] = ne * T / (tf_sys.gamma_e - 1) + ke_e;

      // pass in view of qout_E instead of shslab to avoid data copying
      return tf_sys.num_flux(
        dg::make_view_sequence(qout_ion, qout_elec,
          dg::make_view_sequence(qout_E.subarray(dg::all{}), qout_B)),
        qin, normal);
    }
  }
}

#endif
