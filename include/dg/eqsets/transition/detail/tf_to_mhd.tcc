#ifndef DG_TF_TO_MHD_TCC
#define DG_TF_TO_MHD_TCC

#include "dg/utils/hslab.hpp"

namespace dg
{
  namespace eqsets
  {
    template <class TFSys, class MHDSys>
    template <class QOut, class QIn, class N>
    auto tf_to_mhd<TFSys, MHDSys>::num_flux(
      const QOut& qout, const QIn& qin, const N& normal)
    {
      // TODO: could call into mhd_to_tf(qin, qout, -normal)
      // only problem is reversing normal...
      auto& q_mhd = dg::get<0>(dg::get<0>(qin));
      auto& qin_B = dg::get<1>(dg::get<0>(qin));
      auto& j_mhd = dg::get<1>(qin);

      typedef typename std::decay_t<decltype(q_mhd)>::value_type real_type;

      // convert MHD variables in qin to two-fluid variables
      dg::shslab<real_type, 5, 1> qin_ion(5);
      dg::shslab<real_type, 5, 1> qin_elec(5);
      constexpr size_t rho = 0;
      constexpr size_t px = 1;
      constexpr size_t py = 2;
      constexpr size_t pz = 3;
      constexpr size_t e = 4;
      constexpr size_t x = 0;
      constexpr size_t y = 1;
      constexpr size_t z = 2;
      auto inv_rho = 1 / q_mhd[rho];

      qin_ion[rho] = q_mhd[rho];
      qin_ion[px] = q_mhd[px];
      qin_ion[py] = q_mhd[py];
      qin_ion[pz] = q_mhd[pz];

      auto ne = -tf_sys.Zi * q_mhd[rho] / (tf_sys.Ze * tf_sys.Ai);

      qin_elec[rho] = tf_sys.Ae * ne;
      qin_elec[px] =
        tf_sys.Ae / tf_sys.Ze * (j_mhd[0] - tf_sys.Zi * q_mhd[px] / tf_sys.Ai);
      qin_elec[py] =
        tf_sys.Ae / tf_sys.Ze * (j_mhd[1] - tf_sys.Zi * q_mhd[py] / tf_sys.Ai);
      qin_elec[pz] =
        tf_sys.Ae / tf_sys.Ze * (j_mhd[2] - tf_sys.Zi * q_mhd[pz] / tf_sys.Ai);

      // ideal MHD Gen. Ohm's law
      auto qin_E = mhd_sys.compute_E(dg::get<0>(qin), inv_rho);

      // ion kinetic energy
      auto ke_i = 0.5 * (qin_ion[px] * qin_ion[px] + qin_ion[py] * qin_ion[py] +
                          qin_ion[pz] * qin_ion[pz]) /
                  qin_ion[rho];
      // electron kinetic energy
      auto ke_e = 0.5 * (qin_elec[px] * qin_elec[px] + qin_elec[py] * qin_elec[py] +
                          qin_elec[pz] * qin_elec[pz]) /
                  qin_elec[rho];
      // magnetic field energy
      auto mag_energy =
        0.5 * (qin_B[x] * qin_B[x] + qin_B[y] * qin_B[y] + qin_B[z] * qin_B[z]);

      // compute T
      auto ni = q_mhd[rho] / tf_sys.Ai;
      auto T = (q_mhd[e] - ke_i - mag_energy) * (mhd_sys.gamma - 1) / (ne + ni);

      qin_ion[e] = ni * T / (tf_sys.gamma_i - 1) + ke_i;
      qin_elec[e] = ne * T / (tf_sys.gamma_e - 1) + ke_e;

      // pass in view of qin_E instead of shslab to avoid data copying
      auto qin_tf = dg::make_view_sequence(qin_ion, qin_elec,
        dg::make_view_sequence(qin_E.subarray(dg::all{}), qin_B));

      auto res = dg::make_view_sequence(
        tf_sys.ion_fluid.num_flux(dg::get<0>(qout), dg::get<0>(qin_tf), normal),
        dg::shslab<real_type, 3, 1>(3));

      auto& res_f = dg::get<0>(res);
      auto& res_b = dg::get<1>(res);

      {
        // electrons
        auto tmp = tf_sys.elec_fluid.num_flux(
          dg::get<1>(qout), dg::get<1>(qin_tf), normal);
        // TODO: what to do about energy?
        for (size_t i = 4; i < 5; ++i)
        {
          res_f[i] += tmp[i];
        }
      }

      {
        // E&M
        auto tmp =
          tf_sys.msys.b2_num_flux(qout, qin_tf, normal, mhd_sys.gamma);
        for (size_t i = 0; i < 3; ++i)
        {
          res_b[i] = dg::get<0>(tmp)[i];
        }
        res_f[4] += dg::get<1>(tmp)[0];
      }

      return res;
    }
  }
}

#endif
