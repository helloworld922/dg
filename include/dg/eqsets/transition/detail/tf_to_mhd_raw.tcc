#ifndef DG_TF_TO_MHD_RAW_TCC
#define DG_TF_TO_MHD_RAW_TCC

#include "dg/utils/hslab.hpp"

namespace dg
{
  namespace eqsets
  {
    template <class TFSys, class MHDSys>
    template <class QOut, class QIn, class N>
    auto tf_to_mhd_raw<TFSys, MHDSys>::num_flux(
      const QOut& qout, const QIn& qin, const N& normal)
    {
      auto& qion = dg::get<0>(qout);
      auto& qelec = dg::get<1>(qout);
      auto& E = dg::get<0>(dg::get<2>(qout));
      auto& B = dg::get<1>(dg::get<2>(qout));

      typedef typename std::decay_t<decltype(qion)>::value_type real_type;

      // convert two-fluid variables to MHD variables
      dg::shslab<real_type, 5, 1> qout_mhd(5);
      constexpr size_t rho = 0;
      constexpr size_t px = 1;
      constexpr size_t py = 2;
      constexpr size_t pz = 3;
      constexpr size_t e = 4;
      constexpr size_t x = 0;
      constexpr size_t y = 1;
      constexpr size_t z = 2;
      
      qout_mhd[rho] = qion[rho];
      qout_mhd[px] = qion[px];
      qout_mhd[py] = qion[py];
      qout_mhd[pz] = qion[pz];

      auto Be = B[x] * B[x] + B[y] * B[y] + B[z] * B[z];
      auto ke_i =
        0.5 *
        (qion[px] * qion[px] + qion[py] * qion[py] + qion[pz] * qion[pz]) /
        qion[rho];
      auto ke_e =
        0.5 *
        (qelec[px] * qelec[px] + qelec[py] * qelec[py] + qelec[pz] * qelec[pz]) /
        qelec[rho];

      qout_mhd[e] =
        (tf_sys.gamma_i - 1) * (qion[e] - ke_i) / (mhd_sys.gamma - 1) + ke_i +
        (tf_sys.gamma_e - 1) * (qelec[e] - ke_e) / (mhd_sys.gamma - 1) +
        0.5 * Be;

      // pass in view of qin_E instead of shslab to avoid data copying
      return mhd_sys.num_flux(dg::make_view_sequence(qout_mhd, B), qin, normal);
    }
  }
}

#endif
