#ifndef DG_MHD_TO_TF_HPP
#define DG_MHD_TO_TF_HPP

namespace dg
{
  namespace eqsets
  {
    ///
    /// @brief helper for computing the two-fluid to single temperature MHD
    /// numerical flux
    ///
    template<class TFSys, class MHDSys>
    class mhd_to_tf
    {
      TFSys& tf_sys;
      MHDSys& mhd_sys;

      typedef decltype(tf_sys.msys.c0) real_type;

      real_type inv_c0_2;

      const real_type ne_mult;
      const real_type pe_mult;

    public:
      mhd_to_tf(TFSys& tf_sys, MHDSys& mhd_sys)
        : tf_sys(tf_sys), mhd_sys(mhd_sys),
          inv_c0_2(1 / (tf_sys.msys.c0 * tf_sys.msys.c0)),
          ne_mult(-tf_sys.Zi / (tf_sys.Ze * tf_sys.Ai)),
          pe_mult(tf_sys.Ae / (tf_sys.Ze*tf_sys.Ai))
      {}
      
      ///
      /// @param qout MHD variables
      /// @param qin two-fluid variables
      /// @return Two-fluid fluxes
      ///
      template<class QOut, class QIn, class N>
      auto num_flux(const QOut& qout, const QIn& qin, const N& normal);
    };
  }
}

#include "detail/mhd_to_tf.tcc"

#endif
