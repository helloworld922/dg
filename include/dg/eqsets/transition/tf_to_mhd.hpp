#ifndef DG_TF_TO_MHD_HPP
#define DG_TF_TO_MHD_HPP

namespace dg
{
  namespace eqsets
  {
    ///
    /// @brief helper for computing the two-fluid to single temperature MHD
    /// numerical flux
    ///
    template<class TFSys, class MHDSys>
    class tf_to_mhd
    {
      TFSys& tf_sys;
      MHDSys& mhd_sys;

      typedef decltype(tf_sys.msys.c0) real_type;

    public:
      tf_to_mhd(TFSys& tf_sys, MHDSys& mhd_sys)
        : tf_sys(tf_sys), mhd_sys(mhd_sys)
      {}
      
      ///
      /// @param qout two-fluid variables
      /// @param qin MHD variables
      /// @return Two-fluid fluxes
      ///
      template<class QOut, class QIn, class N>
      auto num_flux(const QOut& qout, const QIn& qin, const N& normal);
    };
  }
}

#include "detail/tf_to_mhd.tcc"

#endif
