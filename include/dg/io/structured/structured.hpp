#ifndef DG_IO_STRUCTURED_HPP
#define DG_IO_STRUCTURED_HPP

namespace dg
{
  namespace io
  {
    ///
    /// @brief I/O utilities for a structured dataset
    ///
    template <size_t ndims>
    struct structured;
  }
}

#endif
