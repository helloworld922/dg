#ifndef DG_IO_STRUCTURED_1D_TCC
#define DG_IO_STRUCTURED_1D_TCC

namespace dg
{
  namespace io
  {
    structured<1>::structured(dg::hdf5::location& loc) : loc(loc)
    {
      dg::hdf5::plists::file_access fa(loc);
      fa.get_mpio(&comm.reset(), nullptr);
      if (comm)
      {
        MPI_Comm_rank(comm, &rank);
        MPI_Comm_size(comm, &nprocs);
      }
    }

    template <size_t Degree>
    void structured<1>::write_uniform_mesh(
      double xmin, double xmax, size_t nelems)
    {
      // get rank/nprocs
      auto dx = (xmax - xmin) / nelems;

      auto fspace = dg::hdf5::dataspace::simple(nelems * (Degree + 1));
      dg::hdf5::dataset d(
        loc, "mesh", dg::hdf5::type_id<double>::value(), fspace);

      static dg::spatial::elements::lagrange<double, Degree> hi_elem;
      static dg::spatial::elements::lagrange<double, 1> flat_elem;

      dg::shslab<double, 2, 1> coords(2);
      std::array<double, 1> xi = {0};
      dg::shslab<double, 1, 1> pos(1);

      hsize_t starts[] = {0};
      hsize_t counts[] = {1};

      for (size_t elem = nelems * rank / nprocs;
           elem < nelems * (rank + 1) / nprocs; ++elem)
      {
        coords(0) = xmin + elem * dx;
        coords(1) = xmin + (elem + 1) * dx;

        for (size_t i = 0; i <= Degree; ++i)
        {
          xi[0] = hi_elem.absc[0][i];
          starts[0] = elem * (Degree + 1) + i;
          pos[0] = flat_elem.interp(xi, coords);
          fspace.select_hyperslab(starts, counts);
          d.write(pos, fspace);
        }
      }
      loc.get_file().flush();
    }

    void structured<1>::write_time(size_t frame, double t)
    {
      dg::hdf5::group g(loc, std::to_string(frame).c_str());
      {
        // write out the time of the
        dg::view<double, 1> tv(&t, dg::extents<1>{1});
        dg::hdf5::attribute attr(g, "time", tv);
      }
    }

    template <class Q, class I>
    void structured<1>::write_solution(size_t frame, const Q& q,
      size_t nelems, size_t elem_offset,
      const std::vector<std::pair<std::string, I>>& components)
    {
      dg::hdf5::group g(loc, std::to_string(frame).c_str());
      auto fspace = dg::hdf5::dataspace::simple(nelems, q.shape()[1]);

      hsize_t starts[] = {elem_offset, 0};
      hsize_t counts[] = {q.shape()[0], q.shape()[1]};
      fspace.select_hyperslab(starts, counts);

      // write out each component
      for (auto& c : components)
      {
        auto comp = q.subarray(dg::all{}, dg::all{}, c.second);
        dg::hdf5::dataset d(
          g, c.first.c_str(), dg::hdf5::type_id<double>::value(), fspace);
        d.write(comp, fspace);
      }
      loc.get_file().flush();
    }

    template <class Q, class I>
    void structured<1>::write_solution(size_t frame, double t, const Q& q,
      size_t nelems, size_t elem_offset,
      const std::vector<std::pair<std::string, I>>& components)
    {
      write_time(frame, t);
      write_solution(frame, q, nelems, elem_offset, components);
    }
  }
}

#endif
