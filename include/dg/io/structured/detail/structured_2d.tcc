#ifndef DG_IO_STRUCTURED_2D_TCC
#define DG_IO_STRUCTURED_2D_TCC

namespace dg
{
  namespace io
  {
    structured<2>::structured(dg::hdf5::location& loc) : loc(loc)
    {
      dg::hdf5::plists::file_access fa(loc);
      fa.get_mpio(&comm.reset(), nullptr);
      if (comm)
      {
        MPI_Comm_rank(comm, &rank);
        MPI_Comm_size(comm, &nprocs);
      }
    }

    template <size_t Degree_y, size_t Degree_x>
    void structured<2>::write_uniform_mesh(double xmin, double xmax,
      double ymin, double ymax, size_t nelems_x, size_t nelems_y)
    {
      // get rank/nprocs
      auto dx = (xmax - xmin) / nelems_x;
      auto dy = (ymax - ymin) / nelems_y;

      auto fspace = dg::hdf5::dataspace::simple(
        nelems_x * (Degree_x + 1), nelems_y * (Degree_y + 1), 2);
      dg::hdf5::dataset d(
        loc, "mesh", dg::hdf5::type_id<double>::value(), fspace);

      static dg::spatial::elements::lagrange<double, Degree_x, Degree_y>
        hi_elem;
      static dg::spatial::elements::lagrange<double, 1, 1> flat_elem;

      dg::shslab<double, 8, 3> coords(2, 2, 2);
      std::array<double, 2> xi = {0, 0};
      dg::shslab<double, 2, 1> pos(2);
      auto xs = coords.subarray(dg::all{}, dg::all{}, 0);
      auto ys = coords.subarray(dg::all{}, dg::all{}, 1);

      hsize_t starts[] = {0, 0, 0};
      hsize_t counts[] = {1, 1, 2};

      // TODO: partitioning scheme for mesh?
      // for now, generate only on rank 0
      if (rank == 0)
      {
        for (size_t elem_x = 0; elem_x < nelems_x; ++elem_x)
        {
          coords(0, 0, 0) = elem_x * dx + xmin;
          coords(0, 1, 0) = elem_x * dx + xmin;
          coords(1, 0, 0) = (elem_x + 1) * dx + xmin;
          coords(1, 1, 0) = (elem_x + 1) * dx + xmin;
          for (size_t i_x = 0; i_x <= Degree_x; ++i_x)
          {
            xi[0] = hi_elem.absc[0][i_x];
            for (size_t elem_y = 0; elem_y < nelems_y; ++elem_y)
            {
              coords(0, 0, 1) = elem_y * dy + ymin;
              coords(0, 1, 1) = (elem_y + 1) * dy + ymin;
              coords(1, 0, 1) = elem_y * dy + ymin;
              coords(1, 1, 1) = (elem_y + 1) * dy + ymin;
              starts[0] = elem_x * (Degree_x + 1) + i_x;

              for (size_t i_y = 0; i_y <= Degree_y; ++i_y)
              {
                xi[1] = hi_elem.absc[1][i_y];
                starts[1] = elem_y * (Degree_y + 1) + i_y;
                pos[0] = flat_elem.interp(xi, xs);
                pos[1] = flat_elem.interp(xi, ys);
                fspace.select_hyperslab(starts, counts);
                d.write(pos, fspace);
              }
            }
          }
        }
      }
      loc.get_file().flush();
    }

    template <size_t Degree_x, size_t Degree_y, class Q, class I>
    void structured<2>::write_solution(size_t frame, double t, const Q& q,
      size_t nelems_x, size_t nelems_y, size_t offset_x, size_t offset_y,
      const std::vector<std::pair<std::string, I>>& components)
    {
      dg::hdf5::group g(loc, std::to_string(frame).c_str());
      if (!g.has_attribute("time"))
      {
        // write out the time of the
        dg::view<double, 1> tv(&t, dg::extents<1>{1});
        dg::hdf5::attribute attr(g, "time", tv);
      }

      auto fspace = dg::hdf5::dataspace::simple(
        nelems_x * (Degree_x + 1), nelems_y * (Degree_y + 1));

      for (auto& c : components)
      {
        // 5-D q(elem_x, elem_y, deg_x, deg_y, comp)
        auto comp =
          q.subarray(dg::all{}, dg::all{}, dg::all{}, dg::all{}, c.second);
        dg::hdf5::dataset d(
          g, c.first.c_str(), dg::hdf5::type_id<double>::value(), fspace);
        hsize_t starts[] = {0, 0};
        hsize_t counts[] = {1, Degree_y + 1};
        for (size_t elem_x = 0; elem_x < nelems_x; ++elem_x)
        {
          for (size_t i_x = 0; i_x <= Degree_x; ++i_x)
          {
            starts[0] = elem_x * (Degree_x + 1) + i_x;
            for (size_t elem_y = 0; elem_y < nelems_y; ++elem_y)
            {
              starts[1] = elem_y * (Degree_y + 1);
              fspace.select_hyperslab(starts, counts);
              d.write(comp.subarray(elem_x, elem_y, i_x, dg::all{}), fspace);
            }
          }
        }
        loc.get_file().flush();
      }
    }
  }
}

#endif
