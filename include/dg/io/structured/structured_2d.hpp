#ifndef DG_IO_STRUCTURED_2D_HPP
#define DG_IO_STRUCTURED_2D_HPP

#include "structured.hpp"

#include "dg/utils/hdf5_utils.hpp"
#include "dg/utils/mpi_utils.hpp"

#include <vector>

namespace dg
{
  namespace io
  {
    template <>
    struct structured<2>
    {
      dg::hdf5::location& loc;
      dg::mpi::comm comm;
      int rank = 0, nprocs = 1;

      inline structured(dg::hdf5::location& loc);

      template <size_t Degree_x, size_t Degree_y>
      void write_uniform_mesh(double xmin, double xmax, double ymin,
        double ymax, size_t nelems_x, size_t nelems_y);

      template <size_t Degree_x, size_t Degree_y, class Q, class I>
      void write_solution(size_t frame, double t, const Q& q, size_t nelems_x,
        size_t nelems_y, size_t offset_x, size_t offset_y,
        const std::vector<std::pair<std::string, I>>& components);
    };
  } // namespace io
} // namespace dg

#include "detail/structured_2d.tcc"

#endif
