#ifndef DG_IO_STRUCTURED_1D_HPP
#define DG_IO_STRUCTURED_1D_HPP

#include "structured.hpp"

#include "dg/utils/hdf5_utils.hpp"
#include "dg/utils/mpi_utils.hpp"

#include <vector>

namespace dg
{
  namespace io
  {
    ///
    /// @brief 1D specialization
    ///
    template <>
    struct structured<1>
    {
      dg::hdf5::location& loc;
      dg::mpi::comm comm;
      int rank = 0, nprocs = 1;

      ///
      /// @param loc location node (typically the file node) where to read/write a
      /// structured dataset from
      ///
      inline structured(dg::hdf5::location& loc);

      ///
      /// @brief Writes a 1D structured uniform mesh
      /// @param nelems number of elements globally
      ///
      template <size_t Degree>
      void write_uniform_mesh(double xmin, double xmax, size_t nelems);

      void write_time(size_t frame, double t);

      ///
      /// @brief Write components out for the given frame
      /// @param frame frame number
      /// @param q a view-like type of local data to write out
      /// @param nelems global number of elements
      /// @param elem_offset
      /// @param components list of components to write out
      ///
      template <class Q, class I>
      void write_solution(size_t frame, const Q& q, size_t nelems,
        size_t elem_offset,
        const std::vector<std::pair<std::string, I>>& components);

      ///
      /// @brief Write components out for the given frame
      /// @param frame frame number
      /// @param t simulation time of the frame
      /// @param q a view-like type of local data to write out
      /// @param nelems global number of elements
      /// @param elem_offset
      /// @param components list of components to write out
      ///
      template <class Q, class I>
      void write_solution(size_t frame, double t, const Q& q, size_t nelems,
        size_t elem_offset,
        const std::vector<std::pair<std::string, I>>& components);
    };
  }
}

#include "detail/structured_1d.tcc"

#endif
