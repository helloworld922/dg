#ifndef DG_HERMITE_HPP
#define DG_HERMITE_HPP

#include "dg/utils/layouts.hpp"
#include "dg/utils/view.hpp"

#include "dg/dg_config.hpp"

#include <boost/math/special_functions/gamma.hpp>
#include <vector>

namespace dg
{
  ///
  /// @brief Physicist's Hermite polynomials
  ///
  template <class Real>
  struct hermite
  {
    constexpr hermite() noexcept = default;

    constexpr hermite(const hermite&) noexcept = default;

    ~hermite() noexcept = default;

    ///
    /// @brief Evaluate the nth Hermite polynomial at x
    /// @param n
    /// @param x
    /// @return
    ///
    inline Real operator()(Real x, size_t n) const noexcept;

    ///
    /// @brief Evaluate 1st derivative of the nth Hermite polynomial at x
    /// @param x
    /// @param n
    /// @return
    ///
    Real deriv(Real x, size_t n) const noexcept;

    ///
    /// @brief Returns the normalization parameter
    ///
    Real norm(size_t n) const noexcept
    {
      using namespace boost::multiprecision;
      using namespace boost::math;
      using boost::math::constants::root_pi;
      return root_pi<Real>() * pow(Real{2}, n) *
             tgamma(static_cast<Real>(n + 1));
    }

    ///
    /// @brief
    /// @param abscissa
    /// @param res
    /// @tparam T
    /// @tparam U
    ///
    template <class T, class U>
    void weights(const T& abscissa, U&& res) const;

    ///
    /// @brief Gauss-Legendre quadrature weights
    /// @param abscissa
    /// @return
    ///
    template <class T>
    std::vector<Real> weights(const T& abscissa) const;

    ///
    /// @brief Absissas for Gauss-legendre quadrature with n points.
    /// Accurate for polynomials up to order 2*n-1.
    /// @param n Order/number of integration points. Must be less than or equal
    /// to 64.
    /// @return
    ///
    std::vector<Real> abscissas(size_t n) const;
  };
}

#include "detail/hermite.tcc"

#endif
