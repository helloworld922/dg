#ifndef DG_LOBATTO_HPP
#define DG_LOBATTO_HPP

#include "dg/utils/layouts.hpp"
#include "dg/utils/view.hpp"

#include "dg/dg_config.hpp"

#include <vector>

namespace dg
{
  ///
  /// @brief First derivative of the legendre polynomials.
  ///
  template <class Real>
  struct lobatto
  {
    constexpr lobatto() noexcept = default;

    constexpr lobatto(const lobatto&) noexcept = default;

    ~lobatto() = default;

    ///
    /// @brief Return the normalization parameter
    ///
    Real norm(size_t n) const noexcept
    {
      return Real{2} / (2 * n + 1);
    }

    ///
    /// @brief Evaluates first derivative of (n-1)th Legendre polynomial at x
    /// @param n
    /// @param x
    /// @return
    ///
    inline Real operator()(Real x, size_t n) const noexcept;

    /**
     * @brief Evaluate 2nd derivative of the
     * (n-1)th Legendre polynomial at x
     * @param n
     * @param x
     * @return
     */
    inline Real deriv(Real x, size_t n) const noexcept;

    template <class T, class U>
    void weights(const T& abscissa, U&& res) const;

    /**
     * @brief Gauss-Lobatto quadrature weights
     * @param abscissa
     * @tparam T view
     * @return
     */
    template <class T>
    std::vector<Real> weights(const T& abscissa) const;

    /**
     * @brief Abscissas for Gauss-Lobatto quadrature with n points.
     * Exact for polynomials up to order is 2*n-3.
     * @param n number of points, n <= 64.
     * @return
     */
    std::vector<Real> abscissas(size_t n) const;
  };
}

#include "detail/lobatto.tcc"

#endif
