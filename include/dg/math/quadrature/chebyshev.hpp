#ifndef DG_CHEBYSHEV_HPP
#define DG_CHEBYSHEV_HPP

#include "jacobi.hpp"

#include "dg/utils/layouts.hpp"
#include "dg/utils/view.hpp"

#include "dg/dg_config.hpp"

#include <boost/math/constants/constants.hpp>
#include <cmath>
#include <cstddef>
#include <vector>

namespace dg
{
  /// \cond PREDEF
  template <class Real, size_t Kind>
  struct chebyshev;
  /// \endcond

  ///
  /// @brief Chebyshev polynomials of the first kind.
  /// equivalent to Jacobi(-1/2,-1/2)(x,i)/Jacobi(-1/2,-1/2)(1,i)
  ///
  template <class Real>
  struct chebyshev<Real, 1>
  {
    constexpr chebyshev() noexcept = default;

    constexpr chebyshev(const chebyshev&) noexcept = default;

    ~chebyshev() noexcept = default;

    ///
    /// @brief Evaluate nth Chebyshev polynomial at x
    /// @param n
    /// @param x
    /// @return
    ///
    inline Real operator()(Real x, size_t n) const noexcept;

    ///
    /// @brief Evaluate 1st derivative of the
    /// nth Chebyshev polynomial at x
    /// @param n
    /// @param x
    /// @return
    ///
    inline Real deriv(Real x, size_t n) const noexcept;

    ///
    /// @brief Returns the normalization parameter
    ///
    Real norm(size_t n) const noexcept
    {
      using boost::math::constants::pi;
      using boost::math::constants::half_pi;
      return n == 0 ? pi<Real>() : half_pi<Real>();
    }

    template <class T, class U>
    void weights(const T& abscissa, U&& res) const;

    ///
    /// @brief Gauss-Chebyshev quadrature weights
    /// @param abscissa
    /// @return
    ///
    template <class T>
    std::vector<Real> weights(const T& abscissa) const;

    ///
    /// @brief Absissas for Gauss-Chebyshev quadrature with n points.
    /// Accurate for polynomials up to order 2*n-1.
    /// @param n Order/number of integration points
    /// @return
    ///
    inline std::vector<Real> abscissas(size_t n) const;
  };

  ///
  /// @brief Chebyshev polynomials of the second kind
  /// Equivalent to Jacobi(1/2,1/2)(x,i)/Jacobi(1/2,1/2)(1,i)
  ///
  template <class Real>
  struct chebyshev<Real, 2>
  {
    constexpr chebyshev() noexcept = default;

    constexpr chebyshev(const chebyshev&) noexcept = default;

    ~chebyshev() noexcept = default;

    ///
    /// @brief Evaluate nth Chebyshev polynomial at x
    /// @param n
    /// @param x
    /// @return
    ///
    inline Real operator()(Real x, size_t n) const noexcept;

    ///
    /// @brief Evaluate 1st derivative of the
    /// nth Chebyshev polynomial at x
    /// @param n
    /// @param x
    /// @return
    ///
    inline Real deriv(Real x, size_t n) const noexcept;

    ///
    /// @brief Returns the normalization parameter
    ///
    Real norm(size_t n) const noexcept
    {
      using boost::math::constants::half_pi;
      return half_pi<Real>();
    }

    template <class T, class U>
    void weights(const T& abscissa, U&& res) const;

    ///
    /// @brief Gauss-Chebyshev quadrature weights
    /// @param abscissa
    /// @return
    ///
    template <class T>
    std::vector<Real> weights(const T& abscissa) const;

    ///
    /// @brief Absissas for Gauss-Chebyshev quadrature with n points.
    /// Accurate for polynomials up to order 2*n-1.
    /// @param n Order/number of integration points
    /// @return
    ///
    inline std::vector<Real> abscissas(size_t n) const;
  };
}

#include "detail/chebyshev.tcc"

#endif
