#ifndef DG_JACOBI_TCC
#define DG_JACOBI_TCC

namespace dg
{
  template <class Real>
  Real jacobi<Real>::operator()(const Real& x, size_t n) const noexcept
  {
    if (n)
    {
      Real res = 0.5 * (2 * (alpha + 1) + (alpha + beta + 2) * (x - 1));
      Real prev = 1;
      for (size_t i = 1; i < n; ++i)
      {
        Real next =
          ((2 * i + 1 + alpha + beta) *
              ((2 * i + 2 + alpha + beta) * (2 * i + alpha + beta) * x +
                alpha * alpha - beta * beta) *
              res -
            2 * (i + alpha) * (i + beta) * (2 * i + 2 + alpha + beta) * prev) /
          (2 * (i + 1) * (i + 1 + alpha + beta) * (2 * i + alpha + beta));
        prev = res;
        res = next;
      }
      return res;
    }
    return 1;
  }

  template <class Real>
  Real jacobi<Real>::deriv(const Real& x, size_t n) const noexcept
  {
    if (n)
    {
      jacobi helper(alpha + 1, beta + 1);
      return 0.5 * (n + alpha + beta + 1) * helper(x, n - 1);
    }
    return 0;
  }

  template <class Real>
  Real jacobi<Real>::norm(size_t n) const noexcept
  {
    using namespace boost::multiprecision;
    using namespace boost::math;
    return pow(2, alpha + beta + 1) * tgamma(n + alpha + 1) *
           tgamma(n + beta + 1) /
           ((2 * n + alpha + beta + 1) * tgamma(n + alpha + beta + 1) *
             tgamma(static_cast<Real>(n + 1)));
  }
}

#endif
