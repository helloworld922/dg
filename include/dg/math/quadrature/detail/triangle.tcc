#ifndef DG_TRIANGLE_TCC
#define DG_TRIANGLE_TCC

namespace dg
{
  template <class Real>
  Real triangle_modes<Real>::norm(size_t i, size_t j) const noexcept
  {
    jacobi<Real> jac(2 * i + 1, 0);
    return jac.norm(j) / (2 * i + 1);
  }

  template <class Real>
  Real triangle_modes<Real>::operator()(
    const Real& x, const Real& y, size_t i, size_t j) const noexcept
  {
    jacobi<Real> jac(2 * i + 1, 0);
    Real leg = 1;
    if (i)
    {
      Real a = 2 * x + y + 1;
      leg = a;
      Real prev = 1;
      for (size_t idx = 1; idx < i; ++idx)
      {
        Real next = ((2 * idx + 1) * a * leg - idx * prev * (1 - y) * (1 - y)) /
                    (idx + 1);
        prev = leg;
        leg = next;
      }
    }
    return leg * jac(y, j);
  }

  template <class Real>
  Real triangle_modes<Real>::deriv_x(
    const Real& x, const Real& y, size_t i, size_t j) const noexcept
  {
    if (i)
    {
      jacobi<Real> jac(2 * i + 1, 0);
      Real a = 2 * x + y + 1;
      Real dprev = 0;
      Real dleg = 2;
      Real prev = 1;
      Real leg = a;
      for (size_t idx = 1; idx < i; ++idx)
      {
        Real next = ((2 * idx + 1) * a * leg - idx * prev * (1 - y) * (1 - y)) /
                    (idx + 1);
        Real dnext = ((2 * idx + 1) * (2 * leg + a * dleg) -
                       idx * dprev * (1 - y) * (1 - y)) /
                     (idx + 1);
        prev = leg;
        leg = next;
        dprev = dleg;
        dleg = dnext;
      }
      return dleg * jac(y, j);
    }
    return 0;
  }

  template <class Real>
  Real triangle_modes<Real>::deriv_y(
    const Real& x, const Real& y, size_t i, size_t j) const noexcept
  {
    jacobi<Real> jac(2 * i + 1, 0);
    if (i)
    {
      Real a = 2 * x + y + 1;
      Real dprev = 0;
      Real dleg = 1;
      Real prev = 1;
      Real leg = a;
      for (size_t idx = 1; idx < i; ++idx)
      {
        Real next = ((2 * idx + 1) * a * leg - idx * prev * (1 - y) * (1 - y)) /
                    (idx + 1);
        Real dnext = ((2 * idx + 1) * (leg + a * dleg) -
                       idx * (dprev * (1 - y) * (1 - y) - 2 * (1 - y) * prev)) /
                     (idx + 1);
        prev = leg;
        leg = next;
        dprev = dleg;
        dleg = dnext;
      }
      return (dleg * jac(y, j) + leg * jac.deriv(y, j));
    }
    return jac.deriv(y, j);
  }

  template <class Real>
  template <class T>
  triangle_nodes<Real>::triangle_nodes(const T& abscissa)
    : vander(triangle_nodes::vandermonde(abscissa)), vander_lu(vander),
      degree(0.5 * (sqrt(1 + 8 * abscissa.extent(0)) - 3))
  {
  }

  template <class Real>
  Eigen::Matrix<Real, Eigen::Dynamic, 1> triangle_nodes<Real>::operator()(
    const Real& x, const Real& y) const noexcept
  {
    constexpr triangle_modes<Real> modes{};
    // TODO: is there a better place to have rhs pre-allocated?
    Eigen::Matrix<Real, Eigen::Dynamic, 1> rhs(vander.cols());
    {
      size_t i_ = 0, j_ = 0;
      for(size_t row = 0; row < vander.cols(); ++row)
      {
        rhs(row,0) = modes(x, y, i_, j_);
        ++j_;
        if (j_ > degree - i_)
        {
          j_ = 0;
          ++i_;
        }
      }
    }

    return vander_lu.solve(rhs);
  }

  template <class Real>
  Real triangle_nodes<Real>::operator()(
    const Real& x, const Real& y, size_t i, size_t j) const noexcept
  {
    return operator()(x, y)(j + (3 + 2 * degree - i) * i / 2);
  }

  template <class Real>
  Eigen::Matrix<Real, Eigen::Dynamic, 1> triangle_nodes<Real>::deriv_x(
    const Real& x, const Real& y) const noexcept
  {
    constexpr triangle_modes<Real> modes{};
    // TODO: is there a better place to have rhs pre-allocated?
    Eigen::Matrix<Real, Eigen::Dynamic, 1> rhs(vander.cols());
    {
      size_t i_ = 0, j_ = 0;
      for(size_t row = 0; row < vander.cols(); ++row)
      {
        rhs(row,0) = modes.deriv_x(x, y, i_, j_);
        ++j_;
        if (j_ > degree - i_)
        {
          j_ = 0;
          ++i_;
        }
      }
    }

    return vander_lu.solve(rhs);
  }

  template <class Real>
  Real triangle_nodes<Real>::deriv_x(
    const Real& x, const Real& y, size_t i, size_t j) const noexcept
  {
    return deriv_x(x, y)(j + (3 + 2 * degree - i) * i / 2);
  }

  template <class Real>
  Eigen::Matrix<Real, Eigen::Dynamic, 1> triangle_nodes<Real>::deriv_y(
    const Real& x, const Real& y) const noexcept
  {
    constexpr triangle_modes<Real> modes{};
    // TODO: is there a better place to have rhs pre-allocated?
    Eigen::Matrix<Real, Eigen::Dynamic, 1> rhs(vander.cols());
    {
      size_t i_ = 0, j_ = 0;
      for(size_t row = 0; row < vander.cols(); ++row)
      {
        rhs(row,0) = modes.deriv_y(x, y, i_, j_);
        ++j_;
        if (j_ > degree - i_)
        {
          j_ = 0;
          ++i_;
        }
      }
    }

    return vander_lu.solve(rhs);
  }

  template <class Real>
  Real triangle_nodes<Real>::deriv_y(
    const Real& x, const Real& y, size_t i, size_t j) const noexcept
  {
    return deriv_y(x, y)(j + (3 + 2 * degree - i) * i / 2);
  }

  template <class Real>
  template <class T>
  Eigen::Matrix<Real, Eigen::Dynamic, Eigen::Dynamic>
  triangle_nodes<Real>::vandermonde(const T& abscissa)
  {
    constexpr triangle_modes<Real> modes{};

    Eigen::Matrix<Real, Eigen::Dynamic, Eigen::Dynamic> vander(
      abscissa.extent(0), abscissa.extent(0));
    // compute the polynomial degree
    // TODO: is this exact for reasonably small number of nodes?
    size_t degree = 0.5 * (sqrt(1 + 8 * abscissa.extent(0)) - 3);
    size_t i = 0, j = 0;
    for (size_t row = 0; row < abscissa.extent(0); ++row)
    {
      for (size_t col = 0; col < abscissa.extent(0); ++col)
      {
        vander(row, col) = modes(abscissa(col, 0), abscissa(col, 1), i, j);
      }
      ++j;
      if (j > degree - i)
      {
        j = 0;
        ++i;
      }
    }
    return vander;
  }

  template <class Real>
  template <class T>
  std::vector<Real> triangle_nodes<Real>::weights(const T& abscissa)
  {
    triangle_nodes nodes(abscissa);
    std::vector<Real> res(abscissa.extent(0), 0);
    res[0] = 2;
    // construct RHS in-place in res
    // this allows in-place solve
    Eigen::Map<Eigen::Matrix<Real, Eigen::Dynamic, 1>> rhs(
      res.data(), res.size());
    // solve for weights
    rhs = nodes.vander_lu.solve(rhs);
    return res;
  }
}
#endif
