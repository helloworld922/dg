#ifndef DG_HERMITE_TCC
#define DG_HERMITE_TCC

#include "mpfr_helper.tcc"

namespace dg
{
  namespace detail
  {
    ///
    /// @internal
    /// @brief Gets the base 62 mpfr data. Note: only contain non-zero abscissa,
    /// negative values stored in most negative to most positive order
    /// @param n number of integration points
    ///
    const char** get_hermite_absc_cache(size_t n);
  }

  template <class Real>
  Real hermite<Real>::operator()(Real x, size_t n) const noexcept
  {
    if (n)
    {
      Real res = 2 * x;
      Real prev = 1;
      for (size_t i = 1; i < n; ++i)
      {
        Real next = 2 * x * res - 2 * i * prev;
        prev = res;
        res = next;
      }
      return res;
    }
    return 1;
  }

  template <class Real>
  Real hermite<Real>::deriv(Real x, size_t n) const noexcept
  {
    if (n)
    {
      return 2 * n * operator()(x, n - 1);
    }
    return 0;
  }

  template <class Real>
  template <class T>
  std::vector<Real> hermite<Real>::weights(const T& abscissa) const
  {
    std::vector<Real> res(abscissa.size());
    weights(abscissa, res);
    return res;
  }

  template <class Real>
  template <class T, class U>
  void hermite<Real>::weights(const T& abscissa, U&& res) const
  {
    using namespace boost::multiprecision;
    using namespace boost::math;
    using boost::math::constants::root_pi;
    auto n = abscissa.size();
    if (n)
    {
      auto factor = pow(Real{2}, n - 1) * tgamma(n + 1) * root_pi;
      auto stop = n >> 1;
      size_t j = 0;
      for (size_t i = 0; i < stop; ++i)
      {
        Real xi = abscissa[i];
        Real p = operator()(xi, n - 1);
        res[j] = factor / (n * n * p * p);
        ++j;
      }
      if (n & 1)
      {
        Real xi = abscissa[stop];
        Real p = operator()(xi, n - 1);
        res[j] = factor / (n * n * p * p);
        ++j;
      }
      for (size_t i = stop; i > 0; --i)
      {
        res[j] = res[i - 1];
        ++j;
      }
    }
  }

  template <class Real>
  std::vector<Real> hermite<Real>::abscissas(size_t n) const
  {
    std::vector<Real> res(n);
    if (n > 1)
    {
      auto half = (n >> 1);
      auto offset = n & 1;
      // add positive numbers
      auto cache = detail::get_hermite_absc_cache(n);
      for (size_t i = 0; i < half; ++i)
      {
        detail::real_from_string<Real>::get(res[half + offset + i], cache[i]);
      }
      if (offset)
      {
        // add 0
        res[half] = 0;
      }
      // mirror to get negative numbers
      for (size_t i = 0; i < half; ++i)
      {
        res[i] = -res[2 * half + offset - 1 - i];
      }
    }
    else if (n == 1)
    {
      res[0] = 0;
    }
    return res;
  }
}

#endif
