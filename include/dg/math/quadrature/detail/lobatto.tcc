#ifndef DG_LOBATTO_TCC
#define DG_LOBATTO_TCC

#include "../legendre.hpp"
#include "mpfr_helper.tcc"

namespace dg
{
  namespace detail
  {
    ///
    /// @internal
    /// @brief Gets the base 62 mpfr data. Note: only contain non-zero abscissa,
    /// negative values stored in most negative to most positive order
    /// @param n number of integration points
    ///
    const char** get_lob_absc_cache(size_t n);
  }

  template <class Real>
  Real lobatto<Real>::operator()(Real x, size_t n) const noexcept
  {
    if (n > 1)
    {
      n -= 1;
      Real res = 1;
      Real prev = 0;
      for (size_t i = 1; i < n; ++i)
      {
        Real next = ((2 * i + 1) * x * res - (i + 1) * prev) / i;
        prev = res;
        res = next;
      }
      return res;
    }
    return 0;
  }

  template <class Real>
  Real lobatto<Real>::deriv(Real x, size_t n) const noexcept
  {
    // optimized version of J_(2,2)(x,n-3)*(n+1)*n/4
    if (n > 2)
    {
      Real res = (n + 1) * n * 0.25;
      Real prev = 0;
      for (size_t i = 1; i < n - 2; ++i)
      {
        Real next =
          (i + 2) * ((3 + 2 * i) * x * res - (i + 1) * prev) / (i * (i + 4));
        prev = res;
        res = next;
      }
      return res;
    }
    return 0;
  }

  template <class Real>
  template <class T>
  std::vector<Real> lobatto<Real>::weights(const T& abscissa) const
  {
    std::vector<Real> res(abscissa.size());
    weights(abscissa, res);
    return res;
  }

  template <class Real>
  template <class T, class U>
  void lobatto<Real>::weights(const T& abscissa, U&& res) const
  {
    auto n = abscissa.size();
    if (n >= 2)
    {
      constexpr legendre<Real> d{};
      size_t j = 0;
      res[j] = Real{2} / (n * (n - 1));
      ++j;
      for (auto i = 1; i < n - 1; ++i)
      {
        Real xi = abscissa[i];
        Real p = d(xi, n - 1);
        res[j] = Real{2} / (n * (n - 1) * p * p);
        ++j;
      }
      res[j] = res[0];
    }
    else
    {
      res[0] = 1;
    }
  }

  template <class Real>
  std::vector<Real> lobatto<Real>::abscissas(size_t n) const
  {
    std::vector<Real> res(n);
    if (n >= 2)
    {
      auto half = n >> 1;
      auto offset = n & 1;
      // add positive numbers
      if (n > 3)
      {
        auto cache = detail::get_lob_absc_cache(n);
        for (size_t i = 0; i < half - 1; ++i)
        {
          detail::real_from_string<Real>::get(res[half + offset + i], cache[i]);
        }
      }
      res.back() = 1;
      if (offset)
      {
        // add 0
        res[half] = 0;
      }
      // mirror to get negative numbers
      for (size_t i = 0; i < half; ++i)
      {
        res[i] = -res[2 * half + offset - 1 - i];
      }
    }
    else if (n == 1)
    {
      res[0] = 0;
    }
    return res;
  }
}

#endif
