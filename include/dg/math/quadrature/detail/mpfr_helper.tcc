#ifndef DG_MPFR_HELPER_TCC
#define DG_MPFR_HELPER_TCC
namespace dg
{
  namespace detail
  {
    template <class Real>
    struct real_from_string
    {
      static void get(Real& res, const char* str)
      {
        // don't know what Real is, default to real256
        real256 val{0};
        auto& raw = val.backend().data();
        mpfr_set_str(raw, str, 62, MPFR_RNDN);
        res = val.template convert_to<Real>();
      }
    };

    template <unsigned digits10,
      boost::multiprecision::mpfr_allocation_type alloc_type,
      boost::multiprecision::expression_template_option et_op>
    struct real_from_string<boost::multiprecision::number<
      boost::multiprecision::mpfr_float_backend<digits10, alloc_type>, et_op>>
    {
      typedef boost::multiprecision::number<
        boost::multiprecision::mpfr_float_backend<digits10, alloc_type>, et_op>
        real_type;

      static void get(real_type& res, const char* str)
      {
        auto& raw = res.backend().data();
        mpfr_set_str(raw, str, 62, MPFR_RNDN);
      }
    };
  }
}
#endif
