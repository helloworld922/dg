#ifndef DG_CHEBYSHEV_TCC
#define DG_CHEBYSHEV_TCC

#include <cmath>

namespace dg
{
  template <class Real>
  Real chebyshev<Real, 1>::operator()(Real x, size_t n) const noexcept
  {
    if (n)
    {
      Real res = x;
      Real prev = 1;
      for (size_t i = 1; i < n; ++i)
      {
        Real next = 2 * x * res - prev;
        prev = res;
        res = next;
      }
      return res;
    }
    return 1;
  }

  template <class Real>
  Real chebyshev<Real, 1>::deriv(Real x, size_t n) const noexcept
  {
    if (n)
    {
      Real res = x;
      Real prev = 1;
      Real dres = 1;
      Real dprev = 0;
      for (size_t i = 1; i < n; ++i)
      {
        Real dnext = 2 * (res + x * dres) - dprev;
        dprev = dres;
        dres = dnext;

        Real next = 2 * x * res - prev;
        prev = res;
        res = next;
      }
      return dres;
    }
    return 0;
  }

  template <class Real>
  std::vector<Real> chebyshev<Real, 1>::abscissas(size_t n) const
  {
    const static Real pi{boost::math::constants::pi<Real>()};
    std::vector<Real> res(n);
    for (size_t i = 0; i < n; ++i)
    {
      res[i] = -cos((2 * i + 1) * pi / (2 * n));
    }
    return res;
  }

  template <class Real>
  template <class T, class U>
  void chebyshev<Real, 1>::weights(const T& abscissa, U&& res) const
  {
    const static Real pi{boost::math::constants::pi<Real>()};
    auto n = abscissa.size();
    for (size_t i = 0; i < n; ++i)
    {
      res[i] = pi / n;
    }
  }

  template <class Real>
  template <class T>
  std::vector<Real> chebyshev<Real, 1>::weights(const T& abscissa) const
  {
    std::vector<Real> res(abscissa.size());
    weights(abscissa, res);
    return res;
  }

  template <class Real>
  Real chebyshev<Real, 2>::operator()(Real x, size_t n) const noexcept
  {
    if (n)
    {
      Real res = 2 * x;
      Real prev = 1;
      for (size_t i = 1; i < n; ++i)
      {
        Real next = 2 * x * res - prev;
        prev = res;
        res = next;
      }
      return res;
    }
    return 1;
  }

  template <class Real>
  Real chebyshev<Real, 2>::deriv(Real x, size_t n) const noexcept
  {
    if (n)
    {
      Real res = 2 * x;
      Real prev = 1;
      Real dres = 2;
      Real dprev = 0;
      for (size_t i = 1; i < n; ++i)
      {
        Real dnext = 2 * (res + x * dres) - dprev;
        dprev = dres;
        dres = dnext;

        Real next = 2 * x * res - prev;
        prev = res;
        res = next;
      }
      return dres;
    }
    return 0;
  }

  template <class Real>
  template <class T, class U>
  void chebyshev<Real, 2>::weights(const T& abscissa, U&& res) const
  {
    auto n = abscissa.size();
    const static Real pi{boost::math::constants::pi<Real>()};
    for (size_t i = 0; i < n; ++i)
    {
      auto x = (i + 1) * pi / (n + 1);
      auto y = sin(x);
      res[i] = x * y * y;
    }
  }

  template <class Real>
  std::vector<Real> chebyshev<Real, 2>::abscissas(size_t n) const
  {
    const static Real pi{boost::math::constants::pi<Real>()};
    std::vector<Real> res(n);
    for (size_t i = 0; i < n; ++i)
    {
      res[i] = -cos((i + 1) * pi / (n + 1));
    }
    return res;
  }

  template <class Real>
  template <class T>
  std::vector<Real> chebyshev<Real, 2>::weights(const T& abscissa) const
  {
    std::vector<Real> res(abscissa.size());
    weights(abscissa, res);
    return res;
  }
}

#endif
