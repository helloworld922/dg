#ifndef DG_JACOBI_HPP
#define DG_JACOBI_HPP

#include "dg/utils/layouts.hpp"
#include "dg/utils/view.hpp"

#include "dg/dg_config.hpp"

#include <boost/math/special_functions/gamma.hpp>
#include <vector>

namespace dg
{
  ///
  /// @brief Jacobi polynomials
  ///
  template <class Real>
  struct jacobi
  {
    Real alpha, beta;

    jacobi(Real alpha, Real beta) noexcept : alpha(alpha), beta(beta)
    {
    }

    jacobi(const jacobi&) = default;

    jacobi(jacobi&&) = default;

    ~jacobi() noexcept = default;

    ///
    /// @brief Evaluate nth Jacobi polynomial at x
    /// @param n
    /// @param x
    /// @return
    ///
    inline Real operator()(const Real& x, size_t n) const noexcept;

    ///
    /// @brief Evaluate 1st derivative of the
    /// nth Jacobi polynomial at x
    /// @param n
    /// @param x
    /// @return
    ///
    inline Real deriv(const Real& x, size_t n) const noexcept;

    ///
    /// @brief Returns the the normalization parameter
    ///
    inline Real norm(size_t n) const noexcept;
  };
}

#include "detail/jacobi.tcc"

#endif
