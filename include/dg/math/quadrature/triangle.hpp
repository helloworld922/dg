#ifndef DG_TRIANGLE_HPP
#define DG_TRIANGLE_HPP

#include "jacobi.hpp"
#include "legendre.hpp"

#include "dg/utils/layouts.hpp"
#include "dg/utils/view.hpp"

#include "dg/dg_config.hpp"

#include <Eigen/Dense>

#include <vector>

namespace dg
{
  ///
  /// @brief Orthogonal triangle basis
  /// TODO: need to fix bounds from [-1,1]
  ///
  template <class Real>
  struct triangle_modes
  {
    ///
    /// @brief Evaluates P_{i,j}(x,y)
    /// @param x in [0,1-y].
    /// @param y in [0,1].
    /// @param i
    /// @param j
    ///
    Real operator()(const Real& x, const Real& y, size_t i, size_t j) const
      noexcept;

    ///
    /// @brief Evaluates the 1st partial derivative of the nth basis function at
    /// (x,y) w.r.t. x
    /// @param x in [0,1-y]
    /// @param y in [0,1]
    /// @param i
    /// @param j
    ///
    Real deriv_x(const Real& x, const Real& y, size_t i, size_t j) const
      noexcept;

    ///
    /// @brief Evaluates the 1st partial derivative of the nth basis function at
    /// (x,y) w.r.t. y
    /// @param x
    /// @param y
    /// @param i
    /// @param j
    ///
    Real deriv_y(const Real& x, const Real& y, size_t i, size_t j) const
      noexcept;

    ///
    /// @brief Integral of P_{i,j}^2 over the triangle
    ///
    Real norm(size_t i, size_t j) const noexcept;
  };

  ///
  /// @brief Nodal triangle basis
  ///
  template <class Real>
  struct triangle_nodes
  {
    Eigen::Matrix<Real, Eigen::Dynamic, Eigen::Dynamic> vander;
    Eigen::PartialPivLU<Eigen::Matrix<Real, Eigen::Dynamic, Eigen::Dynamic>>
      vander_lu;
    size_t degree;

    template <class T>
    triangle_nodes(const T& abscissa);

    ///
    /// @brief Evaluates the Vandermonde matrix V_{ij} = P_{i}(r_j)
    ///
    template <class T>
    static Eigen::Matrix<Real, Eigen::Dynamic, Eigen::Dynamic> vandermonde(
      const T& abscissa);

    ///
    /// @brief Evaluates l_{i,j}(x,y) for all i,j with degree in the triangle
    /// basis
    /// @param x in [0,1-y].
    /// @param y in [0,1].
    ///
    Eigen::Matrix<Real, Eigen::Dynamic, 1> operator()(
      const Real& x, const Real& y) const noexcept;

    ///
    /// @brief Evaluates l_{i,j}(x,y)
    /// @param x in [0,1-y].
    /// @param y in [0,1].
    /// @param i
    /// @param j
    ///
    Real operator()(const Real& x, const Real& y, size_t i, size_t j) const
      noexcept;

    ///
    /// @brief Evaluates the 1st partial derivative of the basis functions at
    /// (x,y) w.r.t. x
    /// @param x in [0,1-y]
    /// @param y in [0,1]
    ///
    Eigen::Matrix<Real, Eigen::Dynamic, 1> deriv_x(
      const Real& x, const Real& y) const noexcept;

    ///
    /// @brief Evaluates the 1st partial derivative of the basis function at
    /// (x,y) w.r.t. x
    /// @param x in [0,1-y]
    /// @param y in [0,1]
    /// @param i
    /// @param j
    ///
    Real deriv_x(const Real& x, const Real& y, size_t i, size_t j) const
      noexcept;

    ///
    /// @brief Evaluates the 1st partial derivative of the basis function at
    /// (x,y) w.r.t. y
    /// @param x
    /// @param y
    ///
    Eigen::Matrix<Real, Eigen::Dynamic, 1> deriv_y(
      const Real& x, const Real& y) const noexcept;

    ///
    /// @brief Evaluates the 1st partial derivative of the basis function at
    /// (x,y) w.r.t. y
    /// @param x
    /// @param y
    /// @param i
    /// @param j
    ///
    Real deriv_y(const Real& x, const Real& y, size_t i, size_t j) const
      noexcept;

    ///
    /// @brief Integral of l_{i,j}^2 over the triangle
    ///
    Real norm(size_t i, size_t j) const noexcept;

    ///
    /// @brief compute quadrature integration weights
    /// @param abscissa
    /// @tparam T 2D view<node index, dimension=2>
    ///
    template <class T>
    static std::vector<Real> weights(const T& abscissa);
  };
}

#include "detail/triangle.tcc"

#endif
