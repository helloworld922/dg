#ifndef DG_LEGENDRE_HPP
#define DG_LEGENDRE_HPP

#include "dg/utils/layouts.hpp"
#include "dg/utils/view.hpp"

#include "dg/dg_config.hpp"

#include <vector>

namespace dg
{
  ///
  /// @brief Legendre polynomials
  ///
  template <class Real>
  struct legendre
  {
    constexpr legendre() noexcept = default;

    constexpr legendre(const legendre&) noexcept = default;

    ~legendre() noexcept = default;

    ///
    /// @brief Evaluate nth Legendre polynomial at x
    /// @param n
    /// @param x
    /// @return
    ///
    inline Real operator()(Real x, size_t n) const noexcept;

    ///
    /// @brief Evaluate 1st derivative of the
    /// nth Legendre polynomial at x
    /// @param n
    /// @param x
    /// @return
    ///
    inline Real deriv(Real x, size_t n) const noexcept;

    ///
    /// @brief Returns the normalization parameter
    ///
    Real norm(size_t n) const noexcept
    {
      return Real{2} / (2 * n + 1);
    }

    template <class T, class U>
    void weights(const T& abscissa, U&& res) const;

    ///
    /// @brief Gauss-Legendre quadrature weights
    /// @param abscissa
    /// @return
    ///
    template <class T>
    std::vector<Real> weights(const T& abscissa) const;

    ///
    /// @brief Absissas for Gauss-legendre quadrature with n points.
    /// Accurate for polynomials up to order 2*n-1.
    /// @param n Order/number of integration points. Must be less than or equal
    /// to 64.
    /// @return
    ///
    std::vector<Real> abscissas(size_t n) const;

  private:
  };
}

#include "detail/legendre.tcc"

#endif
