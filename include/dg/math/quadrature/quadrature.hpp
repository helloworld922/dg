#ifndef DG_QUADRATURE_HPP
#define DG_QUADRATURE_HPP

#include "chebyshev.hpp"
#include "hermite.hpp"
#include "jacobi.hpp"
#include "legendre.hpp"
#include "lobatto.hpp"

//#include "integrate.hpp"

#endif
