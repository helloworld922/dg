#ifndef DG_INTEGRATE_HPP
#define DG_INTEGRATE_HPP

#include "dg/utils/meta.hpp"
#include "dg/utils/view_utils.hpp"

#include <array>

namespace dg
{
  ///
  /// @brief Integrates a function f using N-D quadrature
  /// @tparam Absc view<npoints1, npoints2, npoints3, ..., N>
  /// @tparam Wghts view 1D, flattened stack
  ///
  template <class Absc, class Wghts>
  struct integrator
  {
    struct extractor
    {
      Absc absc;

      template <class A>
      constexpr extractor(A&& absc) : absc(std::forward<A>(absc))
      {
      }

      extractor(const extractor&) = default;
      extractor(extractor&&) = default;

      extractor& operator=(const extractor&) = default;
      extractor& operator=(extractor&&) = default;

      template <class... Args>
      auto operator()(Args&&... args) const
        -> decltype(absc.subarray(std::forward<Args>(args)..., dg::all{}))
      {
        return absc.subarray(std::forward<Args>(args)..., dg::all{});
      }
    };
    extractor absc_;
    Wghts wghts;

    real compute_wghts(
      const std::array<size_t, Absc::extents_type::rank() - 1>& idcs) const
      noexcept
    {
      real res = wghts(idcs[0]);
      size_t idx = absc_.absc.extent(0);
      for (size_t i = 1; i < idcs.size(); ++i)
      {
        res *= wghts(idcs[i]);
        idx += absc_.absc.extent(i);
      }
      return res;
    }

  public:
    typedef const decltype(dg::meta::
        explode_args<std::make_index_sequence<Absc::rank() - 1>>::invoke(
          std::declval<extractor>(),
          std::declval<std::array<size_t, Absc::extents_type::rank() - 1>>()))&
      xi_type;

    static_assert(Absc::rank() >= 2, "Absc must have rank() >= 2");

    template <class A, class W>
    integrator(A&& absc, W&& wghts)
      : absc_(std::forward<A>(absc)), wghts(std::forward<W>(wghts))
    {
    }

    integrator(const integrator& o) = default;
    integrator(integrator&& o) = default;

    integrator& operator=(const integrator& o) = default;
    integrator& operator=(integrator&& o) = default;

    /**
     * @brief Integrate f over the local dimensions (specific to the
     * abscissa/weights).
     * Note that f must contain any Jacobian transforms if integrating over a
     * non-local coordinate
     * @param res where to add the result to (note: this is not zeroed out at
     * the beginning!
     * @param void f(res, xi, weight, args...)
     */
    template <class Res, class Fun, class... Args>
    void operator()(Res& res, const Fun& f, const Args&... args) const
    {
      std::array<size_t, Absc::extents_type::rank() - 1> idcs{0};

      for (; idcs[0] < absc_.absc.extent(0);)
      {
        // call f using subarray for absc[idx, all]
        f(res,
          dg::meta::explode_args<
            std::make_index_sequence<Absc::rank() - 1>>::invoke(absc_,
            idcs),
          compute_wghts(idcs), args...);
        // advance to next point
        ++idcs.back();
        for (size_t j = idcs.size() - 1;
             j > 0 && idcs[j] >= absc_.absc.extent(j); --j)
        {
          ++idcs[j - 1];
          idcs[j] = 0;
        }
      }
    }
  };
}

#endif
