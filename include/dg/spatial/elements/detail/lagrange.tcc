#ifndef DG_LAGRANGE_TCC
#define DG_LAGRANGE_TCC

#include "dg/utils/meta.hpp"

#include <Eigen/Dense>

namespace dg
{
  namespace spatial
  {
    namespace elements
    {
      template <class Real, size_t... Degrees>
      constexpr size_t lagrange<Real, Degrees...>::size() noexcept
      {
        return meta::sequence_accumulate<size_t, 1>(
          degrees{}, [](size_t a, size_t b) { return (a + 1) * b; });
      }

      template <class Real, size_t... Degrees>
      template <size_t Dim>
      Real lagrange<Real, Degrees...>::lagrange1d(
        const Real& xi, size_t idx) const
      {
        Real res = 1;
        Real den = 1;
        for (size_t i = 0; i < idx; ++i)
        {
          res *= (xi - absc[Dim][i]);
          den *= (absc[Dim][idx] - absc[Dim][i]);
        }
        for (size_t i = idx + 1;
             i <= meta::sequence_element<Dim, degrees>::value; ++i)
        {
          res *= (xi - absc[Dim][i]);
          den *= (absc[Dim][idx] - absc[Dim][i]);
        }
        return res / den;
      }

      template <class Real, size_t... Degrees>
      template <size_t Dim>
      Real lagrange<Real, Degrees...>::deriv_lagrange1d(
        const Real& xi, size_t idx) const
      {
        Real res = 0;
        for (size_t j = 0; j < idx; ++j)
        {
          Real num = 1;
          Real den = 1;
          for (size_t i = 0; i < idx; ++i)
          {
            if (i != j)
            {
              num *= (xi - absc[Dim][i]);
            }
            den *= (absc[Dim][idx] - absc[Dim][i]);
          }
          for (size_t i = idx + 1;
               i <= meta::sequence_element<Dim, degrees>::value; ++i)
          {
            if (i != j)
            {
              num *= (xi - absc[Dim][i]);
            }
            den *= (absc[Dim][idx] - absc[Dim][i]);
          }
          res += num / den;
        }
        for (size_t j = idx + 1;
             j <= meta::sequence_element<Dim, degrees>::value; ++j)
        {
          Real num = 1;
          Real den = 1;
          for (size_t i = 0; i < idx; ++i)
          {
            if (i != j)
            {
              num *= (xi - absc[Dim][i]);
            }
            den *= (absc[Dim][idx] - absc[Dim][i]);
          }
          for (size_t i = idx + 1;
               i <= meta::sequence_element<Dim, degrees>::value; ++i)
          {
            if (i != j)
            {
              num *= (xi - absc[Dim][i]);
            }
            den *= (absc[Dim][idx] - absc[Dim][i]);
          }
          res += num / den;
        }
        return res;
      }

      template <class Real, size_t... Degrees>
      template <class T, class... Idcs>
      typename std::enable_if<sizeof...(Idcs) == sizeof...(Degrees), Real>::type
      lagrange<Real, Degrees...>::operator()(
        const T& xi, Idcs... idcs) const
      {
        return eval_impl<0>(xi, idcs...);
      }

      template <class Real, size_t... Degrees>
      template <size_t Dim, class T, class... Idcs>
      typename std::enable_if<sizeof...(Idcs) == sizeof...(Degrees), Real>::type
      lagrange<Real, Degrees...>::deriv(
        const T& xi, Idcs... idcs) const
      {
        return deriv_impl<Dim, 0>(xi, idcs...);
      }

      template <class Real, size_t... Degrees>
      template <size_t deriv_Dim, size_t Dim, class T>
      Real lagrange<Real, Degrees...>::deriv_impl(
        const T& xi, size_t idx0) const
      {
        return deriv_Dim == Dim ? deriv_lagrange1d<deriv_Dim>(xi[Dim], idx0)
                                : lagrange1d<Dim>(xi[Dim], idx0);
      }

      template <class Real, size_t... Degrees>
      template <size_t deriv_Dim, size_t Dim, class T, class... Idcs>
      Real lagrange<Real, Degrees...>::deriv_impl(
        const T& xi, size_t idx0, size_t idx1, Idcs... idcs) const
      {
        return (deriv_Dim == Dim ? deriv_lagrange1d<deriv_Dim>(xi[Dim], idx0)
                                 : lagrange1d<Dim>(xi[Dim], idx0)) *
               deriv_impl<deriv_Dim, Dim + 1>(xi, idx1, idcs...);
      }

      template <class Real, size_t... Degrees>
      template <size_t Dim, class T>
      Real lagrange<Real, Degrees...>::eval_impl(
        const T& xi, size_t idx0) const
      {
        return lagrange1d<Dim>(xi[Dim], idx0);
      }

      template <class Real, size_t... Degrees>
      template <size_t Dim, class T, class... Idcs>
      Real lagrange<Real, Degrees...>::eval_impl(
        const T& xi, size_t idx0, size_t idx1, Idcs... idcs) const
      {
        return lagrange1d<Dim>(xi[Dim], idx0) *
               eval_impl<Dim + 1>(xi, idx1, idcs...);
      }

      template <class Real, size_t... Degrees>
      lagrange<Real, Degrees...>::lagrange()
        : absc(init_absc(absc_data_.data())),
          weights(init_weights(absc, weights_data_.data()))
      {
      }

      template <class Real, size_t... Degrees>
      auto lagrange<Real, Degrees...>::init_absc(Real* data)
      {
        array_type res;
        absc_initializer_<0, 0, Degrees...>::apply(res, data);
        return res;
      }

      template <class Real, size_t... Degrees>
      auto lagrange<Real, Degrees...>::init_weights(
        const array_type& abscs, Real* data)
      {
        array_type res;
        weight_initializer_<0, 0, Degrees...>::apply(abscs, res, data);
        return res;
      }

      template<class Real, size_t... Degrees>
      template<class Fun>
      Real lagrange<Real, Degrees...>::vol_integral(Fun&& fun) const
      {
        std::array<Real, ndims()> xi;
        return vol_int_impl<0>::apply(*this, xi, fun, 1);
      }

      template<class Real, size_t... Degrees>
      template<class Xi, class C>
      auto lagrange<Real, Degrees...>::jacobian(const Xi& xi, const C& coords) const
      {
        Eigen::Matrix<Real, sizeof...(Degrees), sizeof...(Degrees)> res =
          Eigen::Matrix<Real, sizeof...(Degrees), sizeof...(Degrees)>::Zero();
        
        for(size_t row = 0; row < sizeof...(Degrees); ++row)
        {
          // iterating through col needs to happen at compile time
          jac_int_impl<0>::apply(*this, res, xi, coords, row);
        }
        return res;
      }

      template <class Real, size_t... Degrees>
      template <class T, class Xs>
      Real lagrange<Real, Degrees...>::interp(const T& xi, const Xs& xs) const
      {
        // iterate through every basis function
        std::array<size_t, ndims()> idcs;
        return interp_impl<0>::apply(*this, idcs, xi, xs);
      }

      template<class Real, size_t... Degrees>
      template<size_t face, class Fun>
      Real lagrange<Real, Degrees...>::surf_integral(Fun&& fun) const
      {
        std::array<Real, ndims()> xi;
        constexpr size_t face_dim = (face >> 1);
        constexpr int face_side = face & 1;
        xi[face_dim] = 2 * face_side - 1;
        return surf_int_impl<face_dim, 0>::apply(
          *this, xi, fun, 1);
      }
    }
  }
}

#endif
