#ifndef DG_LAGRANGE_HPP
#define DG_LAGRANGE_HPP

#include "dg/utils/compiler_support.hpp"
#include "dg/utils/layouts.hpp"
#include "dg/utils/view.hpp"

#include "dg/math/quadrature/lobatto.hpp"

#include <array>

namespace dg
{
  namespace spatial
  {
    namespace elements
    {
      ///
      /// N-D element, made up of a tensor product of 1D nodal Lagrange
      /// basis.
      /// Coordinates are specified in cartesian.
      /// Canonical element spans [-1,1] in each dimension
      /// @tparam Degrees 1D basis polynomial degree in each dimension. Note
      /// that there is 1 more point per dimension than the polynomial degree.
      ///
      template <class Real, size_t... Degrees>
      struct lagrange
      {

        ///
        /// @brief Number of dimensions of this tensor element
        ///
        static constexpr size_t ndims() noexcept
        {
          return sizeof...(Degrees);
        }

        ///
        /// @brief Number of nodes present in this tensor element
        ///
        static constexpr size_t size() noexcept;

        /// \{
        typedef std::index_sequence<Degrees...> degrees;
        typedef std::array<view<Real, 1>, ndims()> array_type;

        std::array<Real, meta::sequence_sum(degrees{}) + ndims()> absc_data_;
        std::array<Real, meta::sequence_sum(degrees{}) + ndims()> weights_data_;
        /// \}

        ///
        /// @brief Abscissa, segmented by each dimension. First index is
        /// dimension index,
        /// second index is for abscissa
        ///
        array_type absc;
        ///
        /// @brief Quadrature weights, segmented by each dimension. First index
        /// is dimension index, second index is for abscissa
        ///
        array_type weights;

        /// \{
        lagrange();
        /// \}

        ///
        /// @brief Interpolates to x(xi)
        /// @param xi local coordinate position to interpolate to
        /// @param xs global vertex node locations
        /// @tparam T Real
        /// @tparam X N-D view: idcs
        ///
        template <class T, class Xs>
        Real interp(const T& xi, const Xs& xs) const;

        ///
        /// @brief Evaluates a given nodal basis functions at local cordinate xi
        /// @param xi local coordinate
        /// @param idcs which nodal basis function to evaluate
        ///
        template <class T, class... Idcs>
        typename std::enable_if<sizeof...(Idcs) == sizeof...(Degrees),
          Real>::type
        operator()(const T& xi, Idcs... idcs) const;

        ///
        /// @brief Evaluates the first derivative of a given nodal basis
        /// function with respect to xi_{Dim} evaluate at xi
        /// @param xi
        /// @param idcs
        ///
        template <size_t Dim, class T, class... Idcs>
        std::enable_if_t<sizeof...(Idcs) == sizeof...(Degrees), Real> deriv(
          const T& xi, Idcs... idcs) const;

        ///
        /// @brief Volume integrate a given function over a single normalized
        /// element. Includes no Jacobian determinant transformation.
        /// @param fun
        /// @tparam Fun
        ///
        template <class Fun>
        Real vol_integral(Fun&& fun) const;

        ///
        /// @brief Computes the Jacobian matrix evaluated at dx_{row}/dxi_{col} at xi.
        /// @param xi local coordinate of where to evaluate Jacobian matrix
        /// @param coords global coordinate node locations of the element.
        /// Ordering of coordinates should be in "column major" ordering,
        /// starting from the "most negative" coordinate. For example, a 2D
        /// ordering might look like:
        ///    c,d
        ///    a,b
        ///
        /// @return An Eigen dense matrix of the Jacobian. Taking the
        /// determinant of this matrix can be used for vol_integral.
        /// @tparam Xi 1D view-like type
        /// @tparam C view type. Number of dimensions should be the number of
        /// dimensions this element exists in + 1. Last dimension is the dimension index, first N dimensions are indices as passed to functions like operator() an deriv.
        ///
        template <class Xi, class C>
        auto jacobian(const Xi& xi, const C& coords) const;

        ///
        /// @brief Surface integrate a given function over a single normalized
        /// element
        /// @param fun
        /// @tparam face face index. Faces are ordered by dim0(min), dim0(max),
        /// dim1(min), dim1(max), ..., where dimn is the dimension which is
        /// fixed,
        /// (min) denotes fixed dim xi = -1, (max) denotes fixed dim xi = 1.
        ///
        template <size_t face, class Fun>
        Real surf_integral(Fun&& fun) const;

      private:
        ///
        /// @brief Evaluates a 1D lagrange interpolation function
        /// @tparam Dim what dimension to vary
        ///
        template <size_t Dim>
        Real lagrange1d(const Real& xi, size_t idx) const;

        ///
        /// @brief Evaluates a 1D lagrange interpolation function
        /// @tparam Dim what dimension to vary
        ///
        template <size_t Dim>
        Real deriv_lagrange1d(const Real& xi, size_t idx) const;

        template <size_t I, size_t Idx, size_t... Dims>
        struct absc_initializer_;

        template <size_t I, size_t Idx>
        struct absc_initializer_<I, Idx>
        {
          static void apply(array_type& res, Real* data)
          {
          }
        };

        template <size_t I, size_t Idx, size_t D, size_t... Dims>
        struct absc_initializer_<I, Idx, D, Dims...>
        {
          static void apply(array_type& res, Real* data)
          {
            constexpr dg::lobatto<Real> quad{};
            auto a = quad.abscissas(D + 1);
            res[I].data() = data + Idx;
            std::copy(a.begin(), a.end(), res[I].data());
            res[I].shape()[0] = D + 1;
            absc_initializer_<I + 1, Idx + D + 1, Dims...>::apply(res, data);
          }
        };

        template <size_t I, size_t Idx, size_t... Dims>
        struct weight_initializer_;

        template <size_t I, size_t Idx>
        struct weight_initializer_<I, Idx>
        {
          static void apply(
            const array_type& abscs, array_type& res, Real* data)
          {
          }
        };

        template <size_t I, size_t Idx, size_t D, size_t... Dims>
        struct weight_initializer_<I, Idx, D, Dims...>
        {
          static void apply(
            const array_type& abscs, array_type& res, Real* data)
          {
            constexpr dg::lobatto<Real> quad{};
            res[I].data() = data + Idx;
            res[I].shape()[0] = D + 1;
            quad.weights(abscs[I], res[I]);
            weight_initializer_<I + 1, Idx + D + 1, Dims...>::apply(
              abscs, res, data);
          }
        };

        template <size_t FaceDim, size_t Idx,
          bool Last = (Idx == sizeof...(Degrees)),
          bool IsFixed = (FaceDim == Idx)>
        struct surf_int_impl
        {
          template <class Fun>
          static Real apply(const lagrange& self, std::array<Real, ndims()>& xi,
            Fun&& fun, const Real& w)
          {
            // default recursion state
            Real res = 0;
            for (size_t i = 0; i <= meta::sequence_element<Idx, degrees>::value;
                 ++i)
            {
              xi[Idx] = self.absc[Idx][i];
              res += surf_int_impl<FaceDim, Idx + 1>::apply(
                self, xi, fun, w * self.weights[Idx][i]);
            }
            return res;
          }
        };

        template <size_t FaceDim, size_t Idx>
        struct surf_int_impl<FaceDim, Idx, false, true>
        {
          template <class Fun>
          static Real apply(const lagrange& self, std::array<Real, ndims()>& xi,
            Fun&& fun, const Real& w)
          {
            // face recursion state
            return surf_int_impl<FaceDim, Idx + 1>::apply(self, xi, fun, w);
          }
        };

        template <size_t FaceDim, size_t Idx>
        struct surf_int_impl<FaceDim, Idx, true, false>
        {
          template <class Fun>
          static Real apply(const lagrange& self, std::array<Real, ndims()>& xi,
            Fun&& fun, const Real& w)
          {
            // default base case
            // can never have template specialization true,true
            return fun(xi) * w;
          }
        };

        template <size_t Idx, bool Last = (Idx == sizeof...(Degrees))>
        struct interp_impl
        {
          template <class T, class Xs>
          static Real apply(const lagrange& self,
            std::array<size_t, ndims()>& idcs, const T& xi, const Xs& xs)
          {
            Real res = 0;
            for (size_t i = 0; i <= meta::sequence_element<Idx, degrees>::value;
                 ++i)
            {
              idcs[Idx] = i;
              res += interp_impl<Idx + 1>::apply(self, idcs, xi, xs);
            }
            return res;
          }
        };

        template <size_t Idx>
        struct interp_impl<Idx, true>
        {
          template <class T, class Xs>
          static Real apply(const lagrange& self,
            std::array<size_t, ndims()>& idcs, const T& xi, const Xs& xs)
          {
            return xs.get(idcs) *
                   meta::explode_args<std::make_index_sequence<ndims()>>::
                     invoke(
                       [&self, &xi](auto... args) { return self(xi, args...); },
                       idcs);
          }
        };

        template <size_t Idx, bool Last = (Idx == sizeof...(Degrees))>
        struct vol_int_impl
        {
          template <class Fun>
          static Real apply(const lagrange& self, std::array<Real, ndims()>& xi,
            Fun&& fun, const Real& w)
          {
            // default recursion state
            Real res = 0;
            for (size_t i = 0; i <= meta::sequence_element<Idx, degrees>::value;
                 ++i)
            {
              xi[Idx] = self.absc[Idx][i];
              res += vol_int_impl<Idx + 1>::apply(
                self, xi, fun, w * self.weights[Idx][i]);
            }
            return res;
          }
        };

        template <size_t Idx>
        struct vol_int_impl<Idx, true>
        {
          template <class Fun>
          static Real apply(const lagrange& self, std::array<Real, ndims()>& xi,
            Fun&& fun, const Real& w)
          {
            // base case
            return fun(xi) * w;
          }
        };

        template<size_t Idx, bool Last = (Idx == 0)>
        struct advance_coord_impl
        {
          static void apply(std::array<size_t, sizeof...(Degrees)>& coord_idx)
          {
            ++coord_idx[Idx - 1];
            if (coord_idx[Idx - 1] ==
                meta::sequence_element<Idx - 1,
                  std::index_sequence<Degrees...>>::value +
                  1)
            {
              coord_idx[Idx - 1] = 0;
              advance_coord_impl<Idx - 1>::apply(coord_idx);
            }
          }
        };

        template <size_t Idx>
        struct advance_coord_impl<Idx, true>
        {
          static void apply(std::array<size_t, sizeof...(Degrees)>& coord_idx)
          {
            // base case
          }
        };

        template<size_t Col, bool Last = (Col == sizeof...(Degrees))>
        struct jac_int_impl
        {
          template <class Jac, class Xi, class C>
          static void apply(const lagrange& self, Jac& jac, const Xi& xi,
            const C& coords, size_t row)
          {
            {
              std::array<size_t, sizeof...(Degrees)> coord_idx = {0};
              constexpr auto ncoords =
                meta::sequence_product(std::index_sequence<(Degrees + 1)...>{});
              for (size_t i = 0; i < ncoords; ++i)
              {
                jac(row, Col) +=
                  meta::explode_args<
                    std::make_index_sequence<sizeof...(Degrees)>>::
                    invoke(
                      [&self, &xi](
                        auto... idcs) { return self.template deriv<Col>(xi, idcs...); },
                      coord_idx) *
                  meta::explode_args<
                    std::make_index_sequence<sizeof...(Degrees)>>::
                    invoke([&self, &coords, row](
                             auto... idcs) { return coords(idcs..., row); },
                      coord_idx);
                // advance to the next coord_idx
                advance_coord_impl<sizeof...(Degrees)>::apply(coord_idx);
              }
            }
            jac_int_impl<Col + 1>::apply(self, jac, xi, coords, row);
          }
        };

        template<size_t Col>
        struct jac_int_impl<Col, true>
        {
          template <class Jac, class Xi, class C>
          static void apply(const lagrange& self, Jac& jac, const Xi& xi,
            const C& coords, size_t row)
          {
            // base case
          }
        };

        template <size_t Dim, class T>
        Real eval_impl(const T& xi, size_t idx0) const;

        template <size_t Dim, class T, class... Idcs>
        Real eval_impl(
          const T& xi, size_t idx0, size_t idx1, Idcs... idcs) const;

        template <size_t deriv_Dim, size_t Dim, class T>
        Real deriv_impl(const T& xi, size_t idx0) const;

        template <size_t deriv_Dim, size_t Dim, class T, class... Idcs>
        Real deriv_impl(
          const T& xi, size_t idx0, size_t idx1, Idcs... idcs) const;

        static auto init_absc(Real* data);

        static auto init_weights(const array_type& abscs, Real* data);
      };
    }
  }
}

#include "detail/lagrange.tcc"

#endif
