#ifndef DG_NDG_RECT_2D_TCC
#define DG_NDG_RECT_2D_TCC

#include "dg/utils/view.hpp"

namespace dg
{
  namespace ndg
  {
    template <size_t Degree_x, size_t Degree_y>
    rect<Degree_x, Degree_y>::rect()
    {
    }

    template <size_t Degree_x, size_t Degree_y>
    template <class V, class R>
    void rect<Degree_x, Degree_y>::calc_mass_matrix(
      V&& dst, const R& coords) const
    {
      typedef typename std::decay_t<V>::value_type value_type;

      // Element node iteration order (bottom left is 0,0):
      // c,d
      // a,b
      for (size_t i_x = 0; i_x <= Degree_x; ++i_x)
      {
        for (size_t i_y = 0; i_y <= Degree_y; ++i_y)
        {
          for (size_t j_x = 0; j_x <= Degree_x; ++j_x)
          {
            for (size_t j_y = 0; j_y <= Degree_y; ++j_y)
            {
              dst(i_x * (Degree_y + 1) + i_y, j_x * (Degree_y + 1) + j_y) =
                static_cast<value_type>(integrator.vol_integral(
                  [this, i_x, i_y, j_x, j_y, &coords](
                    const std::array<dg::real256, 2>& xi) {
                    return element(xi, i_x, i_y) * element(xi, j_x, j_y) *
                           flat_element().jacobian(xi, coords).determinant();
                  }));
            }
          }
        }
      }
    }

    template <size_t Degree_x, size_t Degree_y>
    template <class R>
    Eigen::Matrix<dg::real256, (Degree_x + 1) * (Degree_y + 1),
      (Degree_x + 1) * (Degree_y + 1)>
    rect<Degree_x, Degree_y>::calc_imass_matrix(const R& coords) const
    {
      Eigen::Matrix<dg::real256, (Degree_x + 1) * (Degree_y + 1),
        (Degree_x + 1) * (Degree_y + 1)>
        res;
      calc_mass_matrix(res, coords);
      res = res.inverse().eval();
      return res;
    }

    template <size_t Degree_x, size_t Degree_y>
    template <class V, class M, class R>
    void rect<Degree_x, Degree_y>::calc_advect_matrix(
      V&& dst, const M& imass, const R& coords) const
    {
      typedef typename std::decay_t<V>::value_type value_type;

      Eigen::Matrix<dg::real256, (Degree_x + 1) * (Degree_y + 1),
        (Degree_x + 1) * (Degree_y + 1)>
        c_advect_matrix;

      // the first term
      for (size_t i_x = 0; i_x <= Degree_x; ++i_x)
      {
        for (size_t i_y = 0; i_y <= Degree_y; ++i_y)
        {
          for (size_t j_x = 0; j_x <= Degree_x; ++j_x)
          {
            for (size_t j_y = 0; j_y <= Degree_y; ++j_y)
            {
              c_advect_matrix(
                i_x * (Degree_y + 1) + i_y, j_x * (Degree_y + 1) + j_y) =
                integrator.vol_integral(
                  [this, i_x, i_y, j_x, j_y, &coords](
                    const std::array<dg::real256, 2>& xi) {
                    static dg::real256 quarter(".25");
                    return quarter * element(xi, j_x, j_y) *
                           ((coords(0, 1, 1) + coords(1, 1, 1) -
                              coords(0, 0, 1) - coords(1, 0, 1) +
                              (coords(0, 0, 1) + coords(1, 1, 1) -
                                coords(0, 1, 1) - coords(1, 0, 1)) *
                                xi[0]) *
                               element.template deriv<0>(xi, i_x, i_y) +
                             (coords(0, 0, 1) + coords(0, 1, 1) -
                               coords(1, 0, 1) - coords(1, 1, 1) +
                               (coords(1, 0, 1) + coords(0, 1, 1) -
                                 coords(0, 0, 1) - coords(1, 1, 1)) *
                                 xi[1]) *
                               element.template deriv<1>(xi, i_x, i_y));
                  });
            }
          }
        }
      }
      c_advect_matrix = (imass * c_advect_matrix).eval();
      for (size_t i_x = 0; i_x <= Degree_x; ++i_x)
      {
        for (size_t i_y = 0; i_y <= Degree_y; ++i_y)
        {
          for (size_t j_x = 0; j_x <= Degree_x; ++j_x)
          {
            for (size_t j_y = 0; j_y <= Degree_y; ++j_y)
            {
              dst(0, i_x * (Degree_y + 1) + i_y, j_x * (Degree_y + 1) + j_y) =
                static_cast<value_type>(c_advect_matrix(
                  i_x * (Degree_y + 1) + i_y, j_x * (Degree_y + 1) + j_y));
            }
          }
        }
      }

      // the second term
      for (size_t i_x = 0; i_x <= Degree_x; ++i_x)
      {
        for (size_t i_y = 0; i_y <= Degree_y; ++i_y)
        {
          for (size_t j_x = 0; j_x <= Degree_x; ++j_x)
          {
            for (size_t j_y = 0; j_y <= Degree_y; ++j_y)
            {
              c_advect_matrix(
                i_x * (Degree_y + 1) + i_y, j_x * (Degree_y + 1) + j_y) =
                integrator.vol_integral(
                  [this, i_x, i_y, j_x, j_y, &coords](
                    const std::array<dg::real256, 2>& xi) {
                    static dg::real256 quarter(".25");
                    return quarter * element(xi, j_x, j_y) *
                           ((coords(0, 0, 0) + coords(1, 0, 0) -
                              coords(0, 1, 0) - coords(1, 1, 0) +
                              (coords(1, 0, 0) + coords(0, 1, 0) -
                                coords(0, 0, 0) - coords(1, 1, 0)) *
                                xi[0]) *
                               element.template deriv<0>(xi, i_x, i_y) +
                             (coords(1, 0, 0) + coords(1, 1, 0) -
                               coords(0, 0, 0) - coords(0, 1, 0) +
                               (coords(0, 0, 0) + coords(1, 1, 0) -
                                 coords(1, 0, 0) - coords(0, 1, 0)) *
                                 xi[1]) *
                               element.template deriv<1>(xi, i_x, i_y));
                  });
            }
          }
        }
      }
      c_advect_matrix = (imass * c_advect_matrix).eval();
      for (size_t i_x = 0; i_x <= Degree_x; ++i_x)
      {
        for (size_t i_y = 0; i_y <= Degree_y; ++i_y)
        {
          for (size_t j_x = 0; j_x <= Degree_x; ++j_x)
          {
            for (size_t j_y = 0; j_y <= Degree_y; ++j_y)
            {
              dst(1, i_x * (Degree_y + 1) + i_y, j_x * (Degree_y + 1) + j_y) =
                static_cast<value_type>(c_advect_matrix(
                  i_x * (Degree_y + 1) + i_y, j_x * (Degree_y + 1) + j_y));
            }
          }
        }
      }
    }

    template <size_t Degree_x, size_t Degree_y>
    template <class V, class R>
    void rect<Degree_x, Degree_y>::calc_advect_matrix(
      V&& dst, const R& coords) const
    {
      typedef typename std::decay_t<V>::value_type value_type;

      // the first term
      for (size_t i_x = 0; i_x <= Degree_x; ++i_x)
      {
        for (size_t i_y = 0; i_y <= Degree_y; ++i_y)
        {
          for (size_t j_x = 0; j_x <= Degree_x; ++j_x)
          {
            for (size_t j_y = 0; j_y <= Degree_y; ++j_y)
            {
              dst(0, i_x * (Degree_y + 1) + i_y, j_x * (Degree_y + 1) + j_y) =
                static_cast<value_type>(integrator.vol_integral(
                  [this, i_x, i_y, j_x, j_y, &coords](
                    const std::array<dg::real256, 2>& xi) {
                    static dg::real256 quarter(".25");
                    return quarter * element(xi, j_x, j_y) *
                           ((coords(0, 1, 1) + coords(1, 1, 1) -
                              coords(0, 0, 1) - coords(1, 0, 1) +
                              (coords(0, 0, 1) + coords(1, 1, 1) -
                                coords(0, 1, 1) - coords(1, 0, 1)) *
                                xi[0]) *
                               element.template deriv<0>(xi, i_x, i_y) +
                             (coords(0, 0, 1) + coords(0, 1, 1) -
                               coords(1, 0, 1) - coords(1, 1, 1) +
                               (coords(1, 0, 1) + coords(0, 1, 1) -
                                 coords(0, 0, 1) - coords(1, 1, 1)) *
                                 xi[1]) *
                               element.template deriv<1>(xi, i_x, i_y));
                  }));
            }
          }
        }
      }

      // the second term
      for (size_t i_x = 0; i_x <= Degree_x; ++i_x)
      {
        for (size_t i_y = 0; i_y <= Degree_y; ++i_y)
        {
          for (size_t j_x = 0; j_x <= Degree_x; ++j_x)
          {
            for (size_t j_y = 0; j_y <= Degree_y; ++j_y)
            {
              dst(1, i_x * (Degree_y + 1) + i_y, j_x * (Degree_y + 1) + j_y) =
                static_cast<value_type>(integrator.vol_integral(
                  [this, i_x, i_y, j_x, j_y, &coords](
                    const std::array<dg::real256, 2>& xi) {
                    static dg::real256 quarter(".25");
                    return quarter * element(xi, j_x, j_y) *
                           ((coords(0, 0, 0) + coords(1, 0, 0) -
                              coords(0, 1, 0) - coords(1, 1, 0) +
                              (coords(1, 0, 0) + coords(0, 1, 0) -
                                coords(0, 0, 0) - coords(1, 1, 0)) *
                                xi[0]) *
                               element.template deriv<0>(xi, i_x, i_y) +
                             (coords(1, 0, 0) + coords(1, 1, 0) -
                               coords(0, 0, 0) - coords(0, 1, 0) +
                               (coords(0, 0, 0) + coords(1, 1, 0) -
                                 coords(1, 0, 0) - coords(0, 1, 0)) *
                                 xi[1]) *
                               element.template deriv<1>(xi, i_x, i_y));
                  }));
            }
          }
        }
      }
    }

    template <size_t Degree_x, size_t Degree_y>
    template <class V, class M, class R>
    void rect<Degree_x, Degree_y>::calc_lift_matrix(
      V&& dst, const M& imass, const R& coords) const
    {
      typedef typename std::decay_t<V>::value_type value_type;
      Eigen::Matrix<dg::real256, (Degree_x + 1) * (Degree_y + 1),
        2 * (Degree_x + 1) + 2 * (Degree_y + 1)>
        lift;
      lift.setZero();
      // left face
      {
        dg::real256 jac =
          dg::real256{".5"} * sqrt(pow(coords(0, 0, 0) - coords(0, 1, 0), 2) +
                                   pow(coords(0, 0, 1) - coords(0, 1, 1), 2));

        for (size_t i_y = 0; i_y <= Degree_y; ++i_y)
        {
          for (size_t j_y = 0; j_y <= Degree_y; ++j_y)
          {
            lift(i_y, j_y) = integrator.template surf_integral<0>(
              [this, i_y, j_y, &jac](const std::array<dg::real256, 2>& xi) {
                return jac * element(xi, 0, i_y) * element(xi, 0, j_y);
              });
          }
        }
      }
      // right face
      {
        dg::real256 jac =
          dg::real256{".5"} * sqrt(pow(coords(1, 0, 0) - coords(1, 1, 0), 2) +
                                   pow(coords(1, 0, 1) - coords(1, 1, 1), 2));

        for (size_t i_y = 0; i_y <= Degree_y; ++i_y)
        {
          for (size_t j_y = 0; j_y <= Degree_y; ++j_y)
          {
            lift(Degree_x * (Degree_y + 1) + i_y, Degree_y + 1 + j_y) =
              integrator.template surf_integral<1>(
                [this, i_y, j_y, &jac](const std::array<dg::real256, 2>& xi) {
                  return jac * element(xi, Degree_x, i_y) *
                         element(xi, Degree_x, j_y);
                });
          }
        }
      }
      // bottom face
      {
        dg::real256 jac =
          dg::real256{".5"} * sqrt(pow(coords(0, 0, 0) - coords(1, 0, 0), 2) +
                                   pow(coords(0, 0, 1) - coords(1, 0, 1), 2));

        for (size_t i_x = 0; i_x <= Degree_x; ++i_x)
        {
          for (size_t j_x = 0; j_x <= Degree_x; ++j_x)
          {
            lift(i_x * (Degree_y + 1), 2 * (Degree_y + 1) + j_x) =
              integrator.template surf_integral<2>(
                [this, i_x, j_x, &jac](const std::array<dg::real256, 2>& xi) {
                  return jac * element(xi, i_x, 0) * element(xi, j_x, 0);
                });
          }
        }
      }
      // top face
      {
        dg::real256 jac =
          dg::real256{".5"} * sqrt(pow(coords(0, 1, 0) - coords(1, 1, 0), 2) +
                                   pow(coords(0, 1, 1) - coords(1, 1, 1), 2));

        for (size_t i_x = 0; i_x <= Degree_x; ++i_x)
        {
          for (size_t j_x = 0; j_x <= Degree_x; ++j_x)
          {
            lift(Degree_y + i_x * (Degree_y + 1),
              (Degree_x + 1) + 2 * (Degree_y + 1) + j_x) =
              integrator.template surf_integral<3>(
                [this, i_x, j_x, &jac](const std::array<dg::real256, 2>& xi) {
                  return jac * element(xi, i_x, Degree_y) *
                         element(xi, j_x, Degree_y);
                });
          }
        }
      }
      lift = (imass * lift).eval();
      // write into dst, typecast
      for (size_t i = 0; i < (Degree_x + 1) * (Degree_y + 1); ++i)
      {
        for (size_t j = 0; j < 2 * (Degree_x + 1) + 2 * (Degree_y + 1); ++j)
        {
          dst(i, j) = static_cast<value_type>(lift(i, j));
        }
      }
    }

    template <size_t Degree_x, size_t Degree_y>
    template <class V, class R>
    void rect<Degree_x, Degree_y>::calc_lift_matrix(
      V&& dst, const R& coords) const
    {
      // left face
      {
        auto& lift = dst.template get<0>();
        typedef typename std::decay_t<decltype(lift)>::value_type value_type;

        dg::real256 jac =
          dg::real256{".5"} * sqrt(pow(coords(0, 0, 0) - coords(0, 1, 0), 2) +
                                   pow(coords(0, 0, 1) - coords(0, 1, 1), 2));

        for (size_t i_y = 0; i_y <= Degree_y; ++i_y)
        {
          for (size_t j_y = 0; j_y <= Degree_y; ++j_y)
          {
            lift(i_y, j_y) =
              static_cast<value_type>(integrator.template surf_integral<0>(
                [this, i_y, j_y, &jac](const std::array<dg::real256, 2>& xi) {
                  return jac * element(xi, 0, i_y) * element(xi, 0, j_y);
                }));
          }
        }
      }
      // right face
      {
        auto& lift = dst.template get<1>();
        typedef typename std::decay_t<decltype(lift)>::value_type value_type;

        dg::real256 jac =
          dg::real256{".5"} * sqrt(pow(coords(1, 0, 0) - coords(1, 1, 0), 2) +
                                   pow(coords(1, 0, 1) - coords(1, 1, 1), 2));

        for (size_t i_y = 0; i_y <= Degree_y; ++i_y)
        {
          for (size_t j_y = 0; j_y <= Degree_y; ++j_y)
          {
            lift(i_y, j_y) =
              static_cast<value_type>(integrator.template surf_integral<1>(
                [this, i_y, j_y, &jac](const std::array<dg::real256, 2>& xi) {
                  return jac * element(xi, Degree_x, i_y) *
                         element(xi, Degree_x, j_y);
                }));
          }
        }
      }
      // bottom face
      {
        auto& lift = dst.template get<2>();
        typedef typename std::decay_t<decltype(lift)>::value_type value_type;

        dg::real256 jac =
          dg::real256{".5"} * sqrt(pow(coords(0, 0, 0) - coords(1, 0, 0), 2) +
                                   pow(coords(0, 0, 1) - coords(1, 0, 1), 2));

        for (size_t i_x = 0; i_x <= Degree_x; ++i_x)
        {
          for (size_t j_x = 0; j_x <= Degree_x; ++j_x)
          {
            lift(i_x, j_x) =
              static_cast<value_type>(integrator.template surf_integral<2>(
                [this, i_x, j_x, &jac](const std::array<dg::real256, 2>& xi) {
                  return jac * element(xi, i_x, 0) * element(xi, j_x, 0);
                }));
          }
        }
      }
      // top face
      {
        auto& lift = dst.template get<3>();
        typedef typename std::decay_t<decltype(lift)>::value_type value_type;

        dg::real256 jac =
          dg::real256{".5"} * sqrt(pow(coords(0, 1, 0) - coords(1, 1, 0), 2) +
                                   pow(coords(0, 1, 1) - coords(1, 1, 1), 2));

        for (size_t i_x = 0; i_x <= Degree_x; ++i_x)
        {
          for (size_t j_x = 0; j_x <= Degree_x; ++j_x)
          {
            lift(i_x, j_x) =
              static_cast<value_type>(integrator.template surf_integral<3>(
                [this, i_x, j_x, &jac](const std::array<dg::real256, 2>& xi) {
                  return jac * element(xi, i_x, Degree_y) *
                         element(xi, j_x, Degree_y);
                }));
          }
        }
      }
    }

    template <size_t Degree_x, size_t Degree_y>
    template <class Q, class R, class F>
    void rect<Degree_x, Degree_y>::source(const Q& q, R&& r, F&& func) const
    {
      // compute r += func(q)
      for (size_t i_x = 0; i_x < Degree_x + 1; ++i_x)
      {
        for (size_t i_y = 0; i_y < Degree_y + 1; ++i_y)
        {
          auto qp = q.subarray(i_x, i_y, dg::all{});
          auto rp = func.source(qp);
          detail::apply_all_comps<std::decay_t<decltype(rp)>>::apply(
            [](auto& dst, auto& src) { dst += src; },
            r.subarray(i_x, i_y, dg::all{}), rp);
        }
      }
    }

    template <size_t Degree_x, size_t Degree_y>
    template <class Q, class R, class F, class C>
    void rect<Degree_x, Degree_y>::source(
      const Q& q, R&& r, F&& func, const C& coords) const
    {
      // compute r += func(q, x)
      typedef typename std::decay_t<Q>::value_type value_type;
      std::array<value_type, 2> xi = {0, 0};
      std::array<value_type, 2> pos = {0, 0};
      auto xs = coords.subarray(dg::all{}, dg::all{}, 0);
      auto ys = coords.subarray(dg::all{}, dg::all{}, 1);

      static dg::spatial::elements::lagrange<value_type, Degree_x, Degree_y>
        hi_elem;
      static dg::spatial::elements::lagrange<value_type, 1, 1> flat_elem;

      for (size_t i_x = 0; i_x < Degree_x + 1; ++i_x)
      {
        xi[0] = hi_elem.absc[0][i_x];
        for (size_t i_y = 0; i_y < Degree_y + 1; ++i_y)
        {
          xi[1] = hi_elem.absc[1][i_y];
          pos[0] = flat_elem.interp(xi, xs);
          pos[1] = flat_elem.interp(xi, ys);
          
          auto qp = q.subarray(i_x, i_y, dg::all{});
          auto rp = func.source(qp, pos);
          detail::apply_all_comps<std::decay_t<decltype(rp)>>::apply(
            [](auto& dst, auto& src) { dst += src; },
            r.subarray(i_x, i_y, dg::all{}), rp);
        }
      }
    }

    template <size_t Degree_x, size_t Degree_y>
    template <class Q, class R, class F, class M>
    void rect<Degree_x, Degree_y>::msource(
      const Q& q, R&& r, F&& func, const M& mass_matrix) const
    {
      // compute r += func(q)
      for (size_t col_x = 0; col_x < Degree_x + 1; ++col_x)
      {
        for (size_t col_y = 0; col_y < Degree_y + 1; ++col_y)
        {
          auto qp = q.subarray(col_x, col_y, dg::all{});
          auto rp = func.msource(qp);
          for (size_t row_x = 0; row_x < Degree_x + 1; ++row_x)
          {
            for (size_t row_y = 0; row_y < Degree_y + 1; ++row_y)
            {
              detail::apply_all_comps<std::decay_t<decltype(rp)>>::apply(
                [& mult = mass_matrix(row_x * (Degree_y + 1) + row_y,
                   col_x * (Degree_y + 1) + col_y)](
                  auto& dst, auto& src) { dst += src * mult; },
                r.subarray(row_x, row_y, dg::all{}), rp);
            }
          }
        }
      }
    }

    template <size_t Degree_x, size_t Degree_y>
    template <class Q, class R, class F, class M, class C>
    void rect<Degree_x, Degree_y>::msource(
      const Q& q, R&& r, F&& func, const M& mass_matrix, const C& coords) const
    {
      // compute r += func(q, x)
      typedef typename std::decay_t<Q>::value_type value_type;
      std::array<value_type, 2> xi = {0, 0};
      std::array<value_type, 2> pos = {0, 0};
      auto xs = coords.subarray(dg::all{}, dg::all{}, 0);
      auto ys = coords.subarray(dg::all{}, dg::all{}, 1);

      static dg::spatial::elements::lagrange<value_type, Degree_x, Degree_y>
        hi_elem;
      static dg::spatial::elements::lagrange<value_type, 1, 1> flat_elem;
      
      for (size_t col_x = 0; col_x < Degree_x + 1; ++col_x)
      {
        xi[0] = hi_elem.absc[0][col_x];
        
        for (size_t col_y = 0; col_y < Degree_y + 1; ++col_y)
        {
          xi[1] = hi_elem.absc[1][col_y];
          pos[0] = flat_elem.interp(xi, xs);
          pos[1] = flat_elem.interp(xi, ys);
          
          auto qp = q.subarray(col_x, col_y, dg::all{});
          auto rp = func.msource(qp, pos);
          for (size_t row_x = 0; row_x < Degree_x + 1; ++row_x)
          {
            for (size_t row_y = 0; row_y < Degree_y + 1; ++row_y)
            {
              detail::apply_all_comps<std::decay_t<decltype(rp)>>::apply(
                [& mult = mass_matrix(row_x * (Degree_y + 1) + row_y,
                   col_x * (Degree_y + 1) + col_y)](
                  auto& dst, auto& src) { dst += src * mult; },
                r.subarray(row_x, row_y, dg::all{}), rp);
            }
          }
        }
      }
    }

    template <size_t Degree_x, size_t Degree_y>
    template <class Q, class R, class F, class M>
    void rect<Degree_x, Degree_y>::source_jac(
      const Q& q, R&& r, F&& func, const M& mass_matrix) const
    {
      for (size_t col_x = 0; col_x < Degree_x + 1; ++col_x)
      {
        for (size_t col_y = 0; col_y < Degree_y + 1; ++col_y)
        {
          auto qp = q.subarray(col_x, col_y, dg::all{});
          for (size_t row_x = 0; row_x < Degree_x + 1; ++row_x)
          {
            for (size_t row_y = 0; row_y < Degree_y + 1; ++row_y)
            {
              func.source_jac(qp,
                mass_matrix(row_x * (Degree_y + 1) + row_y,
                  col_x * (Degree_y + 1) + col_y),
                r.subarray(dg::all{}, row_y, row_x, dg::all{}, col_y, col_x));
            }
          }
        }
      }
    }

    template <size_t Degree_x, size_t Degree_y>
    template <class Q, class R, class F, class M, class C>
    void rect<Degree_x, Degree_y>::source_jac(
      const Q& q, R&& r, F&& func, const M& mass_matrix, const C& coords) const
    {
      typedef typename std::decay_t<Q>::value_type value_type;
      std::array<value_type, 2> xi = {0, 0};
      std::array<value_type, 2> pos = {0, 0};
      auto xs = coords.subarray(dg::all{}, dg::all{}, 0);
      auto ys = coords.subarray(dg::all{}, dg::all{}, 1);

      static dg::spatial::elements::lagrange<value_type, Degree_x, Degree_y>
        hi_elem;
      static dg::spatial::elements::lagrange<value_type, 1, 1> flat_elem;
      
      for (size_t col_x = 0; col_x < Degree_x + 1; ++col_x)
      {
        xi[0] = hi_elem.absc[0][col_x];
        
        for (size_t col_y = 0; col_y < Degree_y + 1; ++col_y)
        {
          xi[1] = hi_elem.absc[1][col_y];
          pos[0] = flat_elem.interp(xi, xs);
          pos[1] = flat_elem.interp(xi, ys);
          
          auto qp = q.subarray(col_x, col_y, dg::all{});
          for (size_t row_x = 0; row_x < Degree_x + 1; ++row_x)
          {
            for (size_t row_y = 0; row_y < Degree_y + 1; ++row_y)
            {
              func.source_jac(qp,
                mass_matrix(row_x * (Degree_y + 1) + row_y,
                  col_x * (Degree_y + 1) + col_y),
                r.subarray(dg::all{}, row_y, row_x, dg::all{}, col_y, col_x),
                pos);
            }
          }
        }
      }
    }

    template <size_t Degree_x, size_t Degree_y>
    template <class Q, class R, class T, class L, class F>
    void rect<Degree_x, Degree_y>::int_flux(
      const Q& q, R&& r, const dg::view<T, 3, L>& advect_matrix, F&& func) const
    {
      for (size_t col_x = 0; col_x < Degree_x + 1; ++col_x)
      {
        for (size_t col_y = 0; col_y < Degree_y + 1; ++col_y)
        {
          // q order: node idx, comp
          auto qp = q.subarray(col_x, col_y, dg::all{});
          // rp order: comp, dim
          auto rp = func.int_flux(qp);
          for (size_t row_x = 0; row_x < Degree_x + 1; ++row_x)
          {
            for (size_t row_y = 0; row_y < Degree_y + 1; ++row_y)
            {
              detail::apply_all_comps_nd<std::decay_t<decltype(rp)>>::apply(
                [mults = advect_matrix.subarray(dg::all{},
                   row_x * (Degree_y + 1) + row_y,
                   col_x * (Degree_y + 1) + col_y)](auto&& dst, auto&& src) {
                  dst += mults[0] * src[0] + mults[1] * src[1];
                },
                r.subarray(row_x, row_y, dg::all{}), rp);
            }
          }
        }
      }
    }

    template <size_t Degree_x, size_t Degree_y>
    template <class Q, class M, class T, class L, class F>
    void rect<Degree_x, Degree_y>::int_flux_jac(const Q& q, M&& mat,
      const dg::view<T, 3, L>& advect_matrix, F&& func) const
    {
      for (size_t col_x = 0; col_x < Degree_x + 1; ++col_x)
      {
        for (size_t col_y = 0; col_y < Degree_y + 1; ++col_y)
        {
          auto qp = q.subarray(col_x, col_y, dg::all{});
          // TODO: update function?
          for (size_t row_x = 0; row_x < Degree_x + 1; ++row_x)
          {
            for (size_t row_y = 0; row_y < Degree_y + 1; ++row_y)
            {
              func.int_flux_jac(qp,
                advect_matrix.subarray(dg::all{},
                  row_x * (Degree_y + 1) + row_y,
                  col_x * (Degree_y + 1) + col_y),
                mat.subarray(dg::all{}, row_y, row_x, dg::all{}, col_y, col_x));
            }
          }
        }
      }
    }

    template <size_t Degree_x, size_t Degree_y>
    template <class Q, class R, class T, class L, class F>
    void rect<Degree_x, Degree_y>::int_nflux(
      const Q& q, R&& r, const dg::view<T, 3, L>& advect_matrix, F&& func) const
    {
      for (size_t col_x = 0; col_x < Degree_x + 1; ++col_x)
      {
        for (size_t col_y = 0; col_y < Degree_y + 1; ++col_y)
        {
          // q order: node idx, comp
          auto qj = q.subarray(col_x, col_y, dg::all{});
          for (size_t row_x = 0; row_x < Degree_x + 1; ++row_x)
          {
            for (size_t row_y = 0; row_y < Degree_y + 1; ++row_y)
            {
              auto qi = q.subarray(row_x, row_y, dg::all{});
              // rp order: comp, dim
              auto rp = func.int_nflux(qi, qj);
              detail::apply_all_comps_nd<std::decay_t<decltype(rp)>>::apply(
                [mults = advect_matrix.subarray(dg::all{},
                   row_x * (Degree_y + 1) + row_y,
                   col_x * (Degree_y + 1) + col_y)](auto&& dst, auto&& src) {
                  dst += mults[0] * src[0] + mults[1] * src[1];
                },
                r.subarray(row_x, row_y, dg::all{}), rp);
            }
          }
        }
      }
    }

    template <size_t Degree_x, size_t Degree_y>
    template <class Q, class R, class T, class L, class F, class C>
    void rect<Degree_x, Degree_y>::int_flux(const Q& q, R&& r,
      const dg::view<T, 3, L>& advect_matrix, F&& func, const C& coords) const
    {
      typedef typename std::decay_t<Q>::value_type value_type;
      std::array<value_type, 2> xi = {0, 0};
      std::array<value_type, 2> pos = {0, 0};
      auto xs = coords.subarray(dg::all{}, dg::all{}, 0);
      auto ys = coords.subarray(dg::all{}, dg::all{}, 1);
      static dg::spatial::elements::lagrange<value_type, Degree_x, Degree_y>
        hi_elem;
      static dg::spatial::elements::lagrange<value_type, 1, 1> flat_elem;
      for (size_t col_x = 0; col_x < Degree_x + 1; ++col_x)
      {
        xi[0] = hi_elem.absc[0][col_x];
        for (size_t col_y = 0; col_y < Degree_y + 1; ++col_y)
        {
          xi[1] = hi_elem.absc[1][col_y];
          pos[0] = flat_elem.interp(xi, xs);
          pos[1] = flat_elem.interp(xi, ys);

          auto qp = q.subarray(col_x, col_y, dg::all{});
          // rp order: comp, dim
          auto rp = func.int_flux(qp, pos);
          for (size_t row_x = 0; row_x < Degree_x + 1; ++row_x)
          {
            for (size_t row_y = 0; row_y < Degree_y + 1; ++row_y)
            {
              detail::apply_all_comps_nd<std::decay_t<decltype(rp)>>::apply(
                [mults = advect_matrix.subarray(dg::all{},
                   row_x * (Degree_y + 1) + row_y,
                   col_x * (Degree_y + 1) + col_y)](auto&& dst, auto&& src) {
                  dst += mults[0] * src[0] + mults[1] * src[1];
                },
                r.subarray(row_x, row_y, dg::all{}), rp);
            }
          }
        }
      }
    }

    template <size_t Degree_x, size_t Degree_y>
    template <class Q, class R, class T, class L, class F, class C>
    void rect<Degree_x, Degree_y>::int_nflux(const Q& q, R&& r,
      const dg::view<T, 3, L>& advect_matrix, F&& func, const C& coords) const
    {
      typedef typename std::decay_t<Q>::value_type value_type;
      std::array<value_type, 2> xi_j = {0, 0};
      std::array<value_type, 2> pos_j = {0, 0};
      std::array<value_type, 2> xi_i = {0, 0};
      std::array<value_type, 2> pos_i = {0, 0};
      auto xs = coords.subarray(dg::all{}, dg::all{}, 0);
      auto ys = coords.subarray(dg::all{}, dg::all{}, 1);
      static dg::spatial::elements::lagrange<value_type, Degree_x, Degree_y>
        hi_elem;
      static dg::spatial::elements::lagrange<value_type, 1, 1> flat_elem;

      for (size_t col_x = 0; col_x < Degree_x + 1; ++col_x)
      {
        xi_j[0] = hi_elem.absc[0][col_x];
        for (size_t col_y = 0; col_y < Degree_y + 1; ++col_y)
        {
          xi_j[1] = hi_elem.absc[1][col_y];
          pos_j[0] = flat_elem.interp(xi_j, xs);
          pos_j[1] = flat_elem.interp(xi_j, ys);
          auto qj = q.subarray(col_x, col_y, dg::all{});

          for (size_t row_x = 0; row_x < Degree_x + 1; ++row_x)
          {
            xi_i[0] = hi_elem.absc[0][row_x];
            for (size_t row_y = 0; row_y < Degree_y + 1; ++row_y)
            {
              xi_i[1] = hi_elem.absc[1][row_y];

              pos_i[0] = flat_elem.interp(xi_i, xs);
              pos_i[1] = flat_elem.interp(xi_i, ys);
              auto qi = q.subarray(row_x, row_y, dg::all{});

              auto rp = func.int_nflux(qi, qj, pos_i, pos_j);

              detail::apply_all_comps_nd<std::decay_t<decltype(rp)>>::apply(
                [mults = advect_matrix.subarray(dg::all{},
                   row_x * (Degree_y + 1) + row_y,
                   col_x * (Degree_y + 1) + col_y)](auto&& dst, auto&& src) {
                  dst += mults[0] * src[0] + mults[1] * src[1];
                },
                r.subarray(row_x, row_y, dg::all{}), rp);
            }
          }
        }
      }
    }

    template <size_t Degree_x, size_t Degree_y>
    template <class QOut, class QIn, class R, class N, class IOut, class IIn,
      class L, class F>
    void rect<Degree_x, Degree_y>::num_flux(const QOut& qout, const QIn& qin,
      R&& r, const N& normal, const IOut& idcs_out, const IIn& idcs_in,
      const L& lift, F&& fun) const
    {
      for (size_t fidx = 0; fidx < idcs_in.size(); ++fidx)
      {
        auto f = fun.num_flux(qout.subarray(idcs_out[fidx], dg::all{}),
          qin.subarray(idcs_in[fidx], dg::all{}), normal);
        for (size_t i_x = 0; i_x < (Degree_x + 1); ++i_x)
        {
          for (size_t i_y = 0; i_y < (Degree_y + 1); ++i_y)
          {
            detail::apply_all_comps<std::decay_t<decltype(f)>>::apply(
              [& mult = lift(i_y + i_x * (Degree_y + 1), fidx)](
                auto& dst, auto& src) { dst -= src * mult; },
              r.subarray(i_x, i_y, dg::all{}), f);
          }
        }
      }
    }

    template <size_t Degree_x, size_t Degree_y>
    template <class Lmbda, class QIn, class RL, class R, class N, class IOut,
      class IIn, class L, class F>
    void rect<Degree_x, Degree_y>::num_flux_hdg(const Lmbda& lmbda,
      const QIn& qin, RL&& rl, R&& r, N normal, const IOut& idcs_out,
      const IIn& idcs_in, const L& lift, F&& func) const
    {
      for (size_t f_idx = 0; f_idx < idcs_in.size(); ++f_idx)
      {
        // evaluate flux function
        auto f = func.num_flux(lmbda.subarray(idcs_out[f_idx], dg::all{}),
          qin.subarray(idcs_in[f_idx], dg::all{}), normal);
        for (size_t i = 0; i < idcs_in.size(); ++i)
        {
          detail::apply_all_comps<std::decay_t<decltype(f)>>::apply(
            [& mult = lift(f_idx, i)](
              auto& dst, auto& src) { dst -= src * mult; },
            r.subarray(idcs_in[i], dg::all{}), f);
          detail::apply_all_comps<std::decay_t<decltype(f)>>::apply(
            [& mult = lift(f_idx, i)](
              auto& dst, auto& src) { dst -= src * mult; },
            rl.subarray(idcs_out[i], dg::all{}), f);
        }
      }
    }

    template <size_t Degree_x, size_t Degree_y>
    template <class Lmbda, class QIn, class R, class N, class IOut, class IIn,
      class L, class F>
    void rect<Degree_x, Degree_y>::num_flux_local(const Lmbda& lmbda,
      const QIn& qin, R&& r, N normal, const IOut& idcs_out, const IIn& idcs_in,
      const L& lift, F&& func) const
    {
      for (size_t f_idx = 0; f_idx < idcs_in.size(); ++f_idx)
      {
        // evaluate flux function
        auto f = func.num_flux(lmbda.subarray(idcs_out[f_idx], dg::all{}),
          qin.subarray(idcs_in[f_idx], dg::all{}), normal);
        for (size_t i = 0; i < idcs_in.size(); ++i)
        {
          detail::apply_all_comps<std::decay_t<decltype(f)>>::apply(
            [& mult = lift(f_idx, i)](
              auto& dst, auto& src) { dst -= src * mult; },
            r.subarray(idcs_in[i], dg::all{}), f);
        }
      }
    }

    template <size_t Degree_x, size_t Degree_y>
    template <class Lmbda, class QIn, class RL, class N, class IOut, class IIn,
      class L, class F>
    void rect<Degree_x, Degree_y>::num_flux_global(const Lmbda& lmbda,
      const QIn& qin, RL&& rl, N normal, const IOut& idcs_out,
      const IIn& idcs_in, const L& lift, F&& func) const
    {
      for (size_t f_idx = 0; f_idx < idcs_in.size(); ++f_idx)
      {
        // evaluate flux function
        auto f = func.num_flux(lmbda.subarray(idcs_out[f_idx], dg::all{}),
          qin.subarray(idcs_in[f_idx], dg::all{}), normal);
        for (size_t i = 0; i < idcs_in.size(); ++i)
        {
          detail::apply_all_comps<std::decay_t<decltype(f)>>::apply(
            [& mult = lift(f_idx, i)](
              auto& dst, auto& src) { dst -= src * mult; },
            rl.subarray(idcs_out[i], dg::all{}), f);
        }
      }
    }

    template <size_t Degree_x, size_t Degree_y>
    template <class Lmbda, class QIn, class A, class B, class C, class D,
      class N, class IOut, class IIn, class L, class F>
    void rect<Degree_x, Degree_y>::num_flux_jac(const Lmbda& lmbda,
      const QIn& qin, A&& A_mat, B&& B_mat, C&& C_mat, D&& D_mat,
      const N& normal, const IOut& idcs_out, const IIn& idcs_in, const L& lift,
      F&& func)
    {
      for (size_t f_idx = 0; f_idx < idcs_in.size(); ++f_idx)
      {
        for (size_t i = 0; i < idcs_in.size(); ++i)
        {
          func.num_flux_jac(lmbda.subarray(idcs_out[f_idx], dg::all{}),
            qin.subarray(idcs_in[f_idx], dg::all{}), lift(f_idx, i),
            A_mat.subarray(dg::all{}, idcs_in[i], dg::all{}, idcs_in[f_idx]),
            B_mat.subarray(dg::all{}, idcs_in[i], dg::all{}, idcs_out[f_idx]),
            C_mat.subarray(idcs_out[f_idx], dg::all{}, idcs_out[i], dg::all{}),
            D_mat.subarray(idcs_out[f_idx], dg::all{}, idcs_out[i], dg::all{}),
            normal);
        }
      }
    }

    template <size_t Degree_x, size_t Degree_y>
    template <class Lmbda, class QIn, class A, class B, class N, class IOut,
      class IIn, class L, class F>
    void rect<Degree_x, Degree_y>::num_flux_local_jac(const Lmbda& lmbda,
      const QIn& qin, A&& A_mat, B&& B_mat, const N& normal,
      const IOut& idcs_out, const IIn& idcs_in, const L& lift, F&& func)
    {
      for (size_t f_idx = 0; f_idx < idcs_in.size(); ++f_idx)
      {
        for (size_t i = 0; i < idcs_in.size(); ++i)
        {
          func.num_flux_local_jac(lmbda.subarray(idcs_out[f_idx], dg::all{}),
            qin.subarray(idcs_in[f_idx], dg::all{}), lift(f_idx, i),
            A_mat.subarray(dg::all{}, idcs_in[i], dg::all{}, idcs_in[f_idx]),
            B_mat.subarray(dg::all{}, idcs_in[i], dg::all{}, idcs_out[f_idx]),
            normal);
        }
      }
    }

    template <size_t Degree_x, size_t Degree_y>
    template <class Lmbda, class QIn, class C, class D, class N, class IOut,
      class IIn, class L, class F>
    void rect<Degree_x, Degree_y>::num_flux_global_jac(const Lmbda& lmbda,
      const QIn& qin, C&& C_mat, D&& D_mat, const N& normal,
      const IOut& idcs_out, const IIn& idcs_in, const L& lift, F&& func)
    {
      for (size_t f_idx = 0; f_idx < idcs_in.size(); ++f_idx)
      {
        for (size_t i = 0; i < idcs_in.size(); ++i)
        {
          func.num_flux_global_jac(lmbda.subarray(idcs_out[f_idx], dg::all{}),
            qin.subarray(idcs_in[f_idx], dg::all{}), lift(f_idx, i),
            C_mat.subarray(idcs_out[f_idx], dg::all{}, idcs_out[i], dg::all{}),
            D_mat.subarray(idcs_out[f_idx], dg::all{}, idcs_out[i], dg::all{}),
            normal);
        }
      }
    }

    template <size_t Degree_x, size_t Degree_y>
    template <class Q, class R, class T, class L, class F, class Func>
    void rect<Degree_x, Degree_y>::num_nflux(const Q& q, R&& r,
      const dg::view<T, 1, L>& lift, const F& fluxes, Func&& func) const
    {
      // TODO: need to update to new lift matrix and num flux interface
      for (size_t i_x = 0; i_x < (Degree_x + 1); ++i_x)
      {
        for (size_t i_y = 0; i_y < (Degree_y + 1); ++i_y)
        {
          auto res = func.num_nflux(q.subarray(i_x, i_y, dg::all{}), fluxes);
          detail::apply_all_comps<std::decay_t<F>>::apply(
            [& mult = lift[i_y + i_x * (Degree_y + 1)]](
              auto& dst, auto& src) { dst -= src * mult; },
            r.subarray(i_x, i_y, dg::all{}), res);
        }
      }
    }

    template <size_t Degree_x, size_t Degree_y>
    template <class Q, class R, class T, class L, class F, class Func, class C>
    void rect<Degree_x, Degree_y>::num_nflux(const Q& q, R&& r,
      const dg::view<T, 1, L>& lift, const F& fluxes, Func&& func,
      const C& coords) const
    {
      // TODO: need to update to new lift matrix and num flux interface
      typedef typename std::decay_t<Q>::value_type value_type;
      std::array<value_type, 2> xi_i = {0, 0};
      std::array<value_type, 2> pos_i = {0, 0};
      auto xs = coords.subarray(dg::all{}, dg::all{}, 0);
      auto ys = coords.subarray(dg::all{}, dg::all{}, 1);
      static dg::spatial::elements::lagrange<value_type, Degree_x, Degree_y>
        hi_elem;
      static dg::spatial::elements::lagrange<value_type, 1, 1> flat_elem;

      for (size_t row_x = 0; row_x < Degree_x + 1; ++row_x)
      {
        xi_i[0] = hi_elem.absc[0][row_x];
        for (size_t row_y = 0; row_y < Degree_y + 1; ++row_y)
        {
          xi_i[1] = hi_elem.absc[1][row_y];
          pos_i[0] = flat_elem.interp(xi_i, xs);
          pos_i[1] = flat_elem.interp(xi_i, ys);
          auto res =
            func.num_nflux(q.subarray(row_x, row_y, dg::all{}), fluxes, pos_i);
          detail::apply_all_comps<std::decay_t<F>>::apply(
            [& mult = lift[row_y + row_x * (Degree_y + 1)]](
              auto& dst, auto& src) { dst -= src * mult; },
            r.subarray(row_x, row_y, dg::all{}), res);
        }
      }
    }
  } // namespace ndg
} // namespace dg
#endif
