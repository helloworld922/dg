#ifndef DG_NDG_RECT_1D_TCC
#define DG_NDG_RECT_1D_TCC

#include "dg/utils/view.hpp"

namespace dg
{
  namespace ndg
  {
    template <size_t Degree>
    template <class V>
    void rect<Degree>::calc_mass_matrix(
      V&& dst, typename std::decay_t<V>::value_type jac) const
    {
      typedef typename std::decay_t<V>::value_type real_type;
      for (size_t i = 0; i <= Degree; ++i)
      {
        for (size_t j = 0; j <= Degree; ++j)
        {
          dst(i, j) = static_cast<real_type>(integrator.vol_integral(
            [this, i, j, jac](const std::array<dg::real256, 1>& xi) {
              return element(xi, i) * element(xi, j) * jac;
            }));
        }
      }
    }

    template <size_t Degree>
    template <class V>
    void rect<Degree>::calc_advect_matrix(V&& dst) const
    {
      typedef typename std::decay_t<V>::value_type real_type;
      for (size_t i = 0; i <= Degree; ++i)
      {
        for (size_t j = 0; j <= Degree; ++j)
        {
          dst(i, j) = static_cast<real_type>(integrator.vol_integral(
            [this, i, j](const std::array<dg::real256, 1>& xi) {
              return element(xi, j) * element.template deriv<0>(xi, i);
            }));
        }
      }
    }

    template <size_t Degree>
    template <class V, class M, class R>
    void rect<Degree>::calc_lift_matrix(
      V&& dst, const M& imass, R area0, R area1) const
    {
      for (size_t i = 0; i <= Degree; ++i)
      {
        dst[0](0, i) = area0 * imass(i, 0);
      }
      for (size_t i = 0; i <= Degree; ++i)
      {
        dst[1](0, i) = area1 * imass(i, Degree);
      }
    }

    template <size_t Degree>
    template <class V, class R>
    void rect<Degree>::calc_lift_matrix(V&& dst, R area0, R area1) const
    {
      dst[0](0, 0) = area0;
      dst[1](0, 0) = area1;
    }

    template <size_t Degree>
    template <class Q, class R, class F>
    void rect<Degree>::source(const Q& q, R&& r, F&& func) const
    {
      // compute r += func(q)
      for (size_t i = 0; i < Degree + 1; ++i)
      {
        auto qp = q.subarray(i, dg::all{});
        auto rp = func.source(qp);
        detail::apply_all_comps<std::decay_t<decltype(rp)>>::apply(
          [](auto& dst, auto& src) { dst += src; }, r.subarray(i, dg::all{}),
          rp);
      }
    }

    template <size_t Degree>
    template <class Q, class R, class F, class M>
    void rect<Degree>::source(
      const Q& q, R&& r, F&& func, const M& mass_matrix) const
    {
      // compute r += func(q)
      for (size_t col = 0; col < Degree + 1; ++col)
      {
        auto qp = q.subarray(col, dg::all{});
        auto rp = func.source(qp);
        for (size_t row = 0; row < Degree + 1; ++row)
        {
          detail::apply_all_comps<std::decay_t<decltype(rp)>>::apply(
            [& mult = mass_matrix(row, col)](
              auto& dst, auto& src) { dst += src * mult; },
            r.subarray(row, dg::all{}), rp);
        }
      }
    }

    template <size_t Degree>
    template <class Q, class R, class F, class M>
    void rect<Degree>::source_jac(
      const Q& q, R&& r, F&& func, const M& mass_matrix) const
    {
      for (size_t col = 0; col < Degree + 1; ++col)
      {
        auto qp = q.subarray(col, dg::all{});
        for (size_t row = 0; row < Degree + 1; ++row)
        {
          func.source_jac(qp, mass_matrix(row, col),
            r.subarray(dg::all{}, row, dg::all{}, col));
        }
      }
    }

    template <size_t Degree>
    template <class Q, class R, class F, class T>
    void rect<Degree>::source(const Q& q, R&& r, F&& func, T x0, T x1) const
    {
      // compute r += func(q, x)
      constexpr lobatto<double> quad{};
      static auto absc = quad.abscissas(Degree + 1);
      auto jac = T{0.5} * (x1 - x0);

      for (size_t i = 0; i < Degree + 1; ++i)
      {
        auto qp = q.subarray(i, dg::all{});
        auto rp = func.source(qp, x0 + jac * (absc[i] + 1));
        detail::apply_all_comps<std::decay_t<decltype(rp)>>::apply(
          [](auto& dst, auto& src) { dst += src; }, r.subarray(i, dg::all{}),
          rp);
      }
    }

    template <size_t Degree>
    template <class Q, class R, class F, class M, class T>
    void rect<Degree>::source(
      const Q& q, R&& r, F&& func, const M& mass_matrix, T x0, T x1) const
    {
      // compute r += func(q, x)
      constexpr lobatto<double> quad{};
      static auto absc = quad.abscissas(Degree + 1);
      auto jac = T{0.5} * (x1 - x0);

      for (size_t col = 0; col < Degree + 1; ++col)
      {
        auto qp = q.subarray(col, dg::all{});
        auto rp = func.source(qp, x0 + jac * (absc[col] + 1));
        for (size_t row = 0; row < Degree + 1; ++row)
        {
          detail::apply_all_comps<std::decay_t<decltype(rp)>>::apply(
            [& mult = mass_matrix(row, col)](
              auto& dst, auto& src) { dst += src * mult; },
            r.subarray(row, dg::all{}), rp);
        }
      }
    }

    template <size_t Degree>
    template <class Q, class R, class F, class M, class T>
    void rect<Degree>::source_jac(
      const Q& q, R&& r, F&& func, const M& mass_matrix, T x0, T x1) const
    {
      constexpr lobatto<double> quad{};
      static auto absc = quad.abscissas(Degree + 1);
      auto jac = T{0.5} * (x1 - x0);

      for (size_t col = 0; col < Degree + 1; ++col)
      {
        auto qp = q.subarray(col, dg::all{});
        auto x = x0 + jac * (absc[col] + 1);
        auto rp = func.source(qp, x);
        for (size_t row = 0; row < Degree + 1; ++row)
        {
          func.source_jac(qp, x, mass_matrix(row, col),
            r.subarray(dg::all{}, row, dg::all{}, col));
        }
      }
    }

    template <size_t Degree>
    template <class Q, class R, class T, class L, class F>
    void rect<Degree>::int_flux(
      const Q& q, R&& r, const dg::view<T, 2, L>& advect_matrix, F&& func) const
    {
      // compute r += S * func(q)
      for (size_t col = 0; col < Degree + 1; ++col)
      {
        auto qp = q.subarray(col, dg::all{});
        auto rp = func.int_flux(qp);
        for (size_t row = 0; row < Degree + 1; ++row)
        {
          detail::apply_all_comps<std::decay_t<decltype(rp)>>::apply(
            [& mult = advect_matrix(row, col)](
              auto& dst, auto& src) { dst += src * mult; },
            r.subarray(row, dg::all{}), rp);
        }
      }
    }

    template <size_t Degree>
    template <class Q, class R, class T, class L, class F>
    void rect<Degree>::int_nflux(
      const Q& q, R&& r, const dg::view<T, 2, L>& advect_matrix, F&& func) const
    {
      // compute r += S * func(q)
      for (size_t col = 0; col < Degree + 1; ++col)
      {
        auto qj = q.subarray(col, dg::all{});
        for (size_t row = 0; row < Degree + 1; ++row)
        {
          auto qi = q.subarray(row, dg::all{});
          auto rp = func.int_nflux(qi, qj);
          detail::apply_all_comps<std::decay_t<decltype(rp)>>::apply(
            [& mult = advect_matrix(row, col)](
              auto& dst, auto& src) { dst += src * mult; },
            r.subarray(row, dg::all{}), rp);
        }
      }
    }

    template <size_t Degree>
    template <class Q, class R, class T, class L, class F>
    void rect<Degree>::int_flux(const Q& q, R&& r,
      const dg::view<T, 2, L>& advect_matrix, F&& func, T x0, T x1) const
    {
      constexpr lobatto<double> quad{};
      static auto absc = quad.abscissas(Degree + 1);
      auto jac = T{0.5} * (x1 - x0);
      // compute r += S * func(q)
      for (size_t col = 0; col < Degree + 1; ++col)
      {
        auto qp = q.subarray(col, dg::all{});
        auto rp = func.int_flux(qp, x0 + jac * (absc[col] + 1));
        for (size_t row = 0; row < Degree + 1; ++row)
        {
          detail::apply_all_comps<std::decay_t<decltype(rp)>>::apply(
            [& mult = advect_matrix(row, col)](
              auto& dst, auto& src) { dst += src * mult; },
            r.subarray(row, dg::all{}), rp);
        }
      }
    }

    template <size_t Degree>
    template <class Q, class M, class T, class L, class F>
    void rect<Degree>::int_flux_jac(const Q& q, M&& mat,
      const dg::view<T, 2, L>& advect_matrix, F&& func) const
    {
      for (size_t col = 0; col < Degree + 1; ++col)
      {
        auto qp = q.subarray(col, dg::all{});
        // TODO: update function?
        for (size_t row = 0; row < Degree + 1; ++row)
        {
          // eqsets is responsible for handling all signs
          func.int_flux_jac(qp, advect_matrix(row, col),
            mat.subarray(dg::all{}, row, dg::all{}, col));
        }
      }
    }

    template <size_t Degree>
    template <class Q, class R, class T, class L, class F>
    void rect<Degree>::int_nflux(const Q& q, R&& r,
      const dg::view<T, 2, L>& advect_matrix, F&& func, T x0, T x1) const
    {
      constexpr lobatto<double> quad{};
      static auto absc = quad.abscissas(Degree + 1);
      auto jac = T{0.5} * (x1 - x0);
      // compute r += S * func(q)
      for (size_t col = 0; col < Degree + 1; ++col)
      {
        auto qj = q.subarray(col, dg::all{});
        auto xj = x0 + jac * (absc[col] + 1);
        for (size_t row = 0; row < Degree + 1; ++row)
        {
          auto qi = q.subarray(row, dg::all{});
          auto rp = func.int_nflux(qi, qj, x0 + jac * (absc[row] + 1), xj);
          detail::apply_all_comps<std::decay_t<decltype(rp)>>::apply(
            [& mult = advect_matrix(row, col)](
              auto& dst, auto& src) { dst += src * mult; },
            r.subarray(row, dg::all{}), rp);
        }
      }
    }

    template <size_t Degree>
    template <class QOut, class QIn, class R, class N, class IOut, class IIn,
      class L, class F>
    void rect<Degree>::num_flux(const QOut& qout, const QIn& qin, R&& r,
      N normal, const IOut& idcs_out, const IIn& idcs_in, const L& lift,
      F&& func) const
    {
      auto f = func.num_flux(qout.subarray(idcs_out[0], dg::all{}),
        qin.subarray(idcs_in[0], dg::all{}), normal);
      for (size_t i = 0; i < Degree + 1; ++i)
      {
        detail::apply_all_comps<std::decay_t<decltype(f)>>::apply(
          [& mult = lift(0, i)](auto& dst, auto& src) { dst -= src * mult; },
          r.subarray(i, dg::all{}), f);
      }
    }

    template <size_t Degree>
    template <class QOut, class QIn, class R, class N, class IOut, class IIn,
      class L, class F, class T>
    void rect<Degree>::num_flux(const QOut& qout, const QIn& qin, R&& r,
      N normal, const IOut& idcs_out, const IIn& idcs_in, const L& lift,
      F&& func, T x0, T x1) const
    {
      constexpr lobatto<double> quad{};
      static auto absc = quad.abscissas(Degree + 1);
      auto jac = T{0.5} * (x1 - x0);
      auto f = func.num_flux(qout.subarray(idcs_out[0], dg::all{}),
        qin.subarray(idcs_in[0], dg::all{}), normal,
        x0 + jac * (absc[idcs_in[0]] + 1));
      for (size_t i = 0; i < Degree + 1; ++i)
      {
        detail::apply_all_comps<std::decay_t<decltype(f)>>::apply(
          [& mult = lift(0, i)](auto& dst, auto& src) { dst -= src * mult; },
          r.subarray(i, dg::all{}), f);
      }
    }

    template <size_t Degree>
    template <class Lmbda, class QIn, class RL, class R, class N, class IOut,
      class IIn, class L, class F>
    void rect<Degree>::num_flux_hdg(const Lmbda& lmbda, const QIn& qin, RL&& rl,
      R&& r, N normal, const IOut& idcs_out, const IIn& idcs_in, const L& lift,
      F&& func) const
    {
#if 0
      // technically need these loops, but happen to know that lift is 1x1
      for (size_t f_idx = 0; f_idx < idcs_in.size(); ++f_idx)
      {
        // evaluate flux function
        auto f = func.num_flux(lmbda.subarray(idcs_out[f_idx], dg::all{}),
          qin.subarray(idcs_in[f_idx], dg::all{}), normal);
        for (size_t i = 0; i < idcs_in.size(); ++i)
        {
          detail::apply_all_comps<std::decay_t<decltype(f)>>::apply(
            [& mult = lift(f_idx, i)](auto& dst, auto& src) { dst -= src * mult; },
            r.subarray(idcs_in[i], dg::all{}), f);
          detail::apply_all_comps<std::decay_t<decltype(f)>>::apply(
            [& mult = lift(f_idx, i)](auto& dst, auto& src) { dst -= src * mult; },
            rl.subarray(idcs_out[i], dg::all{}), f);
        }
      }
#else
      // evaluate flux function
      auto f = func.num_flux(lmbda.subarray(idcs_out[0], dg::all{}),
        qin.subarray(idcs_in[0], dg::all{}), normal);
      detail::apply_all_comps<std::decay_t<decltype(f)>>::apply(
        [& mult = lift(0, 0)](auto& dst, auto& src) { dst -= src * mult; },
        r.subarray(idcs_in[0], dg::all{}), f);
      detail::apply_all_comps<std::decay_t<decltype(f)>>::apply(
        [& mult = lift(0, 0)](auto& dst, auto& src) { dst -= src * mult; },
        rl.subarray(idcs_out[0], dg::all{}), f);
#endif
    }

    template <size_t Degree>
    template <class Lmbda, class QIn, class R, class N, class IOut, class IIn,
      class L, class F>
    void rect<Degree>::num_flux_local(const Lmbda& lmbda, const QIn& qin, R&& r,
      N normal, const IOut& idcs_out, const IIn& idcs_in, const L& lift,
      F&& func) const
    {
#if 0
      // technically need these loops, but happen to know that lift is 1x1
      for (size_t f_idx = 0; f_idx < idcs_in.size(); ++i)
      {
        // evaluate flux function
        auto f = func.num_flux(lmbda.subarray(idcs_out[f_idx], dg::all{}),
          qin.subarray(idcs_in[f_idx], dg::all{}), normal);
        for (size_t i = 0; i < idcs_in.size(); ++i)
        {
          detail::apply_all_comps<std::decay_t<decltype(f)>>::apply(
            [& mult = lift(f_idx, i)](auto& dst, auto& src) { dst -= src * mult; },
            r.subarray(idcs_in[i], dg::all{}), f);
        }
      }
#else
      // evaluate flux function
      auto f = func.num_flux(lmbda.subarray(idcs_out[0], dg::all{}),
        qin.subarray(idcs_in[0], dg::all{}), normal);
      detail::apply_all_comps<std::decay_t<decltype(f)>>::apply(
        [& mult = lift(0, 0)](auto& dst, auto& src) { dst -= src * mult; },
        r.subarray(idcs_in[0], dg::all{}), f);
#endif
    }

    template <size_t Degree>
    template <class Lmbda, class QIn, class RL, class N, class IOut, class IIn,
      class L, class F>
    void rect<Degree>::num_flux_global(const Lmbda& lmbda, const QIn& qin,
      RL&& rl, N normal, const IOut& idcs_out, const IIn& idcs_in,
      const L& lift, F&& func) const
    {
#if 0
      // technically need these loops, but happen to know that lift is 1x1
      for (size_t f_idx = 0; f_idx < idcs_in.size(); ++i)
      {
        // evaluate flux function
        auto f = func.num_flux(lmbda.subarray(idcs_out[f_idx], dg::all{}),
          qin.subarray(idcs_in[f_idx], dg::all{}), normal);
        for (size_t i = 0; i < idcs_in.size(); ++i)
        {
          detail::apply_all_comps<std::decay_t<decltype(f)>>::apply(
            [& mult = lift(f_idx, i)](auto& dst, auto& src) { dst -= src * mult; },
            rl.subarray(idcs_out[i], dg::all{}), f);
        }
      }
#else
      // evaluate flux function
      auto f = func.num_flux(lmbda.subarray(idcs_out[0], dg::all{}),
        qin.subarray(idcs_in[0], dg::all{}), normal);
      detail::apply_all_comps<std::decay_t<decltype(f)>>::apply(
        [& mult = lift(0, 0)](auto& dst, auto& src) { dst -= src * mult; },
        rl.subarray(idcs_out[0], dg::all{}), f);
#endif
    }

    template <size_t Degree>
    template <class Lmbda, class QIn, class A, class B, class C, class D,
      class N, class IOut, class IIn, class L, class F>
    void rect<Degree>::num_flux_jac(const Lmbda& lmbda, const QIn& qin,
      A&& A_mat, B&& B_mat, C&& C_mat, D&& D_mat, N normal,
      const IOut& idcs_out, const IIn& idcs_in, const L& lift, F&& func)
    {
#if 0
      // technically need these loops, but happen to know that lift is 1x1
      for (size_t f_idx = 0; f_idx < idcs_in.size(); ++f_idx)
      {
        for (size_t i = 0; i < idcs_in.size(); ++i)
        {
          func.num_flux_jac(lmbda.subarray(idcs_out[f_idx], dg::all{}),
            qin.subarray(idcs_in[f_idx], dg::all{}), lift(f_idx, i),
            A_mat.subarray(dg::all{}, idcs_in[i], dg::all{}, idcs_in[f_idx]),
            B_mat.subarray(dg::all{}, idcs_in[i], dg::all{}, idcs_out[f_idx]),
            C_mat.subarray(idcs_out[f_idx], dg::all{}, idcs_out[i],dg::all{}),
            D_mat.subarray(idcs_out[f_idx], dg::all{}, idcs_out[i],dg::all{}),
            normal);
        }
      }
#else
      func.num_flux_jac(lmbda.subarray(idcs_out[0], dg::all{}),
        qin.subarray(idcs_in[0], dg::all{}), lift(0, 0),
        A_mat.subarray(dg::all{}, idcs_in[0], dg::all{}, idcs_in[0]),
        B_mat.subarray(dg::all{}, idcs_in[0], dg::all{}, idcs_out[0]),
        C_mat.subarray(idcs_out[0], dg::all{}, idcs_out[0], dg::all{}),
        D_mat.subarray(idcs_out[0], dg::all{}, idcs_out[0], dg::all{}), normal);
#endif
    }

    template <size_t Degree>
    template <class Lmbda, class QIn, class A, class B, class N, class IOut,
      class IIn, class L, class F>
    void rect<Degree>::num_flux_local_jac(const Lmbda& lmbda, const QIn& qin,
      A&& A_mat, B&& B_mat, N normal, const IOut& idcs_out, const IIn& idcs_in,
      const L& lift, F&& func)
    {
#if 0
      // technically need these loops, but happen to know that lift is 1x1
      for (size_t f_idx = 0; f_idx < idcs_in.size(); ++f_idx)
      {
        for (size_t i = 0; i < idcs_in.size(); ++i)
        {
          func.num_flux_local_jac(lmbda.subarray(idcs_out[f_idx], dg::all{}),
            qin.subarray(idcs_in[f_idx], dg::all{}), lift(f_idx, i),
            A_mat.subarray(dg::all{}, idcs_in[i], dg::all{}, idcs_in[f_idx]),
            B_mat.subarray(dg::all{}, idcs_in[i], dg::all{}, idcs_out[f_idx]),
            normal);
        }
      }
#else
      func.num_flux_local_jac(lmbda.subarray(idcs_out[0], dg::all{}),
        qin.subarray(idcs_in[0], dg::all{}), lift(0, 0),
        A_mat.subarray(dg::all{}, idcs_in[0], dg::all{}, idcs_in[0]),
        B_mat.subarray(dg::all{}, idcs_in[0], dg::all{}, idcs_out[0]), normal);
#endif
    }

    template <size_t Degree>
    template <class Lmbda, class QIn, class C, class D, class N, class IOut,
      class IIn, class L, class F>
    void rect<Degree>::num_flux_global_jac(const Lmbda& lmbda, const QIn& qin,
      C&& C_mat, D&& D_mat, N normal, const IOut& idcs_out, const IIn& idcs_in,
      const L& lift, F&& func)
    {
#if 0
      // technically need these loops, but happen to know that lift is 1x1
      for (size_t f_idx = 0; f_idx < lift.extent(0); ++f_idx)
      {
        for (size_t i = 0; i < lift.extent(1); ++i)
        {
          func.num_flux_global_jac(lmbda.subarray(idcs_out[f_idx], dg::all{}),
            qin.subarray(idcs_in[f_idx], dg::all{}), lift(f_idx, i),
            C_mat.subarray(idcs_out[f_idx], dg::all{}, idcs_out[i], dg::all{}),
            D_mat.subarray(idcs_out[f_idx], dg::all{}, idcs_out[i], dg::all{}),
            normal);
        }
      }
#else
      func.num_flux_global_jac(lmbda.subarray(idcs_out[0], dg::all{}),
        qin.subarray(idcs_in[0], dg::all{}), lift(0, 0),
        C_mat.subarray(idcs_out[0], dg::all{}, idcs_out[0], dg::all{}),
        D_mat.subarray(idcs_out[0], dg::all{}, idcs_out[0], dg::all{}), normal);
#endif
    }

    template <size_t Degree>
    template <class QOut, class QIn, class R, class N, class IOut, class IIn,
      class L, class F>
    void rect<Degree>::num_nflux(const QOut& qout, const QIn& qin, R&& r,
      N normal, const IOut& idcs_out, const IIn& idcs_in, const L& lift,
      F&& func) const
    {
      auto f = func.num_flux(qout.subarray(idcs_out[0], dg::all{}),
        qin.subarray(idcs_in[0], dg::all{}), normal);
      for (size_t i = 0; i < Degree + 1; ++i)
      {
        auto res = func.num_nflux(qin.subarray(i, dg::all{}), f);
        detail::apply_all_comps<std::decay_t<decltype(res)>>::apply(
          [& mult = lift(0, i)](auto& dst, auto& src) { dst -= src * mult; },
          r.subarray(i, dg::all{}), res);
      }
    }

    template <size_t Degree>
    template <class QOut, class QIn, class R, class N, class IOut, class IIn,
      class L, class F, class T>
    void rect<Degree>::num_nflux(const QOut& qout, const QIn& qin, R&& r,
      N normal, const IOut& idcs_out, const IIn& idcs_in, const L& lift,
      F&& func, T x0, T x1) const
    {
      constexpr lobatto<double> quad{};
      static auto absc = quad.abscissas(Degree + 1);
      auto jac = T{0.5} * (x1 - x0);
      auto f = func.num_flux(qout.subarray(idcs_out[0], dg::all{}),
        qin.subarray(idcs_in[0], dg::all{}), normal,
        x0 + jac * (absc[idcs_in[0]] + 1));
      for (size_t i = 0; i < Degree + 1; ++i)
      {
        auto res = func.num_nflux(
          qin.subarray(i, dg::all{}), f, x0 + jac * (absc[i] + 1));
        detail::apply_all_comps<std::decay_t<decltype(res)>>::apply(
          [& mult = lift(0, i)](auto& dst, auto& src) { dst -= src * mult; },
          r.subarray(i, dg::all{}), res);
      }
    }
  } // namespace ndg
} // namespace dg

#endif
