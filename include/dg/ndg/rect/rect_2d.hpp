#ifndef DG_NDG_RECT_2D_HPP
#define DG_NDG_RECT_2D_HPP

#include "rect.hpp"

#include "dg/spatial/elements/lagrange.hpp"

#include <Eigen/Dense>
#include <array>

namespace dg
{
  namespace ndg
  {
    namespace detail
    {
      struct rect_2d_base
      {
        static dg::spatial::elements::lagrange<dg::real256, 1, 1>&
        flat_element();
      };
    } // namespace detail

    ///
    /// @brief 2D nodal DG, linear coordinate mapping
    ///
    template <size_t Degree_x, size_t Degree_y>
    struct rect<Degree_x, Degree_y> : private detail::rect_2d_base
    {
      ///
      /// @brief
      ///
      constexpr static size_t degree_x()
      {
        return Degree_x;
      }

      ///
      /// @brief
      ///
      constexpr static size_t degree_y()
      {
        return Degree_y;
      }

      dg::spatial::elements::lagrange<dg::real256, Degree_x, Degree_y> element;

      // needs to be 2 orders higher to integrate Jacobian linear terms?
      dg::spatial::elements::lagrange<dg::real256, Degree_x + 3, Degree_y + 3>
        integrator;

      rect();

      ///
      /// @brief computes the inverse mass matrix. Helper function for
      /// calculating the advection matrix/lift matrix.
      /// @param coords 4 vertex locations defining the element.
      /// @tparam R 3d view type. First dim is node row, second dim is node col,
      /// third dim is dimension (x, y)
      /// @return Inverse mass matrix (flattened to 2D).
      ///
      template <class V, class R>
      void calc_mass_matrix(V&& dst, const R& coords) const;

      ///
      /// @brief computes the inverse mass matrix. Helper function for
      /// calculating the advection matrix/lift matrix.
      /// @param coords 4 vertex locations defining the element.
      /// @tparam R 3d view type. First dim is node row, second dim is node col,
      /// third dim is dimension (x, y)
      /// @return Inverse mass matrix (flattened to 2D).
      ///
      template <class R>
      Eigen::Matrix<dg::real256, (Degree_x + 1) * (Degree_y + 1),
        (Degree_x + 1) * (Degree_y + 1)>
      calc_imass_matrix(const R& coords) const;

      ///
      /// @brief computes \f$\mathbf{M}^-1 \mathbf{S}\f$ and stores the result
      /// into dst.
      /// @param imass pre-computed inverse mass matrix (see calc_imass_matrix)
      /// @param dst output. View index order: (dim, basis idx, basis idx).
      /// @param coords 4 vertex locations defining the element.
      /// @tparam V 3d view type. Note: for performance reasons, a left layout
      /// is better for cache locality.
      /// @tparam R 3d view type. First dim is node row, second dim is node col,
      /// third dim is dimension (x, y)
      ///
      template <class V, class M, class R>
      void calc_advect_matrix(V&& dst, const M& imass, const R& coords) const;

      ///
      /// @brief computes \f$\mathbf{S}\f$ and stores the result into dst.
      /// @param dst output. View index order: (dim, basis idx, basis idx).
      /// @param coords 4 vertex locations defining the element.
      /// @tparam V 3d view type. Note: for performance reasons, a left layout
      /// is better for cache locality.
      /// @tparam R 3d view type. First dim is node row, second dim is node col,
      /// third dim is dimension (x, y)
      ///
      template <class V, class R>
      void calc_advect_matrix(V&& dst, const R& coords) const;

      ///
      /// @brief computes the lift matrix multiplied by the inverse mass matrix
      /// \f$\boldsymbol{M}^{-1} \boldsymbol{L}\f$ and stores the result into
      /// dst.
      /// @param imass pre-computed inverse mass matrix (see calc_imass_matrix)
      /// @param dst output. View index order: (node index, face node index)
      /// @param coords 4 vertex locations defining the element.
      /// @tparam V 2d view type.
      /// @tparam R view type. First dim is node row, second dim is node col,
      /// third dim is dimension (x,y)
      ///
      template <class V, class M, class R>
      void calc_lift_matrix(V&& dst, const M& imass, const R& coords) const;

      ///
      /// @brief Computes \f$\boldsymbol{L}\f$ and stores the result into dst.
      /// @param dst output.
      /// @param coords 4 vertex locations defining the element.
      /// @tparam V A view_sequence containing 4 sub-views. All sub-views should
      /// be 2D views, where the first dim is the x index and second dim is y
      /// index. Order of faces: left, right, bottom, top. TODO: recommendations
      /// for layout type?
      /// @tparam R 3d view type. First dim is node row, second dim is node col,
      /// third dim is dimension (x, y)
      ///
      template <class V, class R>
      void calc_lift_matrix(V&& dst, const R& coords) const;

      ///
      /// @brief Implements a source term, evaluating only along the diagonals
      /// (assuming identity mass matrix, or assuming Eq. has been multiplied by
      /// inverse mass matrix already).
      /// r += func(q)
      ///
      /// @param q
      /// @param r
      /// @param func
      /// @tparam Q
      /// @tparam R
      /// @tparam F
      ///
      template <class Q, class R, class F>
      void source(const Q& q, R&& r, F&& func) const;

      ///
      /// @brief Implements a source term, evaluating only along the diagonals
      /// (assuming identity mass matrix, or assuming Eq. has been multiplied by
      /// inverse mass matrix already).
      /// r += func(q, x)
      ///
      /// @param q
      /// @param r
      /// @param func
      /// @param coords
      /// @tparam Q
      /// @tparam R
      /// @tparam F
      /// @tparam C
      ///
      template <class Q, class R, class F, class C>
      void source(const Q& q, R&& r, F&& func, const C& coords) const;

      ///
      /// @brief Implements a source term
      /// r += M*func(q)
      ///
      /// @param q
      /// @param r
      /// @param func
      /// @param mass_matrix
      /// @tparam Q
      /// @tparam R
      /// @tparam F
      /// @tparam M
      ///
      template <class Q, class R, class F, class M>
      void msource(const Q& q, R&& r, F&& func, const M& mass_matrix) const;

      ///
      /// @brief Implements a source term
      /// r += M*func(q, x)
      ///
      /// @param q
      /// @param r
      /// @param func
      /// @param mass_matrix
      /// @param coords
      /// @tparam Q
      /// @tparam R
      /// @tparam F
      /// @tparam M
      /// @tparam C
      ///
      template <class Q, class R, class F, class M, class C>
      void msource(const Q& q, R&& r, F&& func, const M& mass_matrix,
        const C& coords) const;

      ///
      /// @brief Implements a source term Jacobian
      ///
      /// @param q
      /// @param r where to store result
      /// @param func
      /// @param mass_matrix
      /// @tparam Q
      /// @tparam R 4D view. A left layout is preferred.
      /// Dimension order: row component of q, row y node index, row x node
      /// index, column component of q, column y node index, column x node index
      /// @tparam F
      /// @tparam M
      ///
      template <class Q, class R, class F, class M>
      void source_jac(const Q& q, R&& r, F&& func, const M& mass_matrix) const;

      ///
      /// @brief Implements a source term Jacobian
      ///
      /// @param q
      /// @param r where to store result
      /// @param func
      /// @param mass_matrix
      /// @param coords
      /// @tparam Q
      /// @tparam R 4D view. A left layout is preferred.
      /// Dimension order: row component of q, row y node index, row x node
      /// index, column component of q, column y node index, column x node index
      /// @tparam F
      /// @tparam M
      /// @tparam C
      ///
      template <class Q, class R, class F, class M, class C>
      void source_jac(const Q& q, R&& r, F&& func, const M& mass_matrix, const C& coords) const;

      ///
      /// @brief Compute the volume integral term for internal fluxes.
      /// r += S * func(q)
      /// @param q view of the variables for a single element
      /// @param r view of the RHS storage for a single element
      /// @param advect_matrix advection matrix S
      /// @param func internal flux function. Takes a single subarray of q as a
      /// function argument
      /// @tparam Q 3D view or view_sequence: 1st dim is num nodes, second dim
      /// is each component of q
      /// @tparam R 3D view or view_sequence: 1st dim is num nodes, second dim
      /// is each component of q
      /// @tparam A 3D view
      /// @tparam F callable
      ///
      template <class Q, class R, class T, class L, class F>
      void int_flux(const Q& q, R&& r, const dg::view<T, 3, L>& advect_matrix,
        F&& func) const;

      ///
      /// @brief Computes the Jacobian for internal fluxes.
      /// @param q
      /// @param mat where to store result
      /// @param advect_matrix
      /// @param func
      /// @tparam Q
      /// @tparam M 4D view. A left layout is preferred.
      /// Dimension order: row component of q, row y node index, row x node
      /// index, column component of q, column y node index, column x node index
      /// @tparam advect_matrix
      /// @tparam F
      ///
      template <class Q, class M, class T, class L, class F>
      void int_flux_jac(const Q& q, M&& mat,
        const dg::view<T, 3, L>& advect_matrix, F&& func) const;

      ///
      /// @brief Compute the volume integral term for internal fluxes in
      /// non-conservation form.
      /// r += S * func(q_i, q_j)
      /// @param q view of the variables for a single element
      /// @param r view of the RHS storage for a single element
      /// @param advect_matrix advection matrix S
      /// @param func internal flux function. Has a member function
      /// int_nflux(q_i, q_j), where q_i and q_j are subarrays of q. q_i is used
      /// for computing the terms outside the gradient operator, and q_j is used
      /// for computing the terms inside the gradient operator.
      /// @tparam Q 3D view or view_sequence: 1st dim is num nodes in y, 2nd dim
      /// is num nodes in x, 3rd dim is each component of q
      /// @tparam R 3D view or view_sequence: 1st dim is num nodes in y, 2nd dim
      /// is num nodes in x, 3rd dim is each component of q
      /// @tparam T real type
      /// @tparam L layout type
      /// @tparam F callable (see func above)
      ///
      template <class Q, class R, class T, class L, class F>
      void int_nflux(const Q& q, R&& r, const dg::view<T, 3, L>& advect_matrix,
        F&& func) const;

      ///
      /// @brief Compute the volume integral term for internal fluxes.
      /// r += S * func(q, x)
      /// @param q view of the variables for a single element
      /// @param r view of the RHS storage for a single element
      /// @param advect_matrix advection matrix S
      /// @param func internal flux function. Has a member function int_flux(q,
      /// x)
      /// @param coords 4 vertex locations defining the element.
      /// @tparam Q 3D view or view_sequence: 1st dim is num nodes in y, 2nd dim
      /// is num nodes in x, 3rd dim is each component of q
      /// @tparam R 3D view or view_sequence: 1st dim is num nodes, 2nd dim
      /// is num nodes in x, 3rd dim is each component of q
      /// @tparam T real type
      /// @tparam L layout type
      /// @tparam F callable
      /// @tparam C view type. First dim is node row, second dim is node col,
      /// third dim is dimension (x,y)
      ///
      template <class Q, class R, class T, class L, class F, class C>
      void int_flux(const Q& q, R&& r, const dg::view<T, 3, L>& advect_matrix,
        F&& func, const C& coords) const;

      ///
      /// @brief Compute the volume integral term for internal fluxes in
      /// non-conservation form.
      /// r += S * func(q_i, q_j, x_i, x_j)
      /// @param q view of the variables for a single element
      /// @param r view of the RHS storage for a single element
      /// @param advect_matrix advection matrix S
      /// @param func internal flux function. Has a member function
      /// int_nflux(q_i, q_j, x_i, x_j), where q_i and q_j are subarrays of q.
      /// q_i is used for computing the terms outside the gradient operator, and
      /// q_j is used for computing the terms inside the gradient operator.
      /// @param coords 4 vertex locations defining the element.
      /// @tparam Q 3D view or view_sequence: 1st dim is num nodes in y, 2nd dim
      /// is num nodes in x, 3rd dim is each component of q
      /// @tparam R 3D view or view_sequence: 1st dim is num nodes in y, 2nd dim
      /// is num nodes in x, 3rd dim is each component of q
      /// @tparam T real type
      /// @tparam L layout type
      /// @tparam F callable (see func above)
      /// @tparam C view type. First dim is node row, second dim is node col,
      /// third dim is dimension (x,y)
      ///
      template <class Q, class R, class T, class L, class F, class C>
      void int_nflux(const Q& q, R&& r, const dg::view<T, 3, L>& advect_matrix,
        F&& func, const C& coords) const;

      ///
      /// @brief Accumulates numerical flux contribution (assuming DG weak
      /// form).
      /// r -= L * fluxes
      ///
      template <class QOut, class QIn, class R, class N, class IOut, class IIn,
        class L, class F>
      void num_flux(const QOut& qout, const QIn& qin, R&& r, const N& normal,
        const IOut& idcs_out, const IIn& idcs_in, const L& lift, F&& fun) const;

      ///
      /// @brief Accumulates numerical flux contribution (assuming DG weak
      /// form). This formulation works with a sparse lift matrix. It is
      /// designed for HDG (both the local and global systems).
      ///
      /// rl -= L * func(lmbda, qin, n)
      /// r -= L * func(lmbda, qin, n)
      ///
      template <class Lmbda, class QIn, class RL, class R, class N, class IOut,
        class IIn, class L, class F>
      void num_flux_hdg(const Lmbda& lmbda, const QIn& qin, RL&& rl, R&& r,
        N normal, const IOut& idcs_out, const IIn& idcs_in, const L& lift,
        F&& func) const;

      template <class Lmbda, class QIn, class R, class N, class IOut, class IIn,
        class L, class F>
      void num_flux_local(const Lmbda& lmbda, const QIn& qin, R&& r, N normal,
        const IOut& idcs_out, const IIn& idcs_in, const L& lift,
        F&& func) const;

      template <class Lmbda, class QIn, class RL, class N, class IOut,
        class IIn, class L, class F>
      void num_flux_global(const Lmbda& lmbda, const QIn& qin, RL&& rl,
        N normal, const IOut& idcs_out, const IIn& idcs_in, const L& lift,
        F&& func) const;

      ///
      /// @brief Computes the Jacobian contribution of the Numerical Flux
      /// (assuming HDG weak form).
      ///
      /// @param lmbda
      /// @param qin
      /// @param A_mat
      /// @param B_mat
      /// @param C_mat
      /// @param D_mat
      /// @param normal
      /// @param idcs_out
      /// @param idcs_in
      /// @param lift
      /// @param func
      /// @tparam Lmbda
      /// @tparam QIn
      /// @tparam A 4D view. A left layout is preferred.
      /// Dimension order: row component of q, row y node index, row x node
      /// index, column component of q, column y node index, column x node index
      /// @tparam B 5D view. A left layout is preferred.
      /// Dimension order: row component of q, row y node index, row x node
      /// index, column component of lambda, column face node index.
      /// @tparam C 4D view. A right layout is preferred.
      /// Dimension order: row face node index, row component of lambda, column
      /// condensed element node index, column component of q
      /// @tparam D 4D view. A right layout is preferred. Dimension order: row
      /// face node index, face component of lambda, column face node index,
      /// column component of lambda
      /// @tparam N
      /// @tparam IOut
      /// @tparam IIn
      /// @tparam L
      /// @tparam F
      ///
      template <class Lmbda, class QIn, class A, class B, class C, class D,
        class N, class IOut, class IIn, class L, class F>
      void num_flux_jac(const Lmbda& lmbda, const QIn& qin, A&& A_mat,
        B&& B_mat, C&& C_mat, D&& D_mat, const N& normal, const IOut& idcs_out,
        const IIn& idcs_in, const L& lift, F&& func);

      ///
      /// @brief Computes the Jacobian contribution of the Numerical Flux
      /// (assuming HDG weak form).
      ///
      /// @param lmbda
      /// @param qin
      /// @param A_mat
      /// @param B_mat
      /// @param normal
      /// @param idcs_out
      /// @param idcs_in
      /// @param lift
      /// @param func
      /// @tparam Lmbda
      /// @tparam QIn
      /// @tparam A 4D view. A left layout is preferred.
      /// Dimension order: row component of q, row y node index, row x node
      /// index, column component of q, column y node index, column x node index
      /// @tparam B 5D view. A left layout is preferred.
      /// Dimension order: row component of q, row y node index, row x node
      /// index, column component of lambda, column face node index.
      /// @tparam N
      /// @tparam IOut
      /// @tparam IIn
      /// @tparam L
      /// @tparam F
      ///
      template <class Lmbda, class QIn, class A, class B, class N, class IOut,
        class IIn, class L, class F>
      void num_flux_local_jac(const Lmbda& lmbda, const QIn& qin, A&& A_mat,
        B&& B_mat, const N& normal, const IOut& idcs_out, const IIn& idcs_in,
        const L& lift, F&& func);

      ///
      /// @brief Computes the Jacobian contribution of the Numerical Flux
      /// (assuming HDG weak form).
      ///
      /// @param lmbda
      /// @param qin
      /// @param C_mat
      /// @param D_mat
      /// @param normal
      /// @param idcs_out
      /// @param idcs_in
      /// @param lift
      /// @param func
      /// @tparam Lmbda
      /// @tparam QIn
      /// @tparam C 4D view. A right layout is preferred.
      /// Dimension order: row face node index, row component of lambda, column
      /// condensed element node index, column component of q
      /// @tparam D 4D view. A right layout is preferred. Dimension order: row
      /// face node index, face component of lambda, column face node index,
      /// column component of lambda
      /// @tparam N
      /// @tparam IOut
      /// @tparam IIn
      /// @tparam L
      /// @tparam F
      ///
      template <class Lmbda, class QIn, class C, class D, class N, class IOut,
        class IIn, class L, class F>
      void num_flux_global_jac(const Lmbda& lmbda, const QIn& qin, C&& C_mat,
        D&& D_mat, const N& normal, const IOut& idcs_out, const IIn& idcs_in,
        const L& lift, F&& func);

      ///
      /// @brief Accumulates a non-conservation form numerical flux contribution
      /// (assuming DG weak form).
      /// r -= L * func(q_i, fluxes)
      /// @param q
      /// @param r
      /// @param lift
      /// @param fluxes Numerical flux evaluated at the face node
      /// @param func functor for computing the flux contribution. Has a member
      /// function num_nflux(q_i, fluxes)
      /// @tparam Q
      /// @tparam R
      /// @tparam T real type
      /// @tparam L layout type
      /// @tparam F
      /// @tparam Func
      ///
      template <class Q, class R, class T, class L, class F, class Func>
      void num_nflux(const Q& q, R&& r, const dg::view<T, 1, L>& lift,
        const F& fluxes, Func&& func) const;

      ///
      /// @brief Accumulates a non-conservation form numerical flux contribution
      /// (assuming DG weak form).
      /// r -= L * func(q_i, fluxes, x)
      /// @param q
      /// @param r
      /// @param lift
      /// @param fluxes Numerical flux evaluated at the face node
      /// @param func functor for computing the flux contribution. Has a member
      /// function num_nflux(q_i, fluxes, x)
      /// @param coords 4 vertex locations defining the element.
      /// @tparam Q
      /// @tparam R
      /// @tparam T real type
      /// @tparam L layout type
      /// @tparam F
      /// @tparam Func
      /// @tparam C
      ///
      template <class Q, class R, class T, class L, class F, class Func,
        class C>
      void num_nflux(const Q& q, R&& r, const dg::view<T, 1, L>& lift,
        const F& fluxes, Func&& func, const C& coords) const;
    };
  } // namespace ndg
} // namespace dg

#include "detail/rect_2d.tcc"

#endif
