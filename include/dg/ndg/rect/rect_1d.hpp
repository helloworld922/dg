#ifndef DG_NDG_RECT_1D_HPP
#define DG_NDG_RECT_1D_HPP

#include "rect.hpp"

#include "dg/spatial/elements/lagrange.hpp"

#include "dg/utils/instructions_list.hpp"

#include <Eigen/Dense>

namespace dg
{
  namespace ndg
  {
    ///
    /// @brief 1D nodal DG, linear coordinate mapping. Uses the DG Weak form.
    /// This class is designed to help with implementing explicit DG and
    /// implicit HDG. Methods are written to handle source terms and first
    /// derivitive "flux" terms. By convention, face 0 contains node 0, and face
    /// 1 contains node Degree.
    ///
    /// It is the responsibility of the user to decompose any higher order
    /// derivatives into a coupled PDE system. For example, Laplace's equation
    /// can be written as: \f{gather}{
    /// \nabla \cdot \left(\kappa \vec{\sigma}\right) = 0\\
    /// \vec{\sigma} - \nabla u = 0
    /// \f}
    ///
    template <size_t Degree>
    struct rect<Degree>
    {
      /// \{
      dg::spatial::elements::lagrange<dg::real256, Degree> element;
      // needs to be 1 degree higher to accurately integrate using quadrature
      dg::spatial::elements::lagrange<dg::real256, Degree + 1> integrator;
      /// \}

      ///
      /// @brief computes the mass matrix and stores the result into dst.
      /// @param dst output. Stores result to first ([0,Degree], [0,Degree])
      /// block
      /// @param jac Jacobian of the element \f$\frac{\Delta x}{2}\f$.
      /// @tparam V 2d view-like type.
      ///
      template <class V>
      void calc_mass_matrix(
        V&& dst, typename std::decay_t<V>::value_type jac = 1) const;

      ///
      /// @brief computes \f$\boldsymbol{S}\f$ and stores the result
      /// into dst
      /// @param dst output. Stores result to first ([0,Degree], [0,Degree])
      /// block
      /// @tparam V 2d view-like type. Note: for performance reasons, a left
      /// layout is better for cache locality.
      ///
      template <class V>
      void calc_advect_matrix(V&& dst) const;

      ///
      /// @brief computes \f$\boldsymbol{L}\f$ and stores the result
      ///   into dst
      /// @param dst array-like type containing two view-like types. Each view
      /// is a reduced portion of the lift matrix for that face.
      /// @param area0 surface area of face at x0
      /// @param area1 surface area of face at x1
      ///
      template <class V, class M, class R = double>
      void calc_lift_matrix(V&& dst, const M& imass, R area0, R area1) const;

      ///
      /// @brief computes \f$\boldsymbol{L}\f$ and stores the result
      ///   into dst
      /// @param dst array-like type containing two view-like types. Each view
      /// is a reduced portion of the lift matrix for that face.
      /// @param area0 surface area of face at x0
      /// @param area1 surface area of face at x1
      ///
      template <class V, class R = double>
      void calc_lift_matrix(V&& dst, R area0, R area1) const;

      ///
      /// @brief Implements a source term, evaluating only along the diagonals
      /// (assuming identity mass matrix, or assuming Eq. has been multiplied by
      /// inverse mass matrix already).
      /// r += func(q)
      ///
      /// @param q
      /// @param r
      /// @param func
      /// @tparam Q
      /// @tparam R
      /// @tparam F
      ///
      template <class Q, class R, class F>
      void source(const Q& q, R&& r, F&& func) const;

      ///
      /// @brief Implements a source term
      /// r += M*func(q)
      ///
      /// @param q
      /// @param r
      /// @param func
      /// @param mass_matrix
      /// @tparam Q
      /// @tparam R
      /// @tparam F
      /// @tparam M
      ///
      template <class Q, class R, class F, class M>
      void source(const Q& q, R&& r, F&& func, const M& mass_matrix) const;

      ///
      /// @brief Implements a source term Jacobian
      ///
      /// @param q
      /// @param r
      /// @param func
      /// @param mass_matrix
      /// @tparam Q
      /// @tparam R
      /// @tparam F
      /// @tparam M
      ///
      template <class Q, class R, class F, class M>
      void source_jac(const Q& q, R&& r, F&& func, const M& mass_matrix) const;

      ///
      /// @brief Implements a source term, evaluating only along the diagonals
      /// (assuming identity mass matrix, or assuming Eq. has been multiplied by
      /// inverse mass matrix already).
      /// r += func(q, x)
      ///
      /// @param q
      /// @param r
      /// @param func
      /// @param x0
      /// @param x1
      /// @tparam Q
      /// @tparam R
      /// @tparam F
      /// @tparam T
      ///
      template <class Q, class R, class F, class T>
      void source(const Q& q, R&& r, F&& func, T x0, T x1) const;

      ///
      /// @brief Implements a source term
      /// r += M*func(q, x)
      ///
      /// @param q
      /// @param r
      /// @param func
      /// @param mass_matrix
      /// @param x0
      /// @param x1
      /// @tparam Q
      /// @tparam R
      /// @tparam F
      /// @tparam M
      /// @tparam T
      ///
      template <class Q, class R, class F, class M, class T>
      void source(
        const Q& q, R&& r, F&& func, const M& mass_matrix, T x0, T x1) const;

      ///
      /// @brief Implements a source term Jacobian
      ///
      /// @param q
      /// @param r
      /// @param func
      /// @param mass_matrix
      /// @param x0
      /// @param x1
      /// @tparam Q
      /// @tparam R
      /// @tparam F
      /// @tparam M
      /// @tparam T
      ///
      template <class Q, class R, class F, class M, class T>
      void source_jac(
        const Q& q, R&& r, F&& func, const M& mass_matrix, T x0, T x1) const;

      ///
      /// @brief Compute the volume integral term for internal fluxes.
      /// r += S * func(q)
      /// @param q view of the variables for a single element
      /// @param r view of the RHS storage for a single element
      /// @param advect_matrix advection matrix
      /// @param func internal flux functor. Has a member function
      /// int_flux(q_j), where
      /// q_j is a single subarray of q
      /// @tparam Q 2D view: 1st dim is num nodes, second dim is each component
      /// of q
      /// @tparam R 2D view: 1st dim is num nodes, second dim is each component
      /// of q (or a subset which are written to)
      /// @tparam T real type
      /// @tparam L layout type
      /// @tparam F callable
      ///
      template <class Q, class R, class T, class L, class F>
      void int_flux(const Q& q, R&& r, const dg::view<T, 2, L>& advect_matrix,
        F&& func) const;

      ///
      /// @brief Compute the volume integral term for internal fluxes.
      /// r += S * func(q, x)
      /// @param q view of the variables for a single element
      /// @param r view of the RHS storage for a single element
      /// @param advect_matrix advection matrix
      /// @param func internal flux functor. Has a member function int_flux(q_j,
      /// x) where q_j is a subarray of q, and x is the node location (in the
      /// global coordinates)
      /// @param x0
      /// @param x1
      /// @tparam Q 2D view: 1st dim is num nodes, second dim is each component
      /// of q
      /// @tparam R 2D view: 1st dim is num nodes, second dim is each component
      /// of q (or a subset which are written to)
      /// @tparam T real type
      /// @tparam L layout type
      /// @tparam F callable
      ///
      template <class Q, class R, class T, class L, class F>
      void int_flux(const Q& q, R&& r, const dg::view<T, 2, L>& advect_matrix,
        F&& func, T x0, T x1) const;

      ///
      /// @brief Computes the internal flux contribution to the Jacobian for
      /// the (non)-linear solver.
      /// @param q
      /// @param mat Resulting Jacobian Matrix. Expects a dense matrix format.
      /// @param advect_matrix
      /// @param func
      /// @tparam Q
      /// @tparam M 4D view. It is better to use left layout. 1st dim is
      /// equation number, 2nd dim is num nodes in an element, 3rd dim
      /// is variable component number, 4th dim is num nodes in an element.
      /// @tparam T
      /// @tparam L
      /// @tparam F
      ///
      template <class Q, class M, class T, class L, class F>
      void int_flux_jac(const Q& q, M&& mat,
        const dg::view<T, 2, L>& advect_matrix, F&& func) const;

      ///
      /// @brief Compute the volume integral term for internal fluxes in
      /// non-conservation form.
      /// r += S * func(q_i, q_j)
      /// @param q view of the variables for a single element
      /// @param r view of the RHS storage for a single element
      /// @param advect_matrix advection matrix
      /// @param func internal flux functor. Has a member function
      /// int_nflux(q_i, q_j), where q_i and q_j are subarrays of q. q_i is used
      /// for computing the terms outside the gradient operator, and q_j is used
      /// for computing the terms inside the gradient operator.
      /// @tparam Q 2D view: 1st dim is num nodes, second dim is each component
      /// of q
      /// @tparam R 2D view: 1st dim is num nodes, second dim is each component
      /// of q (or a subset which are written to)
      /// @tparam T real type
      /// @tparam L layout type
      /// @tparam F callable (see func above)
      ///
      template <class Q, class R, class T, class L, class F>
      void int_nflux(const Q& q, R&& r, const dg::view<T, 2, L>& advect_matrix,
        F&& func) const;

      ///
      /// @brief Compute the volume integral term for internal fluxes in
      /// non-conservative form. r += S * func(q_i, q_j, x_i, x_j)
      /// @param q view of the variables for a single element
      /// @param r view of the RHS storage for a single element
      /// @param advect_matrix advection matrix
      /// @param func internal flux functor. Has a member function
      /// int_nflux(q_i, q_j, x_i, x_j) where q_i and q_j are subarrays of q,
      /// and x_i and x_j are node locations (in the global coordinates). The i
      /// subscript arguments are used for computing the terms outside the
      /// gradient operator, and the j subscript arguments are used for
      /// computing the terms inside the gradient operator.
      /// @param x0
      /// @param x1
      /// @tparam Q 2D view: 1st dim is num nodes, second dim is each component
      /// of q
      /// @tparam R 2D view: 1st dim is num nodes, second dim is each component
      /// of q (or a subset which are written to)
      /// @tparam T real type
      /// @tparam L layout type
      /// @tparam F callable
      ///
      template <class Q, class R, class T, class L, class F>
      void int_nflux(const Q& q, R&& r, const dg::view<T, 2, L>& advect_matrix,
        F&& func, T x0, T x1) const;

      ///
      /// @brief Accumulates numerical flux contribution of a single face
      /// (assuming DG weak form). Assumes faces are flat.
      /// r -= L * func(qout, qin, n)
      ///
      /// @param qout view of q outside the face
      /// @param qin view of q inside the face
      /// @param r view of the RHS storage for a single element
      /// @param normal outwards pointing face normal
      /// @param idcs_out list of indexes for where in qout the face nodes are
      /// @param idcs_in list of indexes for where in qin the face nodes are
      /// @param lift slice of the lift matrix for a particular face (see
      /// calc_lift_matrix)
      /// @param func Numerical flux functor, computes outward numerical fluxes
      /// n*Fnumerical
      /// @tparam QOut 2D view: 1st dim is num nodes, second dim is each
      /// component of q
      /// @tparam QIn 2D view: 1st dim is num nodes, second dim is each
      /// component of q
      /// @tparam R 2D view: 1st dim is num nodes, second dim is each component
      /// of q (subset which are written to)
      /// @tparam N real type
      /// @tparam IOut
      /// @tparam IIn
      /// @tparam L 2D view type
      /// @tparam F callable (TODO: define interface)
      ///
      template <class QOut, class QIn, class R, class N, class IOut, class IIn,
        class L, class F>
      void num_flux(const QOut& qout, const QIn& qin, R&& r, N normal,
        const IOut& idcs_out, const IIn& idcs_in, const L& lift,
        F&& func) const;

      ///
      /// @brief Accumulates numerical flux contribution of a single face
      /// (assuming DG weak form). Assumes faces are flat.
      /// r -= L * func(qout, qin, n, x)
      ///
      /// @param qout view of q outside the face
      /// @param qin view of q inside the face
      /// @param r view of the RHS storage for a single element
      /// @param normal outwards pointing face normal
      /// @param idcs_out list of indexes for where in qout the face nodes are
      /// @param idcs_in list of indexes for where in qin the face nodes are
      /// @param lift slice of the lift matrix for a particular face (see
      /// calc_lift_matrix)
      /// @param func Numerical flux functor, computes outward numerical fluxes
      /// n*Fnumerical
      /// @param x0
      /// @param x1
      /// @tparam QOut 2D view: 1st dim is num nodes, second dim is each
      /// component of q
      /// @tparam QIn 2D view: 1st dim is num nodes, second dim is each
      /// component of q
      /// @tparam R 2D view: 1st dim is num nodes, second dim is each component
      /// of q (subset which are written to)
      /// @tparam N real type
      /// @tparam IOut
      /// @tparam IIn
      /// @tparam L 2D view type
      /// @tparam F callable (TODO: define interface)
      /// @tparam T real type
      ///
      template <class QOut, class QIn, class R, class N, class IOut, class IIn,
        class L, class F, class T>
      void num_flux(const QOut& qout, const QIn& qin, R&& r, N normal,
        const IOut& idcs_out, const IIn& idcs_in, const L& lift, F&& func, T x0,
        T x1) const;

      ///
      /// @brief Accumulates numerical flux contribution (assuming DG weak
      /// form). This formulation works with a sparse lift matrix. It is
      /// designed for HDG (both the local and global systems).
      ///
      /// rl -= L * func(lmbda, qin, n)
      /// r -= L * func(lmbda, qin, n)
      ///
      /// @param lmbda hybridized variable for the face
      /// @param qin q inside the face
      /// @param rl global system RHS
      /// @param r local system RHS
      /// @param normal outwards face normal
      /// @param idcs_out
      /// @param idcs_in list of indexes for where in qin the face nodes are
      /// @param lift slice of the lift matrix for a particular face (see
      /// calc_lift_matrix)
      /// @param func TODO
      /// @tparam Lmbda
      /// @tparam QIn
      /// @tparam RL
      /// @tparam R
      /// @tparam N
      /// @tparam IOut
      /// @tparam IIn
      /// @tparam L
      /// @tparam F
      ///
      template <class Lmbda, class QIn, class RL, class R, class N, class IOut,
        class IIn, class L, class F>
      void num_flux_hdg(const Lmbda& lmbda, const QIn& qin, RL&& rl, R&& r,
        N normal, const IOut& idcs_out, const IIn& idcs_in, const L& lift,
        F&& func) const;

      ///
      /// @brief Accumulates numerical flux contribution (assuming DG weak
      /// form). This formulation works with a sparse lift matrix. It is
      /// designed for local HDG system.
      ///
      /// r -= L * func(lmbda, qin, n)
      ///
      /// @param lmbda hybridized variable for the face
      /// @param qin q inside the face
      /// @param r local system RHS
      /// @param normal outwards face normal
      /// @param idcs_out
      /// @param idcs_in list of indexes for where in qin the face nodes are
      /// @param lift slice of the lift matrix for a particular face (see
      /// calc_lift_matrix)
      /// @param func TODO
      /// @tparam Lmbda
      /// @tparam QIn
      /// @tparam R
      /// @tparam N
      /// @tparam IOut
      /// @tparam IIn
      /// @tparam L
      /// @tparam F
      ///
      template <class Lmbda, class QIn, class R, class N, class IOut, class IIn,
        class L, class F>
      void num_flux_local(const Lmbda& lmbda, const QIn& qin, R&& r, N normal,
        const IOut& idcs_out, const IIn& idcs_in, const L& lift,
        F&& func) const;

      ///
      /// @brief Accumulates numerical flux contribution (assuming DG weak
      /// form). This formulation works with a sparse lift matrix. It is
      /// designed for global HDG system.
      ///
      /// rl -= L * func(lmbda, qin, n)
      ///
      /// @param lmbda hybridized variable for the face
      /// @param qin q inside the face
      /// @param rl global system RHS
      /// @param normal outwards face normal
      /// @param idcs_out
      /// @param idcs_in list of indexes for where in qin the face nodes are
      /// @param lift slice of the lift matrix for a particular face (see
      /// calc_lift_matrix)
      /// @param func TODO
      /// @tparam Lmbda
      /// @tparam QIn
      /// @tparam RL
      /// @tparam N
      /// @tparam IOut
      /// @tparam IIn
      /// @tparam L
      /// @tparam F
      ///
      template <class Lmbda, class QIn, class RL, class N, class IOut,
        class IIn, class L, class F>
      void num_flux_global(const Lmbda& lmbda, const QIn& qin, RL&& rl,
        N normal, const IOut& idcs_out, const IIn& idcs_in, const L& lift,
        F&& func) const;

      ///
      /// @brief Computes the Jacobian contribution of the Numerical Flux
      /// (assuming HDG weak form).
      ///
      template <class Lmbda, class QIn, class A, class B, class C, class D,
        class N, class IOut, class IIn, class L, class F>
      void num_flux_jac(const Lmbda& lmbda, const QIn& qin, A&& A_mat,
        B&& B_mat, C&& C_mat, D&& D_mat, N normal, const IOut& idcs_out,
        const IIn& idcs_in, const L& lift, F&& func);

      ///
      /// @brief Computes the Jacobian contribution of the Numerical Flux
      /// (assuming HDG weak form).
      ///
      template <class Lmbda, class QIn, class A, class B, class N, class IOut,
        class IIn, class L, class F>
      void num_flux_local_jac(const Lmbda& lmbda, const QIn& qin, A&& A_mat,
        B&& B_mat, N normal, const IOut& idcs_out, const IIn& idcs_in,
        const L& lift, F&& func);

      ///
      /// @brief Computes the Jacobian contribution of the Numerical Flux
      /// (assuming HDG weak form).
      ///
      template <class Lmbda, class QIn, class C, class D, class N, class IOut,
        class IIn, class L, class F>
      void num_flux_global_jac(const Lmbda& lmbda, const QIn& qin, C&& C_mat,
        D&& D_mat, N normal, const IOut& idcs_out, const IIn& idcs_in,
        const L& lift, F&& func);

      ///
      /// @brief Accumulates a non-conservation form numerical flux contribution
      /// (assuming DG weak form).
      /// r -= L * func(q_i, fluxes)
      /// @param q
      /// @param r
      /// @param lift
      /// @param fluxes
      /// @param func functor for computing the flux. Has a member function
      /// num_nflux(q_i, fluxes)
      /// @tparam Q
      /// @tparam R
      /// @tparam T
      /// @tparam L
      /// @tparam F
      /// @tparam Func
      ///
      template <class QOut, class QIn, class R, class N, class IOut, class IIn,
        class L, class F>
      void num_nflux(const QOut& qout, const QIn& qin, R&& r, N normal,
        const IOut& idcs_out, const IIn& idcs_in, const L& lift,
        F&& func) const;

      ///
      /// @brief Accumulates a non-conservation form numerical flux contribution
      /// (assuming DG weak form).
      /// r -= L * func(q_i, fluxes)
      /// @param q
      /// @param r
      /// @param lift
      /// @param fluxes
      /// @param func functor for computing the flux. Has a member function
      /// num_nflux(q_i, fluxes)
      /// @tparam Q
      /// @tparam R
      /// @tparam T
      /// @tparam L
      /// @tparam F
      /// @tparam Func
      ///
      template <class QOut, class QIn, class R, class N, class IOut, class IIn,
        class L, class F, class T>
      void num_nflux(const QOut& qout, const QIn& qin, R&& r, N normal,
        const IOut& idcs_out, const IIn& idcs_in, const L& lift, F&& func, T x0,
        T x1) const;
    };
  } // namespace ndg
} // namespace dg

#include "detail/rect_1d.tcc"

#endif
