#ifndef DG_NDG_RECT_HPP
#define DG_NDG_RECT_HPP

#include "dg/ndg/ndg.hpp"

#include <cstddef>

namespace dg
{
  namespace ndg
  {
    ///
    /// @brief N-D nodal DG hypercube elements (each dimension spans [-1, 1])
    /// All sides map N-linearly to global coordinates.
    ///
    template <size_t... Degrees>
    struct rect;
  }
}

#endif
