#ifndef DG_NDG_HPP
#define DG_NDG_HPP

#include "dg/utils/view.hpp"

#include <cstddef>

namespace dg
{
  namespace ndg
  {
#ifndef DG_DOC
    namespace detail
    {
      ///
      /// @brief A helper to apply an operation to all components in a view, or
      /// a view_sequence
      ///
      template <class R>
      struct apply_all_comps
      {
        // default: assume a view-like type
        template <class F, class R2, class RP>
        static void apply(F&& f, R2&& r, RP&& rp)
        {
          for (size_t comp = 0; comp < rp.size(); ++comp)
          {
            f(r[comp], rp[comp]);
          }
        }
      };

      /// helper for implementing apply_all_comps for view_sequence
      template<class V, class Seq>
      struct apply_all_comps_impl
      {
        // base case: do nothing
        template <class F, class R, class RP>
        static void apply(F&& f, R&& r, RP&& rp)
        {
        }
      };

      template <class... Vs, size_t V0, size_t... Idcs>
      struct apply_all_comps_impl<dg::view_sequence<Vs...>,
        std::index_sequence<V0, Idcs...>>
      {
        template <class F, class R, class RP>
        static void apply(F&& f, R&& r, RP&& rp)
        {
          // iterate through all views in r and rp in lock-step
          apply_all_comps<std::decay_t<decltype(dg::get<V0>(r))>>::apply(
            f, dg::get<V0>(r), dg::get<V0>(rp));
          apply_all_comps_impl<view_sequence<Vs...>,
            std::index_sequence<Idcs...>>::apply(f, r, rp);
        }
      };

      template <class... Vs>
      struct apply_all_comps<view_sequence<Vs...>>
      {
        template <class F, class R, class RP>
        static void apply(F&& f, R&& r, RP&& rp)
        {
          // iterate through all views in r and rp in lock-step
          apply_all_comps_impl<view_sequence<Vs...>,
            std::make_index_sequence<sizeof...(Vs)>>::apply(f, r, rp);
        }
      };

      ///
      /// @brief A helper to apply an operation to all components in a view, or
      /// a view_sequence
      ///
      template <class R>
      struct apply_all_comps_nd
      {
        // default: assume a view-like type
        template <class F, class R2, class RP>
        static void apply(F&& f, R2&& r, RP&& rp)
        {
          for (size_t comp = 0; comp < rp.extent(0); ++comp)
          {
            f(r[comp], rp.subarray(comp, dg::all{}));
          }
        }
      };

      /// helper for implementing apply_all_comps for view_sequence
      template<class V, class Seq>
      struct apply_all_comps_nd_impl
      {
        // base case: do nothing
        template <class F, class R, class RP>
        static void apply(F&& f, R&& r, RP&& rp)
        {
        }
      };

      template <class... Vs, size_t V0, size_t... Idcs>
      struct apply_all_comps_nd_impl<dg::view_sequence<Vs...>,
        std::index_sequence<V0, Idcs...>>
      {
        template <class F, class R, class RP>
        static void apply(F&& f, R&& r, RP&& rp)
        {
          // iterate through all views in r and rp in lock-step
          apply_all_comps_nd<std::decay_t<decltype(dg::get<V0>(r))>>::apply(
            f, dg::get<V0>(r), dg::get<V0>(rp));
          apply_all_comps_nd_impl<view_sequence<Vs...>,
            std::index_sequence<Idcs...>>::apply(f, r, rp);
        }
      };

      template <class... Vs>
      struct apply_all_comps_nd<view_sequence<Vs...>>
      {
        template <class F, class R, class RP>
        static void apply(F&& f, R&& r, RP&& rp)
        {
          // iterate through all views in r and rp in lock-step
          apply_all_comps_nd_impl<view_sequence<Vs...>,
            std::make_index_sequence<sizeof...(Vs)>>::apply(f, r, rp);
        }
      };
    }
#endif
  }
}

#endif
