#ifndef DG_LAYOUTS_HPP
#define DG_LAYOUTS_HPP

// Various common layouts for how to index a chunk of memory

#include "extents.hpp"

#include "compiler_support.hpp"
#include "meta.hpp"

#include <array>
#include <cstddef>
#include <type_traits>

namespace dg
{
  ///
  /// @brief The left index is contiguous. This is the Fortran-style indexing
  ///
  struct layout_left
  {
    typedef std::size_t size_type;

  private:
    template <class Exts, class Idx>
    constexpr size_type idx_impl_(const Exts& d, Idx idx0) const noexcept
    {
      return idx0;
    }

    template <class Exts, class Idx, class... Idcs>
    constexpr size_type idx_impl_(const Exts& d, Idx idx0, Idcs... idcs) const
      noexcept
    {
      return idx0 + d[d.rank() - 1 - sizeof...(idcs)] * idx_impl_(d, idcs...);
    }

  public:
    constexpr layout_left() noexcept = default;
    constexpr layout_left(const layout_left& o) noexcept = default;
    layout_left& operator=(const layout_left& o) noexcept = default;

    ///
    /// @brief Flattens a list of indices with no bounds checking
    ///
    template <class Exts, class... Idcs>
    size_type operator()(const Exts& d, Idcs... idcs) const noexcept
    {
      static_assert(sizeof...(idcs) == std::decay<Exts>::type::rank(),
        "wrong number of indices given");
      return idx_impl_(d, idcs...);
    }
  };

  ///
  /// @brief The right index is the contiguous index. This is the C-style
  /// indexing
  ///
  struct layout_right
  {
    typedef std::size_t size_type;

  private:
    template <class Exts, class Idx>
    constexpr size_type idx_impl_(const Exts& d, size_type curr, Idx idx0) const
      noexcept
    {
      return curr * d[d.rank() - 1] + idx0;
    }

    template <class Exts, class Idx, class... Idcs>
    constexpr size_type idx_impl_(
      const Exts& d, size_type curr, Idx idx0, Idcs... idcs) const noexcept
    {
      return idx_impl_(
        d, curr * d[d.rank() - 1 - sizeof...(idcs)] + idx0, idcs...);
    }

  public:
    constexpr layout_right() noexcept = default;
    constexpr layout_right(const layout_right&) noexcept = default;
    layout_right& operator=(const layout_right& o) noexcept = default;

    ///
    /// @brief Flattens a list of indices with no bounds checking
    ///
    template <class Exts, class... Idcs>
    constexpr size_type operator()(const Exts& d, Idcs... idcs) const noexcept
    {
      static_assert(sizeof...(idcs) == std::decay<Exts>::type::rank(),
        "wrong number of indices given");
      return idx_impl_(d, 0, idcs...);
    }
  };

  ///
  /// @brief Adds a constant offset to the index
  ///
  template <class Base = layout_right, class OffsetType = long long>
  struct layout_offset : public Base
  {
    OffsetType offset;

    typedef std::size_t size_type;

    template <
      class = std::enable_if_t<std::is_default_constructible<Base>::value>>
    layout_offset(OffsetType offset = 0) : Base(), offset(offset)
    {
    }

    layout_offset(OffsetType offset, const Base& base)
      : Base(base), offset(offset)
    {
    }

    layout_offset(OffsetType offset, Base&& base)
      : Base(std::move(base)), offset(offset)
    {
    }
    
    explicit operator Base()
    {
      return *this;
    }

    layout_offset(const layout_offset&) = default;
    layout_offset& operator=(const layout_offset& o) = default;
    layout_offset& operator=(layout_offset&& o) = default;

    ///
    /// @brief Flattens a list of indices with no bounds checking
    ///
    template <class Exts, class... Idcs>
    constexpr size_type operator()(const Exts& d, Idcs... idcs) const noexcept
    {
      static_assert(sizeof...(idcs) == std::decay<Exts>::type::rank(),
        "wrong number of indices given");
      return Base::operator()(d, idcs...) + offset;
    }
  };

  ///
  /// @brief Represents a slice which contains the entire extent of a single
  /// dimension
  ///
  using all = std::tuple<>;

  /// \cond DEV
  namespace detail
  {
    ///
    /// @internal
    /// @brief Helper for slice layouts for determining whether to use argument
    /// value, or
    /// a pre-defined value in the base slice specifier
    ///
    /// @tparam IsVar true if the argument at index Idx should be taken into
    /// account or not
    /// @tparam Slice Base slice specifier
    /// @tparam Idx Index into arguments to use
    ///
    template <bool IsVar, class Slice, size_t Idx>
    struct arg_selector;

    ///
    /// @brief Internal implementation of the slice_layout
    /// @tparam BaseLayout The base layout, which is not a slice_layout
    /// @tparam BaseExts The base extents
    /// @tparam Vals Integer sequence for indexing into SliceSpecifiers,
    ///         starts at 0 and is contiguous up to sizeof...(SliceSpecifiers)
    /// @tparam SliceSpecifiers sequence of slice specifiers
    ///
    template <class BaseLayout, class BaseExts, class Seq,
      class SlicesContainer>
    struct slice_invoker;

    ///
    /// @brief sliced layout.
    /// @tparam BaseLayout base layout type. Must not be another layout_slice.
    ///   Use slice_creator to take a sub slice of a slice layout
    /// @tparam BaseExts base extents type
    /// @tparam SliceSpecifiers slice specifiers
    ///
    template <class BaseLayout, class BaseExts, class... SliceSpecifiers>
    struct layout_slice;
  }
  /// \endcond

  ///
  /// @brief Creates a sliced layout given any base layout type
  /// @tparam BaseLayout
  /// @tparam BaseExts
  /// @tparam SliceSpecifiers
  ///
  template<class BaseLayout, class BaseExts, class... SliceSpecifiers>
  struct slice_creator
  {
    ///
    /// @brief The type of the layout created
    ///
    typedef detail::layout_slice<BaseLayout, BaseExts, SliceSpecifiers...> type;

    ///
    /// @brief Creates the child sliced extents
    ///
    template <class BaseL, class BaseD, class... S>
    constexpr static auto make_extents(
        BaseL&& base, BaseD&& dims, S&&... slices)
    {
      return detail::layout_slice<BaseLayout, BaseExts,
          SliceSpecifiers...>::make_extents(dims, slices...);
    }

    ///
    /// @brief Creates the layout
    ///
    template <class BaseL, class BaseD, class... S>
    constexpr static auto create(BaseL&& base, BaseD&& dims, S&&... slices)
    {
      return detail::layout_slice<BaseLayout, BaseExts, SliceSpecifiers...>(
        std::forward<BaseL>(base), std::forward<BaseD>(dims),
        std::forward<S>(slices)...);
    }
  };
}

#include "detail/slice_layout.hpp"

#include "detail/layouts.tcc"

#endif
