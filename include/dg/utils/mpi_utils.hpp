#ifndef DG_MPI_UTILS_HPP
#define DG_MPI_UTILS_HPP

#include "raii_base.hpp"

#include <mpi.h>
#include <cassert>

namespace dg
{
  ///
  /// @brief MPI helper utils
  ///
  namespace mpi
  {
#define MPI_RAII(name, type, free_func, null_val)                              \
  class name : public raii_base<name, type, type, type>                        \
  {                                                                            \
    typedef raii_base<name, type, type, type> super;                           \
                                                                               \
  public:                                                                      \
    typedef type value_type;                                                   \
    typedef type pointer_type;                                                 \
    typedef type const_pointer_type;                                           \
    static type null_value() noexcept                                          \
    {                                                                          \
      return null_val;                                                         \
    }                                                                          \
                                                                               \
    void dec()                                                                 \
    {                                                                          \
      auto p = get();                                                          \
      if (p != null_val)                                                       \
      {                                                                        \
        int err = free_func(&p);                                               \
        assert(err == MPI_SUCCESS);                                            \
      }                                                                        \
    }                                                                          \
                                                                               \
    name() noexcept : super(null_val)                                          \
    {                                                                          \
    }                                                                          \
                                                                               \
    name(type p) noexcept : super(p)                                           \
    {                                                                          \
    }                                                                          \
                                                                               \
    name(name&& o) noexcept = default;                                         \
    name& operator=(name&& o) noexcept = default;                              \
                                                                               \
    name& operator=(type d)                                                    \
    {                                                                          \
      reset(d);                                                                \
      return *this;                                                            \
    }                                                                          \
    ~name() = default;                                                         \
  }

    MPI_RAII(comm, MPI_Comm, MPI_Comm_free, MPI_COMM_NULL);
    MPI_RAII(datatype, MPI_Datatype, MPI_Type_free, MPI_DATATYPE_NULL);
    MPI_RAII(
      error_handler, MPI_Errhandler, MPI_Errhandler_free, MPI_ERRHANDLER_NULL);
    MPI_RAII(group, MPI_Group, MPI_Group_free, MPI_GROUP_NULL);
    MPI_RAII(info, MPI_Info, MPI_Info_free, MPI_INFO_NULL);
    MPI_RAII(operation, MPI_Op, MPI_Op_free, MPI_OP_NULL);
    MPI_RAII(request, MPI_Request, MPI_Request_free, MPI_REQUEST_NULL);
    MPI_RAII(window, MPI_Win, MPI_Win_free, MPI_WIN_NULL);
#undef MPI_RAII

    // clang-format off

    ///
    /// @class dg::mpi::comm
    /// @brief MPI MPI_Comm object wrapper
    /// @typedef dg::mpi::comm::value_type
    /// @brief base MPI comm value type
    /// @typedef dg::mpi::comm::super
    /// @internal
    /// @brief base class type helper
    /// @typedef dg::mpi::comm::pointer_type
    /// @brief base MPI comm value type
    /// @typedef dg::mpi::comm::const_pointer_type
    /// @brief base MPI comm value type
    /// @fn static dg::mpi::comm::value_type dg::mpi::comm::null_value()
    /// @return MPI_COMM_NULL
    /// @fn dg::mpi::dg::mpi::comm::comm()
    /// @brief Initializes a null communicator
    /// @fn dg::mpi::comm::comm(MPI_Comm p)
    /// @brief Wraps a given communicator
    /// @param p
    /// @fn dg::mpi::comm::comm(dg::mpi::comm&& o)
    /// @brief Moves the communicator from o to a newly constructed
    /// communicator.
    /// @param o
    /// @fn dg::mpi::comm& dg::mpi::comm::operator=(dg::mpi::comm&& o)
    /// @brief Moves the communicator from o to this comm. Destructs any
    /// existing communicator.
    /// @param o
    /// @return *this
    /// @fn dg::mpi::comm& dg::mpi::comm::operator=(MPI_Comm o)
    /// @brief Wraps a given communicator
    /// @param o
    /// @return *this
    /// @fn void comm::dec()
    /// @internal
    /// @brief used by the raii_base class to manage ownership
    ///

    ///
    /// @class dg::mpi::datatype
    /// @brief MPI MPI_Datatype object wrapper
    /// @typedef dg::mpi::datatype::value_type
    /// @brief base MPI datatype value type
    /// @typedef dg::mpi::datatype::pointer_type
    /// @brief base MPI datatype value type
    /// @typedef dg::mpi::datatype::const_pointer_type
    /// @brief base MPI datatype value type
    /// @typedef dg::mpi::datatype::super
    /// @internal
    /// @brief base class type helper
    /// @fn static dg::mpi::datatype::value_type dg::mpi::datatype::null_value()
    /// @return MPI_ERRHANDLER_NULL
    /// @fn dg::mpi::dg::mpi::datatype::datatype()
    /// @brief Initializes a null datatype
    /// @fn dg::mpi::datatype::datatype(MPI_Datatype p)
    /// @brief Wraps a given datatype
    /// @param p
    /// @fn dg::mpi::datatype::datatype(dg::mpi::datatype&& o)
    /// @brief Moves the datatype from o to a newly constructed error
    /// handler.
    /// @param o
    /// @fn dg::mpi::datatype& dg::mpi::datatype::operator=(dg::mpi::datatype&& o)
    /// @brief Moves the datatype from o to this. Destructs any existing
    /// datatype.
    /// @param o
    /// @return *this
    /// @fn dg::mpi::datatype& dg::mpi::datatype::operator=(MPI_Datatype o)
    /// @brief Wraps a given datatype
    /// @param o
    /// @return *this
    /// @fn void datatype::dec()
    /// @internal
    /// @brief used by the raii_base class to manage ownership
    ///

    ///
    /// @class dg::mpi::error_handler
    /// @brief MPI MPI_Errhandler object wrapper
    /// @typedef dg::mpi::error_handler::value_type
    /// @brief base MPI error handler value type
    /// @typedef dg::mpi::error_handler::pointer_type
    /// @brief base MPI error handler value type
    /// @typedef dg::mpi::error_handler::const_pointer_type
    /// @brief base MPI error handler value type
    /// @typedef dg::mpi::error_handler::super
    /// @internal
    /// @brief base class type helper
    /// @fn static dg::mpi::error_handler::value_type dg::mpi::error_handler::null_value()
    /// @return MPI_ERRHANDLER_NULL
    /// @fn dg::mpi::dg::mpi::error_handler::error_handler()
    /// @brief Initializes a null error handler
    /// @fn dg::mpi::error_handler::error_handler(MPI_Errhandler p)
    /// @brief Wraps a given error handler
    /// @param p
    /// @fn dg::mpi::error_handler::error_handler(dg::mpi::error_handler&& o)
    /// @brief Moves the error handler from o to a newly constructed error
    /// handler.
    /// @param o
    /// @fn dg::mpi::error_handler& dg::mpi::error_handler::operator=(dg::mpi::error_handler&& o)
    /// @brief Moves the error handler from o to this. Destructs any existing
    /// error handler.
    /// @param o
    /// @return *this
    /// @fn dg::mpi::error_handler& dg::mpi::error_handler::operator=(MPI_Errhandler o)
    /// @brief Wraps a given error handler
    /// @param o
    /// @return *this
    /// @fn void error_handler::dec()
    /// @internal
    /// @brief used by the raii_base class to manage ownership
    ///

    ///
    /// @class dg::mpi::group
    /// @brief MPI MPI_Group object wrapper
    /// @typedef dg::mpi::group::value_type
    /// @brief base MPI group value type
    /// @typedef dg::mpi::group::pointer_type
    /// @brief base MPI group value type
    /// @typedef dg::mpi::group::const_pointer_type
    /// @brief base MPI group value type
    /// @typedef dg::mpi::group::super
    /// @internal
    /// @brief base class type helper
    /// @fn static dg::mpi::group::value_type dg::mpi::group::null_value()
    /// @return MPI_ERRHANDLER_NULL
    /// @fn dg::mpi::dg::mpi::group::group()
    /// @brief Initializes a null group
    /// @fn dg::mpi::group::group(MPI_Group p)
    /// @brief Wraps a given group
    /// @param p
    /// @fn dg::mpi::group::group(dg::mpi::group&& o)
    /// @brief Moves the group from o to a newly constructed error
    /// handler.
    /// @param o
    /// @fn dg::mpi::group& dg::mpi::group::operator=(dg::mpi::group&& o)
    /// @brief Moves the group from o to this. Destructs any existing
    /// group.
    /// @param o
    /// @return *this
    /// @fn dg::mpi::group& dg::mpi::group::operator=(MPI_Group o)
    /// @brief Wraps a given group
    /// @param o
    /// @return *this
    /// @fn void group::dec()
    /// @internal
    /// @brief used by the raii_base class to manage ownership
    ///

    ///
    /// @class dg::mpi::info
    /// @brief MPI MPI_Info object wrapper
    /// @typedef dg::mpi::info::value_type
    /// @brief base MPI info value type
    /// @typedef dg::mpi::info::pointer_type
    /// @brief base MPI info value type
    /// @typedef dg::mpi::info::const_pointer_type
    /// @brief base MPI info value type
    /// @typedef dg::mpi::info::super
    /// @internal
    /// @brief base class type helper
    /// @fn static dg::mpi::info::value_type dg::mpi::info::null_value()
    /// @return MPI_ERRHANDLER_NULL
    /// @fn dg::mpi::dg::mpi::info::info()
    /// @brief Initializes a null info
    /// @fn dg::mpi::info::info(MPI_Info p)
    /// @brief Wraps a given info
    /// @param p
    /// @fn dg::mpi::info::info(dg::mpi::info&& o)
    /// @brief Moves the info from o to a newly constructed error
    /// handler.
    /// @param o
    /// @fn dg::mpi::info& dg::mpi::info::operator=(dg::mpi::info&& o)
    /// @brief Moves the info from o to this. Destructs any existing
    /// info.
    /// @param o
    /// @return *this
    /// @fn dg::mpi::info& dg::mpi::info::operator=(MPI_Info o)
    /// @brief Wraps a given info
    /// @param o
    /// @return *this
    /// @fn void info::dec()
    /// @internal
    /// @brief used by the raii_base class to manage ownership
    ///

    ///
    /// @class dg::mpi::operation
    /// @brief MPI MPI_Op object wrapper
    /// @typedef dg::mpi::operation::value_type
    /// @brief base MPI operation value type
    /// @typedef dg::mpi::operation::pointer_type
    /// @brief base MPI operation value type
    /// @typedef dg::mpi::operation::const_pointer_type
    /// @brief base MPI operation value type
    /// @typedef dg::mpi::operation::super
    /// @internal
    /// @brief base class type helper
    /// @fn static dg::mpi::operation::value_type dg::mpi::operation::null_value()
    /// @return MPI_ERRHANDLER_NULL
    /// @fn dg::mpi::dg::mpi::operation::operation()
    /// @brief Initializes a null operation
    /// @fn dg::mpi::operation::operation(MPI_Op p)
    /// @brief Wraps a given operation
    /// @param p
    /// @fn dg::mpi::operation::operation(dg::mpi::operation&& o)
    /// @brief Moves the operation from o to a newly constructed error
    /// handler.
    /// @param o
    /// @fn dg::mpi::operation& dg::mpi::operation::operator=(dg::mpi::operation&& o)
    /// @brief Moves the operation from o to this. Destructs any existing
    /// operation.
    /// @param o
    /// @return *this
    /// @fn dg::mpi::operation& dg::mpi::operation::operator=(MPI_Op o)
    /// @brief Wraps a given operation
    /// @param o
    /// @return *this
    /// @fn void operation::dec()
    /// @internal
    /// @brief used by the raii_base class to manage ownership
    ///

    ///
    /// @class dg::mpi::request
    /// @brief MPI MPI_Request object wrapper
    /// @typedef dg::mpi::request::value_type
    /// @brief base MPI request value type
    /// @typedef dg::mpi::request::pointer_type
    /// @brief base MPI request value type
    /// @typedef dg::mpi::request::const_pointer_type
    /// @brief base MPI request value type
    /// @typedef dg::mpi::request::super
    /// @internal
    /// @brief base class type helper
    /// @fn static dg::mpi::request::value_type dg::mpi::request::null_value()
    /// @return MPI_ERRHANDLER_NULL
    /// @fn dg::mpi::dg::mpi::request::request()
    /// @brief Initializes a null request
    /// @fn dg::mpi::request::request(MPI_Request p)
    /// @brief Wraps a given request
    /// @param p
    /// @fn dg::mpi::request::request(dg::mpi::request&& o)
    /// @brief Moves the request from o to a newly constructed error
    /// handler.
    /// @param o
    /// @fn dg::mpi::request& dg::mpi::request::operator=(dg::mpi::request&& o)
    /// @brief Moves the request from o to this. Destructs any existing
    /// request.
    /// @param o
    /// @return *this
    /// @fn dg::mpi::request& dg::mpi::request::operator=(MPI_Request o)
    /// @brief Wraps a given request
    /// @param o
    /// @return *this
    /// @fn void request::dec()
    /// @internal
    /// @brief used by the raii_base class to manage ownership
    ///
    
    ///
    /// @class dg::mpi::window
    /// @brief MPI MPI_Win object wrapper
    /// @typedef dg::mpi::window::value_type
    /// @brief base MPI window value type
    /// @typedef dg::mpi::window::pointer_type
    /// @brief base MPI window value type
    /// @typedef dg::mpi::window::const_pointer_type
    /// @brief base MPI window value type
    /// @typedef dg::mpi::window::super
    /// @internal
    /// @brief base class type helper
    /// @fn static dg::mpi::window::value_type dg::mpi::window::null_value()
    /// @return MPI_ERRHANDLER_NULL
    /// @fn dg::mpi::dg::mpi::window::window()
    /// @brief Initializes a null window
    /// @fn dg::mpi::window::window(MPI_Win p)
    /// @brief Wraps a given window
    /// @param p
    /// @fn dg::mpi::window::window(dg::mpi::window&& o)
    /// @brief Moves the window from o to a newly constructed error
    /// handler.
    /// @param o
    /// @fn dg::mpi::window& dg::mpi::window::operator=(dg::mpi::window&& o)
    /// @brief Moves the window from o to this. Destructs any existing
    /// window.
    /// @param o
    /// @return *this
    /// @fn dg::mpi::window& dg::mpi::window::operator=(MPI_Win o)
    /// @brief Wraps a given window
    /// @param o
    /// @return *this
    /// @fn void window::dec()
    /// @internal
    /// @brief used by the raii_base class to manage ownership
    ///
    
    // clang-format on

    ///
    /// @brief Helper for common datatypes
    /// @tparam T C++ type to find an appropriate MPI datatype object of
    ///
    template <class T>
    struct mpi_type;
  } // namespace mpi
} // namespace dg

#include "detail/mpi_utils.tcc"

#endif
