#ifndef DG_LOG_HPP
#define DG_LOG_HPP

#define BOOST_LOG_DYN_LINK

#include "log/trace_feature.hpp"
#include "log/raw_msg_feature.hpp"
#include "log/severity_levels.hpp"

#include <boost/log/common.hpp>
#include <boost/log/sinks.hpp>
#include <shared_mutex>

namespace dg
{
  ///
  /// @brief Logging information
  ///
  namespace log
  {
    ///
    /// @brief Single threaded logger
    ///
    class logger : public boost::log::sources::basic_composite_logger<char,
                     logger, boost::log::sources::single_thread_model,
                     boost::log::sources::features<
                       boost::log::sources::severity<severity_level>,
                       features::trace_tagger, features::raw_msg_tagger>>
    {
    public:
      ~logger();

      ///
      /// @brief Construct a new logger
      /// @param strm_sev iostream severity filter level
      /// @param log_sev log file severity filter level
      /// @param fname log filename
      ///
      logger(severity_level strm_sev = severity_level::warning,
        severity_level log_sev = severity_level::info,
        const std::string& fname = "");

      /// @brief copy constructor
      logger(const logger&) = default;

      /// @brief move constructor
      logger(logger&&) = default;

      /// @brief copy assignment
      logger& operator=(const logger&) = default;
      /// @brief move assignment
      logger& operator=(logger&&) = default;

    private:
      boost::shared_ptr<boost::log::sinks::sink> log_file;

      boost::shared_ptr<boost::log::sinks::sink> term_strm;
    };

#if 0
    ///
    /// @brief Multi threaded logger
    ///
    class logger_mt
      : public boost::log::sources::basic_composite_logger<char, logger_mt,
          boost::log::sources::multi_thread_model<std::shared_timed_mutex>,
          boost::log::sources::features<
            boost::log::sources::severity<severity_level>,
            features::trace_tagger, features::raw_msg_tagger>>
    {
      BOOST_LOG_FORWARD_LOGGER_MEMBERS_TEMPLATE(logger_mt)
    };
#endif
  }
}

#if defined(__GNUC__) || defined(__clang__)
#define DG_FUNCTION __PRETTY_FUNCTION__
#elif defined(_MSC_VER)
#define DG_FUNCTION __FUNCSIG__
#else
#define DG_FUNCTION __func__
#endif

#define DG_TRACE_IMPL(log_, sev, dg_trace)                                     \
  BOOST_LOG_STREAM_WITH_PARAMS(                                                \
    (log_), (::dg::log::keywords::trace = (dg_trace))(                         \
              ::dg::log::keywords::raw_msg = false)(                           \
              ::boost::log::keywords::severity = (sev)))

///
/// @def DG_TRACE(sev)
/// @brief Logger which prints out trace information including:
///   - file name
///   - line number
///   - time
///   - severity level
/// @param log
/// @param sev severity level
///
#define DG_TRACE(log_, sev)                                                    \
  DG_TRACE_IMPL((log_), (::dg::log::severity_level::sev),                      \
    (__FILE__ ":" BOOST_PP_STRINGIZE(__LINE__)))

///
/// @def DG_FTRACE(sev)
/// @brief Logger which prints out trace information including:
///   - file name
///   - function signature
///   - line number
///   - time
///   - severity level
/// @param log
/// @param sev severity level
///
#define DG_FTRACE(log_, sev)                                                   \
  DG_TRACE_IMPL((log_), (::dg::log::severity_level::sev),                      \
    (std::string(DG_FUNCTION) +                                                \
      (" - " __FILE__ ":" BOOST_PP_STRINGIZE(__LINE__))))

///
/// @def DG_LOG(sev)
/// @brief Logger which prints out basic logging information including:
///   - severity level
///   - time
/// @param log
/// @param sev severity level
///
#define DG_LOG(log_, sev)                                                      \
  BOOST_LOG_STREAM_WITH_PARAMS((log_),                                         \
    (::dg::log::keywords::raw_msg = false)(                                    \
      ::boost::log::keywords::severity = (::dg::log::severity_level::sev)))

///
/// @def DG_INFO(sev)
/// @brief Logger which prints the raw message.
/// @param log
/// @param sev severity level
///
#define DG_INFO(log_, sev)                                                     \
  BOOST_LOG_STREAM_WITH_PARAMS((log_),                                         \
    (::dg::log::keywords::raw_msg = true)(                                     \
      ::boost::log::keywords::severity = (::dg::log::severity_level::sev)))

namespace dg
{
  namespace log
  {
    BOOST_LOG_ATTRIBUTE_KEYWORD(severity, "Severity", severity_level)
  }
}

#endif
