#ifndef DG_ALLOCATOR_BASE_HPP
#define DG_ALLOCATOR_BASE_HPP

#include <memory>

namespace dg
{
  /**
   * @brief Some memory buffer which has been allocated and must be de-allocated
   * by it's associated destructor
   */
  struct mem_block
  {
    void* data_ = nullptr;
    const size_t size = 0;

    mem_block(void* d, size_t s) noexcept : data_(d), size(s)
    {
    }

    mem_block(const mem_block&) noexcept = default;

    mem_block& operator=(const mem_block& o) noexcept
    {
      data_ = o.data_;
      const_cast<size_t&>(size) = o.size;
      return *this;
    }

    template <class T>
    operator T*() noexcept
    {
      return reinterpret_cast<T*>(data_);
    }
  };

  /**
   * @brief Base class for polymorphic allocators
   */
  class allocator_base
  {
  public:
    virtual ~allocator_base() = default;

    /**
     * @brief allocate at least a given number of bytes of memory
     */
    virtual mem_block allocate(size_t bytes) = 0;

    /**
     * @brief deallocate a ptr allocated by this allocator_base
     */
    virtual void deallocate(mem_block b) = 0;
  };

  /**
   * Forwards into std::allocator
   */
  template <class T>
  class std_allocator : public allocator_base
  {
    std::allocator<T> alloc_;

  public:
    mem_block allocate(size_t bytes) override
    {
      bytes = (bytes + sizeof(T) - 1) / sizeof(T);
      mem_block res{alloc_.allocate(bytes), bytes};
      return res;
    }

    void deallocate(mem_block b) override
    {
      alloc_.deallocate(b.data_, b.size);
    }
  };
}

#endif
