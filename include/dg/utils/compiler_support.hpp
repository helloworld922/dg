#ifndef DG_COMPILER_SUPPORT_HPP
#define DG_COMPILER_SUPPORT_HPP

#include <tuple>
#include <utility>
#include <array>
#include <cstdint>
#include <type_traits>

#ifdef __GNUC__
#define DG_UNREACHABLE __builtin_unreachable()
#endif

#ifdef __INTEL_COMPILER
#define DG_UNREACHABLE __builtin_unreachable()
#endif

#ifdef __clang__
#define DG_UNREACHABLE __builtin_unreachable()
#endif

// TODO: what about on Cray's compiler or PGI compiler?
#ifndef DG_UNREACHABLE
#define DG_UNREACHABLE std::terminate()
#endif

namespace dg
{
}

#endif
