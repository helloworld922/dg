#ifndef DG_RAW_MSG_FEATURE_HPP
#define DG_RAW_MSG_FEATURE_HPP

#include <boost/log/common.hpp>

#include <boost/scope_exit.hpp>

namespace dg
{
  namespace log
  {
    namespace keywords
    {
      BOOST_PARAMETER_KEYWORD(tag, raw_msg)
    }

    namespace features
    {
      ///
      /// @brief Prints out the raw log message, with no headers
      ///
      template <typename Base>
      class raw_msg_feature : public Base
      {
      public:
        // Let's import some types that we will need. These imports should be
        // public, in order to allow other features that may derive from
        // record_tagger to do the same.
        
        /// @brief base character type
        typedef typename Base::char_type char_type;
        /// @brief What type of thread locks to use for the log
        typedef typename Base::threading_model threading_model;

        /// Default constructor. Initializes m_Tag to an invalid value.
        raw_msg_feature() = default;

        /// Copy constructor. Initializes m_Tag to a value, equivalent to
        /// that.m_Tag.
        raw_msg_feature(raw_msg_feature const& that) = default;
        /// Forwarding constructor with named parameters
        template <typename ArgsT>
        raw_msg_feature(ArgsT const& args) : Base(args)
        {
        }

        /// The method will require locking, so we have to define locking
        /// requirements for it.
        /// We use the strictest_lock trait in order to choose the most
        /// restricting lock type.
        typedef typename boost::log::strictest_lock<
          boost::lock_guard<threading_model>, typename Base::open_record_lock,
          typename Base::add_attribute_lock,
          typename Base::remove_attribute_lock>::type open_record_lock;

      protected:
        /// Lock-less implementation of operations
        template <typename ArgsT>
        boost::log::record open_record_unlocked(ArgsT const& args)
        {
          // Extract the named argument from the parameters pack
          bool value = args[keywords::raw_msg | false];

          boost::log::attribute_set& attrs = Base::attributes();
          boost::log::attribute_set::iterator tag = attrs.end();

          // Add the tag as a new attribute
          std::pair<boost::log::attribute_set::iterator, bool> res =
            Base::add_attribute_unlocked(
              "RawMsg", boost::log::attributes::constant<bool>(value));
          if (res.second)
          {
            tag = res.first;
          }

          // In any case, after opening a record remove the tag from the
          // attributes
          BOOST_SCOPE_EXIT_TPL((&tag)(&attrs))
          {
            if (tag != attrs.end())
              attrs.erase(tag);
          }
          BOOST_SCOPE_EXIT_END

          // Forward the call to the base feature
          return Base::open_record_unlocked(args);
        }
      };

      struct raw_msg_tagger : public boost::mpl::quote1<raw_msg_feature>
      {
      };
    }
  }
}

#endif
