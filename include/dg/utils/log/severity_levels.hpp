#ifndef DG_SEVERITY_LEVELS_HPP
#define DG_SEVERITY_LEVELS_HPP

namespace dg
{
  namespace log
  {
    enum struct severity_level
    {
      trace = 0,
      debug,
      info,
      warning,
      error,
      fatal,
      quiet
    };
  }
}
#endif
