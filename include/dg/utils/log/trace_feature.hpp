#ifndef DG_TRACE_FEATURE_HPP
#define DG_TRACE_FEATURE_HPP

#include <boost/log/common.hpp>

#include <boost/scope_exit.hpp>

namespace dg
{
  namespace log
  {
    namespace keywords
    {
      BOOST_PARAMETER_KEYWORD(tag, trace)
    }

    namespace features
    {
      ///
      /// @brief Allows a boost logger to print out a simple trace to associate
      /// some point of the source code with log line
      ///
      template <class Base>
      class trace_feature : public Base
      {
      public:
        /// \{
        typedef typename Base::char_type char_type;
        typedef typename Base::threading_model threading_model;

        typedef typename boost::log::strictest_lock<
          boost::lock_guard<threading_model>, typename Base::open_record_lock,
          typename Base::add_attribute_lock,
          typename Base::remove_attribute_lock>::type open_record_lock;

        trace_feature() = default;

        trace_feature(const trace_feature&) = default;

        template <class Args>
        trace_feature(const Args& args) : Base(args)
        {
        }

      protected:
        template <class Args>
        boost::log::record open_record_unlocked(Args const& args)
        {
          // Extract the named argument from the parameters pack
          std::string value = args[keywords::trace | std::string()];

          boost::log::attribute_set& attrs = Base::attributes();
          boost::log::attribute_set::iterator tag = attrs.end();
          if (!value.empty())
          {
            // Add the tag as a new attribute
            std::pair<boost::log::attribute_set::iterator, bool> res =
              Base::add_attribute_unlocked(
                "Trace", boost::log::attributes::constant<std::string>(value));
            if (res.second)
            {
              tag = res.first;
            }
          }

          // In any case, after opening a record remove the tag from the
          // attributes
          BOOST_SCOPE_EXIT_TPL((&tag)(&attrs))
          {
            if (tag != attrs.end())
              attrs.erase(tag);
          }
          BOOST_SCOPE_EXIT_END

          // Forward the call to the base feature
          return Base::open_record_unlocked(args);
        }
        /// \}
      };

      /// \{
      struct trace_tagger : public boost::mpl::quote1<trace_feature>
      {
      };
      /// \}

    }
  }
}

#endif
