#ifndef DG_HDF5_UTILS_HPP
#define DG_HDF5_UTILS_HPP

#include "raii_base.hpp"

#include "view.hpp"

#include "meta.hpp"

#include <hdf5.h>
#include <cassert>

#include <string>
#include <vector>

namespace dg
{
  ///
  /// @brief HDF5 helper utils
  ///
  namespace hdf5
  {
    class file;

    class object;

    class location;

    class attribute;

    class dataset;

    class dataspace;
    class group;

    class plist;

    ///
    /// @brief Any HDF5 identifier
    ///
    class identifier : public raii_base<identifier, hid_t, hid_t, hid_t>
    {
      typedef raii_base<identifier, hid_t, hid_t, hid_t> super;

    public:
      /// @brief base HDF5 identifier type
      typedef hid_t value_type;
      /// @brief pointer type is just the value type
      typedef hid_t pointer_type;
      /// @brief pointer type is just the value type
      typedef hid_t const_pointer_type;

      ///
      /// @brief Decreases reference count
      ///
      void dec();

      ///
      /// @brief Increases reference count
      ///
      void inc();

      /// @brief default constructor
      identifier() noexcept : super(0)
      {
      }

      ///
      /// @param p
      ///
      explicit identifier(hid_t p) noexcept : super(p)
      {
      }

      ///
      /// @param o
      ///
      identifier(const identifier& o) = default;

      ///
      /// @param o
      ///
      identifier(identifier&& o) noexcept = default;

      ///
      /// @param o
      ///
      identifier& operator=(const identifier& o) = default;

      ///
      /// @param o
      ///
      identifier& operator=(identifier&& o) noexcept = default;

      ///
      /// @param d
      ///
      identifier& operator=(hid_t d)
      {
        reset(d);
        return *this;
      }

      ~identifier() = default;
    };

    ///
    /// @brief Base property list type
    ///
    class plist : public identifier
    {
    };

    ///
    /// @brief HDF5 property lists
    ///
    namespace plists
    {
      ///
      /// @brief file access property list
      ///
      class file_access : public identifier
      {
      public:
        using identifier::identifier;

        /// @brief default constructor
        file_access();

        ///
        /// @brief Create a file access property list which uses MPI
        /// @param comm
        /// @param info
        ///
        file_access(MPI_Comm comm, MPI_Info info = MPI_INFO_NULL);

        ///
        /// @brief Get the file access property list for the associated location
        ///
        file_access(const location& loc);

        ///
        /// @brief Sets the MPI connection to use
        /// @param comm
        /// @param info
        ///
        void set_mpio(MPI_Comm comm, MPI_Info info = MPI_INFO_NULL);

        ///
        /// @brief Gets the MPI connection HDF5 will use
        /// @param comm
        /// @param info
        ///
        void get_mpio(MPI_Comm* comm, MPI_Info *info) const;
      };
    }

    ///
    /// @brief A location or an attribute
    ///
    class object : public identifier
    {

    public:
      using identifier::identifier;
      
      ///
      /// @brief Gets the file this object belongs to
      ///
      file get_file() const;

      ///
      /// @brief The path to reach this object
      /// If object is an attribute, then returns path to reach containing object.
      ///
      std::string path() const;
    };

    ///
    /// @brief Some sort of navigable node location in the HDF5 file
    ///
    class location : public object
    {
    public:
      using object::object;

      ///
      /// @brief Determines if a child node with the given name exists already or not
      /// @param name
      ///
      bool exists(const char* name) const;

      ///
      /// @brief Determines if a child node with the given name exists already or not
      /// @param name
      /// @param link_access link access property list to use
      ///
      bool exists(const char* name, const plist& link_access) const;

      ///
      /// @brief Determines if this location has a given attribute or not
      /// @param name
      ///
      bool has_attribute(const char* name) const;
    };

    ///
    /// @brief Some sort of HDF5 dataspace
    ///
    class dataspace : public identifier
    {
      dataspace(hid_t id) : identifier(id)
      {}
      
    public:
      
      ///
      /// @brief Initialize a contiguous hyperslab dataspace
      ///
      template <class... Ts, class = typename std::enable_if<meta::all<
                               std::is_integral<Ts>::value...>::value>::type>
      static dataspace simple(Ts... dims);

      ///
      /// @brief Initialize a contiguous hyperslab dataspace
      ///
      template <size_t N, class T,
        class = typename std::enable_if<!std::is_same<T, hsize_t>::value>::type>
      static dataspace simple(const extents<N, T>& e);

      ///
      /// @brief Initialize a contiguous hyperslab dataspace
      ///
      template <size_t N>
      static dataspace simple(const extents<N, hsize_t>& e);

      /// @brief Number of dimensions of this dataspace
      int ndims() const
      {
        return H5Sget_simple_extent_ndims(get());
      }

      // TODO: replace with some kind of small vector
      /// @brief Gets the shape of this dataspace
      std::vector<hsize_t> shape() const;

      ///
      /// @brief Selects a hyperslab section
      /// @param start
      /// @param count
      /// @param stride
      /// @param block
      /// @param op One of:
      ///   - H5S_SELECT_SET
      ///   - H5S_SELECT_OR
      ///   - H5S_SELECT_AND
      ///   - H5S_SELECT_XOR
      ///   - H5S_SELECT_NOTB
      ///   - H5S_SELECT_NOTA
      ///
      void select_hyperslab(const hsize_t* start, const hsize_t* count,
        const hsize_t* stride, const hsize_t* block,
        H5S_seloper_t op = H5S_SELECT_SET);
      
      ///
      /// @brief Selects a hyperslab section
      /// @param start
      /// @param count
      /// @param op One of:
      ///   - H5S_SELECT_SET
      ///   - H5S_SELECT_OR
      ///   - H5S_SELECT_AND
      ///   - H5S_SELECT_XOR
      ///   - H5S_SELECT_NOTB
      ///   - H5S_SELECT_NOTA
      ///
      void select_hyperslab(const hsize_t* start, const hsize_t* count,
        H5S_seloper_t op = H5S_SELECT_SET)
      {
        select_hyperslab(start, count, nullptr, nullptr, op);
      }

      ///
      /// @brief Selects a hyperslab section
      /// @param start
      /// @param count
      /// @param stride
      /// @param op One of:
      ///   - H5S_SELECT_SET
      ///   - H5S_SELECT_OR
      ///   - H5S_SELECT_AND
      ///   - H5S_SELECT_XOR
      ///   - H5S_SELECT_NOTB
      ///   - H5S_SELECT_NOTA
      ///
      void select_hyperslab(const hsize_t* start, const hsize_t* count,
        const hsize_t* stride, H5S_seloper_t op = H5S_SELECT_SET)
      {
        select_hyperslab(start, count, stride, nullptr, op);
      }
    };
    
    ///
    /// @brief represents an HDF5 file
    ///
    class file : public location
    {
    public:
      ///
      /// @brief Create or open the given HDF5 file. Does not use MPI
      /// @param fname filename
      /// @param flags flags to use for opening/creating the file
      ///
      file(const char* fname, unsigned flags);

      ///
      /// @brief Create or open the given HDF5 file
      /// @param fname filename
      /// @param flags flags to use for opening/creating the file
      /// @param access file access property list
      ///
      file(const char* fname, unsigned flags, const plists::file_access& access);

      ///
      /// @brief Gets the file associated with a given object
      ///
      file(const object& o);

      ///
      /// @brief The filename
      ///
      std::string name() const;

      ///
      /// @brief Flushes the current contents of the file
      ///
      void flush() const;

      using location::location;
    };

    /// @brief An HDF5 group
    class group : public location
    {
    public:
      ///
      /// @brief Creates or opens the given group
      /// @param parent
      /// @param path to node to create/open
      ///
      group(const location& parent, const char* path);
    };

    /// @brief An HDF5 attribute
    class attribute : public object
    {
    public:
      ///
      /// @brief Opens an an attribute specified by a location and name
      /// @param loc
      /// @param name
      ///
      attribute(const location& loc, const char* name);

      ///
      /// @brief create a new attribute
      /// @param loc
      /// @param name name of the attribute to create
      /// @param space
      /// @tparam T type of data the attribute holds
      ///
      template<class T>
      attribute(const location& loc, const char* name, const dataspace& space);

      ///
      /// @brief Creates an attribute and write the data specified by a view v
      /// @param loc
      /// @param name
      /// @param v A simple contiguous view (i.e. one to one mapping of elements). Note that no checks are made to ensure that the data is contiguous or simple.
      ///
      template<class T, size_t N, class L>
      attribute(const location& loc, const char* name, const view<T, N, L>& v);

      ///
      /// @brief create a new attribute
      /// @param loc
      /// @param name name of the attribute to create
      /// @param type hdf5 datatype this attribute holds
      /// @param space
      ///
      attribute(const location& loc, const char* name, hid_t type, const dataspace& space);

      ///
      /// @brief Writes a given view to this attribute
      /// @param v a contiguous simple view
      ///
      template<class T, size_t N, class L>
      void write(const view<T, N, L>& v);

      ///
      /// @brief Reads the attribute data into a given view
      /// @param v a contiguous simple view
      ///
      template<class T, size_t N, class L>
      void read(view<T, N, L>& v) const;
    };

    ///
    /// @brief An HDF5 dataset
    ///
    class dataset : public location
    {
    public:
      ///
      /// @brief
      /// @param parent
      /// @param path
      ///
      dataset(const location& parent, const char* path);
      
      ///
      /// @brief
      /// @param parent
      /// @param path
      /// @param access
      ///
      dataset(const location& parent, const char* path, const plist& access);

      ///
      /// @brief Creates a new dataset with the given type and size
      /// @param parent
      /// @param path
      /// @param type
      /// @param space
      ///
      dataset(const location& parent, const char* path, hid_t type, const dataspace& space);

      ///
      /// Write a view to the dataset. Assumes filespace is H5S_ALL
      /// @param v
      ///
      template<class T, size_t N, class L>
      void write(const view<T, N, L>& v);

      ///
      /// Reads the dataset into the given view. Assumes filespace is H5S_ALL
      /// @param v
      ///
      template<class T, size_t N, class L>
      void read(view<T, N, L>& v) const;
      
      ///
      /// Write a view to the target location
      /// @param v
      /// @param target
      ///
      template<class T, size_t N, class L>
      void write(const view<T, N, L>& v, const dataspace& target);

      ///
      /// Reads a view from the src location
      /// @param v view to read into
      /// @param src soure dataspace
      ///
      template<class T, size_t N, class L>
      void read(view<T, N, L>& v, const dataspace& src) const;
    };
  }
}

#include "detail/hdf5_utils.tcc"

#endif
