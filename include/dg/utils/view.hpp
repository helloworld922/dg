#ifndef DG_VIEW_HPP
#define DG_VIEW_HPP

///
/// @file
/// @brief based on Kokkos View/proposed std::array_ref
///

#include "indexer.hpp"

namespace dg
{
  ///
  /// @brief Multi-dimensional array view of a chunk of memory
  /// @tparam T
  /// @tparam ndims
  /// @tparam Layout
  ///
  template <class T, size_t ndims, class Layout = layout_right>
  class view : public indexer<ndims, Layout>
  {
    T* ptr_ = nullptr;

    typedef indexer<ndims, Layout> super_type;

  public:
    /// @brief value type
    typedef T value_type;
    /// @brief shape type
    typedef extents<ndims> extents_type;
    /// @brief layout type
    typedef Layout layout_type;
    /// @brief size type of the underlying layout
    typedef typename Layout::size_type size_type;

    /// @brief default constructor
    constexpr view() = default;
    /// @brief copy constructor
    constexpr view(const view& o) = default;
    /// @brief move constructor
    constexpr view(view&& o) = default;
    /// @brief copy assignment
    view& operator=(const view& o) = default;
    /// @brief move assignment
    view& operator=(view&& o) = default;

    ///
    /// @brief
    /// @param data pointer to underlying storage
    /// @param exts extents to view data as
    /// @tparam Ds
    ///
    template <class Ls = Layout,
      class =
        typename std::enable_if<std::is_default_constructible<Ls>::value>::type>
    constexpr view(T* data, const dg::extents<ndims>& exts) noexcept
      : super_type(exts), ptr_(data)
    {
    }

    ///
    /// @brief
    /// @param data pointer to underlying storage
    /// @param idcs size of each dimension
    ///
    template <class... Idcs, class Ls = Layout,
      class =
        typename std::enable_if<std::is_default_constructible<Ls>::value>::type,
      class = typename std::enable_if<
        meta::all<std::is_integral<Idcs>::value...>::value &&
        sizeof...(Idcs) == ndims>::type>
    constexpr view(T* data, Idcs... idcs) noexcept
      : super_type(idcs...), ptr_(data)
    {
    }

    ///
    /// @brief
    /// @param data pointer to underlying storage
    /// @param layout layout to use for indexing into data
    /// @param idcs size of each dimension
    ///
    template <class Ls, class... Idcs,
      class = typename std::enable_if<
        !std::is_integral<typename std::decay_t<Ls>>::value>::type,
      class = typename std::enable_if<
        meta::all<std::is_integral<Idcs>::value...>::value &&
        sizeof...(Idcs) == ndims>::type>
    constexpr view(T* data, Ls&& layout, Idcs... idcs) noexcept
      : super_type(std::forward<Ls>(layout), idcs...), ptr_(data)
    {
    }

    ///
    /// @brief
    /// @param data pointer to underlying storage
    /// @param exts extents to view data as
    /// @param layout layout to use for indexing into data
    /// @tparam Ds
    ///
    template <class Ls, class = typename std::enable_if<
                          !std::is_integral<std::decay_t<Ls>>::value>::type>
    constexpr view(
      T* data, const dg::extents<ndims>& exts, Ls&& layout) noexcept
      : super_type(exts, std::forward<Ls>(layout)), ptr_(data)
    {
    }

    using super_type::extent;
    using super_type::layout;
    using super_type::rank;
    using super_type::shape;
    using super_type::size;

    /// @brief Gets the value at a specified indices. Only defined in 1D
    template <class I, class Ext = extents_type>
    typename std::enable_if<1 == Ext::rank(), T&>::type operator[](
      I idx) noexcept
    {
      return ptr_[layout()(shape(), idx)];
    }

    /// @brief Gets the value at a specified indices. Only defined in 1D
    template <class I, class Ext = extents_type>
    typename std::enable_if<1 != Ext::rank(), T&>::type operator[](
      I idx) noexcept
    {
      static_assert(
        1 == extents_type::rank(), "operator[] only supported for 1D views");
      return std::declval<T>();
    }

    /// @brief Gets the value at a specified indices. Only defined in 1D
    template <class I, class Ext = extents_type>
    typename std::enable_if<1 == Ext::rank(), const T&>::type operator[](
      I idx) const noexcept
    {
      return ptr_[layout()(shape(), idx)];
    }

    /// @brief Gets the value at a specified indices. Only defined in 1D
    template <class I, class Ext = extents_type>
    typename std::enable_if<1 != Ext::rank(), const T&>::type operator[](
      I idx) const noexcept
    {
      static_assert(1 == Ext::rank(), "operator[] only supported for 1D views");
      return std::declval<T>();
    }

    /// @brief Gets the value at a specified indices
    template <class... Idcs>
    typename std::enable_if<sizeof...(Idcs) != extents_type::rank(),
      const T&>::type
    operator()(Idcs... idcs) noexcept
    {
      static_assert(sizeof...(idcs) == extents_type::rank(),
        "wrong number of indices given");
      return std::declval<T>();
    }

    /// @brief Gets the value at a specified indices
    template <class... Idcs>
    typename std::enable_if<sizeof...(Idcs) == extents_type::rank(), T&>::type
    operator()(Idcs... idcs) noexcept
    {
      return ptr_[layout()(shape(), idcs...)];
    }

    /// @brief Gets the value at a specified indices
    template <class... Idcs>
    constexpr typename std::enable_if<sizeof...(Idcs) != extents_type::rank(),
      const T&>::type
    operator()(Idcs... idcs) const noexcept
    {
      static_assert(sizeof...(idcs) == extents_type::rank(),
        "wrong number of indices given");
      return std::declval<T>();
    }

    /// @brief Gets the value at a specified indices
    template <class... Idcs>
    constexpr typename std::enable_if<sizeof...(Idcs) == extents_type::rank(),
      const T&>::type
    operator()(Idcs... idcs) const noexcept
    {
      return ptr_[layout()(shape(), idcs...)];
    }

    /// @brief Gets the value at a specified index
    /// @param idcs container of all indices
    template <class Idcs>
    constexpr const T& get(const Idcs& idcs) const noexcept
    {
      static_assert(
        std::tuple_size<Idcs>::value == ndims, "Invalid number of indices");
      return ptr_[meta::explode_args<std::make_index_sequence<
        std::tuple_size<Idcs>::value>>::invoke(layout(), idcs, shape())];
    }

    /// @brief Gets the value at a specified index
    /// @param idcs container of all indices
    template <class Idcs>
    T& get(const Idcs& idcs) noexcept
    {
      return ptr_[meta::explode_args<std::make_index_sequence<
        std::tuple_size<Idcs>::value>>::invoke(layout(), idcs, shape())];
    }

    ///
    /// @brief Get the underlying data storage pointer
    ///
    T*& data() noexcept
    {
      return ptr_;
    }

    ///
    /// @brief Get the underlying data storage pointer
    ///
    constexpr T const* data() const noexcept
    {
      return ptr_;
    }

    ///
    /// @brief Creates a sub-array based on slices in each dimension. If a
    /// SliceSpecifier is an integral value, that dimension is fixed in the
    /// resulting sub-array.
    /// @param slice dg::all selects the entire extent of the associated
    /// dimension.
    /// @tparam SliceSpecifiers
    ///
    template <class... SliceSpecifiers>
    constexpr view<
      typename std::enable_if<
        sizeof...(SliceSpecifiers) == extents_type::rank(), T>::type,
      slice_creator<Layout, extents_type,
        std::decay_t<SliceSpecifiers>...>::type::extents_type::rank(),
      typename slice_creator<Layout, extents_type,
        std::decay_t<SliceSpecifiers>...>::type>
    subarray(SliceSpecifiers&&... slice) const
    {
      typedef slice_creator<Layout, extents_type,
        std::decay_t<SliceSpecifiers>...>
        creator_type;

      typedef typename creator_type::type sliced_layout_type;

      return view<T, sliced_layout_type::extents_type::rank(),
        sliced_layout_type>(ptr_,
        creator_type::make_extents(layout(), shape(), slice...),
        creator_type::create(
          layout(), shape(), std::forward<SliceSpecifiers>(slice)...));
    }

    ///
    /// @brief Creates a sub-array based on slices in each dimension. If a
    /// SliceSpecifier is an integral value, that dimension is fixed in the
    /// resulting sub-array.
    /// @param slice dg::all selects the entire extent of the associated
    /// dimension.
    /// @tparam SliceSpecifiers
    ///
    template <class... SliceSpecifiers>
    constexpr typename std::enable_if<
      sizeof...(SliceSpecifiers) != extents_type::rank(), int>::type
    subarray(SliceSpecifiers&&... slice) const
    {
      static_assert(sizeof...(SliceSpecifiers) == extents_type::rank(),
        "wrong number of slices given");
      return 0;
    }
  };

  ///
  /// @brief Makes a view based off of an indexer
  ///
  template <class T, size_t ndims, class Layout>
  view<T, ndims, Layout> make_view(T* ptr, const indexer<ndims, Layout>& idxr)
  {
    return view<T, ndims, Layout>(ptr, idxr.shape(), idxr.layout());
  }

  ///
  /// @brief Helper for creating a view_sequence
  ///
  template <class... Vs>
  auto make_view_sequence(Vs&&... views);

  ///
  /// @brief Helper for creating a view_array
  ///
  template <class V, class... Vs>
  auto make_view_array(V&& v, Vs&&... views);

  ///
  /// @brief A collection of view-like types
  ///
  template <class... Vs>
  class view_sequence : public indexer_sequence<Vs...>
  {
  public:
    typedef typename std::tuple_element<0, std::tuple<Vs...>>::type::value_type
      value_type;

    using indexer_sequence<Vs...>::indexers;
    using indexer_sequence<Vs...>::get;

    /// copy constructor
    view_sequence(const view_sequence& v) = default;
    /// move constructor
    view_sequence(view_sequence&&) = default;

    /// copy assignment
    view_sequence& operator=(const view_sequence&) = default;
    /// move assignment
    view_sequence& operator=(view_sequence&&) = default;

    ///
    /// @brief Helper for constructing an indexer_sequence
    /// @param indexers
    ///
    template <class V, class... Vss,
      class = std::enable_if_t<
        sizeof...(Vss) != 0 ||
        !std::is_same<std::decay_t<V>, view_sequence<Vs...>>::value>>
    view_sequence(V&& v, Vss&&... views)
      : indexer_sequence<Vs...>(std::forward<V>(v), std::forward<Vss>(views)...)
    {
    }

    ///
    /// @brief Slices all held views in lock-step
    ///
    template <class... SliceSpecifiers>
    auto subarray(SliceSpecifiers&&... slices) const
    {
      return meta::explode_args<std::make_index_sequence<sizeof...(Vs)>>::
        invoke(
          [&slices...](auto&&... views) {
            return make_view_sequence(views.subarray(slices...)...);
          },
          indexers);
    }
  };

  template <class V, size_t N>
  class view_array
  {
  public:
    typedef V value_type;
    std::array<V, N> indexers;

    template <class... Vs>
    view_array(Vs&&... vs) : indexers({std::forward<Vs>(vs)...})
    {
    }

    view_array(const view_array& v) = default;
    view_array(view_array&& v) = default;

    constexpr static size_t size() noexcept
    {
      return N;
    }

    template <size_t Idx>
    auto& get()
    {
      return indexers[Idx];
    }

    template <size_t Idx>
    const auto& get() const
    {
      return indexers[Idx];
    }

    auto& operator[](size_t idx)
    {
      return indexers[idx];
    }

    const auto& operator[](size_t idx) const
    {
      return indexers[idx];
    }

    auto& at(size_t idx)
    {
      return indexers.at(idx);
    }

    const auto& at(size_t idx) const
    {
      return indexers.at(idx);
    }

    ///
    /// @brief Slices all held views in lock-step
    ///
    template <class... SliceSpecifiers>
    auto subarray(SliceSpecifiers&&... slices) const
    {
      typedef decltype(indexers[0].subarray(slices...)) sub_value_type;
      return meta::explode_args<std::make_index_sequence<N>>::invoke(
        [&slices...](auto&&... views) {
          return make_view_array(views.subarray(slices...)...);
        },
        indexers);
    }
  };

  ///
  /// @brief Helper for extracting an indexer from a view_array
  ///
  template <size_t I, class V, size_t N>
  auto& get(view_array<V, N>& v)
  {
    return v.template get<I>();
  }

  ///
  /// @brief Helper for extracting an indexer from a view_array
  ///
  template <size_t I, class V, size_t N>
  auto& get(const view_array<V, N>& v)
  {
    return v.template get<I>();
  }
} // namespace dg

#include "detail/view.tcc"

#endif
