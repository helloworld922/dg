#ifndef DG_RAII_HPP
#define DG_RAII_HPP

#include <utility>

namespace dg
{
  /// \cond DEV
  namespace detail
  {
    template <class C, class Signature> struct has_increment : std::false_type
    {
    };

    template <class C, class Ret, class... Args>
    struct has_increment<C, Ret(Args...)>
    {
    private:
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundefined-inline"
      template <class T>
      static constexpr auto check(T*) -> typename std::is_same<
          decltype(std::declval<T>().inc(std::declval<Args>()...)), Ret>::type;

      template <typename> static constexpr std::false_type check(...);
#pragma clang diagnostic pop

      typedef decltype(check<C>(0)) type;

    public:
      /// @brief True if there is an inc member function
      static constexpr bool value = type::value;
    };

    template <class C, class Signature> struct has_null_value : std::false_type
    {
    };

    template <class C, class Ret, class... Args>
    struct has_null_value<C, Ret(Args...)>
    {
    private:
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundefined-inline"
      template <class T>
      static constexpr auto check(T*) -> typename std::is_same<
          decltype(T::null_value(std::declval<Args>()...)), Ret>::type;

      template <typename> static constexpr std::false_type check(...);
#pragma clang diagnostic pop

      typedef decltype(check<C>(0)) type;

    public:
      /// @brief True if there is a specified null value
      static constexpr bool value = type::value;
    };

    template <class Base, class PtrType,
        bool = has_null_value<Base, PtrType()>::value>
    struct extract_null
    {
      /// @brief returns the null value type
      constexpr static PtrType value() noexcept
      {
        // false case
        return PtrType();
      }
    };

    template <class Base, class PtrType>
    struct extract_null<Base, PtrType, true>
    {
      /// @brief returns the null value type
      static PtrType value() noexcept
      {
        // true case
        return Base::null_value();
      }
    };
  }
  /// \endcond

  ///
  /// @brief Resource acquisition is initialization wrapper
  /// The Base classes should implement at least a member function void dec()
  /// which
  /// decreases the reference count of the held object (deleting it when the
  /// count reaches zero). Note that for types which are not reference
  /// countable, this function can directly delete the held object.
  /// For held types which implement reference counting via some other
  /// mechanism, the Base class
  /// should implement a function void inc() which increases the reference count
  /// of the held object.
  /// @tparam Base
  /// @tparam T the raw value type which this wrapper is dereferenced to
  /// @tparam PtrType what is actually stored by this class
  /// @tparam CPtrType constant version of PtrType
  ///
  template <class Base, class T = typename Base::value_type,
      class PtrType = typename Base::pointer_type,
      class CPtrType = typename Base::const_pointer_type,
      bool = dg::detail::has_increment<Base, void()>::value>
  class raii_base
  {
    PtrType d_;

    raii_base(const raii_base&) = delete;
    raii_base& operator=(const raii_base&) = delete;

  protected:
    /// \{
    ~raii_base()
    {
      static_cast<Base&>(*this).dec();
    }

    raii_base(raii_base&& o) noexcept : d_(std::move(o.d_))
    {
      o.d_ = detail::extract_null<Base, PtrType>::value();
    }

    raii_base& operator=(raii_base&& o) noexcept
    {
      if (&o != this)
      {
        swap(static_cast<Base&>(o));
      }
      return *this;
    }
    /// \}

  public:
    ///
    /// @brief Initializes an raii_base which holds nothing
    ///
    raii_base() : d_(dg::detail::extract_null<Base, PtrType>::value())
    {}

    ///
    /// @brief Swap the value held by two raii_base objects
    ///
    void swap(Base& o) noexcept
    {
      auto tmp = d_;
      d_ = o.d_;
      o.d_ = tmp;
    }

    ///
    /// @brief Initializes an raii_base with the given value
    /// @param d
    ///
    raii_base(PtrType d) noexcept : d_(d)
    {
    }

    ///
    /// @brief Releases ownership of the held object and returns it's value.
    /// Note that the caller assumes ownership of the returned value.
    /// Internally held value is set to a null value.
    ///
    PtrType release()
    {
      auto res = d_;
      d_ = detail::extract_null<Base, PtrType>::value();
      return res;
    }
    
    ///
    /// @brief Releases ownership of the held object and returns it's value.
    /// Note that the caller assumes ownership of the returned value.
    /// Internally held value is set to a given value.
    /// @param val
    ///
    PtrType release(PtrType val)
    {
      auto res = d_;
      d_ = val;
      return res;
    }

    ///
    /// @brief Deletes the internally held value, optionally setting the held
    /// value to a new value (default is a null value).
    /// @param p
    /// @return A reference to the internal
    /// storage spot so the reset value can be set externally (for C APIs which
    /// expect to be passed an output pointer parameter)
    ///
    PtrType& reset(PtrType p = detail::extract_null<Base, PtrType>::value())
    {
      static_cast<Base&>(*this).dec();
      d_ = p;
      return d_;
    }

    ///
    /// @brief Returns true if something is held
    ///
    explicit operator bool()
    {
      return d_ != detail::extract_null<Base, PtrType>::value();
    }

    ///
    /// @brief Returns the internally held value
    ///
    operator PtrType() noexcept
    {
      return d_;
    }

    ///
    /// @brief Returns the internally held value
    ///
    operator CPtrType() const noexcept
    {
      return d_;
    }

    ///
    /// @brief Dereferences the internally held value
    ///
    T& operator*()
    {
      return *d_;
    }

    ///
    /// @brief Dereferences the internally held value
    ///
    const T& operator*() const
    {
      return *d_;
    }

    ///
    /// @brief Gets the internally held value
    ///
    PtrType get() noexcept
    {
      return d_;
    }

    ///
    /// @brief Gets the internally held value
    ///
    CPtrType get() const noexcept
    {
      return d_;
    }
  };

  ///
  /// @brief Resource acquisition is initialization wrapper specialization for
  /// objects which support native reference counting.
  ///
  template <class Base, class T, class PtrType, class CPtrType>
  class raii_base<Base, T, PtrType, CPtrType, true>
      : public raii_base<Base, T, PtrType, CPtrType, false>
  {
    typedef raii_base<Base, T, PtrType, CPtrType, false> super;

  protected:
    ///
    /// @brief Initializes an raii_base which holds nothing
    ///
    raii_base() noexcept = default;

    ///
    /// @brief Initializes an raii_base with the given value
    /// @param d
    ///
    raii_base(PtrType d) noexcept : super(d)
    {
    }

    ///
    /// @brief Copies the value held by o, and increments the reference count
    /// for shared ownership.
    /// @param o
    ///
    raii_base(const raii_base& o) noexcept : super(o.get())
    {
      static_cast<Base&>(*this).inc();
    }

    ///
    /// @brief Copies the value held by o, and increments the reference count
    /// for shared ownership. Releases the previously held value.
    /// @param o
    /// @return *this
    ///
    raii_base& operator=(const raii_base& o) noexcept
    {
      if (&o != this)
      {
        reset(o.get());
        Base::inc();
      }
      return *this;
    }
  };
}

#endif
