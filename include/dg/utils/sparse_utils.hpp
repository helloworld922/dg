#ifndef DG_SPARSE_UTILS_HPP
#define DG_SPARSE_UTILS_HPP

#include <utility>

namespace dg
{
  ///
  /// @brief A sparse matrix builder.
  /// @tparam Instrs value "tuplets" which describe how to construct this
  /// matrix.
  /// @tparam RowIdxr indexer which translates a multi-dimensional index into a
  /// flattened row
  /// @tparam ColIdxr indexer which translates a multi-dimensional index into a
  /// flattened row
  ///
  template <class Instrs, class RowIdxr, class ColIdxr>
  class sparse_mat_builder_base
  {
  public:
    Instrs& instrs;
    RowIdxr rows;
    ColIdxr cols;

    template <class R, class C>
    sparse_mat_builder_base(Instrs& instrs, R&& rows, C&& cols)
      : instrs(instrs), rows(std::forward<R>(rows)), cols(std::forward<C>(cols))
    {}

    ///
    /// @brief Inserts a new value
    ///
    template <class V, class Ts, class Us>
    void insert(Ts&& row_idcs, Us&& col_idcs, V&& value);

    ///
    /// @brief
    ///
    template <class Ts, class Us>
    auto block(Ts&& row_slices, Us&& col_slices);

    auto height() const noexcept
    {
      return rows.size();
    }

    auto width() const noexcept
    {
      return cols.size();
    }
  };

  template <class Instrs, class RowIdxr, class ColIdxr>
  class sparse_mat_builder : public sparse_mat_builder_base<Instrs, RowIdxr, ColIdxr>
  {
    typedef sparse_mat_builder_base<Instrs, RowIdxr, ColIdxr> super_type;
    Instrs storage_;
  public:
    template <class R, class C>
    sparse_mat_builder(R&& rows, C&& cols)
      : super_type(storage_, std::forward<R>(rows), std::forward<C>(cols))
    {}
  };
}

#include "detail/sparse_utils.tcc"

#endif
