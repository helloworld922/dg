#ifndef DG_VIEW_UTILS_HPP
#define DG_VIEW_UTILS_HPP

///
/// @file
/// @brief Various helpful utils for view
///

#include "meta.hpp"

#include "dg/utils/layouts.hpp"

#include "detail/view_utils_pre.tcc"

namespace dg
{
  ///
  /// @brief Helper utilities
  ///
  namespace util
  {
    ///
    /// @brief A simple N-D iterator which allows tracking a multi-dimension
    /// index
    /// @tparam Extent
    /// @tparam Layout
    ///
    template <class Extent, class Layout>
    class iterator;

    ///
    /// Specialization for layout_right
    ///
    template <class Extent>
    class iterator<Extent, dg::layout_right>
      : public detail::iterator_base<Extent>
    {
      typedef detail::iterator_base<Extent> super;

    public:
      ///
      /// @brief Initializes an iterator with a given extent
      /// @param e
      ///
      iterator(const Extent& e) : super(e)
      {
      }

      /// \{
      iterator(const iterator&) = default;
      iterator& operator=(const iterator&) = default;
      ~iterator() = default;
      /// \}

      ///
      /// @brief Advances the N-D index in dimension n
      ///
      void advance(size_t n) noexcept
      {
        ++super::idcs[n];
        for (size_t j = n; j > 0 && super::idcs[j] >= super::ext_[j]; --j)
        {
          ++super::idcs[j - 1];
          super::idcs[j] = 0;
        }
      }

      ///
      /// @brief Determines if the N-D index has advanced past the end of the
      /// extents
      /// @return true if index held is still in range
      ///
      constexpr operator bool() const noexcept
      {
        return super::idcs[0] < super::ext_[0];
      }
    };

    ///
    /// Specialization for layout_left
    ///
    template <class Extent>
    class iterator<Extent, dg::layout_left>
      : public detail::iterator_base<Extent>
    {
      typedef detail::iterator_base<Extent> super;

    public:
      ///
      /// @brief Initializes an iterator with a given extent
      /// @param e
      ///
      constexpr iterator(const Extent& e) : detail::iterator_base<Extent>(e)
      {
      }

      /// \{
      iterator(const iterator&) = default;
      iterator& operator=(const iterator&) = default;
      ~iterator() = default;
      /// \}

      ///
      /// @brief Advances the N-D index in dimension n
      ///
      void advance(size_t n) noexcept
      {
        ++super::idcs[n];
        for (size_t j = n;
             j < Extent::rank() - 1 && super::idcs[j] >= super::ext_[j]; ++j)
        {
          ++super::idcs[j + 1];
          super::idcs[j] = 0;
        }
      }

      ///
      /// @brief Determines if the N-D index has advanced past the end of the
      /// extents
      /// @return true if index held is still in range
      ///
      constexpr operator bool() const noexcept
      {
        return super::idcs[Extent::rank() - 1] <
               super::ext_[Extent::rank() - 1];
      }
    };

#ifndef DG_DOC
    namespace detail
    {
      template <class R>
      struct transform_apply
      {
        // base case: assume view-like type
        template <class F, class R2>
        static void apply(F&& f, R2&& r)
        {
          // TODO: proper specialization for layout
          iterator<typename std::decay_t<R2>::extents_type, dg::layout_right>
            idcs(r.shape());
          for (; idcs;)
          {
            f(r.get(idcs.idcs));
            idcs.advance(std::decay_t<R2>::rank() - 1);
          }
        }
      };

      // specialize transform_apply for view_sequence
      template <class V, class Seq>
      struct transform_apply_impl
      {
        // base case: do nothing
        template <class F, class R2>
        static void apply(F&& f, R2&& r)
        {
        }
      };

      template <class... Vs, size_t V0, size_t... Idcs>
      struct transform_apply_impl<dg::view_sequence<Vs...>,
        std::index_sequence<V0, Idcs...>>
      {
        template <class F, class R2>
        static void apply(F&& f, R2&& r)
        {
          transform_apply<std::decay_t<decltype(dg::get<V0>(r))>>::apply(
            f, dg::get<V0>(r));
          transform_apply_impl<dg::view_sequence<Vs...>,
            std::index_sequence<Idcs...>>::apply(f, r);
        }
      };

      template <class... Vs>
      struct transform_apply<dg::view_sequence<Vs...>>
      {
        template <class F, class R2>
        static void apply(F&& f, R2&& r)
        {
          transform_apply_impl<dg::view_sequence<Vs...>,
            std::make_index_sequence<sizeof...(Vs)>>::apply(f, r);
        }
      };

      template <class V>
      struct is_view_sequence : public std::integral_constant<bool, false>
      {
      };

      template <class... Vs>
      struct is_view_sequence<dg::view_sequence<Vs...>>
        : public std::integral_constant<bool, true>
      {
      };

      template <bool any_seqs, class... Rs>
      struct transform_nd_apply
      {
        // base case: assume all are view-like types
        template <class F, class R2, class... R2s>
        static void apply(F&& f, R2&& r, R2s&&... rs)
        {
          // TODO: proper specialization for layout
          iterator<typename std::decay_t<R2>::extents_type, dg::layout_right>
            idcs(r.shape());
          for (; idcs;)
          {
            f(r.get(idcs.idcs), rs.get(idcs.idcs)...);
            idcs.advance(std::decay_t<R2>::rank() - 1);
          }
        }
      };
      
      // specialize transform_nd_apply for view_sequence
      template <class V, class Seq>
      struct transform_nd_apply_impl
      {
        // base case: do nothing
        template <class F, class R2>
        static void apply(F&& f, R2&& r)
        {
        }
      };

      template <class... Vs, size_t V0, size_t... Idcs>
      struct transform_nd_apply_impl<dg::view_sequence<Vs...>,
        std::index_sequence<V0, Idcs...>>
      {
        template <class F, class R2, class... R2s>
        static void apply(F&& f, R2&& r, R2s&&... rs)
        {
          transform_nd_apply<
            detail::is_view_sequence<decltype(dg::get<V0>(r))>::value,
            std::decay_t<decltype(dg::get<V0>(r))>>::apply(f, dg::get<V0>(r),
            dg::get<V0>(rs)...);
          transform_nd_apply_impl<dg::view_sequence<Vs...>,
            std::index_sequence<Idcs...>>::apply(f, r, rs...);
        }
      };

      template <class... Vs, class... Vrems>
      struct transform_nd_apply<true, dg::view_sequence<Vs...>, Vrems...>
      {
        template <class F, class R2, class... R2s>
        static void apply(F&& f, R2&& r, R2s&&... rs)
        {
          transform_nd_apply_impl<dg::view_sequence<Vs...>,
            std::make_index_sequence<sizeof...(Vs)>>::apply(std::forward<F>(f),
            std::forward<R2>(r), std::forward<R2s>(rs)...);
        }
      };

      template <class V, size_t N, class... Vrems>
      struct transform_nd_apply<true, dg::view_array<V, N>, Vrems...>
      {
        template <class F, class R2, class... R2s>
        static void apply(F&& f, R2&& r, R2s&&... rs)
        {
          transform_nd_apply_impl<dg::view_array<V, N>,
            std::make_index_sequence<N>>::apply(std::forward<F>(f),
            std::forward<R2>(r), std::forward<R2s>(rs)...);
        }
      };
    } // namespace detail
#endif

    ///
    /// @brief apply a transformation function to each element of a view or
    /// view_sequence
    ///
    template <class V, class F>
    void transform(V&& seq, F&& fun)
    {
      detail::transform_apply<std::decay_t<V>>::apply(
        std::forward<F>(fun), std::forward<V>(seq));
    }

    ///
    /// @brief Apply a transform function by iterating through several sequences
    /// in lock-step.
    /// @param fun
    /// @param seqs All sequences must have the same shape
    /// @tparam F
    /// @tparam Vs
    ///
    template <class F, class... Vs>
    void nd_transform(F&& fun, Vs&&... seqs)
    {
      detail::transform_nd_apply<
        dg::meta::any<detail::is_view_sequence<Vs>::value...>::value,
        std::decay_t<Vs>...>::apply(std::forward<F>(fun),
        std::forward<Vs>(seqs)...);
    }

    ///
    /// @brief Fills a view or view_sequence with a given value
    ///
    template <class Array, class V>
    void fill(Array& arr, const V& val)
    {
      transform(arr, detail::fill_invoker<V>(val));
    }
  } // namespace util
} // namespace dg

#endif
