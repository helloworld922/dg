#ifndef DG_INDEXER_HPP
#define DG_INDEXER_HPP

#include "layouts.hpp"

#include "compiler_support.hpp"

#include <cstddef>
#include <type_traits>

namespace dg
{
  ///
  /// @brief Multi-dimensional array indexer of a chunk of memory
  /// @tparam NDims
  /// @tparam Layout
  ///
  template <size_t NDims, class Layout = layout_right>
  class indexer
  {
    extents<NDims> exts_;
    Layout layout_;

  public:
    /// @brief shape type
    typedef extents<NDims> extents_type;
    /// @brief layout type
    typedef Layout layout_type;
    /// @brief underlying layout size type
    typedef typename Layout::size_type size_type;

    /// @brief default constructor
    constexpr indexer() = default;
    /// @brief copy constructor
    constexpr indexer(const indexer& o) = default;
    /// @brief move constructor
    constexpr indexer(indexer&& o) = default;
    /// @brief copy assignment
    indexer& operator=(const indexer& o) = default;
    /// @brief move assignment
    indexer& operator=(indexer&& o) = default;

    virtual ~indexer() = default;

    ///
    /// @brief
    /// @param exts extents to view data as
    /// @tparam Ds
    ///
    template <class Ls = Layout,
      class =
        typename std::enable_if<std::is_default_constructible<Ls>::value>::type>
    constexpr indexer(const dg::extents<NDims>& exts) noexcept : exts_(exts)
    {
    }

    ///
    /// @brief
    /// @param exts extents to view data as
    /// @tparam Ds
    ///
    template <class... Idcs, class Ls = Layout,
      class =
        typename std::enable_if<std::is_default_constructible<Ls>::value>::type,
      class = typename std::enable_if<
        meta::all<std::is_integral<Idcs>::value...>::value &&
        sizeof...(Idcs) == NDims>::type>
    constexpr indexer(Idcs... idcs) noexcept
      : exts_{static_cast<size_t>(idcs)...}
    {
    }

    ///
    /// @brief
    /// @param exts extents to view data as
    /// @param layout layout to use for indexing into data
    /// @tparam Ds
    ///
    template <class Ls, class... Idcs,
      class = typename std::enable_if<
        !std::is_integral<typename std::decay_t<Ls>>::value>::type,
      class = typename std::enable_if<
        meta::all<std::is_integral<Idcs>::value...>::value &&
        sizeof...(Idcs) == NDims>::type>
    constexpr indexer(Ls&& layout, Idcs... idcs) noexcept
      : exts_{static_cast<size_t>(idcs)...}, layout_(std::forward<Ls>(layout))
    {
    }

    ///
    /// @brief
    /// @param data pointer to underlying storage
    /// @param exts extents to view data as
    /// @param layout layout to use for indexing into data
    /// @tparam Ds
    ///
    template <class Ls, class = typename std::enable_if<
                          !std::is_integral<std::decay_t<Ls>>::value>::type>
    constexpr indexer(const dg::extents<NDims>& exts, Ls&& layout) noexcept
      : exts_(exts), layout_(std::forward<Ls>(layout))
    {
    }

    ///
    /// @brief Flattens the list of indices to a single index
    ///
    template <class... Idcs>
    constexpr typename std::enable_if<sizeof...(Idcs) != extents_type::rank(),
      size_type>::type
    index(Idcs... idcs) const noexcept
    {
      static_assert(sizeof...(idcs) == extents_type::rank(),
        "wrong number of indices given");
      return 0;
    }

    ///
    /// @brief Flattens the list of indices to a single index
    ///
    template <class... Idcs>
    constexpr typename std::enable_if<sizeof...(Idcs) == extents_type::rank(),
      size_type>::type
    index(Idcs... idcs) const noexcept
    {
      return layout_(exts_, idcs...);
    }

    ///
    /// @brief Gets the flattened index from a sequence of indices
    ///
    template <class Idcs>
    constexpr size_type get_index(const Idcs& idcs) const noexcept
    {
      return meta::explode_args<
        std::make_index_sequence<std::tuple_size<Idcs>::value>>::invoke(layout_,
        idcs, exts_);
    }

    ///
    /// @brief Gets the extent in a given dimension
    /// @param idx
    ///
    constexpr size_type extent(size_type idx) const noexcept
    {
      return exts_[idx];
    }

    ///
    /// @brief Number of dimensions of this array view
    ///
    static constexpr size_type rank() noexcept
    {
      return extents_type::rank();
    }

    ///
    /// @brief Gets the extents object associated with this view
    ///
    extents_type& shape() noexcept
    {
      return exts_;
    }

    ///
    /// @brief returns the number of dimensions of this indexer
    ///
    constexpr static auto ndims() noexcept
    {
      return NDims;
    }

    ///
    /// @brief Gets the extents object associated with this view
    ///
    constexpr const extents_type& shape() const noexcept
    {
      return exts_;
    }

    ///
    /// @brief Gets the layout object associated with this view
    ///
    Layout& layout() noexcept
    {
      return layout_;
    }

    ///
    /// @brief Gets the layout object associated with this view
    ///
    constexpr const Layout& layout() const noexcept
    {
      return layout_;
    }

    ///
    /// @brief Gets the number of elements in this view
    ///
    constexpr size_type size() const noexcept
    {
      return exts_.size();
    }

    ///
    /// @brief Creates a sub-array based on slices in each dimension. If a
    /// SliceSpecifier is an integral value, that dimension is fixed in the
    /// resulting sub-array.
    /// @param slice dg::all selects the entire extent of the associated
    /// dimension.
    /// @tparam SliceSpecifiers
    ///
    template <class... SliceSpecifiers>
    constexpr indexer<
      slice_creator<Layout, extents_type,
        std::decay_t<SliceSpecifiers>...>::type::extents_type::rank(),
      typename std::enable_if<sizeof...(SliceSpecifiers) ==
                                extents_type::rank(),
        typename slice_creator<Layout, extents_type,
          std::decay_t<SliceSpecifiers>...>::type>::type>
    subindexer(SliceSpecifiers&&... slice) const
    {
      typedef slice_creator<Layout, extents_type,
        std::decay_t<SliceSpecifiers>...>
        creator_type;

      typedef typename creator_type::type sliced_layout_type;

      return indexer<sliced_layout_type::extents_type::rank(),
        sliced_layout_type>(
        creator_type::make_extents(layout(), shape(), slice...),
        creator_type::create(
          layout(), shape(), std::forward<SliceSpecifiers>(slice)...));
    }

    ///
    /// @brief Creates a sub-array based on slices in each dimension. If a
    /// SliceSpecifier is an integral value, that dimension is fixed in the
    /// resulting sub-array.
    /// @param slice dg::all selects the entire extent of the associated
    /// dimension.
    /// @tparam SliceSpecifiers
    ///
    template <class... SliceSpecifiers>
    constexpr typename std::enable_if<
      sizeof...(SliceSpecifiers) != extents_type::rank(), int>::type
    subindexer(SliceSpecifiers&&... slice) const
    {
      static_assert(sizeof...(SliceSpecifiers) == extents_type::rank(),
        "wrong number of slices given");
      return 0;
    }
  };

  ///
  /// @brief Helper for creating an indexer_sequence
  ///
  template <class... Is>
  auto make_indexer_sequence(Is&&... indexers);

  template <class... Vs>
  class view_sequence;

  ///
  /// @brief A sequence of indexers. This allows multiple indexers to be sliced
  /// together
  /// @tparam Is index types
  ///
  template <class... Is>
  class indexer_sequence
  {
  public:
    /// @brief contained indexers
    std::tuple<Is...> indexers;

    /// copy constructor
    indexer_sequence(const indexer_sequence&) = default;
    /// move constructor
    indexer_sequence(indexer_sequence&&) = default;

    /// copy assignment
    indexer_sequence& operator=(const indexer_sequence&) = default;
    /// move assignment
    indexer_sequence& operator=(indexer_sequence&&) = default;

    ///
    /// @brief Helper for constructing an indexer_sequence
    /// @param indexers
    ///
    template <class I, class... Iss,
      class = std::enable_if_t<
        sizeof...(Iss) != 0 ||
        !std::is_same<std::decay_t<I>, indexer_sequence<Is...>>::value>>
    indexer_sequence(I&& idx, Iss&&... idxrs)
      : indexers(std::forward<I>(idx), std::forward<Iss>(idxrs)...)
    {
    }

    ///
    /// @brief Gets a contained indexer. Simply forwards to std::get
    /// @tparam Idx
    ///
    template <size_t Idx>
    auto& get()
    {
      return std::get<Idx>(indexers);
    }

    ///
    /// @brief Gets a contained indexer. Simply forwards to std::get
    /// @tparam Idx
    ///
    template <size_t Idx>
    const auto& get() const
    {
      return std::get<Idx>(indexers);
    }

    ///
    /// @brief Slices all held indexers in lock-step
    ///
    template <class... SliceSpecifiers>
    auto subindexer(SliceSpecifiers&&... slices) const
    {
      return meta::explode_args<std::make_index_sequence<sizeof...(Is)>>::
        invoke(
          [&slices...](auto&&... idxrs) {
            return make_indexer_sequence(idxrs.subindexer(slices...)...);
          },
          indexers);
    }
  };

  ///
  /// @brief Helper for extracting an indexer from an indexer_sequence
  ///
  template <size_t I, class... Idcs>
  auto& get(indexer_sequence<Idcs...>& v)
  {
    return v.template get<I>();
  }

  ///
  /// @brief Helper for extracting an indexer from an indexer_sequence
  ///
  template <size_t I, class... Idcs>
  auto& get(const indexer_sequence<Idcs...>& v)
  {
    return v.template get<I>();
  }
} // namespace dg

#include "detail/indexer.tcc"

#endif
