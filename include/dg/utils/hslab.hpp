#ifndef DG_HSLAB_HPP
#define DG_HSLAB_HPP

#include "view.hpp"

namespace dg
{
  ///
  /// @brief Hyperslab array. Owns the underlying data dynamically
  ///
  template <class T, size_t NDims, class Layout = layout_right>
  class hslab : public view<T, NDims, Layout>
  {
    typedef view<T, NDims, Layout> super_type;

  public:
    typedef T value_type;
    typedef extents<NDims> extents_type;
    typedef Layout layout_type;
    typedef typename Layout::size_type size_type;

    hslab() : super_type(nullptr, extents_type())
    {
    }
    
    hslab(const hslab& o)
      : super_type(new T[o.shape().size()], o.shape(), o.layout())
    {
      std::copy(o.data(), o.data() + o.shape().size(), this->data());
    }

    hslab(hslab&& o) : super_type(std::move(o))
    {
      o.data() = nullptr;
    }

    template <class Ls = Layout,
      class =
        typename std::enable_if<std::is_default_constructible<Ls>::value>::type>
    hslab(const extents<NDims> exts) : super_type(new T[exts.size()], exts)
    {
    }

    template <class... Idcs, class Ls = Layout,
      class =
        typename std::enable_if<std::is_default_constructible<Ls>::value>::type,
      class = typename std::enable_if<
        meta::all<std::is_integral<Idcs>::value...>::value>::type>
    hslab(Idcs... idcs) : super_type(nullptr, idcs...)
    {
      this->data() = new T[this->shape().size()];
    }

    template <class Ls, class... Idcs,
      class = typename std::enable_if_t<
        !std::is_integral<typename std::decay_t<Ls>>::value>,
      class = typename std::enable_if_t<
        meta::all<std::is_integral<Idcs>::value...>::value &&
        sizeof...(Idcs) == NDims>>
    hslab(Ls&& layout, Idcs... idcs)
      : super_type(nullptr, std::forward<Ls>(layout), idcs...)
    {
      this->data() = new T[this->shape().size()];
    }

    /**
     * @brief
     * @param data pointer to underlying storage
     * @param exts extents to view data as
     * @param layout layout to use for indexing into data
     * @tparam Ds
     */
    template <class Ds, class Ls,
      class = typename std::enable_if<
        !std::is_integral<std::decay_t<Ds>>::value>::type>
    hslab(Ds&& exts, Ls&& layout)
      : super_type(nullptr, std::forward<Ds>(exts), std::forward<Ls>(layout))
    {
      this->data() = new T[this->shape().size()];
    }

    virtual ~hslab()
    {
      delete[] this->data();
    }

    // TODO: implement when have a general way to iterate over a view
    hslab& operator=(const hslab& o)
    {
      if (&o != this)
      {
        if (o.size() > this->size())
        {
          // need more space
          delete[] this->data();
          this->data() = new T[o.shape().size()];
        }
        std::copy(o.data(), o.data() + o.shape().size(), this->data());
        this->shape() = o.shape();
        this->layout() = o.layout();
      }
      return *this;
    }

    hslab& operator=(hslab&& o)
    {
      if (&o != this)
      {
        delete[] this->data();
        super_type::operator=(std::move(o));
        o.data() = nullptr;
      }
      return *this;
    }

    auto as_view() const
    {
      return super_type(*this);
    }
  };

  ///
  /// @brief Hyperslab array. Statically owns the underlying data (useful for
  /// small data)
  ///
  template <class T, size_t BufSize, size_t NDims, class Layout = layout_right>
  class shslab : public view<T, NDims, Layout>
  {
    typedef view<T, NDims, Layout> super_type;
    T buffer_[BufSize];

  public:
    /// @brief the held value type
    typedef T value_type;
    /// @brief the extents type
    typedef extents<NDims> extents_type;
    /// @brief the layout type
    typedef Layout layout_type;
    /// @brief integer size type
    typedef typename Layout::size_type size_type;

    ///
    /// @brief default constructor
    ///
    shslab() : super_type(buffer_, extents_type{})
    {
    }

    ///
    /// @brief copy constructor
    /// @param o
    ///
    shslab(const shslab& o) : super_type(buffer_, o.shape(), o.layout())
    {
      std::copy(o.buffer_, o.buffer_ + BufSize, buffer_);
    }

    ///
    /// @brief Creates an shslab with given extents
    ///
    template <class Ls = Layout,
      class =
        typename std::enable_if<std::is_default_constructible<Ls>::value>::type>
    shslab(const dg::extents<NDims>& exts) : super_type(buffer_, exts)
    {
    }

    ///
    /// @brief Creates an shslab with given extents
    ///
    template <class... Idcs, class Ls = Layout,
      class =
        typename std::enable_if<std::is_default_constructible<Ls>::value>::type,
      class = typename std::enable_if<
        meta::all<std::is_integral<Idcs>::value...>::value &&
        sizeof...(Idcs) == NDims>::type>
    shslab(Idcs... idcs) : super_type(buffer_, idcs...)
    {
    }

    ///
    /// @brief Creates an shslab with given extents and layout
    ///
    template <class Ls, class... Idcs,
      class = typename std::enable_if<
        !std::is_integral<typename std::decay_t<Ls>>::value>::type,
      class = typename std::enable_if<
        meta::all<std::is_integral<Idcs>::value...>::value &&
        sizeof...(Idcs) == NDims>::type>
    shslab(Ls&& layout, Idcs... idcs)
      : super_type(buffer_, std::forward<Ls>(layout), idcs...)
    {
    }

    ///
    /// @brief
    /// @param data pointer to underlying storage
    /// @param exts extents to view data as
    /// @param layout layout to use for indexing into data
    /// @tparam Ds
    ///
    template <class Ds, class Ls,
      class = typename std::enable_if<
        !std::is_integral<std::decay_t<Ds>>::value>::type>
    shslab(Ds&& exts, Ls&& layout)
      : super_type(buffer_, std::forward<Ds>(exts), std::forward<Ls>(layout))
    {
    }

    virtual ~shslab() = default;

    /// @brief
    shslab& operator=(const shslab& o)
    {
      if (&o != this)
      {
        std::copy(o.data(), o.data() + o.shape().size(), this->data());
        this->shape() = o.shape();
        this->layout() = o.layout();
      }
      return *this;
    }

    auto as_view() const
    {
      return super_type(*this);
    }
  };
} // namespace dg

#include "detail/hslab.tcc"

#endif
