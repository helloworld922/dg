#ifndef DG_EXTENTS_HPP
#define DG_EXTENTS_HPP

#include "compiler_support.hpp"
#include "meta.hpp"

#include <array>
#include <cstddef>
#include <type_traits>

namespace dg
{
  ///
  /// @brief Specifies the extents of a multi-dimension array.
  /// @tparam N number of dimensions this extent exists in
  ///
  template <size_t N, class T = size_t>
  struct extents
  {
  public:
    ///
    /// @internal
    /// @brief internal data storage
    ///
    T data_[N];

    ///
    /// @brief Number of dimensions in this extent
    ///
    constexpr static size_t rank() noexcept
    {
      return N;
    }

    ///
    /// @brief gets the extent in dimension i
    ///
    T& operator[](size_t i) noexcept
    {
      return data_[i];
    }

    ///
    /// @brief gets the extent in dimension i
    ///
    constexpr const T& operator[](size_t i) const noexcept
    {
      return data_[i];
    }

    ///
    /// @brief Computes the number of elements contained in this extent
    ///
    T size() const noexcept
    {
      T res = 1;
      for (T i = 0; i < rank(); ++i)
      {
        res *= data_[i];
      }

      return res;
    }
  };
}

#endif
