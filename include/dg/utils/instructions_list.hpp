#ifndef DG_INSTRUCTIONS_LIST_HPP
#define DG_INSTRUCTIONS_LIST_HPP

#include "petsc_utils.hpp"

#include <vector>

namespace dg
{
  ///
  /// @brief Simple helper for representing instructions for constructing a
  /// PETSc sparse matrix
  ///
  struct instructions_list
  {
    std::vector<PetscInt> rows;
    std::vector<PetscInt> cols;
    std::vector<PetscScalar> values;

    void clear()
    {
      rows.clear();
      cols.clear();
      values.clear();
    }

    void emplace_back(PetscInt row, PetscInt col, PetscScalar val)
    {
      rows.push_back(row);
      cols.push_back(col);
      values.push_back(val);
    }

    ///
    /// @brief Compresses the instructions list by removing duplicate entries.
    /// Also orders the instructions by rows/cols.
    ///
    void compress();

    ///
    /// @brief Apply this instruction list to a given matrix
    /// Caller is responsible for calling MatAssemblyBegin/MatAssemblyEnd
    /// @param mat
    /// @param mode ADD_VALUES or INSERT_VALUES
    ///
    PetscErrorCode apply(Mat mat, InsertMode mode);

  private:
    std::vector<size_t> permute_;
  };
}

#endif
