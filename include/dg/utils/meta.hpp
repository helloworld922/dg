#ifndef DG_META_HPP
#define DG_META_HPP

#include "compiler_support.hpp"

#include <type_traits>

namespace dg
{
  ///
  /// @brief Various meta-programming helpers
  ///
  namespace meta
  {
    ///
    /// @brief wraps a predicate result with a type
    ///
    template <bool P, class T = bool>
    struct predicate : public std::integral_constant<bool, P>
    {
      /// @brief The predicate type
      typedef T type;
    };

    ///
    /// @brief wraps a predicate result with a type and value
    ///
    template <bool P, class T, T Val>
    struct vpredicate : public std::integral_constant<bool, P>
    {
      /// @brief stored value type
      typedef T type;
      /// @brief the stored predicate value
      constexpr static T stored = Val;
    };

    ///
    /// @brief Determines if all predicated values are true or not
    ///
    template <bool... Vals>
    struct all : public std::integral_constant<bool, true>
    {
    };

    ///
    /// @brief Determines if any predicated values are true or not
    ///
    template <bool... Vals>
    struct any : public std::integral_constant<bool, false>
    {
    };
    

    ///
    /// @brief Extracts a value from an integer_sequence. Has a static member
    /// value with the extracted value.
    /// @tparam I 0-based index of which entry to extract
    /// @tparam Seq some sort of std::integer_sequence
    ///
    template <std::size_t I, class Seq>
    struct sequence_element;

    ///
    /// @brief Determines if an integer_sequence contains a given value
    ///
    template <class T, T val, class Seq, size_t Idx = 0>
    struct sequence_contains;

    ///
    /// @brief Filters a parameter pack by a given predecate
    /// @tparam Idx
    /// @tparam Preds predicates
    ///
    template <class... Preds>
    struct filter;

    ///
    /// @brief Creates an integer sequence for all predicates which evaluate to
    /// true
    /// @tparam Idx Current index, base callers should pass 0 to this
    /// @tparam Preds list of predicates
    ///
    template <std::size_t Idx, class... Preds>
    struct filter_idcs;

    ///
    /// @brief Filters a sequence of predicates which also hold a stored value
    /// @tparam T sequence value type
    /// @tparam vPreds sequence of value predicates
    ///
    template <class T, class... vPreds>
    struct filter_values;

    ///
    /// @brief Turns a compile-time sequence of values into a variadic parameter
    /// pack
    /// @tparam Seq Some kind of index_sequence used as a helper for enumerating args
    ///
    template <class Seq>
    struct explode_args;

    /// @internal
    template <size_t... Vals>
    struct explode_args<std::index_sequence<Vals...>>
    {
      ///
      /// @brief Invoke the functor with a given sequence
      /// @param f the callable type, f(args..., cont...), where cont... is
      /// expanded
      /// @param cont container of arguments to expand
      /// @param args extra arguments to pass (these are passed before the
      /// container of args)
      /// @tparam Fun callable type
      /// @tparam Cont container type
      /// @tparam ExtraArgs
      template <class Fun, class Cont, class... ExtraArgs>
      static constexpr auto invoke(Fun&& f, Cont&& cont, ExtraArgs&&... args)
      {
        return f(std::forward<ExtraArgs>(args)...,
          std::get<Vals>(std::forward<Cont>(cont))...);
      }
    };

    ///
    /// @brief Base case for a std::accumulate-like operation
    ///
    template <class T, T val, class BOp>
    constexpr T sequence_accumulate(std::integer_sequence<T>, BOp&& op) noexcept
    {
      return val;
    }

    ///
    /// @brief Accumulates the values in an integer_sequence
    /// @param seq sequence type
    /// @param op binary operation to use for accumulation
    /// @tparam T
    /// @tparam init_val
    /// @tparam BOp callable with two arguments of type T
    /// @tparam v0
    /// @tparam vals
    ///
    template <class T, T init_val, class BOp, T v0, T... vals>
    constexpr T sequence_accumulate(
      std::integer_sequence<T, v0, vals...> seq, BOp&& op) noexcept
    {
      return op(
        v0, sequence_accumulate<T, init_val>(
              std::integer_sequence<T, vals...>{}, std::forward<BOp>(op)));
    }

    ///
    /// @brief Quick helper for sequence_accumulate which multiplies entries
    ///
    template <class T, T v0, T... vals>
    constexpr T sequence_product(
      std::integer_sequence<T, v0, vals...> seq) noexcept
    {
      return sequence_accumulate<T, 1>(seq, std::multiplies<T>{});
    }

    ///
    /// @brief Quick helper for sequence_accumulate which adds entries
    ///
    template <class T, T... vals>
    constexpr T sequence_sum(std::integer_sequence<T, vals...> seq) noexcept
    {
      return sequence_accumulate<T, 0>(seq, std::plus<T>{});
    }

    ///
    /// @brief Checks if some object is callable. An object is callable if it's one of:
    ///  - a function pointer
    ///  - a class/struct with an overloaded operator()
    ///  - a lambda closure
    ///
    template <typename T, class = void>
    struct is_callable : std::is_function<typename std::remove_pointer<T>::type>
    {
    };
  }
}

#include "detail/meta.tcc"

#endif
