#ifndef DG_PETSC_UTILS_HPP
#define DG_PETSC_UTILS_HPP

#include "raii_base.hpp"

#include <petsc.h>

namespace dg
{
  ///
  /// @brief PETSc helper utils
  ///
  namespace petsc
  {
    /// \{
#define PETSC_RAII(name, type, free_func, null_val)                            \
  class name : public raii_base<name, type, type, type>                        \
  {                                                                            \
    typedef raii_base<name, type, type, type, false> super;                    \
                                                                               \
  public:                                                                      \
    typedef type value_type;                                                   \
    typedef type pointer_type;                                                 \
    typedef type const_pointer_type;                                           \
    void dec()                                                                 \
    {                                                                          \
      auto p = get();                                                          \
      auto err = free_func(&p);                                                \
      CHKERRV(err);                                                            \
    }                                                                          \
    name() noexcept : super(null_val)                                          \
    {                                                                          \
    }                                                                          \
    name(type p) noexcept : super(p)                                           \
    {                                                                          \
    }                                                                          \
    name(name&& o) noexcept = default;                                         \
    name& operator=(name&& o) = default;                                       \
    type release()                                                             \
    {                                                                          \
      return super::release(null_val);                                         \
    }                                                                          \
  }

    PETSC_RAII(dm, DM, DMDestroy, nullptr);
    PETSC_RAII(discretization, PetscDS, PetscDSDestroy, nullptr);
    PETSC_RAII(vector, Vec, VecDestroy, nullptr);
    PETSC_RAII(vector_scatter, VecScatter, VecScatterDestroy, nullptr);
    PETSC_RAII(matrix, Mat, MatDestroy, nullptr);
    PETSC_RAII(matrix_coloring, MatColoring, MatColoringDestroy, nullptr);
    PETSC_RAII(partition, MatPartitioning, MatPartitioningDestroy, nullptr);
    PETSC_RAII(
      matrix_fd_coloring, MatFDColoring, MatFDColoringDestroy, nullptr);
    PETSC_RAII(index_set, IS, ISDestroy, nullptr);
    PETSC_RAII(linear_solver, KSP, KSPDestroy, nullptr);
    PETSC_RAII(nonlinear_solver, SNES, SNESDestroy, nullptr);
    PETSC_RAII(line_search, SNESLineSearch, SNESLineSearchDestroy, nullptr);
    PETSC_RAII(viewer, PetscViewer, PetscViewerDestroy, nullptr);

    ///
    /// @class dm
    /// @brief PETSc DM object wrapper
    /// @typedef dm::value_type
    /// @typedef dm::pointer_type
    /// @typedef dm::const_pointer_type
    /// @fn dm::dm() noexcept
    /// @fn dm::dm(DM p) noexcept
    /// @fn dm::dm(dm&& o) noexcept
    /// @fn dm& dm::operator=(dm&& o) noexcept
    /// @fn void dm::dec()
    /// @fn dm::value_type dm::release()
    ///

    ///
    /// @class discretization
    /// @brief PETSc PetscDS object wrapper
    /// @typedef discretization::value_type
    /// @typedef discretization::pointer_type
    /// @typedef discretization::const_pointer_type
    /// @fn discretization::discretization() noexcept
    /// @fn discretization::discretization(PetscDS p) noexcept
    /// @fn discretization::discretization(discretization&& o) noexcept
    /// @fn discretization& discretization::operator=(discretization&& o) noexcept
    /// @fn void discretization::dec()
    /// @fn discretization::value_type discretization::release()
    ///

    ///
    /// @class vector
    /// @brief PETSc Vec object wrapper
    /// @typedef vector::value_type
    /// @typedef vector::pointer_type
    /// @typedef vector::const_pointer_type
    /// @fn vector::vector() noexcept
    /// @fn vector::vector(Vec p) noexcept
    /// @fn vector::vector(vector&& o) noexcept
    /// @fn vector& vector::operator=(vector&& o) noexcept
    /// @fn void vector::dec()
    /// @fn vector::value_type vector::release()
    ///

    ///
    /// @class vector_scatter
    /// @brief PETSc VecScatter object wrapper
    /// @typedef vector_scatter::value_type
    /// @typedef vector_scatter::pointer_type
    /// @typedef vector_scatter::const_pointer_type
    /// @fn vector_scatter::vector_scatter() noexcept
    /// @fn vector_scatter::vector_scatter(VecScatter p) noexcept
    /// @fn vector_scatter::vector_scatter(vector_scatter&& o) noexcept
    /// @fn vector_scatter& vector_scatter::operator=(vector_scatter&& o) noexcept
    /// @fn void vector_scatter::dec()
    /// @fn vector_scatter::value_type vector_scatter::release()
    ///

    ///
    /// @class matrix
    /// @brief PETSc Mat object wrapper
    /// @typedef matrix::value_type
    /// @typedef matrix::pointer_type
    /// @typedef matrix::const_pointer_type
    /// @fn matrix::matrix() noexcept
    /// @fn matrix::matrix(Mat p) noexcept
    /// @fn matrix::matrix(matrix&& o) noexcept
    /// @fn matrix& matrix::operator=(matrix&& o) noexcept
    /// @fn void matrix::dec()
    /// @fn matrix::value_type matrix::release()
    ///

    ///
    /// @class matrix_coloring
    /// @brief PETSc MatColoring object wrapper
    /// @typedef matrix_coloring::value_type
    /// @typedef matrix_coloring::pointer_type
    /// @typedef matrix_coloring::const_pointer_type
    /// @fn matrix_coloring::matrix_coloring() noexcept
    /// @fn matrix_coloring::matrix_coloring(MatColoring p) noexcept
    /// @fn matrix_coloring::matrix_coloring(matrix_coloring&& o) noexcept
    /// @fn matrix_coloring& matrix_coloring::operator=(matrix_coloring&& o) noexcept
    /// @fn void matrix_coloring::dec()
    /// @fn matrix_coloring::value_type matrix_coloring::release()
    ///

    ///
    /// @class partition
    /// @brief PETSc MatPartitioning object wrapper
    /// @typedef partition::value_type
    /// @typedef partition::pointer_type
    /// @typedef partition::const_pointer_type
    /// @fn partition::partition() noexcept
    /// @fn partition::partition(MatPartitioning p) noexcept
    /// @fn partition::partition(partition&& o) noexcept
    /// @fn partition& partition::operator=(partition&& o) noexcept
    /// @fn void partition::dec()
    /// @fn partition::value_type partition::release()
    ///

    ///
    /// @class matrix_fd_coloring
    /// @brief PETSc MatFDColoring object wrapper
    /// @typedef matrix_fd_coloring::value_type
    /// @typedef matrix_fd_coloring::pointer_type
    /// @typedef matrix_fd_coloring::const_pointer_type
    /// @fn matrix_fd_coloring::matrix_fd_coloring() noexcept
    /// @fn matrix_fd_coloring::matrix_fd_coloring(MatFDColoring p) noexcept
    /// @fn matrix_fd_coloring::matrix_fd_coloring(matrix_fd_coloring&& o) noexcept
    /// @fn matrix_fd_coloring& matrix_fd_coloring::operator=(matrix_fd_coloring&& o) noexcept
    /// @fn void matrix_fd_coloring::dec()
    /// @fn matrix_fd_coloring::value_type matrix_fd_coloring::release()
    ///

    ///
    /// @class index_set
    /// @brief PETSc IS object wrapper
    /// @typedef index_set::value_type
    /// @typedef index_set::pointer_type
    /// @typedef index_set::const_pointer_type
    /// @fn index_set::index_set() noexcept
    /// @fn index_set::index_set(IS p) noexcept
    /// @fn index_set::index_set(index_set&& o) noexcept
    /// @fn index_set& index_set::operator=(index_set&& o) noexcept
    /// @fn void index_set::dec()
    /// @fn index_set::value_type index_set::release()
    ///

    ///
    /// @class linear_solver
    /// @brief PETSc KSP object wrapper
    /// @typedef linear_solver::value_type
    /// @typedef linear_solver::pointer_type
    /// @typedef linear_solver::const_pointer_type
    /// @fn linear_solver::linear_solver() noexcept
    /// @fn linear_solver::linear_solver(KSP p) noexcept
    /// @fn linear_solver::linear_solver(linear_solver&& o) noexcept
    /// @fn linear_solver& linear_solver::operator=(linear_solver&& o) noexcept
    /// @fn void linear_solver::dec()
    /// @fn linear_solver::value_type linear_solver::release()
    ///

    ///
    /// @class nonlinear_solver
    /// @brief PETSc SNES object wrapper
    /// @typedef nonlinear_solver::value_type
    /// @typedef nonlinear_solver::pointer_type
    /// @typedef nonlinear_solver::const_pointer_type
    /// @fn nonlinear_solver::nonlinear_solver() noexcept
    /// @fn nonlinear_solver::nonlinear_solver(SNES p) noexcept
    /// @fn nonlinear_solver::nonlinear_solver(nonlinear_solver&& o) noexcept
    /// @fn nonlinear_solver& nonlinear_solver::operator=(nonlinear_solver&& o) noexcept
    /// @fn void nonlinear_solver::dec()
    /// @fn nonlinear_solver::value_type nonlinear_solver::release()
    ///

    ///
    /// @class line_search
    /// @brief PETSc SNESLineSearch object wrapper
    /// @typedef line_search::value_type
    /// @typedef line_search::pointer_type
    /// @typedef line_search::const_pointer_type
    /// @fn line_search::line_search() noexcept
    /// @fn line_search::line_search(SNESLineSearch p) noexcept
    /// @fn line_search::line_search(line_search&& o) noexcept
    /// @fn line_search& line_search::operator=(line_search&& o) noexcept
    /// @fn void line_search::dec()
    /// @fn line_search::value_type line_search::release()
    ///

    ///
    /// @class viewer
    /// @brief PETSc PetscViewer object wrapper
    /// @typedef viewer::value_type
    /// @typedef viewer::pointer_type
    /// @typedef viewer::const_pointer_type
    /// @fn viewer::viewer() noexcept
    /// @fn viewer::viewer(PetscViewer p) noexcept
    /// @fn viewer::viewer(viewer&& o) noexcept
    /// @fn viewer& viewer::operator=(viewer&& o) noexcept
    /// @fn void viewer::dec()
    /// @fn viewer::value_type viewer::release()
    ///

#undef PETSC_RAII
    /// \}

    ///
    /// @brief Helper function for converting a SNESConvergedReason into a readable string
    /// @param res
    /// @return
    ///
    const char* to_string(SNESConvergedReason res);

    ///
    /// @brief Helper function for converting a KSPConvergedReason into a readable string
    /// @param res
    /// @return
    ///
    const char* to_string(KSPConvergedReason res);

    ///
    /// @brief Helper function for converting a SNESLineSearchReason into a readable string
    /// @param res
    /// @return
    ///
    const char* to_string(SNESLineSearchReason res);
  }
}

#endif
