#ifndef DG_SLICE_LAYOUT_TCC
#define DG_SLICE_LAYOUT_TCC

#include <utility>

namespace dg
{
  namespace detail
  {
    ///
    /// @internal
    /// @brief Helper for make_sliced_exts
    /// @tparam T derived extents type
    /// @tparam Seq integer sequence for indexing into slice specifiers
    ///
    template <class T, class Seq>
    struct make_slice_exts_helper;

    ///
    /// @brief Specialization for integer fixed slices or all slices
    ///
    template <>
    struct slice_transform<0>
    {
      ///
      /// @brief Computes the index based on the slice specifier
      /// dg::all specialization
      /// @param slice slice specifier
      /// @param index
      ///
      template <class T, class I>
      constexpr static std::enable_if_t<
        std::is_same<std::decay_t<T>, dg::all>::value, size_t>
      index(T&& slice, I i) noexcept
      {
        return i;
      }

      ///
      /// @brief Computes the index based on the slice specifier
      /// integral type specialization
      /// @param slice slice specifier
      /// @param index
      ///
      template <class T, class I>
      constexpr static std::enable_if_t<
        !std::is_same<std::decay_t<T>, dg::all>::value, size_t>
      index(T&& slice, I i) noexcept
      {
        return slice;
      }

      ///
      /// @brief Returns the stride for the given slice
      ///
      template <class T>
      constexpr static size_t stride(T&& slice) noexcept
      {
        return 1;
      }

      ///
      /// @brief Computes the extent of the sliced base extent
      /// dg::all specialization
      /// @param base base extent size
      /// @param v slice specifier
      ///
      template <class U>
      constexpr static std::enable_if_t<
        std::is_same<std::decay_t<U>, dg::all>::value, size_t>
      extent(size_t base, U&& v) noexcept
      {
        return base;
      }

      ///
      /// @brief Computes the extent of the sliced base extent
      /// fixed integral type specialization
      /// @param base base extent size
      /// @param v slice specifier
      ///
      template <class U>
      constexpr static std::enable_if_t<
        !std::is_same<std::decay_t<U>, dg::all>::value, size_t>
      extent(size_t base, U&& v) noexcept
      {
        // Useful for hdf5 utils if this is 1 so a fixed slice can still be
        // written out properly. Technically this should probably be 0?
        // Not used by anything else, so that's not a big problem.
        return 1;
      }

      ///
      /// @brief Merge dg::all with another slice
      /// dg::all merged with slice1 is slice1
      ///
      template <class T0, class T1>
      constexpr static std::enable_if_t<
        std::is_same<std::decay_t<T0>, dg::all>::value, T1>
      merge(T0&& slice0, T1&& slice1)
      {
        return std::forward<T1>(slice1);
      }

      ///
      /// @brief Merge a fixed slice with another slice
      /// fixed slice merged with anything else is slice0
      ///
      template <class T0, class T1>
      constexpr static std::enable_if_t<
        !std::is_same<std::decay_t<T0>, dg::all>::value, T0>
      merge(T0&& slice0, T1&& slice1)
      {
        return std::forward<T1>(slice0);
      }
    };

    ///
    /// @brief Specialization for max slice specifier
    ///
    template <>
    struct slice_transform<1>
    {
      ///
      /// @brief Computes the index based on the slice specifier
      /// @param slice slice specifier
      /// @param index
      ///
      template <class T, class I>
      constexpr static size_t index(T&& slice, I i) noexcept
      {
        return i;
      }

      ///
      /// @brief Returns the stride for the given slice
      ///
      template <class T>
      constexpr static size_t stride(T&& slice) noexcept
      {
        return 1;
      }

      ///
      /// @brief Computes the extent of the sliced base extent
      /// @param base base extent size
      /// @param v slice specifier
      ///
      template <class U>
      constexpr static size_t extent(size_t base, U&& v) noexcept
      {
        return std::get<0>(v);
      }

      ///
      /// @brief Merge max slice with dg::all
      /// result is slice0
      ///
      template <class T0, class T1>
      constexpr static std::enable_if_t<
        std::is_same<std::decay_t<T1>, dg::all>::value, T0>
      merge(T0&& slice0, T1&& slice1)
      {
        return std::forward<T0>(slice0);
      }

      ///
      /// @brief Merge max slice with anything other than dg::all is the other
      /// slice
      ///
      template <class T0, class T1>
      constexpr static std::enable_if_t<
        !std::is_same<std::decay_t<T1>, dg::all>::value, T1>
      merge(T0&& slice0, T1&& slice1)
      {
        return std::forward<T1>(slice1);
      }
    };

    ///
    /// @brief Specialization for [min,max) slice specifier
    ///
    template <>
    struct slice_transform<2>
    {
      ///
      /// @brief Computes the index based on the slice specifier
      /// @param slice slice specifier
      /// @param index
      ///
      template <class T, class I>
      constexpr static size_t index(T&& slice, I i) noexcept
      {
        return std::get<0>(slice) + i;
      }

      ///
      /// @brief Returns the stride for the given slice
      ///
      template <class T>
      constexpr static size_t stride(T&& slice) noexcept
      {
        return 1;
      }

      ///
      /// @brief Computes the extent of the sliced base extent
      /// @param base base extent size
      /// @param v slice specifier
      ///
      template <class U>
      constexpr static size_t extent(size_t base, U&& v) noexcept
      {
        return std::get<1>(v) - std::get<0>(v);
      }

      ///
      /// @brief Merge [min,max) slice with dg::all
      ///
      template <class T0, class T1>
      constexpr static std::enable_if_t<
        std::is_same<std::decay_t<T1>, dg::all>::value, T0>
      merge(T0&& slice0, T1&& slice1)
      {
        return std::forward<T0>(slice0);
      }

      ///
      /// @brief Merge [min,max) slice with fixed slice
      ///
      template <class T0, class T1>
      constexpr static std::enable_if_t<
        std::is_integral<std::decay_t<T1>>::value, std::decay_t<T1>>
      merge(T0&& slice0, T1&& slice1)
      {
        return std::get<0>(slice0) + std::forward<T1>(slice1);
      }

      ///
      /// @brief Merge [min,max) slice with max slice
      ///
      template <class T0, class T1>
      constexpr static std::enable_if_t<
        std::tuple_size<std::decay_t<T1>>::value == 1, std::array<size_t, 2>>
      merge(T0&& slice0, T1&& slice1)
      {
        return {index(slice0, 0), index(slice0, std::get<0>(slice1))};
      }

      ///
      /// @brief Merge [min,max) slice with [min,max) slice
      ///
      template <class T0, class T1>
      constexpr static std::enable_if_t<
        std::tuple_size<std::decay_t<T1>>::value == 2, std::array<size_t, 2>>
      merge(T0&& slice0, T1&& slice1)
      {
        return {index(slice0, std::get<0>(slice1)),
          index(slice0, std::get<1>(slice1))};
      }

      ///
      /// @brief Merge [min,max) slice with [min,max) stride slice
      ///
      template <class T0, class T1>
      constexpr static std::enable_if_t<
        std::tuple_size<std::decay_t<T1>>::value == 3, std::array<size_t, 3>>
      merge(T0&& slice0, T1&& slice1)
      {
        return {index(slice0, std::get<0>(slice1)),
          index(slice0, std::get<1>(slice1)),
          static_cast<size_t>(std::get<2>(slice1))};
      }
    };

    ///
    /// @brief Specialization for [min,max), stride slice specifier
    ///
    template <>
    struct slice_transform<3>
    {
      ///
      /// @brief Computes the index based on the slice specifier
      /// @param slice slice specifier
      /// @param index
      ///
      template <class T, class I>
      constexpr static size_t index(T&& slice, I i) noexcept
      {
        return std::get<0>(slice) + i * std::get<2>(slice);
      }

      ///
      /// @brief Returns the stride for the given slice
      ///
      template <class T>
      constexpr static size_t stride(T&& slice) noexcept
      {
        return std::get<2>(slice);
      }

      ///
      /// @brief Computes the extent of the sliced base extent
      /// @param base base extent size
      /// @param v slice specifier
      ///
      template <class U>
      constexpr static size_t extent(size_t base, U&& v) noexcept
      {
        return (std::get<1>(v) - std::get<0>(v)) / std::get<2>(v);
      }

      ///
      /// @brief Merge [min,max), stride slice with dg::all
      ///
      template <class T0, class T1>
      constexpr static std::enable_if_t<
        std::is_same<std::decay_t<T1>, dg::all>::value, T0>
      merge(T0&& slice0, T1&& slice1)
      {
        return std::forward<T0>(slice0);
      }

      ///
      /// @brief Merge [min,max), stride slice with fixed slice
      ///
      template <class T0, class T1>
      constexpr static std::enable_if_t<
        std::is_integral<std::decay_t<T1>>::value, size_t>
      merge(T0&& slice0, T1&& slice1)
      {
        return index(slice0, std::forward<T1>(slice1));
      }

      ///
      /// @brief Merge [min,max), stride slice with max slice
      ///
      template <class T0, class T1>
      constexpr static std::enable_if_t<
        std::tuple_size<std::decay_t<T1>>::value == 1, std::array<size_t, 3>>
      merge(T0&& slice0, T1&& slice1)
      {
        return {index(slice0, 0), index(slice0, std::get<0>(slice1)),
          static_cast<size_t>(std::get<2>(slice0))};
      }

      ///
      /// @brief Merge [min,max), stride slice with [min,max) slice
      ///
      template <class T0, class T1>
      constexpr static std::enable_if_t<
        std::tuple_size<std::decay_t<T1>>::value == 2, std::array<size_t, 3>>
      merge(T0&& slice0, T1&& slice1)
      {
        return {index(slice0, std::get<0>(slice1)),
          index(slice0, std::get<1>(slice1)),
          static_cast<size_t>(std::get<2>(slice0))};
      }

      ///
      /// @brief Merge [min,max) slice with [min,max) stride slice
      ///
      template <class T0, class T1>
      constexpr static std::enable_if_t<
        std::tuple_size<std::decay_t<T1>>::value == 3, std::array<size_t, 3>>
      merge(T0&& slice0, T1&& slice1)
      {
        return {index(slice0, std::get<0>(slice1)),
          index(slice0, std::get<1>(slice1)),
          static_cast<size_t>(std::get<2>(slice0) * std::get<2>(slice1))};
      }
    };

    template <class T, size_t... Vals>
    struct make_slice_exts_helper<T, std::index_sequence<Vals...>>
    {
      ///
      /// @internal
      /// @brief
      /// @param bcont base extent
      /// @param scont sequence of slice specifiers
      ///
      template <class BCont, class SCont>
      static constexpr T invoke(const BCont& bcont, const SCont& scont) noexcept
      {
        return T{static_cast<size_t>(
          slice_transform<std::tuple_size<typename std::decay<
            typename std::tuple_element<Vals, SCont>::type>::type>::value>::
            extent(bcont[Vals], std::get<Vals>(scont)))...};
      }
    };

    template <class... SliceSpecifiers, size_t... Idcs>
    struct make_sliced_exts<std::tuple<SliceSpecifiers...>,
      std::index_sequence<Idcs...>>
    {
      typedef extents<sizeof...(SliceSpecifiers)> type;
      typedef std::index_sequence<Idcs...> idcs_type;

      ///
      /// @brief
      /// @param exts base extents type
      /// @param slices sequence of slice specifiers
      ///
      template <class BaseD, class Cont>
      constexpr static type invoke(const BaseD& exts, const Cont& slices)
      {
        return make_slice_exts_helper<type, idcs_type>::invoke(exts, slices);
      }
    };

    template <class Slice, size_t Idx>
    struct arg_selector<true, Slice, Idx>
    {
      ///
      /// @brief Evaluates the index in the slice
      /// @param s base slice
      /// @param i tuple of argument indexes
      ///
      template <class ICont>
      static constexpr size_t index(const Slice& s, const ICont& i)
      {
        // variable slice
        return slice_transform<std::tuple_size<Slice>::value>::index(
          s, std::get<Idx>(i));
      }

      ///
      /// @brief Merges Slice s with the appropriate slice in SCont
      /// @param s base slice
      /// @param i tuple of argument slices
      ///
      template <class SCont>
      static constexpr auto merge(const Slice& s, const SCont& i)
      {
        // variable slice
        return slice_transform<std::tuple_size<Slice>::value>::merge(
          s, std::get<Idx>(i));
      }
    };

    template <class Slice, size_t Idx>
    struct arg_selector<false, Slice, Idx>
    {
      ///
      /// @brief Evaluates the index in the slice
      /// @param s base slice
      /// @param i tuple of argument indexes
      ///
      template <class ICont>
      static constexpr size_t index(const Slice& s, const ICont& i)
      {
        return s;
      }

      ///
      /// @brief Merges Slice s with the appropriate slice in SCont
      /// @param s base slice
      /// @param i tuple of argument slices
      ///
      template <class SCont>
      static constexpr auto merge(const Slice& s, const SCont& i)
      {
        return s;
      }
    };

    template <class ParentSlice, class Seq>
    class slice_merger;

    template <class BaseLayout, class BaseExts, size_t... SliceIdcs,
      class... SliceSpecifiers>
    struct slice_merger<layout_slice<BaseLayout, BaseExts, SliceSpecifiers...>,
      std::index_sequence<SliceIdcs...>>
    {
      ///
      /// @brief Slices which are variable in the base slice layout,
      /// and must be specified by the input (non-integral slice)
      /// Note: there could be 0 or more elements in a var slice
      ///
      typedef typename meta::filter_idcs<0,
        meta::predicate<!std::is_integral<SliceSpecifiers>::value>...>::type
        var_idcs;

      template <class BaseL, class SCont>
      constexpr static auto create(BaseL&& layout, SCont&& slices)
      {
#define DG_ARG_TYPE                                                            \
  arg_selector<                                                                \
    dg::meta::sequence_contains<size_t, SliceIdcs, var_idcs>::value,           \
    SliceSpecifiers,                                                           \
    dg::meta::sequence_contains<size_t, SliceIdcs, var_idcs>::idx>

        return layout_slice<BaseLayout, BaseExts,
          decltype(DG_ARG_TYPE::merge(std::get<SliceIdcs>(layout.get_slices()),
            slices))...>(layout.get_base_layout(), layout.get_base_extents(),
          DG_ARG_TYPE::merge(
            std::get<SliceIdcs>(layout.get_slices()), slices)...);
#undef DG_ARG_TYPE
      }

      template <class BaseL, class BaseD, class SCont>
      constexpr static auto make_extents(
        BaseL&& layout, BaseD&& dims, SCont&& slices)
      {
#define DG_ARG_TYPE                                                            \
  arg_selector<                                                                \
    dg::meta::sequence_contains<size_t, SliceIdcs, var_idcs>::value,           \
    SliceSpecifiers,                                                           \
    dg::meta::sequence_contains<size_t, SliceIdcs, var_idcs>::idx>

        return layout_slice<BaseLayout, BaseExts,
          decltype(DG_ARG_TYPE::merge(std::get<SliceIdcs>(layout.get_slices()),
            slices))...>::make_extents(dims,
          DG_ARG_TYPE::merge(
            std::get<SliceIdcs>(layout.get_slices()), slices)...);
#undef DG_ARG_TYPE
      }
    };

    template <class BaseLayout, class BaseExts, class... SliceSpecifiers>
    struct is_layout_slice<
      layout_slice<BaseLayout, BaseExts, SliceSpecifiers...>>
    {
      constexpr static bool value = true;
    };
  }

  ///
  /// @brief Specialization for sliced layouts merging
  ///
  template <class BaseLayout, class BaseExts, class SlicedExts, class... Slices,
    class... SliceSpecifiers>
  struct slice_creator<detail::layout_slice<BaseLayout, BaseExts, Slices...>,
    SlicedExts, SliceSpecifiers...>
  {
    typedef detail::layout_slice<BaseLayout, BaseExts, Slices...> parent_slice;

    typedef detail::slice_merger<parent_slice,
      std::make_index_sequence<sizeof...(Slices)>>
      merger_type;

    template <class BaseL, class BaseD, class... S>
    constexpr static auto make_extents(
      BaseL&& base, BaseD&& dims, S&&... slices)
    {
      return merger_type::make_extents(std::forward<BaseL>(base),
        base.get_base_extents(),
        std::forward_as_tuple(std::forward<S>(slices)...));
    }

    template <class BaseL, class BaseD, class... S>
    constexpr static auto create(BaseL&& base, BaseD&& dims, S&&... slices)
    {
      return merger_type::create(std::forward<BaseL>(base),
        std::forward_as_tuple(std::forward<S>(slices)...));
    }

    typedef decltype(create(std::declval<parent_slice>(),
      std::declval<typename parent_slice::extents_type>(),
      std::declval<SliceSpecifiers>()...)) type;
  };
}

#endif
