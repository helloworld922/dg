#ifndef DG_SPARSE_UTILS_TCC
#define DG_SPARSE_UTILS_TCC

#include "dg/utils/meta.hpp"

namespace dg
{
  template <class Instrs, class RowIdxr, class ColIdxr>
  template <class V, class Ts, class Us>
  void sparse_mat_builder_base<Instrs, RowIdxr, ColIdxr>::insert(
    Ts&& row_idcs, Us&& col_idcs, V&& value)
  {
    instrs.emplace_back(rows.get_index(std::forward<Ts>(row_idcs)),
      cols.get_index(std::forward<Us>(col_idcs)), value);
  }

  template <class Instrs, class RowIdxr, class ColIdxr>
  template <class Ts, class Us>
  auto sparse_mat_builder_base<Instrs, RowIdxr, ColIdxr>::block(
    Ts&& row_slices, Us&& col_slices)
  {
    auto lmbda = [](const auto& idxr, auto&&... args) {
      return idxr.subindexer(args...);
    };
    typedef decltype(meta::explode_args<std::make_index_sequence<
        std::tuple_size<std::decay_t<Ts>>::value>>::invoke(lmbda,
      std::forward<Ts>(row_slices), rows)) sub_row_;
    typedef decltype(meta::explode_args<std::make_index_sequence<
        std::tuple_size<std::decay_t<Us>>::value>>::invoke(lmbda,
      std::forward<Us>(col_slices), cols)) sub_col_;
    return sparse_mat_builder_base<Instrs, sub_row_, sub_col_>(instrs,
      meta::explode_args<std::make_index_sequence<
        std::tuple_size<std::decay_t<Ts>>::value>>::invoke(lmbda,
        std::forward<Ts>(row_slices), rows),
      meta::explode_args<std::make_index_sequence<
        std::tuple_size<std::decay_t<Us>>::value>>::invoke(lmbda,
        std::forward<Us>(col_slices), cols));
  }
}

#endif
