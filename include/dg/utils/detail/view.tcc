#ifndef DG_VIEW_TCC
#define DG_VIEW_TCC

namespace dg
{
  template <class... Vs>
  auto make_view_sequence(Vs&&... views)
  {
    view_sequence<std::decay_t<Vs>...> res(std::forward<Vs>(views)...);
    return res;
  }

  template <class V, class... Vs>
  auto make_view_array(V&& v, Vs&&... views)
  {
    dg::view_array<std::decay_t<V>, 1 + sizeof...(Vs)> res(
      std::forward<V>(v), std::forward<Vs>(views)...);
    return res;
  }
}

#endif
