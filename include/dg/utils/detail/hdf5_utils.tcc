#ifndef DG_HDF5_UTILS_TCC
#define DG_HDF5_UTILS_TCC

#include "dg/utils/layouts.hpp"

namespace dg
{
  namespace hdf5
  {
    template <class T>
    struct type_id;

    template <>
    struct type_id<char>
    {
      static hid_t value()
      {
        return H5T_NATIVE_CHAR;
      }
    };

    template <>
    struct type_id<unsigned char>
    {
      static hid_t value()
      {
        return H5T_NATIVE_UCHAR;
      }
    };

    template <>
    struct type_id<short>
    {
      static hid_t value()
      {
        return H5T_NATIVE_SHORT;
      }
    };

    template <>
    struct type_id<unsigned short>
    {
      static hid_t value()
      {
        return H5T_NATIVE_USHORT;
      }
    };

    template <>
    struct type_id<int>
    {
      static hid_t value()
      {
        return H5T_NATIVE_INT;
      }
    };

    template <>
    struct type_id<unsigned>
    {
      static hid_t value()
      {
        return H5T_NATIVE_UINT;
      }
    };

    template <>
    struct type_id<long>
    {
      static hid_t value()
      {
        return H5T_NATIVE_LONG;
      }
    };

    template <>
    struct type_id<unsigned long>
    {
      static hid_t value()
      {
        return H5T_NATIVE_ULONG;
      }
    };

    template <>
    struct type_id<long long>
    {
      static hid_t value()
      {
        return H5T_NATIVE_LLONG;
      }
    };

    template <>
    struct type_id<unsigned long long>
    {
      static hid_t value()
      {
        return H5T_NATIVE_ULLONG;
      }
    };

    template <>
    struct type_id<float>
    {
      static hid_t value()
      {
        return H5T_NATIVE_FLOAT;
      }
    };

    template <>
    struct type_id<double>
    {
      static hid_t value()
      {
        return H5T_NATIVE_DOUBLE;
      }
    };

    template <>
    struct type_id<const char>
    {
      static hid_t value()
      {
        return H5T_NATIVE_CHAR;
      }
    };

    template <>
    struct type_id<const unsigned char>
    {
      static hid_t value()
      {
        return H5T_NATIVE_UCHAR;
      }
    };

    template <>
    struct type_id<const short>
    {
      static hid_t value()
      {
        return H5T_NATIVE_SHORT;
      }
    };

    template <>
    struct type_id<const unsigned short>
    {
      static hid_t value()
      {
        return H5T_NATIVE_USHORT;
      }
    };

    template <>
    struct type_id<const int>
    {
      static hid_t value()
      {
        return H5T_NATIVE_INT;
      }
    };

    template <>
    struct type_id<const unsigned>
    {
      static hid_t value()
      {
        return H5T_NATIVE_UINT;
      }
    };

    template <>
    struct type_id<const long>
    {
      static hid_t value()
      {
        return H5T_NATIVE_LONG;
      }
    };

    template <>
    struct type_id<const unsigned long>
    {
      static hid_t value()
      {
        return H5T_NATIVE_ULONG;
      }
    };

    template <>
    struct type_id<const long long>
    {
      static hid_t value()
      {
        return H5T_NATIVE_LLONG;
      }
    };

    template <>
    struct type_id<const unsigned long long>
    {
      static hid_t value()
      {
        return H5T_NATIVE_ULLONG;
      }
    };

    template <>
    struct type_id<const float>
    {
      static hid_t value()
      {
        return H5T_NATIVE_FLOAT;
      }
    };

    template <>
    struct type_id<const double>
    {
      static hid_t value()
      {
        return H5T_NATIVE_DOUBLE;
      }
    };

    namespace detail
    {
      template <class T>
      struct dataset_writer;

      template <>
      struct dataset_writer<layout_right>
      {
        template <class T, size_t ndims>
        static void write(
          const view<T, ndims, layout_right>& v, hid_t d_id, hid_t target)
        {
          auto src = dataspace::simple(v.shape());
          H5Dwrite(
            d_id, type_id<T>::value(), src, target, H5P_DEFAULT, v.data());
        }

        template <class T, size_t ndims>
        static void read(view<T, ndims, layout_right>& v, hid_t d_id, hid_t src)
        {
          auto target = dataspace::simple(v.shape());
          H5Dread(
            d_id, type_id<T>::value(), target, src, H5P_DEFAULT, v.data());
        }

        template <class T, size_t N>
        static hid_t create_attr(const location& loc, const char* name,
          const view<T, N, layout_right>& v)
        {
          auto space = dataspace::simple(v.shape());
          auto res = H5Acreate2(
            loc, name, type_id<T>::value(), space, H5P_DEFAULT, H5P_DEFAULT);
          return res;
        }

        template <class T, size_t N>
        static void write_attr(
          const attribute& attr, const view<T, N, layout_right>& v)
        {
          // write data
          H5Awrite(attr, type_id<T>::value(), v.data());
        }

        template <class T, size_t N>
        static void read_attr(
          const attribute& attr, view<T, N, layout_right>& v)
        {
          // read data
          H5Aread(attr, type_id<T>::value(), v.data());
        }
      };

      // TODO: layout left?

      template <class Base, class OffsetType>
      struct dataset_writer<layout_offset<Base, OffsetType>>
      {
        template <class T, size_t ndims>
        static void write(
          const view<T, ndims, layout_offset<Base, OffsetType>>& v, hid_t d_id,
          hid_t target)
        {
          dataset_writer<Base>::write(
            dg::view<const T, ndims, Base>(v.data() + v.layout().offset,
              v.shape(), static_cast<Base>(v.layout())),
            d_id, target);
        }

        template <class T, size_t ndims>
        static void read(view<T, ndims, layout_offset<Base, OffsetType>>& v,
          hid_t d_id, hid_t src)
        {
          dg::view<T, ndims, Base> tmp(v.data() + v.layout().offset, v.shape(),
            static_cast<Base>(v.layout()));
          dataset_writer<Base>::read(tmp, d_id, src);
        }

        template <class T, size_t N>
        static hid_t create_attr(const location& loc, const char* name,
          const view<T, N, layout_offset<Base, OffsetType>>& v)
        {
          return dataset_writer<Base>::create_attr(
            loc, name, dg::view<const T, N, Base>(v.data() + v.layout().offset,
                         v.shape(), static_cast<Base>(v.layout())));
        }

        template <class T, size_t N>
        static void write_attr(const attribute& attr,
          const view<T, N, layout_offset<Base, OffsetType>>& v)
        {
          // write data
          dataset_writer<Base>::write_attr(
            attr, dg::view<const T, N, Base>(v.data() + v.layout().offset,
                    v.shape(), static_cast<Base>(v.layout())));
        }

        template <class T, size_t N>
        static void read_attr(
          const attribute& attr, view<T, N, layout_offset<Base, OffsetType>>& v)
        {
          // read data
          dg::view<T, N, Base> tmp(v.data() + v.layout().offset, v.shape(),
            static_cast<Base>(v.layout()));
          dataset_writer<Base>::read_attr(attr, tmp);
        }
      };

      ///
      /// @internal
      /// @brief Helper for classifying slices. Classifies integral types as
      /// having rank 0.
      ///
      template <class T,
        bool is_integral = std::is_integral<std::decay_t<T>>::value>
      struct classify_slice_rank
        : public std::integral_constant<size_t, std::tuple_size<T>::value>
      {
      };

      template <class T>
      struct classify_slice_rank<T, true>
        : public std::integral_constant<size_t, 0>
      {
      };

      template <class Seq, class SCont>
      struct slice_selector;

      template <size_t... Idcs, class... SliceSpecifiers>
      struct slice_selector<std::index_sequence<Idcs...>,
        std::tuple<SliceSpecifiers...>>
      {
        ///
        /// @tparam View a view with a slice layout.
        /// Expects the base layout to be layout_right
        ///
        template <class View>
        static void select(dataspace& space, const View& v)
        {
          auto& layout = v.layout();
          auto& exts = layout.get_base_extents();
          hsize_t starts[] = {dg::detail::slice_transform<classify_slice_rank<
            SliceSpecifiers>::value>::index(std::get<Idcs>(layout.get_slices()),
            0)...};
          hsize_t counts[] = {dg::detail::slice_transform<
            classify_slice_rank<SliceSpecifiers>::value>::extent(exts[Idcs],
            std::get<Idcs>(layout.get_slices()))...};
          // TODO: probably a better way to compute the stride?
          hsize_t strides[] = {
            dg::detail::slice_transform<classify_slice_rank<SliceSpecifiers>::
                value>::stride(std::get<Idcs>(layout.get_slices()))...};
          space.select_hyperslab(starts, counts, strides, nullptr);
        }
      };

      template <class BaseExts, class... SliceSpecifiers>
      struct dataset_writer<
        dg::detail::layout_slice<layout_right, BaseExts, SliceSpecifiers...>>
      {
        typedef dg::detail::layout_slice<layout_right, BaseExts,
          SliceSpecifiers...>
          layout_type;

        template <class T, size_t ndims>
        static void write(
          const view<T, ndims, layout_type>& v, hid_t d_id, hid_t target)
        {
          auto& layout = static_cast<const layout_type&>(v.layout());
          dataspace src =
            dg::hdf5::dataspace::simple(layout.get_base_extents());
          slice_selector<std::make_index_sequence<sizeof...(SliceSpecifiers)>,
            std::tuple<SliceSpecifiers...>>::select(src, v);
          H5Dwrite(
            d_id, type_id<T>::value(), src, target, H5P_DEFAULT, v.data());
        }

        template <class T, size_t ndims>
        static void read(view<T, ndims, layout_type>& v, hid_t d_id, hid_t src)
        {
          auto& layout = static_cast<const layout_type&>(v.layout());
          dataspace target =
            dg::hdf5::dataspace::simple(layout.get_base_extents());
          slice_selector<std::make_index_sequence<sizeof...(SliceSpecifiers)>,
            std::tuple<SliceSpecifiers...>>::select(target, v);
          H5Dread(
            d_id, type_id<T>::value(), target, src, H5P_DEFAULT, v.data());
        }

        template <class T, size_t N>
        static hid_t create_attr(const location& loc, const char* name,
          const view<T, N, layout_type>& v)
        {
          auto& layout = static_cast<const layout_type&>(v.layout());
          dataspace space =
            dg::hdf5::dataspace::simple(layout.get_base_extents());
          slice_selector<std::make_index_sequence<sizeof...(SliceSpecifiers)>,
            std::tuple<SliceSpecifiers...>>::select(space, v);
          auto res = H5Acreate2(
            loc, name, type_id<T>::value(), space, H5P_DEFAULT, H5P_DEFAULT);
          return res;
        }

        template <class T, size_t N>
        static void write_attr(
          const attribute& attr, const view<T, N, layout_type>& v)
        {
          // write data
          H5Awrite(attr, type_id<T>::value(),
            &v(static_cast<size_t>(
              std::is_same<SliceSpecifiers, SliceSpecifiers>::value)...));
        }

        template <class T, size_t N>
        static void read_attr(const attribute& attr, view<T, N, layout_type>& v)
        {
          // write data
          H5Aread(attr, type_id<T>::value(),
            &v(static_cast<size_t>(
              std::is_same<SliceSpecifiers, SliceSpecifiers>::value)...));
        }
      };

      struct slice_offset_helper
      {
        template <class Base, class BaseExts, class... SliceSpecifiers>
        auto operator()(
          Base&& base, BaseExts&& base_exts, SliceSpecifiers&&... slices) const
        {
          return dg::detail::layout_slice<std::decay_t<Base>,
            std::decay_t<BaseExts>, std::decay_t<SliceSpecifiers>...>(
            std::forward<Base>(base), std::forward<BaseExts>(base_exts),
            std::forward<SliceSpecifiers>(slices)...);
        }
      };

      template <class Base, class OffsetType, class BaseExts,
        class... SliceSpecifiers>
      struct dataset_writer<dg::detail::layout_slice<
        layout_offset<Base, OffsetType>, BaseExts, SliceSpecifiers...>>
      {
        typedef dg::detail::layout_slice<layout_offset<Base, OffsetType>,
          BaseExts, SliceSpecifiers...>
          layout_type;

        typedef dg::detail::layout_slice<Base, BaseExts, SliceSpecifiers...>
          base_layout_type;

        template <class T, size_t ndims>
        static void write(
          const view<T, ndims, layout_type>& v, hid_t d_id, hid_t target)
        {
          dg::view<T, ndims, base_layout_type> tmp(
            v.data() + v.layout().get_base_layout().offset, v.shape(),
            dg::meta::explode_args<std::make_index_sequence<sizeof...(
              SliceSpecifiers)>>::invoke(slice_offset_helper{},
              v.layout().get_slices(),
              static_cast<Base>(v.layout().get_base_layout()),
              v.layout().get_base_extents()));
          dg::hdf5::detail::dataset_writer<base_layout_type>::write(
            tmp, d_id, target);
        }

        template <class T, size_t ndims>
        static void read(view<T, ndims, layout_type>& v, hid_t d_id, hid_t src)
        {
          dg::view<T, ndims, base_layout_type> tmp(
            v.data() + v.layout().get_base_layout().offset, v.shape(),
            dg::meta::explode_args<std::make_index_sequence<sizeof...(
              SliceSpecifiers)>>::invoke(slice_offset_helper{},
              v.layout().get_slices(),
              static_cast<Base>(v.layout().get_base_layout()),
              v.layout().get_base_extents()));
          dg::hdf5::detail::dataset_writer<base_layout_type>::read(
            tmp, d_id, src);
        }

        template <class T, size_t N>
        static hid_t create_attr(const location& loc, const char* name,
          const view<T, N, layout_type>& v)
        {
          dg::view<T, N, base_layout_type> tmp(
            v.data() + v.layout().get_base_layout().offset, v.shape(),
            dg::meta::explode_args<std::make_index_sequence<sizeof...(
              SliceSpecifiers)>>::invoke(slice_offset_helper{},
              v.layout().get_slices(),
              static_cast<Base>(v.layout().get_base_layout()),
              v.layout().get_base_extents()));
          return dg::hdf5::detail::dataset_writer<
            base_layout_type>::create_attr(loc, name, tmp);
        }

        template <class T, size_t N>
        static void write_attr(
          const attribute& attr, const view<T, N, layout_type>& v)
        {
          dg::view<T, N, base_layout_type> tmp(
            v.data() + v.layout().get_base_layout().offset, v.shape(),
            dg::meta::explode_args<std::make_index_sequence<sizeof...(
              SliceSpecifiers)>>::invoke(slice_offset_helper{},
              v.layout().get_slices(),
              static_cast<Base>(v.layout().get_base_layout()),
              v.layout().get_base_extents()));
          dg::hdf5::detail::dataset_writer<base_layout_type>::write_attr(
            attr, tmp);
        }

        template <class T, size_t N>
        static void read_attr(const attribute& attr, view<T, N, layout_type>& v)
        {
          dg::view<T, N, base_layout_type> tmp(
            v.data() + v.layout().get_base_layout().offset, v.shape(),
            dg::meta::explode_args<std::make_index_sequence<sizeof...(
              SliceSpecifiers)>>::invoke(slice_offset_helper{},
              v.layout().get_slices(),
              static_cast<Base>(v.layout().get_base_layout()),
              v.layout().get_base_extents()));
          dg::hdf5::detail::dataset_writer<base_layout_type>::read_attr(
            attr, tmp);
        }
      };
    }

    template <class... Ts, class>
    dataspace dataspace::simple(Ts... dims)
    {
      dg::extents<sizeof...(Ts), hsize_t> size{static_cast<hsize_t>(dims)...};
      dataspace res(H5Screate_simple(sizeof...(Ts), size.data_, nullptr));
      return res;
    }

    template <size_t N, class T, class>
    dataspace dataspace::simple(const dg::extents<N, T>& e)
    {
      // convert to an hsize_t extent
      dg::extents<N, hsize_t> size;
      std::copy(e.data_, e.data_ + N, size.data_);
      dataspace res(H5Screate_simple(N, size.data_, nullptr));
      return res;
    }

    template <size_t N>
    dataspace dataspace::simple(const dg::extents<N, hsize_t>& e)
    {
      dataspace res(H5Screate_simple(N, e.data_, nullptr));
      return res;
    }

    template <class T>
    attribute::attribute(
      const location& loc, const char* name, const dataspace& space)
      : attribute(loc, name, type_id<T>::value(), space)
    {
    }

    template <class T, size_t N, class L>
    attribute::attribute(
      const location& loc, const char* name, const view<T, N, L>& v)
      : object(detail::dataset_writer<L>::create_attr(loc, name, v))
    {
      write(v);
    }

    template <class T, size_t N, class L>
    void attribute::write(const view<T, N, L>& v)
    {
      detail::dataset_writer<L>::write_attr(*this, v);
    }

    template <class T, size_t N, class L>
    void attribute::read(view<T, N, L>& v) const
    {
      detail::dataset_writer<L>::read_attr(*this, v);
    }

    template <class T, size_t N, class L>
    void dataset::write(const view<T, N, L>& v)
    {
      detail::dataset_writer<L>::write(v, get(), H5S_ALL);
    }

    template <class T, size_t N, class L>
    void dataset::read(view<T, N, L>& v) const
    {
      detail::dataset_writer<L>::read(v, get(), H5S_ALL);
    }

    template <class T, size_t N, class L>
    void dataset::write(const view<T, N, L>& v, const dataspace& target)
    {
      detail::dataset_writer<L>::write(v, get(), target);
    }

    template <class T, size_t N, class L>
    void dataset::read(view<T, N, L>& v, const dataspace& src) const
    {
      detail::dataset_writer<L>::read(v, get(), src);
    }
  }
}

#endif
