#ifndef MPI_UTILS_TCC
#define MPI_UTILS_TCC

namespace dg
{
  namespace mpi
  {
    template <>
    struct mpi_type<char>
    {
      static MPI_Datatype type()
      {
        return MPI_CHAR;
      }
    };

    template <>
    struct mpi_type<unsigned char>
    {
      static MPI_Datatype type()
      {
        return MPI_UNSIGNED_CHAR;
      }
    };

    template <>
    struct mpi_type<short>
    {
      static MPI_Datatype type()
      {
        return MPI_SHORT;
      }
    };

    template <>
    struct mpi_type<unsigned short>
    {
      static MPI_Datatype type()
      {
        return MPI_UNSIGNED_SHORT;
      }
    };

    template <>
    struct mpi_type<int>
    {
      static MPI_Datatype type()
      {
        return MPI_INT;
      }
    };

    template <>
    struct mpi_type<unsigned int>
    {
      static MPI_Datatype type()
      {
        return MPI_UNSIGNED;
      }
    };

    template <>
    struct mpi_type<long>
    {
      static MPI_Datatype type()
      {
        return MPI_LONG;
      }
    };

    template <>
    struct mpi_type<unsigned long>
    {
      static MPI_Datatype type()
      {
        return MPI_UNSIGNED_LONG;
      }
    };

    template <>
    struct mpi_type<long long>
    {
      static MPI_Datatype type()
      {
        return MPI_LONG_LONG;
      }
    };

    template <>
    struct mpi_type<unsigned long long>
    {
      static MPI_Datatype type()
      {
        return MPI_UNSIGNED_LONG_LONG;
      }
    };

    template <>
    struct mpi_type<float>
    {
      static MPI_Datatype type()
      {
        return MPI_FLOAT;
      }
    };

    template <>
    struct mpi_type<double>
    {
      static MPI_Datatype type()
      {
        return MPI_DOUBLE;
      }
    };

    template <>
    struct mpi_type<long double>
    {
      static MPI_Datatype type()
      {
        return MPI_LONG_DOUBLE;
      }
    };
  }
}

#endif
