#ifndef ARRAY_UTILS_PREF_TCC
#define ARRAY_UTILS_PREF_TCC

#include "dg/utils/compiler_support.hpp"

#include <array>

namespace dg
{
  namespace util
  {
    namespace detail
    {
      template<class Extent>
      struct iterator_base
      {
        std::array<size_t, Extent::rank()> idcs;
        Extent ext_;

        constexpr iterator_base(const Extent& e) : idcs{}, ext_(e)
        {}

        iterator_base(const iterator_base&) = default;

      protected:
        iterator_base& operator=(const iterator_base&) = default;
        ~iterator_base() = default;
      };

      template<class B>
      struct fill_invoker
      {
        const B& val;

        fill_invoker(const B& val) noexcept : val(val)
        {
        }

        template <class A>
        void operator()(A& a)
        {
          a = val;
        }
      };
    }
  }
}

#endif
