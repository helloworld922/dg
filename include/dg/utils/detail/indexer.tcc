#ifndef DG_INDEXER_TCC
#define DG_INDEXER_TCC

namespace dg
{
  template <class... Is>
  auto make_indexer_sequence(Is&&... indexers)
  {
    indexer_sequence<std::decay_t<Is>...> res(std::forward<Is>(indexers)...);
    return res;
  }
}

#endif
