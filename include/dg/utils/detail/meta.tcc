#ifndef DG_META_TCC
#define DG_META_TCC

namespace dg
{
  namespace meta
  {
    template <bool... Vals>
    struct all<false, Vals...> : public std::integral_constant<bool, false>
    {
    };

    template <bool... Vals>
    struct all<true, Vals...> : public all<Vals...>
    {
    };

    template <bool... Vals>
    struct any<false, Vals...> : public any<Vals...>
    {
    };

    template <bool... Vals>
    struct any<true, Vals...> : public std::integral_constant<bool, true>
    {
    };

    template <std::size_t I, class T, T Head, T... Tail>
    struct sequence_element<I, std::integer_sequence<T, Head, Tail...>>
    {
      /// @brief The extracted value
      constexpr static T value =
        sequence_element<I - 1, std::integer_sequence<T, Tail...>>::value;
    };

    template <class T, T Head, T... Tail>
    struct sequence_element<0, std::integer_sequence<T, Head, Tail...>>
    {
      /// @brief The extracted value
      constexpr static T value = Head;
    };

    template <class T, size_t Idx, T val, T Head, T... Tail>
    struct sequence_contains<T, val, std::integer_sequence<T, Head, Tail...>,
      Idx> : public std::conditional<val == Head, std::true_type,
               sequence_contains<T, val, std::integer_sequence<T, Tail...>,
                 Idx + 1>>::type
    {
      /// @brief The index of the found value, if any
      constexpr static size_t idx =
        (val == Head) ? Idx
                      : sequence_contains<T, val,
                          std::integer_sequence<T, Tail...>, Idx + 1>::idx;
    };

    template <class T, size_t Idx, T val>
    struct sequence_contains<T, val, std::integer_sequence<T>, Idx>
      : public std::false_type
    {
      /// @brief Size of sequence + 1 (the given value was not found)
      constexpr static size_t idx = Idx;
    };

    namespace detail
    {
      ///
      /// @brief Helper for pre-pending a type to a tuple
      ///
      template <class T, class Seq>
      struct tuple_builder;

      template <class T, class... Rest>
      struct tuple_builder<T, std::tuple<Rest...>>
      {
        /// @brief The tuple type
        typedef std::tuple<T, Rest...> type;
      };

      ///
      /// @brief Helper for pre-pending a value to an integer_sequence
      ///
      template <class T, T V, class Seq>
      struct sequence_builder;

      template <class T, T V, T... Rest>
      struct sequence_builder<T, V, std::integer_sequence<T, Rest...>>
      {
        /// @brief The integer_sequence type
        typedef std::integer_sequence<T, V, Rest...> type;
      };
    }

    template <>
    struct filter<>
    {
      ///
      /// @brief Types for which the filter evaluated true
      ///
      typedef std::tuple<> type;
    };

    template <class Head, class... Tail>
    struct filter<Head, Tail...>
    {
      ///
      /// @brief Types for which the filter evaluated true
      ///
      typedef typename std::conditional<Head::value,
        typename detail::tuple_builder<typename Head::type,
          typename filter<Tail...>::type>::type,
        typename filter<Tail...>::type>::type type;
    };

    template <size_t Idx>
    struct filter_idcs<Idx>
    {
      /// @brief The empty sequence if no predicates are given
      typedef std::integer_sequence<size_t> type;
    };

    template <std::size_t Idx, class Head, class... Tail>
    struct filter_idcs<Idx, Head, Tail...>
    {
      /// @brief The filtered sequence
      typedef typename std::conditional<Head::value,
        typename detail::sequence_builder<size_t, Idx,
          typename filter_idcs<Idx + 1, Tail...>::type>::type,
        typename filter_idcs<Idx + 1, Tail...>::type>::type type;
    };

    template <class T>
    struct filter_values<T>
    {
      /// @brief The empty sequence if no predicates are given
      typedef std::integer_sequence<T> type;
    };

    template <class T, class Head, class... Tail>
    struct filter_values<T, Head, Tail...>
    {
      /// @brief the filtered value sequence
      typedef typename std::conditional<Head::value,
        typename detail::sequence_builder<T, Head::stored,
          typename filter_values<T, Tail...>::type>::type,
        typename filter_values<T, Tail...>::type>::type type;
    };

    /// \cond DEV
    namespace detail
    {
      template <class T>
      struct is_callable_class_helper
      {
        // https://stackoverflow.com/questions/15393938/find-out-if-a-c-object-is-callable
      private:
        typedef char (&yes)[1];
        typedef char (&no)[2];

        struct Fallback
        {
          void operator()();
        };

        struct Derived : T, Fallback
        {
        };

        template <typename U, U>
        struct Check;

        template <typename>
        static yes test(...);

        template <typename C>
        static no test(Check<void (Fallback::*)(), &C::operator()>*);

      public:
        /// @brief True if the given object is callable
        constexpr static bool value = (sizeof(test<Derived>(0)) == sizeof(yes));
      };
    }
    /// \endcond

    
    // specialization for functor/lambda
    template <class T>
    struct is_callable<T,
      std::enable_if_t<
        !std::is_function<typename std::remove_pointer<T>::type>::value &&
        std::is_class<T>::value>>
      : std::conditional<detail::is_callable_class_helper<T>::value,
          std::true_type, std::false_type>::type
    {
    };
  }
}

#endif
