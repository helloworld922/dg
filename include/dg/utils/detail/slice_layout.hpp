#ifndef DG_SLICE_LAYOUT_HPP
#define DG_SLICE_LAYOUT_HPP

namespace dg
{
  /// \cond DEV
  namespace detail
  {

    template <class BaseLayout, class BaseExts, class... SliceSpecifiers>
    struct layout_slice;

    ///
    /// @internal
    /// @brief Helper class for computing the index from a slice specifier
    /// @tparam N number of slice specifier values.
    ///   0: fixed integer, or dg::all
    ///   1: [0, max)
    ///   2: [min, max)
    ///   3: [min, max), stride
    ///
    template <size_t N>
    struct slice_transform;

    ///
    /// @internal
    /// @brief Creates extents given a sequence of slice specifiers
    /// @tparam SliceSpecifiers
    /// @tparam Idcs list of indices to extract from SliceSpecifiers for making
    /// the extent
    ///
    template <class VarSliceTuple, class VarSliceIdcs>
    struct make_sliced_exts;

    ///
    /// @brief Internal implementation of the slice_layout
    /// @tparam BaseLayout The base layout, which is not a slice_layout
    /// @tparam BaseExts The base extents
    /// @tparam Vals Integer sequence for indexing into SliceSpecifiers,
    ///         starts at 0 and is contiguous up to sizeof...(SliceSpecifiers)
    /// @tparam SliceSpecifiers sequence of slice specifiers
    ///
    template <class BaseLayout, class BaseExts, size_t... SliceIdcs,
      class... SliceSpecifiers>
    struct slice_invoker<BaseLayout, BaseExts,
      std::index_sequence<SliceIdcs...>,
      std::tuple<SliceSpecifiers...>>
    {
      /// @brief size type to use
      typedef std::size_t size_type;
      /// @brief type for storing all the slices
      typedef std::tuple<SliceSpecifiers...> slice_container;

      /// @brief base layout type
      typedef BaseLayout base_layout;
      /// @brief base extents type
      typedef BaseExts base_extents;

      /// @brief base layout
      BaseLayout layout_;
      /// @brief base shape
      BaseExts dims_;
      /// @brief slices to apply
      slice_container slice_;

      ///
      /// @brief Slices which are still variable and must be specified by the
      /// input (non-integral slice)
      /// Note: there could be 0 or more elements in a var slice
      ///
      typedef typename meta::filter_idcs<0,
        meta::predicate<!std::is_integral<SliceSpecifiers>::value>...>::type
        var_idcs;

      ///
      /// @brief tuple of types which are variable slices
      ///
      typedef typename meta::filter<meta::predicate<
        !std::is_integral<SliceSpecifiers>::value, SliceSpecifiers>...>::type
        var_slices_types;

      ///
      /// @brief Sub extents type
      ///
      typedef typename make_sliced_exts<var_slices_types, var_idcs>::type
        extents_type;

      ///
      /// @brief Creates extents for the sliced layout
      ///
      template <class BaseE, class... S>
      constexpr static extents_type make_extents(
        const BaseE& base_ext, const S&... slices)
      {
        return make_sliced_exts<var_slices_types, var_idcs>::invoke(
          base_ext, std::forward_as_tuple(slices...));
      }

      ///
      /// @brief
      /// @param layout base layout
      /// @param dims base shape
      /// @param slices slices in each dimension of the base layout to take
      ///
      template <class BaseL, class BaseD, class... Slices>
      constexpr slice_invoker(BaseL&& layout, BaseD&& dims,
        Slices&&... slices) noexcept : layout_(std::forward<BaseL>(layout)),
                                       dims_(std::forward<BaseD>(dims)),
                                       slice_(std::forward<Slices>(slices)...)
      {
      }

      /// @brief copy constructor
      constexpr slice_invoker(const slice_invoker& o) = default;
      /// @brief move constructor
      constexpr slice_invoker(slice_invoker&& o) = default;

      /// @brief copy assignment
      slice_invoker& operator=(const slice_invoker& o) = default;
      /// @brief move assignment
      slice_invoker& operator=(slice_invoker&& o) = default;

      /// @brief Computes the flattened index for this slice
      template <class Cont>
      constexpr size_type operator()(Cont&& idcs) const noexcept
      {
        // reverse map of var_idcs
        return layout_(
          dims_,
          arg_selector<
            dg::meta::sequence_contains<size_t, SliceIdcs, var_idcs>::value,
            SliceSpecifiers,
            dg::meta::sequence_contains<size_t, SliceIdcs,
              var_idcs>::idx>::index(std::get<SliceIdcs>(slice_),
            idcs)...);
      }
    };

    // sanity check to ensure layout_slice doesn't recurse directly
    // specialization case in slice_layout.tcc
    template<class Layout>
    struct is_layout_slice
    {
      /// @brief given layout is not a slice layout
      constexpr static bool value = false;
    };

    ///
    /// @brief sliced layout.
    /// @tparam BaseLayout base layout type. Must not be another layout_slice.
    ///   Use slice_creator to take a sub slice of a slice layout
    /// @tparam BaseExts base extents type
    /// @tparam SliceSpecifiers slice specifiers
    ///
    template <class BaseLayout, class BaseExts, class... SliceSpecifiers>
    struct layout_slice
    {
      /// @brief size type to use
      typedef std::size_t size_type;
      static_assert(sizeof...(SliceSpecifiers) == BaseExts::rank(),
        "dimensions mismatch between BaseExts and SliceSpecifiers");

      static_assert(!is_layout_slice<BaseLayout>::value,
        "BaseLayout cannot be a layout_slice. Use slice_creator.");

      /// Invoker helper type that actually does heavy lifting
      typedef slice_invoker<BaseLayout, BaseExts,
        std::make_index_sequence<sizeof...(SliceSpecifiers)>,
        std::tuple<SliceSpecifiers...>>
        invoker_type;
      /// @brief The sliced extents type
      typedef typename invoker_type::extents_type extents_type;

    private:
      invoker_type invoker_;

    public:
      /// @brief Construct a sliced layout from the base layout
      template <class BaseL, class BaseD, class... S>
      constexpr layout_slice(BaseL&& base, BaseD&& dims, S&&... slices)
        : invoker_(std::forward<BaseL>(base), std::forward<BaseD>(dims),
            std::forward<S>(slices)...)
      {
      }

      /// @brief copy constructor
      constexpr layout_slice(const layout_slice& o) = default;

      /// @brief move constructor
      constexpr layout_slice(layout_slice&& o) = default;

      /// @brief Constructs the sliced shape from the base shape and slice specifiers
      template <class BaseE, class... S>
      constexpr static extents_type make_extents(
        const BaseE& base_ext, const S&... slices)
      {
        return invoker_type::make_extents(base_ext, slices...);
      }

      ///
      /// @brief Gets the base layout
      ///
      const auto& get_base_layout() const noexcept
      {
        return invoker_.layout_;
      }

      /// @brief Gets the slice specifiers
      const auto& get_slices() const noexcept
      {
        return invoker_.slice_;
      }

      ///
      /// @brief Returns the base extents of the underlying data
      ///
      const auto& get_base_extents() const noexcept
      {
        return invoker_.dims_;
      }

      /// @brief copy constructor
      layout_slice& operator=(const layout_slice& o) = default;

      /// @brief move constructor
      layout_slice& operator=(layout_slice&& o) = default;

      ///
      /// @brief Converts the given indices to a flattened index for a sliced layout 
      ///
      template <class Exts, class... Idcs>
      constexpr size_type operator()(const Exts& d, Idcs... idcs) const noexcept
      {
        static_assert(
          invoker_type::var_idcs::size() == std::decay_t<Exts>::rank(),
          "Dimensions mismatch");
        return invoker_(
          std::forward_as_tuple(std::forward<Idcs>(idcs)...));
      }
    };
  }
  /// \endcond
}

#include "slice_layout.tcc"

#endif
