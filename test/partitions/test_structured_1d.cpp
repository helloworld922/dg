#include "dg/partitions/structured.hpp"
#include "dg/context.hpp"

#include "tutils/tutils.hpp"

TEST(test_structured_1d, constructor1)
{
  dg::partitions::structured p(0, 1, 100, 1, 10, false);

  EXPECT_EQ(p.global_idxr.extent(0), 100);
  EXPECT_EQ(p.global_idxr.extent(1), 2);
  EXPECT_EQ(p.global_idxr.extent(2), 10);

  EXPECT_EQ(p.global_idxr.index(0, 0, 0), 10);
  EXPECT_EQ(p.global_idxr.index(99, 1, 9), 10 + 99 * 2 * 10 + 1 * 10 + 9);

  EXPECT_EQ(p.global_left.extent(0), 10);
  EXPECT_EQ(p.global_right.extent(0), 10);

  EXPECT_EQ(p.global_left.index(0), 0);
  EXPECT_EQ(p.global_right.index(0), 100 * 2 * 10 + 10);
  EXPECT_EQ(p.bc_L_global.index(0), 0);
  EXPECT_EQ(p.bc_R_global.index(0), 100 * 2 * 10 + 10);

  EXPECT_EQ(p.local_idxr.extent(0), 100);
  EXPECT_EQ(p.local_idxr.extent(1), 2);
  EXPECT_EQ(p.local_idxr.extent(2), 10);

  EXPECT_EQ(p.local_idxr.index(0, 0, 0), 10);
  EXPECT_EQ(p.local_idxr.index(99, 1, 9), 10 + 99 * 2 * 10 + 1 * 10 + 9);

  EXPECT_EQ(p.local_left.index(0), 0);
  EXPECT_EQ(p.local_right.index(0), 100 * 2 * 10);
  
  EXPECT_EQ(p.local_elem_start, 0);
  EXPECT_EQ(p.local_elem_stop, 100);
  EXPECT_EQ(p.nelems, 100);
  EXPECT_EQ(p.degree, 1);
  EXPECT_EQ(p.num_comps, 10);
  EXPECT_EQ(p.periodic, false);
}

TEST(test_structured_1d, constructor2)
{
  dg::partitions::structured p(0, 1, 100, 1, 10, true);

  EXPECT_EQ(p.global_idxr.extent(0), 100);
  EXPECT_EQ(p.global_idxr.extent(1), 2);
  EXPECT_EQ(p.global_idxr.extent(2), 10);

  EXPECT_EQ(p.global_idxr.index(0, 0, 0), 0);
  EXPECT_EQ(p.global_idxr.index(99, 1, 9), 99 * 2 * 10 + 1 * 10 + 9);

  EXPECT_EQ(p.global_left.extent(0), 10);
  EXPECT_EQ(p.global_right.extent(0), 10);

  EXPECT_EQ(p.global_left.index(0), 99 * 2 * 10 + 1 * 10);
  EXPECT_EQ(p.global_right.index(0), 0);

  EXPECT_EQ(p.local_idxr.extent(0), 100);
  EXPECT_EQ(p.local_idxr.extent(1), 2);
  EXPECT_EQ(p.local_idxr.extent(2), 10);

  EXPECT_EQ(p.local_idxr.index(0, 0, 0), 0);
  EXPECT_EQ(p.local_idxr.index(99, 1, 9), 99 * 2 * 10 + 1 * 10 + 9);

  EXPECT_EQ(p.local_left.index(0), 99 * 2 * 10 + 1 * 10);
  EXPECT_EQ(p.local_right.index(0), 0);
}

TEST(test_structured_1d, constructor3)
{
  dg::partitions::structured p(0, 5, 100, 1, 10, false);

  EXPECT_EQ(p.global_idxr.extent(0), 20);
  EXPECT_EQ(p.global_idxr.extent(1), 2);
  EXPECT_EQ(p.global_idxr.extent(2), 10);

  EXPECT_EQ(p.global_idxr.index(0, 0, 0), 10);
  EXPECT_EQ(p.global_idxr.index(19, 1, 9), 10 + 19 * 2 * 10 + 1 * 10 + 9);

  EXPECT_EQ(p.global_left.extent(0), 10);
  EXPECT_EQ(p.global_right.extent(0), 10);

  EXPECT_EQ(p.global_left.index(0), 0);
  EXPECT_EQ(p.global_right.index(0), 20 * 2 * 10 + 10);
  EXPECT_EQ(p.bc_L_global.index(0), 0);

  EXPECT_EQ(p.local_idxr.extent(0), 20);
  EXPECT_EQ(p.local_idxr.extent(1), 2);
  EXPECT_EQ(p.local_idxr.extent(2), 10);

  EXPECT_EQ(p.local_idxr.index(0, 0, 0), 10);
  EXPECT_EQ(p.local_idxr.index(19, 1, 9), 10 + 19 * 2 * 10 + 1 * 10 + 9);

  EXPECT_EQ(p.local_left.index(0), 0);
  EXPECT_EQ(p.local_right.index(0), 20 * 2 * 10);
}

TEST(test_structured_1d, constructor4)
{
  dg::partitions::structured p(1, 5, 100, 1, 10, false);
  EXPECT_EQ(p.global_idxr.extent(0), 20);
  EXPECT_EQ(p.global_idxr.extent(1), 2);
  EXPECT_EQ(p.global_idxr.extent(2), 10);

  EXPECT_EQ(p.global_idxr.index(0, 0, 0), 10 + 20 * 2 * 10);
  EXPECT_EQ(p.global_idxr.index(19, 1, 9), 10 + 39 * 2 * 10 + 1 * 10 + 9);

  EXPECT_EQ(p.global_left.extent(0), 10);
  EXPECT_EQ(p.global_right.extent(0), 10);

  EXPECT_EQ(p.global_left.index(0), 20 * 2 * 10);
  EXPECT_EQ(p.global_right.index(0), 40 * 2 * 10 + 10);

  EXPECT_EQ(p.local_idxr.extent(0), 20);
  EXPECT_EQ(p.local_idxr.extent(1), 2);
  EXPECT_EQ(p.local_idxr.extent(2), 10);

  EXPECT_EQ(p.local_idxr.index(0, 0, 0), 0);
  EXPECT_EQ(p.local_idxr.index(19, 1, 9), 19 * 2 * 10 + 1 * 10 + 9);

  EXPECT_EQ(p.local_left.index(0), 20 * 2 * 10);
  EXPECT_EQ(p.local_right.index(0), 20 * 2 * 10 + 10);
}

TEST(test_structured_1d, constructor5)
{
  dg::partitions::structured p(4, 5, 100, 1, 10, false);
  EXPECT_EQ(p.global_idxr.extent(0), 20);
  EXPECT_EQ(p.global_idxr.extent(1), 2);
  EXPECT_EQ(p.global_idxr.extent(2), 10);

  EXPECT_EQ(p.global_idxr.index(0, 0, 0), 10 + 80 * 2 * 10);
  EXPECT_EQ(p.global_idxr.index(19, 1, 9), 10 + 99 * 2 * 10 + 1 * 10 + 9);

  EXPECT_EQ(p.global_left.extent(0), 10);
  EXPECT_EQ(p.global_right.extent(0), 10);

  EXPECT_EQ(p.global_left.index(0), 80 * 2 * 10);
  EXPECT_EQ(p.global_right.index(0), 100 * 2 * 10 + 10);
  EXPECT_EQ(p.bc_R_global.index(0), 10 + 100 * 2 * 10);

  EXPECT_EQ(p.local_idxr.extent(0), 20);
  EXPECT_EQ(p.local_idxr.extent(1), 2);
  EXPECT_EQ(p.local_idxr.extent(2), 10);

  EXPECT_EQ(p.local_idxr.index(0, 0, 0), 0);
  EXPECT_EQ(p.local_idxr.index(19, 1, 9), 19 * 2 * 10 + 1 * 10 + 9);

  EXPECT_EQ(p.local_left.index(0), 20 * 2 * 10 + 10);
  EXPECT_EQ(p.local_right.index(0), 20 * 2 * 10);
}

TEST(test_structured_1d, constructor6)
{
  dg::partitions::structured p(0, 5, 100, 1, 10, true);
  EXPECT_EQ(p.global_idxr.extent(0), 20);
  EXPECT_EQ(p.global_idxr.extent(1), 2);
  EXPECT_EQ(p.global_idxr.extent(2), 10);

  EXPECT_EQ(p.global_idxr.index(0, 0, 0), 0);
  EXPECT_EQ(p.global_idxr.index(19, 1, 9), 19 * 2 * 10 + 1 * 10 + 9);

  EXPECT_EQ(p.global_left.extent(0), 10);
  EXPECT_EQ(p.global_right.extent(0), 10);

  EXPECT_EQ(p.global_left.index(0), 100 * 2 * 10 - 10);
  EXPECT_EQ(p.global_right.index(0), 20 * 2 * 10);

  EXPECT_EQ(p.local_idxr.extent(0), 20);
  EXPECT_EQ(p.local_idxr.extent(1), 2);
  EXPECT_EQ(p.local_idxr.extent(2), 10);

  EXPECT_EQ(p.local_idxr.index(0, 0, 0), 0);
  EXPECT_EQ(p.local_idxr.index(19, 1, 9), 19 * 2 * 10 + 1 * 10 + 9);

  EXPECT_EQ(p.local_left.index(0), 20 * 2 * 10);
  EXPECT_EQ(p.local_right.index(0), 20 * 2 * 10 + 10);
}

TEST(test_structured_1d, constructor7)
{
  dg::partitions::structured p(1, 5, 100, 1, 10, true);
  EXPECT_EQ(p.global_idxr.extent(0), 20);
  EXPECT_EQ(p.global_idxr.extent(1), 2);
  EXPECT_EQ(p.global_idxr.extent(2), 10);

  EXPECT_EQ(p.global_idxr.index(0, 0, 0), 20 * 2 * 10);
  EXPECT_EQ(p.global_idxr.index(19, 1, 9), 39 * 2 * 10 + 1 * 10 + 9);

  EXPECT_EQ(p.global_left.extent(0), 10);
  EXPECT_EQ(p.global_right.extent(0), 10);

  EXPECT_EQ(p.global_left.index(0), 20 * 2 * 10 - 10);
  EXPECT_EQ(p.global_right.index(0), 40 * 2 * 10);

  EXPECT_EQ(p.local_idxr.extent(0), 20);
  EXPECT_EQ(p.local_idxr.extent(1), 2);
  EXPECT_EQ(p.local_idxr.extent(2), 10);

  EXPECT_EQ(p.local_idxr.index(0, 0, 0), 0);
  EXPECT_EQ(p.local_idxr.index(19, 1, 9), 19 * 2 * 10 + 1 * 10 + 9);

  EXPECT_EQ(p.local_left.index(0), 20 * 2 * 10);
  EXPECT_EQ(p.local_right.index(0), 20 * 2 * 10 + 10);
}

TEST(test_structured_1d, constructor8)
{
  dg::partitions::structured p(4, 5, 100, 1, 10, true);
  EXPECT_EQ(p.global_idxr.extent(0), 20);
  EXPECT_EQ(p.global_idxr.extent(1), 2);
  EXPECT_EQ(p.global_idxr.extent(2), 10);

  EXPECT_EQ(p.global_idxr.index(0, 0, 0), 80 * 2 * 10);
  EXPECT_EQ(p.global_idxr.index(19, 1, 9), 99 * 2 * 10 + 1 * 10 + 9);

  EXPECT_EQ(p.global_left.extent(0), 10);
  EXPECT_EQ(p.global_right.extent(0), 10);

  EXPECT_EQ(p.global_left.index(0), 80 * 2 * 10 - 10);
  EXPECT_EQ(p.global_right.index(0), 0);

  EXPECT_EQ(p.local_idxr.extent(0), 20);
  EXPECT_EQ(p.local_idxr.extent(1), 2);
  EXPECT_EQ(p.local_idxr.extent(2), 10);

  EXPECT_EQ(p.local_idxr.index(0, 0, 0), 0);
  EXPECT_EQ(p.local_idxr.index(19, 1, 9), 19 * 2 * 10 + 1 * 10 + 9);

  EXPECT_EQ(p.local_left.index(0), 20 * 2 * 10);
  EXPECT_EQ(p.local_right.index(0), 20 * 2 * 10 + 10);
}

TEST(test_structured_1d, calc_ghost_idcs1)
{
  dg::partitions::structured p(0, 1, 100, 1, 5, false);
  auto idcs = p.calc_ghost_idcs();
  std::vector<PetscInt> expected = {};
  EXPECT_RANGE_EQ(idcs, expected);
}

TEST(test_structured_1d, calc_ghost_idcs2)
{
  dg::partitions::structured p(0, 1, 100, 1, 5, false);
  auto idcs = p.calc_ghost_idcs();
  std::vector<PetscInt> expected = {};
  EXPECT_RANGE_EQ(idcs, expected);
}

TEST(test_structured_1d, calc_ghost_idcs3)
{
  dg::partitions::structured p(0, 5, 100, 1, 5, false);
  auto idcs = p.calc_ghost_idcs();
  std::vector<PetscInt> expected = {
    static_cast<PetscInt>(p.global_idxr.index(20, 0, 0)),
    static_cast<PetscInt>(p.global_idxr.index(20, 0, 1)),
    static_cast<PetscInt>(p.global_idxr.index(20, 0, 2)),
    static_cast<PetscInt>(p.global_idxr.index(20, 0, 3)),
    static_cast<PetscInt>(p.global_idxr.index(20, 0, 4))};
  EXPECT_RANGE_EQ(idcs, expected);
}

TEST(test_structured_1d, calc_ghost_idcs4)
{
  dg::partitions::structured p(1, 5, 100, 1, 5, false);
  auto idcs = p.calc_ghost_idcs();
  std::vector<PetscInt> expected = {
    static_cast<PetscInt>(p.global_idxr.index(0, 0, 0)) - 5,
    static_cast<PetscInt>(p.global_idxr.index(0, 0, 1)) - 5,
    static_cast<PetscInt>(p.global_idxr.index(0, 0, 2)) - 5,
    static_cast<PetscInt>(p.global_idxr.index(0, 0, 3)) - 5,
    static_cast<PetscInt>(p.global_idxr.index(0, 0, 4)) - 5,
    static_cast<PetscInt>(p.global_idxr.index(20, 0, 0)),
    static_cast<PetscInt>(p.global_idxr.index(20, 0, 1)),
    static_cast<PetscInt>(p.global_idxr.index(20, 0, 2)),
    static_cast<PetscInt>(p.global_idxr.index(20, 0, 3)),
    static_cast<PetscInt>(p.global_idxr.index(20, 0, 4))};
  EXPECT_RANGE_EQ(idcs, expected);
}

TEST(test_structured_1d, calc_ghost_idcs5)
{
  dg::partitions::structured p(4, 5, 100, 1, 5, false);
  auto idcs = p.calc_ghost_idcs();
  std::vector<PetscInt> expected = {
    static_cast<PetscInt>(p.global_idxr.index(0, 0, 0)) - 5,
    static_cast<PetscInt>(p.global_idxr.index(0, 0, 1)) - 5,
    static_cast<PetscInt>(p.global_idxr.index(0, 0, 2)) - 5,
    static_cast<PetscInt>(p.global_idxr.index(0, 0, 3)) - 5,
    static_cast<PetscInt>(p.global_idxr.index(0, 0, 4)) - 5};
  EXPECT_RANGE_EQ(idcs, expected);
}

TEST(test_structured_1d, calc_ghost_idcs6)
{
  dg::partitions::structured p(0, 5, 100, 1, 5, true);
  auto idcs = p.calc_ghost_idcs();
  std::vector<PetscInt> expected = {
    static_cast<PetscInt>(p.global_idxr.index(100, 0, 0)) - 5,
    static_cast<PetscInt>(p.global_idxr.index(100, 0, 1)) - 5,
    static_cast<PetscInt>(p.global_idxr.index(100, 0, 2)) - 5,
    static_cast<PetscInt>(p.global_idxr.index(100, 0, 3)) - 5,
    static_cast<PetscInt>(p.global_idxr.index(100, 0, 4)) - 5,
    static_cast<PetscInt>(p.global_idxr.index(20, 0, 0)),
    static_cast<PetscInt>(p.global_idxr.index(20, 0, 1)),
    static_cast<PetscInt>(p.global_idxr.index(20, 0, 2)),
    static_cast<PetscInt>(p.global_idxr.index(20, 0, 3)),
    static_cast<PetscInt>(p.global_idxr.index(20, 0, 4))};
  EXPECT_RANGE_EQ(idcs, expected);
}

TEST(test_structured_1d, calc_ghost_idcs7)
{
  dg::partitions::structured p(1, 5, 100, 1, 5, true);
  auto idcs = p.calc_ghost_idcs();
  std::vector<PetscInt> expected = {
    static_cast<PetscInt>(p.global_idxr.index(0, 0, 0)) - 5,
    static_cast<PetscInt>(p.global_idxr.index(0, 0, 1)) - 5,
    static_cast<PetscInt>(p.global_idxr.index(0, 0, 2)) - 5,
    static_cast<PetscInt>(p.global_idxr.index(0, 0, 3)) - 5,
    static_cast<PetscInt>(p.global_idxr.index(0, 0, 4)) - 5,
    static_cast<PetscInt>(p.global_idxr.index(20, 0, 0)),
    static_cast<PetscInt>(p.global_idxr.index(20, 0, 1)),
    static_cast<PetscInt>(p.global_idxr.index(20, 0, 2)),
    static_cast<PetscInt>(p.global_idxr.index(20, 0, 3)),
    static_cast<PetscInt>(p.global_idxr.index(20, 0, 4))};
  EXPECT_RANGE_EQ(idcs, expected);
}

TEST(test_structured_1d, calc_ghost_idcs8)
{
  dg::partitions::structured p(4, 5, 100, 1, 5, true);
  auto idcs = p.calc_ghost_idcs();
  std::vector<PetscInt> expected = {
    static_cast<PetscInt>(p.global_idxr.index(0, 0, 0)) - 5,
    static_cast<PetscInt>(p.global_idxr.index(0, 0, 1)) - 5,
    static_cast<PetscInt>(p.global_idxr.index(0, 0, 2)) - 5,
    static_cast<PetscInt>(p.global_idxr.index(0, 0, 3)) - 5,
    static_cast<PetscInt>(p.global_idxr.index(0, 0, 4)) - 5, 0, 1, 2, 3, 4};
  EXPECT_RANGE_EQ(idcs, expected);
}

TEST(test_structured_1d, alloc1)
{
  dg::mpi::comm comm;
  MPI_Comm_dup(MPI_COMM_WORLD, &comm.reset());
  int rank, nprocs;
  MPI_Comm_rank(comm, &rank);
  MPI_Comm_size(comm, &nprocs);
  dg::partitions::structured p(rank, nprocs, 100, 1, 5, true);
  dg::petsc::vector vec = p.alloc(comm);
  
  PetscInt low, high;
  VecGetOwnershipRange(vec, &low, &high);
  EXPECT_EQ(low, p.global_idxr.index(0,0,0));
  EXPECT_EQ(high, p.global_idxr.index(p.global_idxr.extent(0), 0, 0));
}

TEST(test_structured_1d, alloc2)
{
  dg::mpi::comm comm;
  MPI_Comm_dup(MPI_COMM_WORLD, &comm.reset());
  int rank, nprocs;
  MPI_Comm_rank(comm, &rank);
  MPI_Comm_size(comm, &nprocs);
  dg::partitions::structured p(rank, nprocs, 100, 1, 5, false);
  dg::petsc::vector vec = p.alloc(comm);

  PetscInt size;
  VecGetSize(vec, &size);
  EXPECT_EQ(size, 100 * 2 * 5 + 2 * 5);

  PetscInt low, high;
  VecGetOwnershipRange(vec, &low, &high);

  if (rank == 0)
  {
    if(nprocs == 1)
    {
      EXPECT_EQ(low, 0);
      EXPECT_EQ(high, 100 * 2 * 5 + 2 * 5);
    }
    else
    {
      EXPECT_EQ(low, 0);
      EXPECT_EQ(high, 100 / nprocs * 2 * 5 + 5);
    }
  }
  else if(rank == nprocs - 1)
  {
    EXPECT_EQ(low, p.global_idxr.index(0, 0, 0));
    EXPECT_EQ(high, p.global_idxr.index(p.global_idxr.extent(0), 0, 0) + 5);
  }
  else
  {
    EXPECT_EQ(low, p.global_idxr.index(0, 0, 0));
    EXPECT_EQ(high, p.global_idxr.index(p.global_idxr.extent(0), 0, 0));
  }
}

int main(int argc, char** argv)
{
  init_tutils(&argc, argv);

  dg::context ctx(argc, argv);

  return run_tutils();
}
