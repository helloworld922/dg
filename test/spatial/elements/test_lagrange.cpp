#include "dg/spatial/elements/lagrange.hpp"

#include "dg/utils/hslab.hpp"

#include "tutils/tutils.hpp"

TEST(test_lagrange, size)
{
  using dg::spatial::elements::lagrange;
  // 1D
  EXPECT_EQ((lagrange<double, 1>::size()), 2);
  EXPECT_EQ((lagrange<double, 2>::size()), 3);
  EXPECT_EQ((lagrange<double, 3>::size()), 4);

  // 2D
  EXPECT_EQ((lagrange<double, 1, 1>::size()), 2 * 2);
  EXPECT_EQ((lagrange<double, 2, 1>::size()), 3 * 2);
  EXPECT_EQ((lagrange<double, 1, 2>::size()), 2 * 3);

  EXPECT_EQ((lagrange<double, 1, 1, 1>::size()), 2 * 2 * 2);
  EXPECT_EQ((lagrange<double, 2, 1, 3>::size()), 3 * 2 * 4);
  EXPECT_EQ((lagrange<double, 1, 2, 4>::size()), 2 * 3 * 5);
}

TEST(test_lagrange, constructor)
{
  constexpr dg::lobatto<double> quad{};
  auto expected2 = quad.abscissas(2);
  auto weights2 = quad.weights(expected2);
  auto expected3 = quad.abscissas(3);
  auto weights3 = quad.weights(expected3);
  auto expected6 = quad.abscissas(6);
  auto weights6 = quad.weights(expected6);
  {
    dg::spatial::elements::lagrange<double, 1, 1> elem;
    EXPECT_EQ(elem.absc_data_.size(), 4);
    EXPECT_EQ(elem.absc.size(), 2);

    EXPECT_EQ(elem.absc[0].size(), 2);
    EXPECT_EQ(elem.absc[0](0), expected2[0]);
    EXPECT_EQ(elem.absc[0](1), expected2[1]);
    EXPECT_EQ(elem.weights[0](0), weights2[0]);
    EXPECT_EQ(elem.weights[0](1), weights2[1]);

    EXPECT_EQ(elem.absc[1].size(), 2);
    EXPECT_EQ(elem.absc[1](0), expected2[0]);
    EXPECT_EQ(elem.absc[1](1), expected2[1]);
    EXPECT_EQ(elem.weights[1](0), weights2[0]);
    EXPECT_EQ(elem.weights[1](1), weights2[1]);
  }
  {
    dg::spatial::elements::lagrange<double, 2, 1> elem;

    EXPECT_EQ(elem.absc_data_.size(), 5);
    EXPECT_EQ(elem.absc.size(), 2);
    EXPECT_EQ(elem.absc[0].size(), 3);
    EXPECT_EQ(elem.absc[0](0), expected3[0]);
    EXPECT_EQ(elem.absc[0](1), expected3[1]);
    EXPECT_EQ(elem.absc[0](2), expected3[2]);
    EXPECT_EQ(elem.weights[0](0), weights3[0]);
    EXPECT_EQ(elem.weights[0](1), weights3[1]);
    EXPECT_EQ(elem.weights[0](2), weights3[2]);

    EXPECT_EQ(elem.absc[1].size(), 2);
    EXPECT_EQ(elem.absc[1](0), expected2[0]);
    EXPECT_EQ(elem.absc[1](1), expected2[1]);
    EXPECT_EQ(elem.weights[1](0), weights2[0]);
    EXPECT_EQ(elem.weights[1](1), weights2[1]);
  }
  {
    dg::spatial::elements::lagrange<double, 1, 2> elem;
    EXPECT_EQ(elem.absc_data_.size(), 5);
    EXPECT_EQ(elem.absc.size(), 2);
    EXPECT_EQ(elem.absc[0].size(), 2);
    EXPECT_EQ(elem.absc[0](0), expected2[0]);
    EXPECT_EQ(elem.absc[0](1), expected2[1]);
    EXPECT_EQ(elem.weights[0](0), weights2[0]);
    EXPECT_EQ(elem.weights[0](1), weights2[1]);

    EXPECT_EQ(elem.absc[1].size(), 3);
    EXPECT_EQ(elem.absc[1](0), expected3[0]);
    EXPECT_EQ(elem.absc[1](1), expected3[1]);
    EXPECT_EQ(elem.absc[1](2), expected3[2]);
    EXPECT_EQ(elem.weights[1](0), weights3[0]);
    EXPECT_EQ(elem.weights[1](1), weights3[1]);
    EXPECT_EQ(elem.weights[1](2), weights3[2]);
  }
  {
    dg::spatial::elements::lagrange<double, 5, 5> elem;
    EXPECT_EQ(elem.absc_data_.size(), 12);
    EXPECT_EQ(elem.absc.size(), 2);
    EXPECT_EQ(elem.absc[0].size(), 6);
    for (size_t i = 0; i < 6; ++i)
    {
      EXPECT_EQ(elem.absc[0](i), expected6[i]);
      EXPECT_EQ(elem.weights[0](i), weights6[i]);
    }
    EXPECT_EQ(elem.absc[1].size(), 6);
    for (size_t i = 0; i < 6; ++i)
    {
      EXPECT_EQ(elem.absc[1](i), expected6[i]);
      EXPECT_EQ(elem.weights[1](i), weights6[i]);
    }
  }
}

TEST(test_lagrange, eval128_2d)
{
  dg::real128 tol{"1e-31"};
  {
    dg::spatial::elements::lagrange<dg::real128, 1, 1> elem;
    std::array<dg::real128, 2> xi = {0};
    for (size_t i = 0; i <= 50; ++i)
    {
      xi[0] = dg::real128{".04"} * i - 1;
      SCOPED_TRACE(xi[0]);
      for (size_t j = 0; j <= 50; ++j)
      {
        xi[1] = dg::real128{".04"} * j - 1;
        SCOPED_TRACE(xi[1]);
        EXPECT_NEAR(elem(xi, 0, 0), (1 - xi[0]) * (1 - xi[1]) / 4, tol);
        EXPECT_NEAR(elem(xi, 1, 0), (xi[0] + 1) * (1 - xi[1]) / 4, tol);
        EXPECT_NEAR(elem(xi, 0, 1), (1 - xi[0]) * (xi[1] + 1) / 4, tol);
        EXPECT_NEAR(elem(xi, 1, 1), (xi[0] + 1) * (xi[1] + 1) / 4, tol);
      }
    }
  }

  {
    dg::spatial::elements::lagrange<dg::real128, 2, 1> elem;
    std::array<dg::real128, 2> xi = {0};
    for (size_t i = 0; i <= 50; ++i)
    {
      xi[0] = dg::real128{".04"} * i - 1;
      SCOPED_TRACE(xi[0]);
      for (size_t j = 0; j <= 50; ++j)
      {
        xi[1] = dg::real128{".04"} * j - 1;
        SCOPED_TRACE(xi[1]);
        EXPECT_NEAR(
          elem(xi, 0, 0), -xi[0] * (xi[0] - 1) * (xi[1] - 1) / 4, tol);
        EXPECT_NEAR(
          elem(xi, 1, 0), (xi[0] - 1) * (xi[0] + 1) * (xi[1] - 1) / 2, tol);
        EXPECT_NEAR(
          elem(xi, 2, 0), -xi[0] * (xi[0] + 1) * (xi[1] - 1) / 4, tol);

        EXPECT_NEAR(elem(xi, 0, 1), xi[0] * (xi[0] - 1) * (xi[1] + 1) / 4, tol);
        EXPECT_NEAR(
          elem(xi, 1, 1), -(xi[0] - 1) * (xi[0] + 1) * (xi[1] + 1) / 2, tol);
        EXPECT_NEAR(elem(xi, 2, 1), xi[0] * (xi[0] + 1) * (xi[1] + 1) / 4, tol);
      }
    }
  }
}

TEST(test_lagrange, deriv128_2d)
{
  dg::real128 tol{"1e-31"};
  {
    dg::spatial::elements::lagrange<dg::real128, 1, 1> elem;
    std::array<dg::real128, 2> xi = {0};
    for (size_t i = 0; i <= 50; ++i)
    {
      xi[0] = dg::real128{".04"} * i - 1;
      SCOPED_TRACE(xi[0]);
      for (size_t j = 0; j <= 50; ++j)
      {
        xi[1] = dg::real128{".04"} * j - 1;
        SCOPED_TRACE(xi[1]);
        EXPECT_NEAR(elem.deriv<0>(xi, 0, 0), (xi[1] - 1) / 4, tol);
        EXPECT_NEAR(elem.deriv<1>(xi, 0, 0), (xi[0] - 1) / 4, tol);

        EXPECT_NEAR(elem.deriv<0>(xi, 1, 0), -(xi[1] - 1) / 4, tol);
        EXPECT_NEAR(elem.deriv<1>(xi, 1, 0), -(xi[0] + 1) / 4, tol);

        EXPECT_NEAR(elem.deriv<0>(xi, 0, 1), -(xi[1] + 1) / 4, tol);
        EXPECT_NEAR(elem.deriv<1>(xi, 0, 1), -(xi[0] - 1) / 4, tol);

        EXPECT_NEAR(elem.deriv<0>(xi, 1, 1), (xi[1] + 1) / 4, tol);
        EXPECT_NEAR(elem.deriv<1>(xi, 1, 1), (xi[0] + 1) / 4, tol);
      }
    }
  }

  {
    dg::spatial::elements::lagrange<dg::real128, 2, 1> elem;
    std::array<dg::real128, 2> xi = {0};
    for (size_t i = 0; i <= 50; ++i)
    {
      xi[0] = dg::real128{".04"} * i - 1;
      SCOPED_TRACE(xi[0]);
      for (size_t j = 0; j <= 50; ++j)
      {
        xi[1] = dg::real128{".04"} * j - 1;
        SCOPED_TRACE(xi[1]);

        EXPECT_NEAR(elem.deriv<0>(xi, 0, 0),
          -((xi[0] - 1) + xi[0]) * (xi[1] - 1) / 4, tol);
        EXPECT_NEAR(elem.deriv<1>(xi, 0, 0), -xi[0] * (xi[0] - 1) / 4, tol);

        EXPECT_NEAR(elem.deriv<0>(xi, 1, 0),
          ((xi[0] - 1) + (xi[0] + 1)) * (xi[1] - 1) / 2, tol);
        EXPECT_NEAR(
          elem.deriv<1>(xi, 1, 0), (xi[0] - 1) * (xi[0] + 1) / 2, tol);

        EXPECT_NEAR(elem.deriv<0>(xi, 2, 0),
          -(xi[0] + (xi[0] + 1)) * (xi[1] - 1) / 4, tol);
        EXPECT_NEAR(elem.deriv<1>(xi, 2, 0), -xi[0] * (xi[0] + 1) / 4, tol);

        EXPECT_NEAR(elem.deriv<0>(xi, 0, 1),
          ((xi[0] - 1) + xi[0]) * (xi[1] + 1) / 4, tol);
        EXPECT_NEAR(elem.deriv<1>(xi, 0, 1), xi[0] * (xi[0] - 1) / 4, tol);

        EXPECT_NEAR(elem.deriv<0>(xi, 1, 1),
          -((xi[0] + 1) + (xi[0] - 1)) * (xi[1] + 1) / 2, tol);
        EXPECT_NEAR(
          elem.deriv<1>(xi, 1, 1), -(xi[0] - 1) * (xi[0] + 1) / 2, tol);

        EXPECT_NEAR(elem.deriv<0>(xi, 2, 1),
          ((xi[0] + 1) + xi[0]) * (xi[1] + 1) / 4, tol);
        EXPECT_NEAR(elem.deriv<1>(xi, 2, 1), xi[0] * (xi[0] + 1) / 4, tol);
      }
    }
  }
}

TEST(test_lagrange, mass_matrix_2d)
{
  // computes the mass matrix of the canonical element
  dg::real128 tol{"1e-31"};
  constexpr size_t elem_nodes = 2;
  constexpr size_t int_nodes = 3;
  dg::spatial::elements::lagrange<dg::real128, elem_nodes - 1, elem_nodes - 1>
    elem;
  dg::spatial::elements::lagrange<dg::real128, int_nodes - 1, int_nodes - 1>
    integrator;

  // expected mass matrix:
  // [[4/9,2/9,2/9,1/9],
  //  [2/9,4/9,1/9,2/9],
  //  [2/9,1/9,4/9,2/9],
  //  [1/9,2/9,2/9,4/9]]
  std::vector<dg::real128> expected = {dg::real128{4} / 9, dg::real128{2} / 9,
    dg::real128{2} / 9, dg::real128{1} / 9, dg::real128{2} / 9,
    dg::real128{4} / 9, dg::real128{1} / 9, dg::real128{2} / 9,
    dg::real128{2} / 9, dg::real128{1} / 9, dg::real128{4} / 9,
    dg::real128{2} / 9, dg::real128{1} / 9, dg::real128{2} / 9,
    dg::real128{2} / 9, dg::real128{4} / 9};

  for (size_t i = 0; i < elem_nodes; ++i)
  {
    for (size_t j = 0; j < elem_nodes; ++j)
    {
      auto row = 2 * i + j;
      SCOPED_TRACE(row);
      for (size_t ii = 0; ii < elem_nodes; ++ii)
      {
        for (size_t jj = 0; jj < elem_nodes; ++jj)
        {
          auto col = 2 * ii + jj;
          SCOPED_TRACE(col);

          dg::real128 res = integrator.vol_integral(
            [&elem, i, j, ii, jj](const std::array<dg::real128, 2>& xi) {
              return elem(xi, i, j) * elem(xi, ii, jj);
            });
          EXPECT_NEAR(res, expected[row * 4 + col], tol);
        }
      }
    }
  }
}

TEST(test_lagrange, surf_integral)
{
  {
    dg::spatial::elements::lagrange<dg::real128, 3, 3> elem;
    dg::real128 tol{"1e-31"};
    EXPECT_NEAR(elem.surf_integral<0>([](auto& xi) { return xi[0] + 1; }),
      dg::real128{0}, tol);
    EXPECT_NEAR(elem.surf_integral<1>([](auto& xi) { return xi[0] + 1; }),
      dg::real128{4}, tol);
    EXPECT_NEAR(elem.surf_integral<2>([](auto& xi) { return xi[0] + 1; }),
      dg::real128{2}, tol);
    EXPECT_NEAR(elem.surf_integral<3>([](auto& xi) { return xi[0] + 1; }),
      dg::real128{2}, tol);
  }
}

TEST(test_lagrange, interp)
{
  dg::spatial::elements::lagrange<double, 1, 1> flat_elem;
  dg::shslab<double, 4, 2> field(2, 2);
  field(0, 0) = 0;
  field(1, 0) = 1;
  field(0, 1) = 2;
  field(1, 1) = 3;
  {
    std::array<double, 2> xi = {-1, -1};
    EXPECT_NEAR(flat_elem.interp(xi, field), 0, 1e-13);
  }
  {
    std::array<double, 2> xi = {1, -1};
    EXPECT_NEAR(flat_elem.interp(xi, field), 1, 1e-13);
  }
  {
    std::array<double, 2> xi = {-1, 1};
    EXPECT_NEAR(flat_elem.interp(xi, field), 2, 1e-13);
  }
  {
    std::array<double, 2> xi = {1, 1};
    EXPECT_NEAR(flat_elem.interp(xi, field), 3, 1e-13);
  }
}

TEST(test_lagrange, jacobian_1d)
{
  dg::spatial::elements::lagrange<double, 1> flat_elem;

  {
    // canonical element
    std::array<double, 2> buffer = {-1, 1};
    dg::view<double, 2> coords(buffer.data(), 2, 1);
    std::array<double, 1> xi = {0};
    for (size_t i_x = 0; i_x <= 10; ++i_x)
    {
      SCOPED_TRACE(i_x);
      xi[0] = i_x / 5. - 1;
      auto jac = flat_elem.jacobian(xi, coords);
      EXPECT_NEAR(jac(0, 0), 0.5 * (coords(1, 0) - coords(0, 0)), 1e-15);
    }
  }
  {
    // slightly more complicated element
    std::array<double, 2> buffer = {-1, 3};
    dg::view<double, 2> coords(buffer.data(), 2, 1);
    std::array<double, 1> xi = {0};
    for (size_t i_x = 0; i_x <= 10; ++i_x)
    {
      SCOPED_TRACE(i_x);
      xi[0] = i_x / 5. - 1;
      auto jac = flat_elem.jacobian(xi, coords);
      EXPECT_NEAR(jac(0, 0), 0.5 * (coords(1, 0) - coords(0, 0)), 1e-15);
    }
  }
}

TEST(test_lagrange, jacobian_2d)
{
  dg::spatial::elements::lagrange<double, 1, 1> flat_elem;

  auto expected_jac = [&flat_elem](auto& expected, auto& coords, auto& xi) {
    expected(0, 0) = coords(0, 0, 0) * flat_elem.deriv<0>(xi, 0, 0) +
                     coords(0, 1, 0) * flat_elem.deriv<0>(xi, 0, 1) +
                     coords(1, 0, 0) * flat_elem.deriv<0>(xi, 1, 0) +
                     coords(1, 1, 0) * flat_elem.deriv<0>(xi, 1, 1);
    expected(0, 1) = coords(0, 0, 0) * flat_elem.deriv<1>(xi, 0, 0) +
                     coords(0, 1, 0) * flat_elem.deriv<1>(xi, 0, 1) +
                     coords(1, 0, 0) * flat_elem.deriv<1>(xi, 1, 0) +
                     coords(1, 1, 0) * flat_elem.deriv<1>(xi, 1, 1);
    expected(1, 0) = coords(0, 0, 1) * flat_elem.deriv<0>(xi, 0, 0) +
                     coords(0, 1, 1) * flat_elem.deriv<0>(xi, 0, 1) +
                     coords(1, 0, 1) * flat_elem.deriv<0>(xi, 1, 0) +
                     coords(1, 1, 1) * flat_elem.deriv<0>(xi, 1, 1);
    expected(1, 1) = coords(0, 0, 1) * flat_elem.deriv<1>(xi, 0, 0) +
                     coords(0, 1, 1) * flat_elem.deriv<1>(xi, 0, 1) +
                     coords(1, 0, 1) * flat_elem.deriv<1>(xi, 1, 0) +
                     coords(1, 1, 1) * flat_elem.deriv<1>(xi, 1, 1);
  };
  {
    // canonical element
    std::array<double, 8> buffer = {-1, -1, -1, 1, 1, -1, 1, 1};
    dg::view<double, 3> coords(buffer.data(), 2, 2, 2);
    std::array<double, 2> xi = {0, 0};
    for (size_t i_x = 0; i_x <= 10; ++i_x)
    {
      SCOPED_TRACE(i_x);
      xi[0] = i_x / 5. - 1;
      for (size_t i_y = 0; i_y <= 10; ++i_y)
      {
        SCOPED_TRACE(i_y);
        xi[1] = i_y / 5. - 1;
        auto jac = flat_elem.jacobian(xi, coords);
        dg::shslab<double, 4, 2> expected(2, 2);
        expected_jac(expected, coords, xi);
        for (size_t row = 0; row < 2; ++row)
        {
          SCOPED_TRACE(row);
          for (size_t col = 0; col < 2; ++col)
          {
            SCOPED_TRACE(col);
            EXPECT_NEAR(jac(row, col), expected(row, col), 1e-15);
          }
        }
      }
    }
  }
  {
    // slightly more complicated element
    std::array<double, 8> buffer = {-1, -1, -2, 1, 3, -1, 2, 2};
    dg::view<double, 3> coords(buffer.data(), 2, 2, 2);
    std::array<double, 2> xi = {0, 0};
    for (size_t i_x = 0; i_x <= 10; ++i_x)
    {
      SCOPED_TRACE(i_x);
      xi[0] = i_x / 5. - 1;
      for (size_t i_y = 0; i_y <= 10; ++i_y)
      {
        SCOPED_TRACE(i_y);
        xi[1] = i_y / 5. - 1;
        auto jac = flat_elem.jacobian(xi, coords);
        dg::shslab<double, 4, 2> expected(2, 2);
        expected_jac(expected, coords, xi);
        for (size_t row = 0; row < 2; ++row)
        {
          SCOPED_TRACE(row);
          for (size_t col = 0; col < 2; ++col)
          {
            SCOPED_TRACE(col);
            EXPECT_NEAR(jac(row, col), expected(row, col), 1e-15);
          }
        }
      }
    }
  }
}

int main(int argc, char** argv)
{
  init_tutils(&argc, argv);
  return run_tutils();
}
