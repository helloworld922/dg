#include "dg/utils/view.hpp"
#include "dg/utils/view_utils.hpp"

#include "tutils/tutils.hpp"

#include <array>

TEST(test_view_utils, fill)
{
  std::array<int, 2 * 3> buffer = {0};
  dg::view<int, 2> aview(buffer.data(), dg::extents<2>{2, 3});
  dg::util::fill(aview, 1);

  std::array<int, 2 * 3> res{1,1,1,1,1,1};
  EXPECT_RANGE_EQ(buffer, res);
}

int main(int argc, char **argv)
{
  init_tutils(&argc, argv);
  return run_tutils();
}

