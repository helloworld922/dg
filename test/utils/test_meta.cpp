#include "dg/utils/meta.hpp"
#include "tutils/tutils.hpp"

struct not_functor
{
};

TEST(test_meta, not_callable)
{
  EXPECT_FALSE(dg::meta::is_callable<int>::value);
  EXPECT_FALSE(dg::meta::is_callable<not_functor>::value);
}

TEST(test_meta, callable_function)
{
  typedef void (*myfunc0)();
  typedef void (*myfunc1)(int);
  EXPECT_TRUE(dg::meta::is_callable<void()>::value);
  EXPECT_TRUE(dg::meta::is_callable<void(int)>::value);
  EXPECT_TRUE(dg::meta::is_callable<myfunc0>::value);
  EXPECT_TRUE(dg::meta::is_callable<myfunc1>::value);

#if 0
  EXPECT_EQ(dg::meta::number_of_args<void()>::value, 0);
  EXPECT_EQ(dg::meta::number_of_args<void(int)>::value, 1);
  EXPECT_EQ(dg::meta::number_of_args<myfunc0>::value, 0);
  EXPECT_EQ(dg::meta::number_of_args<myfunc1>::value, 1);
#endif
}

struct functor1
{
  template<class T>
  void operator()(const T& arg)
  {
  }
};

struct functor2
{
  void operator()(int a)
  {
  }

  void operator()(int a, int b)
  {
  }
};


struct functor3
{
  void operator()(int a, int b)
  {
  }
};

TEST(test_meta, callable_functor)
{
  EXPECT_TRUE(dg::meta::is_callable<functor1>::value);
  EXPECT_TRUE(dg::meta::is_callable<functor2>::value);
  EXPECT_TRUE(dg::meta::is_callable<functor3>::value);

#if 0
  EXPECT_EQ(dg::meta::number_of_args<functor1>::value, 1);
  EXPECT_EQ(dg::meta::number_of_args<functor3>::value, 2);
#endif
}

TEST(test_meta, callable_lambda)
{
  auto lmbda1 = [](){};
  auto lmbda2 = [](int a) { return a; };
  EXPECT_TRUE(dg::meta::is_callable<decltype(lmbda1)>::value);
  EXPECT_TRUE(dg::meta::is_callable<decltype(lmbda2)>::value);

#if 0
  EXPECT_EQ(dg::meta::number_of_args<decltype(lmbda1)>::value, 0);
  EXPECT_EQ(dg::meta::number_of_args<decltype(lmbda2)>::value, 1);
#endif
}

int main(int argc, char** argv)
{
  init_tutils(&argc, argv);
  return run_tutils();
}
