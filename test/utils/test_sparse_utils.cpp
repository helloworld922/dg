#include "dg/utils/sparse_utils.hpp"

#include "dg/utils/instructions_list.hpp"
#include "dg/utils/indexer.hpp"

#include "tutils/tutils.hpp"

TEST(test_sparse_utils, insert)
{
  {
    dg::sparse_mat_builder<dg::instructions_list, dg::indexer<1>,
      dg::indexer<1>>
      builder(dg::indexer<1>(10), dg::indexer<1>(5));
    builder.insert(std::make_tuple(0), std::make_tuple(1), 5);

    std::vector<PetscInt> expected_rows = {0};
    std::vector<PetscInt> expected_cols = {1};
    std::vector<PetscScalar> expected_values = {5};
    EXPECT_RANGE_EQ(builder.instrs.rows, expected_rows);
    EXPECT_RANGE_EQ(builder.instrs.cols, expected_cols);
    EXPECT_RANGE_NEAR(builder.instrs.values, expected_values, 1e-13);
  }
  {
    dg::sparse_mat_builder<dg::instructions_list, dg::indexer<2>,
      dg::indexer<2>>
      builder(dg::indexer<2>(10, 5), dg::indexer<2>(5, 7));
    builder.insert(std::make_tuple(3, 1), std::make_tuple(2, 3), 5);

    std::vector<PetscInt> expected_rows = {3 * 5 + 1};
    std::vector<PetscInt> expected_cols = {2 * 7 + 3};
    std::vector<PetscScalar> expected_values = {5};
    EXPECT_RANGE_EQ(builder.instrs.rows, expected_rows);
    EXPECT_RANGE_EQ(builder.instrs.cols, expected_cols);
    EXPECT_RANGE_NEAR(builder.instrs.values, expected_values, 1e-13);
  }
}

TEST(test_sparse_utils, block)
{
  dg::sparse_mat_builder<dg::instructions_list, dg::indexer<2>, dg::indexer<2>>
    builder(dg::indexer<2>(10, 5), dg::indexer<2>(4, 7));
  auto sub_builder =
    builder.block(std::make_tuple(3, dg::all{}), std::make_tuple(dg::all{}, 4));
  EXPECT_EQ(sub_builder.rows.rank(), 1);
  EXPECT_EQ(sub_builder.cols.rank(), 1);
  EXPECT_EQ(sub_builder.height(), 5);
  EXPECT_EQ(sub_builder.width(), 4);

  sub_builder.insert(std::make_tuple(1), std::make_tuple(2), 5);

  std::vector<PetscInt> expected_rows = {3 * 5 + 1};
  std::vector<PetscInt> expected_cols = {2 * 7 + 4};
  std::vector<PetscScalar> expected_values = {5};
  EXPECT_RANGE_EQ(builder.instrs.rows, expected_rows);
  EXPECT_RANGE_EQ(builder.instrs.cols, expected_cols);
  EXPECT_RANGE_NEAR(builder.instrs.values, expected_values, 1e-13);
}

int main(int argc, char** argv)
{
  init_tutils(&argc, argv);
  return run_tutils();
}
