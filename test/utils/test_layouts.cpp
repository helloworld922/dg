#include "dg/utils/layouts.hpp"
#include "tutils/tutils.hpp"

#include <array>


TEST(test_layouts, extents_2d)
{
  typedef dg::extents<2> extents_type;
  EXPECT_EQ(extents_type::rank(), 2);
  extents_type extent{4, 2};
  EXPECT_EQ(extent.size(), 4 * 2);
  EXPECT_EQ(extent[0], 4);
  EXPECT_EQ(extent[1], 2);
}

TEST(test_layouts, layout_left)
{
  constexpr dg::layout_left layout{};
  {
    typedef dg::extents<3> extents_type;
    constexpr extents_type extent{2,3,4};
    EXPECT_EQ(layout(extent, 1, 1, 1), 1 + 2 + 2 * 3);
    EXPECT_EQ(layout(extent, 1, 2, 1), 1 + 2 * 2 + 2 * 3);
  }
}

TEST(test_layouts, layout_right)
{
  constexpr dg::layout_right layout{};
  {
    typedef dg::extents<3> extents_type;
    constexpr extents_type extent{2, 3, 4};
    EXPECT_EQ(layout(extent, 1, 1, 1), 1 * 3 * 4 + 4 + 1);
    EXPECT_EQ(layout(extent, 1, 2, 1), 1 * 3 * 4 + 2 * 4 + 1);
  }
}

int main(int argc, char** argv)
{
  init_tutils(&argc, argv);
  return run_tutils();
}
