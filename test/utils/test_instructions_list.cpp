#include "dg/utils/instructions_list.hpp"

#include "tutils/tutils.hpp"

TEST(test_instructions_list, compress1)
{
  dg::instructions_list instrs;
  instrs.emplace_back(0, 0, 1);
  instrs.emplace_back(0, 0, 1);
  instrs.compress();
  std::vector<PetscInt> expected_rows = {0};
  std::vector<PetscInt> expected_cols = {0};
  std::vector<PetscScalar> expected_values = {2};

  EXPECT_RANGE_EQ(instrs.rows, expected_rows);
  EXPECT_RANGE_EQ(instrs.cols, expected_cols);
  EXPECT_RANGE_NEAR(instrs.values, expected_values, 1e-13);
}

TEST(test_instructions_list, compress2)
{
  dg::instructions_list instrs;
  instrs.emplace_back(0, 0, 1);
  instrs.emplace_back(0, 0, 1);
  instrs.emplace_back(0, 1, 2);
  instrs.emplace_back(0, 2, 3);
  instrs.emplace_back(0, 2, 4);
  instrs.emplace_back(0, 2, 4);
  instrs.compress();
  std::vector<PetscInt> expected_rows = {0, 0, 0};
  std::vector<PetscInt> expected_cols = {0, 1, 2};
  std::vector<PetscScalar> expected_values = {2, 2, 3 + 4 + 4};

  EXPECT_RANGE_EQ(instrs.rows, expected_rows);
  EXPECT_RANGE_EQ(instrs.cols, expected_cols);
  EXPECT_RANGE_NEAR(instrs.values, expected_values, 1e-13);
}

TEST(test_instructions_list, compress3)
{
  dg::instructions_list instrs;
  instrs.emplace_back(0, 0, 1);
  instrs.emplace_back(0, 1, 1);
  instrs.compress();
  instrs.compress();

  std::vector<PetscInt> expected_rows = {0, 0};
  std::vector<PetscInt> expected_cols = {0, 1};
  std::vector<PetscScalar> expected_values = {1, 1};

  EXPECT_RANGE_EQ(instrs.rows, expected_rows);
  EXPECT_RANGE_EQ(instrs.cols, expected_cols);
  EXPECT_RANGE_NEAR(instrs.values, expected_values, 1e-13);
}

int main(int argc, char** argv)
{
  init_tutils(&argc, argv);
  return run_tutils();
}
