#include "dg/utils/hslab.hpp"

#include "tutils/tutils.hpp"

TEST(test_hslab, basic)
{
  typedef dg::extents<2> extents_type;
  dg::hslab<int, extents_type::rank()> slab(2, 3);
  EXPECT_EQ(slab.extent(0), 2);
  EXPECT_EQ(slab.extent(1), 3);
  slab(0, 0) = 1;
  slab(0, 1) = 3;
  EXPECT_EQ(slab(0, 0), 1);
  EXPECT_EQ(slab(0, 1), 3);
  dg::view<int, extents_type::rank()> v(slab);
  EXPECT_EQ(v.rank(), 2);
  EXPECT_EQ(v.extent(0), 2);
  EXPECT_EQ(v.extent(1), 3);
}

int main(int argc, char** argv)
{
  init_tutils(&argc, argv);
  return run_tutils();
}
