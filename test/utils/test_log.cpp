#include "dg/utils/log.hpp"

#include "tutils/tutils.hpp"

TEST(test_log, trace)
{
  dg::log::logger log(dg::log::severity_level::info);
  DG_TRACE(log, warning) << "hello";
}

TEST(test_log, ftrace)
{
  dg::log::logger log(dg::log::severity_level::info);
  DG_FTRACE(log, info) << "hello";
}

TEST(test_log, log)
{
  dg::log::logger log(dg::log::severity_level::info);
  DG_LOG(log, trace) << "hello";
}

TEST(test_log, info)
{
  dg::log::logger log(dg::log::severity_level::info);
  DG_INFO(log, info) << "hello";
}

int main(int argc, char** argv)
{
  init_tutils(&argc, argv);
  return run_tutils();
}
