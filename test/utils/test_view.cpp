#include "dg/utils/view.hpp"
#include "dg/utils/hslab.hpp"

#include "tutils/tutils.hpp"

#include <array>
#include <vector>

TEST(test_view, basic)
{
  typedef dg::extents<2> extents_type;
  std::array<int, 2 * 3> buffer = {6, 7, 3, 4564, 1, 2};
  dg::view<int, extents_type::rank()> aview1(nullptr, 2, 3);
  EXPECT_EQ(aview1.data(), nullptr);
  const auto& cview1 = aview1;
  EXPECT_EQ(cview1.data(), nullptr);
  EXPECT_EQ(aview1.rank(), 2);
  EXPECT_EQ(cview1.rank(), 2);
  EXPECT_EQ(aview1.extent(0), 2);
  EXPECT_EQ(cview1.extent(0), 2);
  EXPECT_EQ(aview1.extent(1), 3);
  EXPECT_EQ(cview1.extent(1), 3);
  aview1.shape();
  cview1.shape();
  aview1.layout();
  cview1.layout();
  EXPECT_EQ(aview1.size(), 6);
  EXPECT_EQ(cview1.size(), 6);
  aview1.data() = buffer.data();
  EXPECT_EQ(cview1.data(), buffer.data());
}

TEST(test_view, indexing)
{
  typedef dg::extents<2> extents_type;
  std::array<int, 2 * 3> buffer = {6, 7, 3, 4564, 1, 2};
  extents_type extent{2, 3};
  ASSERT_EQ(extent[0], 2);
  ASSERT_EQ(extent[1], 3);
  dg::view<int, extents_type::rank()> aview1(buffer.data(), extent);
  const auto& cview1 = aview1;
  ASSERT_EQ(cview1.extent(0), 2);
  ASSERT_EQ(cview1.extent(1), 3);
  for (size_t i = 0; i < 2; ++i)
  {
    for (size_t j = 0; j < 3; ++j)
    {
      EXPECT_EQ(aview1(i, j), cview1(i, j));
      EXPECT_EQ(aview1(i, j), buffer[i * 3 + j]);
    }
  }
}

TEST(test_view, fixed_subarray)
{
  typedef dg::extents<2> extents_type;
  std::array<int, 2 * 3> buffer = {6, 7, 3, 4564, 1, 2};
  extents_type extent{2, 3};
  dg::view<int, extents_type::rank()> aview1(buffer.data(), extent);
  const auto& cview1 = aview1;

  // fixed slice
  auto sview1 = cview1.subarray(1, 2);
  EXPECT_EQ(sview1.rank(), 0);
  EXPECT_EQ(sview1.size(), 0);
  EXPECT_EQ(sview1(), aview1(1, 2));
}

TEST(test_view, max_subarray)
{
  typedef dg::extents<2> extents_type;
  std::array<int, 2 * 3> buffer = {6, 7, 3, 4564, 1, 2};
  extents_type extent{2, 3};
  dg::view<int, extents_type::rank()> aview1(buffer.data(), extent);
  const auto& cview1 = aview1;

  // max slice
  auto sview1 = cview1.subarray(std::make_tuple(2), std::make_tuple(2));
  EXPECT_EQ(sview1.rank(), 2);
  EXPECT_EQ(sview1.size(), 4);
  for (size_t i = 0; i < 2; ++i)
  {
    for (size_t j = 0; j < 2; ++j)
    {
      EXPECT_EQ(sview1(i, j), aview1(i, j));
    }
  }
}

TEST(test_view, minmax_subarray)
{
  typedef dg::extents<2> extents_type;
  std::array<int, 2 * 3> buffer = {6, 7, 3, 4564, 1, 2};
  extents_type extent{2, 3};
  dg::view<int, extents_type::rank()> aview1(buffer.data(), extent);
  const auto& cview1 = aview1;

  // max slice
  auto sview1 = cview1.subarray(std::make_tuple(2), std::make_tuple(1, 3));
  EXPECT_EQ(sview1.rank(), 2);
  EXPECT_EQ(sview1.size(), 4);
  for (size_t i = 0; i < 2; ++i)
  {
    for (size_t j = 0; j < 2; ++j)
    {
      EXPECT_EQ(sview1(i, j), aview1(i, j + 1));
    }
  }
}

TEST(test_view, stride_subarray)
{
  typedef dg::extents<2> extents_type;
  std::array<int, 2 * 10> buffer;
  for (size_t i = 0; i < 20; ++i)
  {
    buffer[i] = i;
  }
  extents_type extent{2, 10};
  dg::view<int, extents_type::rank()> aview1(buffer.data(), extent);
  const auto& cview1 = aview1;

  // max slice
  auto sview1 = cview1.subarray(std::make_tuple(2), std::make_tuple(1, 9, 2));
  EXPECT_EQ(sview1.rank(), 2);
  EXPECT_EQ(sview1.size(), 2 * 4);
  for (size_t i = 0; i < 2; ++i)
  {
    for (size_t j = 0; j < 4; ++j)
    {
      EXPECT_EQ(sview1(i, j), aview1(i, 2 * j + 1));
    }
  }
}

TEST(test_view, mixed_subarray)
{
  typedef dg::extents<3> extents_type;
  std::vector<int> buffer(2 * 5 * 10);
  for (size_t i = 0; i < buffer.size(); ++i)
  {
    buffer[i] = i;
  }
  extents_type extent{2, 5, 10};
  dg::view<int, extents_type::rank()> aview1(buffer.data(), extent);
  const auto& cview1 = aview1;

  // max slice
  auto sview1 =
    cview1.subarray(std::make_tuple(2), 4, std::make_tuple(1, 9, 2));
  EXPECT_EQ(sview1.rank(), 2);
  EXPECT_EQ(sview1.size(), 2 * 4);
  for (size_t i = 0; i < 2; ++i)
  {
    for (size_t j = 0; j < 4; ++j)
    {
      EXPECT_EQ(sview1(i, j), aview1(i, 4, 2 * j + 1));
    }
  }
}

TEST(test_view, recursive_slice)
{
  dg::hslab<int, 3> aview(3 * 4, 3 * 4, 3 * 4);
  for(size_t i = 0; i < aview.extent(0); ++i)
  {
    for(size_t j = 0; j < aview.extent(1); ++j)
    {
      for(size_t k = 0; k < aview.extent(2); ++k)
      {
        aview(i, j, k) = aview.index(i, j, k);
      }
    }
  }

  // test slice layout merging
  
  {
    auto sview = aview.subarray(dg::all{}, dg::all{}, dg::all{});
    // dg::all{} -> dg::all{}
    // dg::all{} -> fixed
    // dg::all{} -> max slice
    {
      auto ssview = sview.subarray(dg::all{}, 2, std::make_tuple(5));
      EXPECT_EQ(ssview.ndims(), 2);
      EXPECT_EQ(ssview.extent(0), sview.extent(0));
      EXPECT_EQ(ssview.extent(1), 5);
      for(size_t i = 0; i < ssview.extent(0); ++i)
      {
        for (size_t j = 0; j < ssview.extent(1); ++j)
        {
          EXPECT_EQ(ssview(i, j), sview(i, 2, j));
        }
      }
    }
    // dg::all{} -> min/max slice
    // dg::all{} -> min/max/stride slice
    {
      auto ssview = sview.subarray(std::make_tuple(2, 5), std::make_tuple(2, 11, 3), 4);
      EXPECT_EQ(ssview.ndims(), 2);
      EXPECT_EQ(ssview.extent(0), 3);
      EXPECT_EQ(ssview.extent(1), 3);
      for(size_t i = 0; i < ssview.extent(0); ++i)
      {
        for (size_t j = 0; j < ssview.extent(1); ++j)
        {
          EXPECT_EQ(ssview(i, j), sview(i + 2, 3 * j + 2, 4));
        }
      }
    }
  }

  {
    auto sview = aview.subarray(std::make_tuple(9), std::make_tuple(8), std::make_tuple(12));
    // max slice -> dg::all{}
    // max slice -> fixed
    // max slice -> max slice
    {
      auto ssview = sview.subarray(dg::all{}, 4, std::make_tuple(5));
      EXPECT_EQ(ssview.ndims(), 2);
      EXPECT_EQ(ssview.extent(0), sview.extent(0));
      EXPECT_EQ(ssview.extent(1), 5);
      for (size_t i = 0; i < ssview.extent(0); ++i)
      {
        for (size_t j = 0; j < ssview.extent(1); ++j)
        {
          EXPECT_EQ(ssview(i, j), sview(i, 4, j));
        }
      }
    }
    // max slice -> min/max slice
    // max slice -> min/max/stride slice
    {
      auto ssview =
        sview.subarray(std::make_tuple(3, 9), std::make_tuple(2, 8, 2), 1);
      EXPECT_EQ(ssview.ndims(), 2);
      EXPECT_EQ(ssview.extent(0), 6);
      EXPECT_EQ(ssview.extent(1), 3);
      for (size_t i = 0; i < ssview.extent(0); ++i)
      {
        for (size_t j = 0; j < ssview.extent(1); ++j)
        {
          EXPECT_EQ(ssview(i, j), sview(i + 3, 2 * j + 2, 1));
        }
      }
    }
  }

  {
    auto sview = aview.subarray(
      std::make_tuple(2, 5), std::make_tuple(1, 11), std::make_tuple(2, 9));
    // min/max slice -> dg::all{}
    // min/max slice -> fixed
    // min/max slice -> max slice
    {
      auto ssview = sview.subarray(dg::all{}, 9, std::make_tuple(5));
      auto& slices = ssview.layout().get_slices();
      EXPECT_EQ(std::tuple_size<std::decay_t<decltype(slices)>>::value, 3);
      {
        auto& s = std::get<0>(slices);
        EXPECT_EQ(std::tuple_size<std::decay_t<decltype(s)>>::value, 2);
        EXPECT_EQ(std::get<0>(s), 2);
        EXPECT_EQ(std::get<1>(s), 5);
      }
      {
        auto& s = std::get<1>(slices);
        EXPECT_EQ(s, 10);
      }
      {
        auto& s = std::get<2>(slices);
        EXPECT_EQ(std::tuple_size<std::decay_t<decltype(s)>>::value, 2);
        EXPECT_EQ(std::get<0>(s), 2);
        EXPECT_EQ(std::get<1>(s), 7);
      }
      EXPECT_EQ(ssview.ndims(), 2);
      EXPECT_EQ(ssview.extent(0), sview.extent(0));
      EXPECT_EQ(ssview.extent(1), 5);
      for (size_t i = 0; i < ssview.extent(0); ++i)
      {
        for (size_t j = 0; j < ssview.extent(1); ++j)
        {
          EXPECT_EQ(ssview(i, j), sview(i, 9, j));
        }
      }
    }
    // min/max slice -> min/max slice
    // min/max slice -> min/max/stride slice
    {
      auto ssview =
        sview.subarray(std::make_tuple(1, 3), std::make_tuple(2, 8, 2), 9);
      EXPECT_EQ(ssview.ndims(), 2);
      EXPECT_EQ(ssview.extent(0), 2);
      EXPECT_EQ(ssview.extent(1), 3);
      for (size_t i = 0; i < ssview.extent(0); ++i)
      {
        for (size_t j = 0; j < ssview.extent(1); ++j)
        {
          EXPECT_EQ(ssview(i, j), sview(i + 1, 2 * j + 2, 9));
        }
      }
    }
  }

  {
    auto sview = aview.subarray(std::make_tuple(2, 10, 1),
      std::make_tuple(3, 11, 2), std::make_tuple(1, 10, 3));
    // min/max/stride slice -> dg::all{}
    // min/max/stride slice -> fixed
    // min/max/stride slice -> max slice
    {
      auto ssview = sview.subarray(dg::all{}, 2, std::make_tuple(2));
      EXPECT_EQ(ssview.ndims(), 2);
      EXPECT_EQ(ssview.extent(0), sview.extent(0));
      EXPECT_EQ(ssview.extent(1), 2);
      for (size_t i = 0; i < ssview.extent(0); ++i)
      {
        for (size_t j = 0; j < ssview.extent(1); ++j)
        {
          EXPECT_EQ(ssview(i, j), sview(i, 2, j));
        }
      }
    }
    // min/max/stride slice -> min/max slice
    // min/max/stride slice -> min/max/stride slice
    {
      auto ssview =
        sview.subarray(std::make_tuple(1, 7, 2), std::make_tuple(2, 4), 1);
      EXPECT_EQ(ssview.ndims(), 2);
      EXPECT_EQ(ssview.extent(0), 3);
      EXPECT_EQ(ssview.extent(1), 2);
      for (size_t i = 0; i < ssview.extent(0); ++i)
      {
        for (size_t j = 0; j < ssview.extent(1); ++j)
        {
          EXPECT_EQ(ssview(i, j), sview(2 * i + 1, j + 2, 1));
        }
      }
    }
  }
}

TEST(test_view, indexer_sequence_get)
{
  // Mostly a compile test
  dg::indexer<3> idx0(10, 20, 30);
  dg::indexer<3> idx1(30, 5, 15);
  dg::indexer<3> idx2(32, 8, 20);
  auto idxrs = dg::make_indexer_sequence(idx0, idx1, idx2);
  auto& idx0_copy = idxrs.get<0>();
  auto& idx1_copy = idxrs.get<1>();
  auto& idx2_copy = idxrs.get<2>();
}

TEST(test_view, indexer_sequence_subindexer)
{
  // Mostly a compile test
  dg::indexer<3> idx0(10, 20, 30);
  dg::indexer<3> idx1(30, 5, 15);
  dg::indexer<3> idx2(32, 8, 20);
  auto idxrs = dg::make_indexer_sequence(idx0, idx1, idx2);
  auto idxrs2 = idxrs.subindexer(dg::all{}, 5, std::make_tuple(5, 10));
  EXPECT_EQ(idxrs2.get<0>().ndims(), 2);
  EXPECT_EQ(idxrs2.get<0>().shape()[0], idx0.shape()[0]);
  EXPECT_EQ(idxrs2.get<0>().shape()[1], 5);
}

TEST(test_view, sequence_nested)
{
  dg::view<double, 1> v0(nullptr, 10);
  dg::view<double, 1> v1(nullptr, 10);
  dg::view<double, 1> v2(nullptr, 10);
  auto idxs0 = dg::make_indexer_sequence(v0, v1);
  auto idxs1 = dg::make_indexer_sequence(idxs0, v2);
  auto views0 = dg::make_view_sequence(v0, v1);
  auto views1 = dg::make_view_sequence(views0, v2);
}

TEST(test_view, view_sequence_get)
{
  // Mostly a compile test
  dg::hslab<double, 3> v0(10, 20, 30);
  dg::hslab<double, 3> v1(30, 5, 15);
  dg::hslab<double, 3> v2(32, 8, 20);
  auto views = dg::make_view_sequence(v0, v1, v2);
  auto& v0_copy = views.get<0>();
  auto& v1_copy = views.get<1>();
  auto& v2_copy = views.get<2>();
}

TEST(test_view, view_sequence_subindexer)
{
  // Mostly a compile test
  dg::hslab<double, 3> v0(10, 20, 30);
  dg::hslab<double, 3> v1(30, 5, 15);
  dg::hslab<double, 3> v2(32, 8, 20);
  auto views = dg::make_view_sequence(v0, v1, v2);
  auto& v0_copy = views.get<0>();
  auto& v1_copy = views.get<1>();
  auto& v2_copy = views.get<2>();
  auto idxrs2 = views.subindexer(dg::all{}, 5, std::make_tuple(5, 10));
  EXPECT_EQ(idxrs2.get<0>().ndims(), 2);
  EXPECT_EQ(idxrs2.get<0>().shape()[0], v0.shape()[0]);
  EXPECT_EQ(idxrs2.get<0>().shape()[1], 5);
}

TEST(test_view, view_sequence_subarray)
{
  // Mostly a compile test
  auto views = dg::make_view_sequence(dg::hslab<double, 3>(10, 20, 30),
    dg::hslab<double, 3>(30, 5, 15), dg::hslab<double, 3>(32, 8, 20));
  auto& v0_copy = views.get<0>();
  auto& v1_copy = views.get<1>();
  auto& v2_copy = views.get<2>();

  {
    auto views2 = views.subarray(dg::all{}, 5, std::make_tuple(5, 10));
    EXPECT_EQ(views2.get<0>().ndims(), 2);
    EXPECT_EQ(views2.get<0>().shape()[0], views.get<0>().shape()[0]);
    EXPECT_EQ(views2.get<0>().shape()[1], 5);

    EXPECT_EQ(views2.get<0>().data(), views.get<0>().data());
    EXPECT_EQ(views2.get<1>().data(), views.get<1>().data());
    EXPECT_EQ(views2.get<2>().data(), views.get<2>().data());

    auto views3 = views2.subarray(dg::all{}, std::make_tuple(0, 3));
    EXPECT_EQ(views3.get<0>().ndims(), 2);
    EXPECT_EQ(views3.get<0>().extent(0), views.get<0>().shape()[0]);
    EXPECT_EQ(views3.get<0>().extent(1), 3);
  }

  {
    auto mixed_views = dg::make_view_sequence(dg::view<double, 3>(v0_copy),
      v1_copy.subarray(std::make_tuple(0, 5), dg::all{}, dg::all{}));
    auto sub = mixed_views.subarray(0, dg::all{}, dg::all{});
    EXPECT_EQ(sub.get<0>().ndims(), 2);
    EXPECT_EQ(sub.get<0>().extent(0), v0_copy.extent(1));
    EXPECT_EQ(sub.get<0>().extent(1), v0_copy.extent(2));
  }
}

int main(int argc, char** argv)
{
  init_tutils(&argc, argv);
  return run_tutils();
}
