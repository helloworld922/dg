#include "dg/utils/hdf5_utils.hpp"
#include "tutils/tutils.hpp"

#include <cstdio>

struct disable_hdf_errors
{
  herr_t (*old_func)(int, void*);
  void* old_client_data;
  disable_hdf_errors()
  {
    H5Eget_auto2(H5E_DEFAULT, &old_func, &old_client_data);
    H5Eset_auto2(H5E_DEFAULT, nullptr, nullptr);
  }

  ~disable_hdf_errors()
  {
    H5Eset_auto2(H5E_DEFAULT, old_func, old_client_data);
  }
};

TEST(test_hdf5, create_file)
{
  {
    if (H5Fis_hdf5("test_create.h5") != 0)
    {
      std::remove("test_create.h5");
    }
    {
      dg::hdf5::file f("test_create.h5", H5F_ACC_EXCL);
    }
    EXPECT_GT(H5Fis_hdf5("test_create.h5"), 0);
  }
  {
    disable_hdf_errors raii{};
    dg::hdf5::file f("test_create.h5", H5F_ACC_EXCL);
    // open should have failed
    EXPECT_LT(f.get(), 0);
  }

  {
    dg::hdf5::file f("test_create.h5", H5F_ACC_TRUNC);
    // open should have succeeded
    EXPECT_GE(f.get(), 0);
    EXPECT_EQ(f.name(), "test_create.h5");
    EXPECT_EQ(f.path(), "/");
  }

  // TODO: test property list create
}

TEST(test_hdf5, open_file)
{
  {
    dg::hdf5::file f("test_open.h5", H5F_ACC_TRUNC);
    ASSERT_GE(f.get(), 0);
  }
  {
    dg::hdf5::file f("test_open.h5", H5F_ACC_RDWR);
    // open should have succeeded
    EXPECT_GE(f.get(), 0);
    EXPECT_EQ(f.name(), "test_open.h5");
    EXPECT_EQ(f.path(), "/");
  }
  {
    dg::hdf5::file f("test_open.h5", H5F_ACC_RDONLY);
    // open should have succeeded
    EXPECT_GE(f.get(), 0);
  }
}

TEST(test_hdf5, create_group)
{
  dg::hdf5::file f("test_create_group.h5", H5F_ACC_TRUNC);
  ASSERT_GE(f.get(), 0);

  {
    // create a new group
    dg::hdf5::group g(f, "group1");
    EXPECT_GE(g.get(), 0);
  }
  {
    // open a group
    dg::hdf5::group g(f, "group1");
    EXPECT_GE(g.get(), 0);
  }
}

TEST(test_hdf5, attributes)
{
  dg::hdf5::file f("test_create_attr.h5", H5F_ACC_TRUNC);
  ASSERT_GE(f.get(), 0);
  // create an attribute
  std::vector<int> buf = {0, 1, 2, 3};
  std::vector<int> read_buf = {0, 0, 0, 0};
  dg::view<int, 2> v(buf.data(), 2, 2);
  dg::view<int, 2> v2(read_buf.data(), 2, 2);
  {
    dg::hdf5::attribute attr(f, "attr1", v);
    ASSERT_GE(attr.get(), 0);
    // read back data
    attr.read(v2);
    EXPECT_RANGE_EQ(buf, read_buf);
  }
  {
    dg::hdf5::group g(f, "group");
    ASSERT_GE(g.get(), 0);
    buf[0] += 10;
    dg::hdf5::attribute attr(g, "attr2", v);
    ASSERT_GE(attr.get(), 0);
    // read back data
    attr.read(v2);
    EXPECT_RANGE_EQ(buf, read_buf);
  }
}

TEST(test_hdf5, simple_space)
{
  {
    auto space = dg::hdf5::dataspace::simple(2);
    EXPECT_EQ(space.ndims(), 1);
    auto ex = space.shape();
    std::vector<hsize_t> expected = {2};
    EXPECT_RANGE_EQ(ex, expected);
    EXPECT_EQ(H5Sget_simple_extent_npoints(space), 2);
  }

  {
    auto space = dg::hdf5::dataspace::simple(2, 3);
    EXPECT_EQ(space.ndims(), 2);
    auto ex = space.shape();
    std::vector<hsize_t> expected = {2, 3};
    EXPECT_RANGE_EQ(ex, expected);
    EXPECT_EQ(H5Sget_simple_extent_npoints(space), 2 * 3);
  }

  {
    auto space = dg::hdf5::dataspace::simple(2, 3, 4);
    EXPECT_EQ(space.ndims(), 3);
    auto ex = space.shape();
    std::vector<hsize_t> expected = {2, 3, 4};
    EXPECT_RANGE_EQ(ex, expected);
    EXPECT_EQ(H5Sget_simple_extent_npoints(space), 2 * 3 * 4);
  }
}

TEST(test_hdf5, read_write_1d)
{
  dg::hdf5::file f("test_write1d.h5", H5F_ACC_TRUNC);
  ASSERT_GE(f.get(), 0);
  std::vector<double> buf = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
    15, 16, 17, 18, 19, 20, 21, 22, 23};
  // 1D
  auto file_space = dg::hdf5::dataspace::simple(buf.size());
  dg::view<double, 1> v(buf.data(), buf.size());
  dg::hdf5::dataset data(
    f, "data0", dg::hdf5::type_id<double>::value(), file_space);
  data.write(v);
  std::vector<double> written(buf.size());
  dg::view<double, 1> read_view(written.data(), written.size());
  data.read(read_view);
  EXPECT_RANGE_EQ(buf, written);

  // overwrite the beginning part of data0
  auto v2 = v.subarray(std::tuple<size_t, size_t>{12, 24});
  auto overwrite_space = dg::hdf5::dataspace::simple(12);
  data.write(v2, overwrite_space);
  std::copy(buf.begin() + 12, buf.end(), buf.begin());
  data.read(read_view);
  EXPECT_RANGE_EQ(buf, written);

  // overwrite in a sub sub slice
  auto v3 = v2.subarray(std::make_tuple(4, 12, 2));
  auto overwrite_space2 = dg::hdf5::dataspace::simple(4);
  data.write(v3, overwrite_space2);

  for (size_t i = 0; i < 4; ++i)
  {
    buf[i] = buf[2 * i + 4 + 12];
  }
  data.read(read_view);
  EXPECT_RANGE_EQ(buf, written);
}

TEST(test_hdf5, read_write_sub1d)
{
  // creates a 24x1 2d view, then takes a slice such that one of the indices is
  // constant
  dg::hdf5::file f("test_write_sub1d.h5", H5F_ACC_TRUNC);
  ASSERT_GE(f.get(), 0);
  std::vector<double> buf = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
    15, 16, 17, 18, 19, 20, 21, 22, 23};
  // 1D
  auto file_space = dg::hdf5::dataspace::simple(4 * 2);
  dg::view<double, 3> v(buf.data(), 4, 3, 2);
  dg::hdf5::dataset data(
    f, "data0", dg::hdf5::type_id<double>::value(), file_space);
  auto sub_v = v.subarray(dg::all{}, 1, dg::all{});
  data.write(sub_v);
  std::vector<double> written(8);
  dg::view<double, 2> read_view(written.data(), 4, 2);
  data.read(read_view);
  for (size_t i = 0; i < v.shape()[0]; ++i)
  {
    for (size_t j = 0; j < v.shape()[2]; ++j)
    {
      EXPECT_EQ(v(i, 1, j), read_view(i, j));
    }
  }
}

TEST(test_hdf5, read_write_offset1d)
{
  // creates a 24x1 2d view, then takes a slice such that one of the indices is
  // constant
  dg::hdf5::file f("test_write_offset1d.h5", H5F_ACC_TRUNC);
  ASSERT_GE(f.get(), 0);
  std::vector<int> buf = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
    16, 17, 18, 19, 20, 21, 22, 23};
  // 1D
  auto file_space = dg::hdf5::dataspace::simple(4);
  dg::view<int, 1, dg::layout_offset<>> v(
    buf.data(), dg::layout_offset<>(buf.size() - 4), 4);
  dg::hdf5::dataset data(
    f, "data0", dg::hdf5::type_id<double>::value(), file_space);
  data.write(v);
  std::vector<int> written(8);
  dg::view<int, 1, dg::layout_offset<>> read_view(
    written.data(), dg::layout_offset<>(4), 4);
  data.read(read_view);
  std::vector<int> expected = {0, 0, 0, 0, 20, 21, 22, 23};
  EXPECT_RANGE_EQ(written, expected);
}

TEST(test_hdf5, read_write_2d)
{
  dg::hdf5::file f("test_write2d.h5", H5F_ACC_TRUNC);
  ASSERT_GE(f.get(), 0);
  std::vector<double> buf = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
    15, 16, 17, 18, 19, 20, 21, 22, 23};
  // 2D
  auto file_space = dg::hdf5::dataspace::simple(buf.size());
  dg::view<double, 2> v(buf.data(), 4, 6);
  dg::hdf5::dataset data(
    f, "data0", dg::hdf5::type_id<double>::value(), file_space);
  data.write(v);
  std::vector<double> written(buf.size());
  dg::view<double, 1> read_view(written.data(), written.size());
  data.read(read_view);
  EXPECT_RANGE_EQ(buf, written);

  // overwrite a block
  auto v2 =
    v.subarray(std::make_tuple(0, 4, 2), std::make_tuple(0, 3));
  auto overwrite_space = dg::hdf5::dataspace::simple(3 * 2);
  data.write(v2, overwrite_space);
  for (size_t i = 0; i < 2; ++i)
  {
    for (size_t j = 0; j < 3; ++j)
    {
      buf[i * 3 + j] = v2(i, j);
    }
  }
  data.read(read_view);
  EXPECT_RANGE_EQ(buf, written);
}

int main(int argc, char** argv)
{
  init_tutils(&argc, argv);
  return run_tutils();
}
