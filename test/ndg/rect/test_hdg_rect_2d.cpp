#include "dg/ndg/rect/rect_2d.hpp"

#include "dg/utils/hslab.hpp"
#include "dg/utils/view_utils.hpp"
#include "dg/context.hpp"

#include "tutils/tutils.hpp"

#include <array>
#include <cmath>
#include <numeric>

#include <fstream>
#include <iomanip>

///
/// @brief Crux as a way to pass context created in main function to tests...
///
static dg::context* glob_context;

///
/// @brief Dirichlet BC's for poisson system
///
struct poisson_dirichlet_bc
{
  double coeff;
  double tau;
  template <class Lmbda, class QIn, class A, class B, class N>
  void num_flux_local_jac(const Lmbda& lmbda, const QIn& qin, double mult,
    A&& A_mat, B&& B_mat, const N& normal) const
  {
    // system is responsible for all signs
    // local system
    A_mat(0, 0) += mult * coeff * tau;
    A_mat(0, 1) -= mult * coeff * normal[0];
    A_mat(0, 2) -= mult * coeff * normal[1];
    // actually ignores B since lmbda is not part of vector of unknowns
  }
};

///
/// @brief Helper for poisson system. Helper for computing 2D gradient of a
/// scalar variable. This only has the num_flux part, since there's no way to
/// safely separate what parts contribute to the local RHS, and which parts to
/// the global RHS...
///
struct gradient_helper
{
  template <class Lmbda, class Q, class N>
  auto num_flux(const Lmbda& lmbda_, const Q& q_, const N& normal) const
  {
    dg::shslab<double, 2, 1> res(2);
    // auxiliary PDE
    res[0] = -lmbda_[0] * normal[0];
    res[1] = -lmbda_[0] * normal[1];
    return res;
  }
};

///
/// @brief Implements:
/// \f{gather}{
/// d_t q - div (k \sigma) = 0\\
/// \sigma - grad(q) = 0
/// \f}
/// Used for testing HDG. Used Implicit Euler for timestepping.
///
struct poisson_system
{
  /// poisson coefficient k
  double coeff;

  // HDG continuity penalty
  double tau;

  template <class Q>
  auto int_flux(const Q& q_) const
  {
    auto& q = dg::get<0>(q_);
    auto& sigma = dg::get<1>(q_);
    dg::shslab<double, 6, 2> res(3, 2);
    res(0, 0) = -coeff * sigma[0];
    res(0, 1) = -coeff * sigma[1];
    res(1, 0) = -q[0];
    res(1, 1) = 0;
    res(2, 0) = 0;
    res(2, 1) = -q[0];
    return res;
  }

  template <class Q, class M, class D>
  void int_flux_jac(const Q& q_, const M& mult, D&& dst) const
  {
    // system is responsible for all signs
    dst(0, 1) += mult[0] * coeff;
    dst(0, 2) += mult[1] * coeff;
    dst(1, 0) += mult[0];
    dst(2, 0) += mult[1];
  }

  template <class Lmbda, class Q, class N>
  auto num_flux(const Lmbda& lmbda_, const Q& q_, const N& normal) const
  {
    auto& q = dg::get<0>(q_);
    auto& sigma = dg::get<1>(q_);
    dg::shslab<double, 1, 1> res(1);
    // primary PDE
    res[0] = coeff * (tau * (q[0] - lmbda_[0]) -
                       (sigma[0] * normal[0] + sigma[1] * normal[1]));
    return res;
  }

  template <class Lmbda, class QIn, class A, class B, class C, class D, class N>
  void num_flux_jac(const Lmbda& lmbda, const QIn& qin, double mult, A&& A_mat,
    B&& B_mat, C&& C_mat, D&& D_mat, const N& normal) const
  {
    // system is responsible for all signs
    // for interior faces
    // local system
    A_mat(0, 0) += mult * coeff * tau;
    A_mat(0, 1) -= mult * coeff * normal[0];
    A_mat(0, 2) -= mult * coeff * normal[1];
    B_mat(0, 0) -= mult * coeff * tau;
    B_mat(1, 0) -= mult * normal[0];
    B_mat(2, 0) -= mult * normal[1];
    // global system
    C_mat(0, 0) += mult * coeff * tau;
    C_mat(0, 1) -= mult * coeff * normal[0];
    C_mat(0, 2) -= mult * coeff * normal[1];
    D_mat(0, 0) -= mult * coeff * tau;
  }

  template <class Q, class P>
  auto msource(const Q& q_, const P& pos) const
  {
    dg::shslab<double, 3, 1> res(3);

    auto& q = dg::get<0>(q_);
    auto& sigma = dg::get<1>(q_);
    // this looks like it is the opposite of the specified elliptic PDE since I
    // negated the entire PDE
    res[0] = 9.869604401089358 * (1 + 4) * coeff *
             sin(3.141592653589793 * pos[0]) *
             sin(3.141592653589793 * 2 * pos[1]);
    res[1] = -sigma[0];
    res[2] = -sigma[1];
    return res;
  }

  template <class Q, class D, class P>
  void source_jac(const Q& q, double mult, D&& dst, const P& pos) const
  {
    // system is responsible for all signs
    // aux eq. identity
    dst(1, 1) += mult;
    dst(2, 2) += mult;
  }

  ///
  /// @brief Computes the analytical solution at a given time
  /// Assumes the initial conditions are a single period of a sin wave and
  /// domain has dirichlet zero boundary conditions.
  ///
  template <class Q, class L, class Ax, class Ay>
  void asol(Q&& q_, L&& lmbda, const Ax& absc_x, const Ay& absc_y, double xmin,
    double xmax, double ymin, double ymax, double t)
  {
    auto& q = dg::get<0>(q_);
    auto& sigma = dg::get<1>(q_);
    auto dx = (xmax - xmin) / q.extent(0);
    auto dy = (ymax - ymin) / q.extent(1);

    for (size_t elem_x = 0; elem_x < q.extent(0); ++elem_x)
    {
      double x0 = dx * elem_x + xmin;
      for (size_t elem_y = 0; elem_y < q.extent(1); ++elem_y)
      {
        double y0 = dy * elem_y + ymin;
        for (size_t i_x = 0; i_x < q.extent(2); ++i_x)
        {
          double x = x0 + 0.5 * dx * (absc_x[i_x] + 1);
          for (size_t i_y = 0; i_y < q.extent(3); ++i_y)
          {
            double y = y0 + 0.5 * dy * (absc_y[i_y] + 1);
            if (t == 0)
            {
              // dummy initial guess
              q(elem_x, elem_y, i_x, i_y, 0) = 0;
              sigma(elem_x, elem_y, i_x, i_y, 0) = 0;
              sigma(elem_x, elem_y, i_x, i_y, 1) = 0;
            }
            else
            {
              // actual analytical solution
              q(elem_x, elem_y, i_x, i_y, 0) =
                sin(3.141592653589793 * x) * sin(3.141592653589793 * 2 * y);
              sigma(elem_x, elem_y, i_x, i_y, 0) =
                3.141592653589793 * cos(3.141592653589793 * x) *
                sin(3.141592653589793 * 2 * y);
              sigma(elem_x, elem_y, i_x, i_y, 1) =
                3.141592653589793 * 2 * sin(3.141592653589793 * x) *
                cos(3.141592653589793 * 2 * y);
            }
          }
        }
      }
    }
    // set lmbda: all vertical oriented faces come first
    size_t fidx = 0;

    for (size_t elem_x = 0; elem_x < q.extent(0) - 1; ++elem_x)
    {
      for (size_t elem_y = 0; elem_y < q.extent(1); ++elem_y)
      {
        // right wall
        for (size_t j = 0; j < q.extent(3); ++j, ++fidx)
        {
          lmbda(fidx, 0) = q(elem_x, elem_y, q.extent(2) - 1, j, 0);
        }
      }
    }

    for (size_t elem_x = 0; elem_x < q.extent(0); ++elem_x)
    {
      for (size_t elem_y = 0; elem_y < q.extent(1) - 1; ++elem_y)
      {
        // top wall
        for (size_t j = 0; j < q.extent(2); ++j, ++fidx)
        {
          lmbda(fidx, 0) = q(elem_x, elem_y, j, q.extent(3) - 1, 0);
        }
      }
    }
    // bc place-holder
    lmbda(lmbda.size() - 1, 0) = 0;
  }
};

// helper for mat transpose-vec multiplication of a single block
template <class S, class X, class Y>
void schur_transpose_mult_helper(const S& schur, const X& x, Y&& y)
{
  for (size_t row = 0; row < schur.extent(0); ++row)
  {
    for (size_t rlmbda = 0; rlmbda < schur.extent(1); ++rlmbda)
    {
      for (size_t col = 0; col < schur.extent(2); ++col)
      {
        for (size_t clmbda = 0; clmbda < schur.extent(3); ++clmbda)
        {
          y(col, clmbda) += schur(row, rlmbda, col, clmbda) * x(row, rlmbda);
        }
      }
    }
  }
}

// helper for mat-vec multiplication of a single block
template <class S, class X, class Y>
void schur_mult_helper(const S& schur, const X& x, Y&& y)
{
  for (size_t row = 0; row < schur.extent(0); ++row)
  {
    for (size_t rlmbda = 0; rlmbda < schur.extent(1); ++rlmbda)
    {
      for (size_t col = 0; col < schur.extent(2); ++col)
      {
        for (size_t clmbda = 0; clmbda < schur.extent(3); ++clmbda)
        {
          y(row, rlmbda) += schur(row, rlmbda, col, clmbda) * x(col, clmbda);
        }
      }
    }
  }
}

///
/// @brief A context to be used in PETSc's Shell Matrices to help with linear
/// solves of the Schur complement. Note: this implementation is not parallel!
/// It is tailored specifically for the 2D poisson HDG test. It should be
/// replaced with a general helper in the future...
///
/// specific assumptions:
/// - 2D (TODO)
/// - structured "2D block"
/// - degree_x == degree_y
/// - Has Dirichlet BC's on all boundaries, boundaries are excluded from
/// unknowns
/// - Uniform number of unknowns over whole mesh
///
struct hdg_schur_context
{
  // dim 0: block idx. Block order:
  // - 0 left, 1 self, 2 right, 3 bottom left, 4 bottom right, 5 top left, 6 top
  // right
  // dim 1: face idx x
  // dim 2: face idx y
  // dim 3: face node idx for row (specific to face)
  // dim 4: lambda for row
  // dim 5: face node idx for col (specific to face)
  // dim 6: lambda for col
  dg::hslab<double, 7> vschur;

  // dim 0: block idx. Block order:
  // - 0 bottom, 1 self, 2 top, 3 left bottom, 4 right bottom, 5 left top, 6
  // right top
  // dim 1: face idx x
  // dim 2: face idx y
  // dim 3: face node idx for row (specific to face)
  // dim 4: lambda for row
  // dim 5: face node idx for col (specific to face)
  // dim 6: lambda for col
  dg::hslab<double, 7> hschur;
  size_t nelems_x, nelems_y;

  hdg_schur_context(
    size_t nnodes, size_t nelems_x, size_t nelems_y, size_t num_lambda)
    : vschur(7, nelems_x - 1, nelems_y, nnodes, num_lambda, nnodes, num_lambda),
      hschur(7, nelems_x, nelems_y - 1, nnodes, num_lambda, nnodes, num_lambda),
      nelems_x(nelems_x), nelems_y(nelems_y)
  {
  }

  ///
  /// @brief Matrix transpose/vector multiplication: y = A^T x
  ///
  PetscErrorCode transpose_mult_vec(Vec x_, Vec y_)
  {
    VecSet(y_, 0);
    const PetscScalar* x_data;
    PetscScalar* y_data;
    VecGetArray(y_, &y_data);
    VecGetArrayRead(x_, &x_data);

    dg::view<const double, 4> x_v(x_data, vschur.extent(1), vschur.extent(2),
      vschur.extent(3), vschur.extent(4));
    dg::view<const double, 4> x_h(x_data + x_v.size(), hschur.extent(1),
      hschur.extent(2), hschur.extent(3), hschur.extent(4));

    dg::view<double, 4> y_v(y_data, vschur.extent(1), vschur.extent(2),
      vschur.extent(3), vschur.extent(4));
    dg::view<double, 4> y_h(y_data + y_v.size(), hschur.extent(1),
      hschur.extent(2), hschur.extent(3), hschur.extent(4));
    // vertical faces
    for (size_t fx = 0; fx < vschur.extent(1); ++fx)
    {
      for (size_t fy = 0; fy < vschur.extent(2); ++fy)
      {
        // self coupling
        schur_transpose_mult_helper(vschur.subarray(1, fx, fy, dg::all{},
                                      dg::all{}, dg::all{}, dg::all{}),
          x_v.subarray(fx, fy, dg::all{}, dg::all{}),
          y_v.subarray(fx, fy, dg::all{}, dg::all{}));
        if (fx != 0)
        {
          // left face coupling
          schur_transpose_mult_helper(vschur.subarray(0, fx, fy, dg::all{},
                                        dg::all{}, dg::all{}, dg::all{}),
            x_v.subarray(fx, fy, dg::all{}, dg::all{}),
            y_v.subarray(fx - 1, fy, dg::all{}, dg::all{}));
        }
        if (fx != vschur.extent(1) - 1)
        {
          // right face coupling
          schur_transpose_mult_helper(vschur.subarray(2, fx, fy, dg::all{},
                                        dg::all{}, dg::all{}, dg::all{}),
            x_v.subarray(fx, fy, dg::all{}, dg::all{}),
            y_v.subarray(fx + 1, fy, dg::all{}, dg::all{}));
        }
        if (fy != 0)
        {
          // bottom left coupling
          schur_transpose_mult_helper(vschur.subarray(3, fx, fy, dg::all{},
                                        dg::all{}, dg::all{}, dg::all{}),
            x_v.subarray(fx, fy, dg::all{}, dg::all{}),
            y_h.subarray(fx, fy - 1, dg::all{}, dg::all{}));
          // bottom right coupling
          schur_transpose_mult_helper(vschur.subarray(4, fx, fy, dg::all{},
                                        dg::all{}, dg::all{}, dg::all{}),
            x_v.subarray(fx, fy, dg::all{}, dg::all{}),
            y_h.subarray(fx + 1, fy - 1, dg::all{}, dg::all{}));
        }
        if (fy != vschur.extent(2) - 1)
        {
          // top left coupling
          schur_transpose_mult_helper(vschur.subarray(5, fx, fy, dg::all{},
                                        dg::all{}, dg::all{}, dg::all{}),
            x_v.subarray(fx, fy, dg::all{}, dg::all{}),
            y_h.subarray(fx, fy, dg::all{}, dg::all{}));
          // top right coupling
          schur_transpose_mult_helper(vschur.subarray(6, fx, fy, dg::all{},
                                        dg::all{}, dg::all{}, dg::all{}),
            x_v.subarray(fx, fy, dg::all{}, dg::all{}),
            y_h.subarray(fx + 1, fy, dg::all{}, dg::all{}));
        }
      }
    }

    // horizontal faces
    for (size_t fx = 0; fx < hschur.extent(1); ++fx)
    {
      for (size_t fy = 0; fy < hschur.extent(2); ++fy)
      {
        // self coupling
        schur_transpose_mult_helper(hschur.subarray(1, fx, fy, dg::all{},
                                      dg::all{}, dg::all{}, dg::all{}),
          x_h.subarray(fx, fy, dg::all{}, dg::all{}),
          y_h.subarray(fx, fy, dg::all{}, dg::all{}));
        if (fy != 0)
        {
          // bottom face coupling
          schur_transpose_mult_helper(hschur.subarray(0, fx, fy, dg::all{},
                                        dg::all{}, dg::all{}, dg::all{}),
            x_h.subarray(fx, fy, dg::all{}, dg::all{}),
            y_h.subarray(fx, fy - 1, dg::all{}, dg::all{}));
        }
        if (fy != hschur.extent(2) - 1)
        {
          // top face coupling
          schur_transpose_mult_helper(hschur.subarray(2, fx, fy, dg::all{},
                                        dg::all{}, dg::all{}, dg::all{}),
            x_h.subarray(fx, fy, dg::all{}, dg::all{}),
            y_h.subarray(fx, fy + 1, dg::all{}, dg::all{}));
        }
        if (fx != 0)
        {
          // left bottom coupling
          schur_transpose_mult_helper(hschur.subarray(3, fx, fy, dg::all{},
                                        dg::all{}, dg::all{}, dg::all{}),
            x_h.subarray(fx, fy, dg::all{}, dg::all{}),
            y_v.subarray(fx - 1, fy, dg::all{}, dg::all{}));
          // left top coupling
          schur_transpose_mult_helper(hschur.subarray(5, fx, fy, dg::all{},
                                        dg::all{}, dg::all{}, dg::all{}),
            x_h.subarray(fx, fy, dg::all{}, dg::all{}),
            y_v.subarray(fx - 1, fy + 1, dg::all{}, dg::all{}));
        }
        if (fx != hschur.extent(1) - 1)
        {
          // right bottom coupling
          schur_transpose_mult_helper(hschur.subarray(4, fx, fy, dg::all{},
                                        dg::all{}, dg::all{}, dg::all{}),
            x_h.subarray(fx, fy, dg::all{}, dg::all{}),
            y_v.subarray(fx, fy, dg::all{}, dg::all{}));
          // right top coupling
          schur_transpose_mult_helper(hschur.subarray(6, fx, fy, dg::all{},
                                        dg::all{}, dg::all{}, dg::all{}),
            x_h.subarray(fx, fy, dg::all{}, dg::all{}),
            y_v.subarray(fx, fy + 1, dg::all{}, dg::all{}));
        }
      }
    }

    VecRestoreArray(y_, &y_data);
    VecRestoreArrayRead(x_, &x_data);
    return 0;
  }

  ///
  /// @brief Matrix/vector multiplication: y = A x
  ///
  PetscErrorCode mult_vec(Vec x_, Vec y_)
  {
    VecSet(y_, 0);
    const PetscScalar* x_data;
    PetscScalar* y_data;
    VecGetArray(y_, &y_data);
    VecGetArrayRead(x_, &x_data);

    dg::view<const double, 4> x_v(x_data, vschur.extent(1), vschur.extent(2),
      vschur.extent(3), vschur.extent(4));
    dg::view<const double, 4> x_h(x_data + x_v.size(), hschur.extent(1),
      hschur.extent(2), hschur.extent(3), hschur.extent(4));

    dg::view<double, 4> y_v(y_data, vschur.extent(1), vschur.extent(2),
      vschur.extent(3), vschur.extent(4));
    dg::view<double, 4> y_h(y_data + y_v.size(), hschur.extent(1),
      hschur.extent(2), hschur.extent(3), hschur.extent(4));
    // vertical faces
    for (size_t fx = 0; fx < vschur.extent(1); ++fx)
    {
      for (size_t fy = 0; fy < vschur.extent(2); ++fy)
      {
        // self coupling
        schur_mult_helper(vschur.subarray(1, fx, fy, dg::all{}, dg::all{},
                            dg::all{}, dg::all{}),
          x_v.subarray(fx, fy, dg::all{}, dg::all{}),
          y_v.subarray(fx, fy, dg::all{}, dg::all{}));
        if (fx != 0)
        {
          // left face coupling
          schur_mult_helper(vschur.subarray(0, fx, fy, dg::all{}, dg::all{},
                              dg::all{}, dg::all{}),
            x_v.subarray(fx - 1, fy, dg::all{}, dg::all{}),
            y_v.subarray(fx, fy, dg::all{}, dg::all{}));
        }
        if (fx != vschur.extent(1) - 1)
        {
          // right face coupling
          schur_mult_helper(vschur.subarray(2, fx, fy, dg::all{}, dg::all{},
                              dg::all{}, dg::all{}),
            x_v.subarray(fx + 1, fy, dg::all{}, dg::all{}),
            y_v.subarray(fx, fy, dg::all{}, dg::all{}));
        }
        if (fy != 0)
        {
          // bottom left coupling
          schur_mult_helper(vschur.subarray(3, fx, fy, dg::all{}, dg::all{},
                              dg::all{}, dg::all{}),
            x_h.subarray(fx, fy - 1, dg::all{}, dg::all{}),
            y_v.subarray(fx, fy, dg::all{}, dg::all{}));
          // bottom right coupling
          schur_mult_helper(vschur.subarray(4, fx, fy, dg::all{}, dg::all{},
                              dg::all{}, dg::all{}),
            x_h.subarray(fx + 1, fy - 1, dg::all{}, dg::all{}),
            y_v.subarray(fx, fy, dg::all{}, dg::all{}));
        }
        if (fy != vschur.extent(2) - 1)
        {
          // top left coupling
          schur_mult_helper(vschur.subarray(5, fx, fy, dg::all{}, dg::all{},
                              dg::all{}, dg::all{}),
            x_h.subarray(fx, fy, dg::all{}, dg::all{}),
            y_v.subarray(fx, fy, dg::all{}, dg::all{}));
          // top right coupling
          schur_mult_helper(vschur.subarray(6, fx, fy, dg::all{}, dg::all{},
                              dg::all{}, dg::all{}),
            x_h.subarray(fx + 1, fy, dg::all{}, dg::all{}),
            y_v.subarray(fx, fy, dg::all{}, dg::all{}));
        }
      }
    }

    // horizontal faces
    for (size_t fx = 0; fx < hschur.extent(1); ++fx)
    {
      for (size_t fy = 0; fy < hschur.extent(2); ++fy)
      {
        // self coupling
        schur_mult_helper(hschur.subarray(1, fx, fy, dg::all{}, dg::all{},
                            dg::all{}, dg::all{}),
          x_h.subarray(fx, fy, dg::all{}, dg::all{}),
          y_h.subarray(fx, fy, dg::all{}, dg::all{}));
        if (fy != 0)
        {
          // bottom face coupling
          schur_mult_helper(hschur.subarray(0, fx, fy, dg::all{}, dg::all{},
                              dg::all{}, dg::all{}),
            x_h.subarray(fx, fy - 1, dg::all{}, dg::all{}),
            y_h.subarray(fx, fy, dg::all{}, dg::all{}));
        }
        if (fy != hschur.extent(2) - 1)
        {
          // top face coupling
          schur_mult_helper(hschur.subarray(2, fx, fy, dg::all{}, dg::all{},
                              dg::all{}, dg::all{}),
            x_h.subarray(fx, fy + 1, dg::all{}, dg::all{}),
            y_h.subarray(fx, fy, dg::all{}, dg::all{}));
        }
        if (fx != 0)
        {
          // left bottom coupling
          schur_mult_helper(hschur.subarray(3, fx, fy, dg::all{}, dg::all{},
                              dg::all{}, dg::all{}),
            x_v.subarray(fx - 1, fy, dg::all{}, dg::all{}),
            y_h.subarray(fx, fy, dg::all{}, dg::all{}));
          // left top coupling
          schur_mult_helper(hschur.subarray(5, fx, fy, dg::all{}, dg::all{},
                              dg::all{}, dg::all{}),
            x_v.subarray(fx - 1, fy + 1, dg::all{}, dg::all{}),
            y_h.subarray(fx, fy, dg::all{}, dg::all{}));
        }
        if (fx != hschur.extent(1) - 1)
        {
          // right bottom coupling
          schur_mult_helper(hschur.subarray(4, fx, fy, dg::all{}, dg::all{},
                              dg::all{}, dg::all{}),
            x_v.subarray(fx, fy, dg::all{}, dg::all{}),
            y_h.subarray(fx, fy, dg::all{}, dg::all{}));
          // right top coupling
          schur_mult_helper(hschur.subarray(6, fx, fy, dg::all{}, dg::all{},
                              dg::all{}, dg::all{}),
            x_v.subarray(fx, fy + 1, dg::all{}, dg::all{}),
            y_h.subarray(fx, fy, dg::all{}, dg::all{}));
        }
      }
    }

    VecRestoreArray(y_, &y_data);
    VecRestoreArrayRead(x_, &x_data);
    return 0;
  }
};

extern "C" {
static PetscErrorCode shell_mult(Mat A, Vec x, Vec y)
{
  hdg_schur_context* ctx;
  MatShellGetContext(A, &ctx);
  return ctx->mult_vec(x, y);
}

static PetscErrorCode shell_transpose_mult(Mat A, Vec x, Vec y)
{
  hdg_schur_context* ctx;
  MatShellGetContext(A, &ctx);
  return ctx->transpose_mult_vec(x, y);
}
}

///
/// @brief Helper for integrating the L-2 error norm
/// @param sol computed solution, in base_degree
/// @param expected expected solution, evaluated in int_degree
///
template <size_t base_x, size_t base_y, size_t int_x, size_t int_y, class Q,
  class E>
double integrate_l2_error(const Q& sol, const E& expected, size_t nelems_x,
  size_t nelems_y, double dx, double dy)
{
  static dg::spatial::elements::lagrange<double, 1, 1> flat_element;
  static dg::spatial::elements::lagrange<double, base_x, base_y> base_element;
  static dg::spatial::elements::lagrange<double, int_x, int_y> exp_element;
  static dg::spatial::elements::lagrange<double, 12, 12> int_element;

  double res = 0;

  std::array<double, 8> buffer = {0, 0, 0, dy, dx, 0, dx, dy};
  dg::view<double, 3> coords(buffer.data(), 2, 2, 2);

  for (size_t comp = 0; comp < sol.extent(4); ++comp)
  {
    double tmp = 0;
    for (size_t elem_x = 0; elem_x < nelems_x; ++elem_x)
    {
      for (size_t elem_y = 0; elem_y < nelems_y; ++elem_y)
      {
        tmp += int_element.vol_integral(
          [dx, base = sol.subarray(elem_x, elem_y, dg::all{}, dg::all{}, comp),
            high =
              expected.subarray(elem_x, elem_y, dg::all{}, dg::all{}, comp),
            &coords](const std::array<double, 2>& xi) {
            // interpolate base solution to xi
            auto val = base_element.interp(xi, base);
            auto exp_ = exp_element.interp(xi, high);
            return (val - exp_) * (val - exp_) *
                   flat_element.jacobian(xi, coords).determinant();
          });
      }
    }
    res += tmp;
  }
  return sqrt(res);
}

TEST(test_hdg_rect_2d, int_flux_jac)
{
  // TODO
  FAIL() << "not implemented yet";
}

TEST(test_hdg_rect_2d, num_flux_jac)
{
  // TODO
  FAIL() << "not implemented yet";
}

template <class C, class Xi, class G>
void grhs_update_helper(const C& c, const Xi& xi, G&& grhs)
{
  for (size_t row = 0; row < c.extent(0); ++row)
  {
    for (size_t lmbda_comp = 0; lmbda_comp < c.extent(1); ++lmbda_comp)
    {
      for (size_t col = 0; col < c.extent(2); ++col)
      {
        for (size_t q_comp = 0; q_comp < c.extent(3); ++q_comp)
        {
          grhs(row, lmbda_comp) -=
            c(row, lmbda_comp, col, q_comp) * xi(col, q_comp);
        }
      }
    }
  }
}

template <class Gamma, class C, class S>
void schur_update_helper(const Gamma& gamma, const C& c, S&& schur)
{
  // basically just matrix multiplication, but ordering makes using a general
  // library function non-trivial...
  // schur -= c * gamma
  // face index
  for (size_t row_fidx = 0; row_fidx < c.extent(0); ++row_fidx)
  {
    // lmbda index
    for (size_t row_lmbda = 0; row_lmbda < c.extent(1); ++row_lmbda)
    {
      // face index
      for (size_t col_fidx = 0; col_fidx < gamma.extent(3); ++col_fidx)
      {
        // lmbda index
        for (size_t col_lmbda = 0; col_lmbda < gamma.extent(2); ++col_lmbda)
        {
          // node index
          for (size_t nidx = 0; nidx < c.extent(2); ++nidx)
          {
            // q component
            for (size_t comp_q = 0; comp_q < c.extent(3); ++comp_q)
            {
              schur(row_fidx, row_lmbda, col_fidx, col_lmbda) -=
                c(row_fidx, row_lmbda, nidx, comp_q) *
                gamma(comp_q, nidx, col_lmbda, col_fidx);
            }
          }
        }
      }
    }
  }
}

/// implementation limitation: nelems_x and nelems_y both need to be >= 2
template <size_t degree>
auto poisson_sim(size_t nelems_x, size_t nelems_y)
{
  // dirichlet boundary conditions are handled with lambda removed from vector
  // of unknowns
  dg::ndg::rect<degree, degree> discr{};

  poisson_system sys;
  gradient_helper aux_sys;
  poisson_dirichlet_bc bc;

  // diffusion coeff.
  constexpr double kappa = 1e-2;
  constexpr size_t nnodes = (degree + 1) * (degree + 1);
  constexpr double xmin = 0;
  constexpr double xmax = 1;
  constexpr double ymin = 0;
  constexpr double ymax = 1;
  double dx = (xmax - xmin) / nelems_x;
  double dy = (ymax - ymin) / nelems_y;
  std::array<double, 8> buffer = {0, 0, 0, dy, dx, 0, dx, dy};
  dg::view<double, 3> coords(buffer.data(), 2, 2, 2);

  sys.coeff = kappa;
  sys.tau = 1;
  bc.coeff = kappa;
  bc.tau = 1;

  std::array<size_t, degree + 1> face_idcs;
  std::array<size_t, degree + 1> bc_idcs;
  for (size_t i = 0; i < degree + 1; ++i)
  {
    face_idcs[i] = i;
    bc_idcs[i] = 0;
  }

  auto left_face = std::make_tuple(degree + 1);
  auto right_face = std::make_tuple(degree + 1, 2 * (degree + 1));
  auto bottom_face = std::make_tuple(2 * (degree + 1), 3 * (degree + 1));
  auto top_face = std::make_tuple(3 * (degree + 1), 4 * (degree + 1));

  // hdg matrices
  dg::shslab<double, nnodes * nnodes, 2> mass_matrix(nnodes, nnodes);
  discr.calc_mass_matrix(mass_matrix, coords);
  dg::hslab<double, 3, dg::layout_left> advect_matrix(2, nnodes, nnodes);
  discr.calc_advect_matrix(advect_matrix, coords);
  auto lift_matrix = dg::make_view_sequence(
    dg::shslab<double, (degree + 1) * (degree + 1), 2>(degree + 1, degree + 1),
    dg::shslab<double, (degree + 1) * (degree + 1), 2>(degree + 1, degree + 1),
    dg::shslab<double, (degree + 1) * (degree + 1), 2>(degree + 1, degree + 1),
    dg::shslab<double, (degree + 1) * (degree + 1), 2>(degree + 1, degree + 1));
  discr.calc_lift_matrix(lift_matrix, coords);

  // solution space
  dg::view_array<dg::hslab<double, 5>, 2> qs;

  for (size_t i = 0; i < qs.size(); ++i)
  {
    qs[i] = dg::hslab<double, 5>(nelems_x, nelems_y, degree + 1, degree + 1, 1);
  }
  // also stash a temporary for boundary condition at the end
  // vertical faces go first, followed by horizontal ones
  dg::hslab<double, 2> lmbda(nelems_y * (nelems_x - 1) * (degree + 1) +
                               nelems_x * (nelems_y - 1) * (degree + 1) + 1,
    1);
  dg::hslab<double, 2> dlmbda(nelems_y * (nelems_x - 1) * (degree + 1) +
                                nelems_x * (nelems_y - 1) * (degree + 1),
    1);
  dg::hslab<double, 5> sigma(nelems_x, nelems_y, degree + 1, degree + 1, 2);

  constexpr static dg::lobatto<double> quad{};
  auto absc = quad.abscissas(degree + 1);

  // initial conditions
  sys.asol(dg::make_view_sequence(qs[0].as_view(), sigma.as_view()), lmbda,
    absc, absc, xmin, xmax, ymin, ymax, 0);

  // initialize qs to something
  for (size_t i = 1; i < qs.size(); ++i)
  {
    std::copy(qs[0].data(), qs[0].data() + qs[0].size(), qs[i].data());
  }

  // local A matrix
  dg::shslab<double, 3 * 3 * nnodes * nnodes, 6, dg::layout_left> A_mat(
    3, degree + 1, degree + 1, 3, degree + 1, degree + 1);
  Eigen::Map<Eigen::Matrix<double, 3 * nnodes, 3 * nnodes>> A_map(A_mat.data());

  // one local C matrix per face in an element, organized into vertical oriented
  // or horizontally oriented faces
  dg::shslab<double, nnodes * 3 * 2, 5> C_horiz(
    2, degree + 1, 1, degree + 1, 3);
  dg::shslab<double, nnodes * 3 * 2, 5> C_vert(2, degree + 1, 1, degree + 1, 3);

  hdg_schur_context sctx(degree + 1, nelems_x, nelems_y, 1);
  // global storage for A^-1 B
  // TODO: how to treat gamma special for elements with a boundary face?
  // For now, this actually allocate the space for them, but never touch that
  // memory
  dg::shslab<double, 3 * nnodes * 4 * (degree + 1), 5, dg::layout_left> B_mat(
    3, degree + 1, degree + 1, 1, 2 * (degree + degree + 2));
  dg::hslab<double, 7, dg::layout_left> gamma_mats(
    3, degree + 1, degree + 1, 1, 4 * (degree + 1), nelems_x, nelems_y);
  // global storage for A^-1 f
  dg::hslab<double, 5> xi_vecs(nelems_x, nelems_y, degree + 1, degree + 1, 3);
  dg::shslab<double, nnodes * 3, 3> f_vec(degree + 1, degree + 1, 3);
  // global RHS
  dg::hslab<double, 2, dg::layout_left> glob_rhs(dlmbda.size(), 1);

  // setup PETSc datastructures
  dg::petsc::matrix schur;

  // TODO: PETSc error checking?
  MatCreateShell(glob_context->comm, dlmbda.size(), dlmbda.size(),
    dlmbda.size(), dlmbda.size(), &sctx, &schur.reset());
  MatShellSetOperation(schur, MATOP_MULT, (void (*)(void))(&shell_mult));
  MatShellSetOperation(
    schur, MATOP_MULT_TRANSPOSE, (void (*)(void))(&shell_transpose_mult));

  dg::petsc::vector dlmbda_vec, glob_vec;
  VecCreateMPIWithArray(glob_context->comm, 1, glob_rhs.size(), glob_rhs.size(),
    glob_rhs.data(), &glob_vec.reset());
  VecCreateMPIWithArray(glob_context->comm, 1, dlmbda.size(), dlmbda.size(),
    dlmbda.data(), &dlmbda_vec.reset());

  dg::petsc::linear_solver ksp;
  KSPCreate(glob_context->comm, &ksp.reset());
  KSPSetFromOptions(ksp);
  KSPSetOperators(ksp, schur, schur);
  // KSPSetType(ksp, KSPIBCGS);
  // KSPSetType(ksp, KSPGMRES);
  KSPSetType(ksp, KSPLGMRES);
  // KSPSetType(ksp, KSPDGMRES);
  // KSPGMRESSetOrthogonalization(
  // ksp, &KSPGMRESModifiedGramSchmidtOrthogonalization);
  KSPSetInitialGuessNonzero(ksp, PETSC_FALSE);
  // KSPSetTolerances(ksp, 1e-70, 1e-14, PETSC_DEFAULT, PETSC_DEFAULT);
  // KSPGMRESSetRestart(ksp, 100);
  // no pre-conditioning
  {
    PC prec;
    KSPGetPC(ksp, &prec);
    PCSetType(prec, PCNONE);
  }

  // compute expected solution
  constexpr size_t int_degree = 12;

  dg::hslab<double, 5> expected_q(
    nelems_x, nelems_y, int_degree + 1, int_degree + 1, 1);
  dg::hslab<double, 5> expected_sigma(
    nelems_x, nelems_y, int_degree + 1, int_degree + 1, 2);
  dg::hslab<double, 2> expected_lmbda(
    nelems_x * (nelems_y - 1) * (int_degree + 1) +
      nelems_y * (nelems_x - 1) * (int_degree + 1) + 1,
    1);

  auto int_absc = quad.abscissas(int_degree + 1);

  {
    // zero-out buffers
    dg::util::fill(sctx.vschur, 0);
    dg::util::fill(sctx.hschur, 0);
    dg::util::fill(glob_rhs, 0);

    for (size_t elem_x = 0; elem_x < nelems_x; ++elem_x)
    {
      coords(0, 0, 0) = elem_x * dx;
      coords(0, 1, 0) = elem_x * dx;
      coords(1, 0, 0) = (elem_x + 1) * dx;
      coords(1, 1, 0) = (elem_x + 1) * dx;
      for (size_t elem_y = 0; elem_y < nelems_y; ++elem_y)
      {
        coords(0, 0, 1) = elem_y * dy;
        coords(0, 1, 1) = (elem_y + 1) * dy;
        coords(1, 0, 1) = elem_y * dy;
        coords(1, 1, 1) = (elem_y + 1) * dy;

        // zero-out buffers
        dg::util::fill(A_mat, 0);
        dg::util::fill(B_mat, 0);
        dg::util::fill(f_vec, 0);
        dg::util::fill(C_horiz, 0);
        dg::util::fill(C_vert, 0);

        // internal vars for the current element
        auto q = dg::make_view_sequence(
          qs[1].subarray(elem_x, elem_y, dg::all{}, dg::all{}, dg::all{}),
          sigma.subarray(elem_x, elem_y, dg::all{}, dg::all{}, dg::all{}));
        // internal vars for the current element, including previous time
        // (used by source helper)
        // source terms
        discr.msource(q, f_vec, sys, mass_matrix, coords);
        discr.source_jac(q, A_mat, sys, mass_matrix, coords);

        // internal flux
        discr.int_flux(q, f_vec, advect_matrix, sys);
        discr.int_flux_jac(q, A_mat, advect_matrix, sys);

        // numerical flux
        // left face
        {
          std::array<double, 2> normal = {-1, 0};
          auto qin = q.subarray(0, dg::all{}, dg::all{});
          if (elem_x == 0)
          {
            // boundary condition
            auto lambda = lmbda.subarray(
              std::make_tuple(lmbda.size() - 1, lmbda.size()), dg::all{});
            discr.num_flux_local(lambda, qin,
              f_vec.subarray(0, dg::all{}, std::make_tuple(0, 1)), normal,
              bc_idcs, face_idcs, dg::get<0>(lift_matrix), sys);
            discr.num_flux_local(lambda, qin,
              f_vec.subarray(0, dg::all{}, std::make_tuple(1, 3)), normal,
              bc_idcs, face_idcs, dg::get<0>(lift_matrix), aux_sys);
            // Jacobian
            // B isn't actually used; being passed as a dummy
            discr.num_flux_local_jac(lambda, qin,
              A_mat.subarray(dg::all{}, dg::all{}, 0, dg::all{}, dg::all{}, 0),
              B_mat.subarray(dg::all{}, dg::all{}, 0, dg::all{}, left_face),
              normal, bc_idcs, face_idcs, dg::get<0>(lift_matrix), bc);
          }
          else
          {
            auto f_idx = ((elem_x - 1) * nelems_y + elem_y) * (degree + 1);
            auto lambda = lmbda.subarray(
              std::make_tuple(f_idx, f_idx + degree + 1), dg::all{});
            auto grhs = glob_rhs.subarray(
              std::make_tuple(f_idx, f_idx + degree + 1), dg::all{});
            discr.num_flux_hdg(lambda, qin, grhs,
              f_vec.subarray(0, dg::all{}, std::make_tuple(0, 1)), normal,
              face_idcs, face_idcs, dg::get<0>(lift_matrix), sys);
            discr.num_flux_local(lambda, qin,
              f_vec.subarray(0, dg::all{}, std::make_tuple(1, 3)), normal,
              face_idcs, face_idcs, dg::get<0>(lift_matrix), aux_sys);
            // Jacobian
            discr.num_flux_jac(lambda, qin,
              A_mat.subarray(dg::all{}, dg::all{}, 0, dg::all{}, dg::all{}, 0),
              B_mat.subarray(dg::all{}, dg::all{}, 0, dg::all{}, left_face),
              C_vert.subarray(0, dg::all{}, dg::all{}, dg::all{}, dg::all{}),
              sctx.vschur.subarray(1, elem_x - 1, elem_y, dg::all{}, dg::all{},
                dg::all{}, dg::all{}),
              normal, face_idcs, face_idcs, dg::get<0>(lift_matrix), sys);
          }
        }
        // right face
        {
          std::array<double, 2> normal = {1, 0};
          auto qin = q.subarray(degree, dg::all{}, dg::all{});
          if (elem_x == nelems_x - 1)
          {
            // boundary condition
            auto lambda = lmbda.subarray(
              std::make_tuple(lmbda.size() - 1, lmbda.size()), dg::all{});
            discr.num_flux_local(lambda, qin,
              f_vec.subarray(degree, dg::all{}, std::make_tuple(0, 1)), normal,
              bc_idcs, face_idcs, dg::get<1>(lift_matrix), sys);
            discr.num_flux_local(lambda, qin,
              f_vec.subarray(degree, dg::all{}, std::make_tuple(1, 3)), normal,
              bc_idcs, face_idcs, dg::get<1>(lift_matrix), aux_sys);
            // Jacobian
            // B isn't actually used; being passed as a dummy
            discr.num_flux_local_jac(lambda, qin,
              A_mat.subarray(
                dg::all{}, dg::all{}, degree, dg::all{}, dg::all{}, degree),
              B_mat.subarray(
                dg::all{}, dg::all{}, degree, dg::all{}, right_face),
              normal, bc_idcs, face_idcs, dg::get<1>(lift_matrix), bc);
          }
          else
          {
            auto f_idx = (elem_x * nelems_y + elem_y) * (degree + 1);
            auto lambda = lmbda.subarray(
              std::make_tuple(f_idx, f_idx + degree + 1), dg::all{});
            auto grhs = glob_rhs.subarray(
              std::make_tuple(f_idx, f_idx + degree + 1), dg::all{});
            discr.num_flux_hdg(lambda, qin, grhs,
              f_vec.subarray(degree, dg::all{}, std::make_tuple(0, 1)), normal,
              face_idcs, face_idcs, dg::get<1>(lift_matrix), sys);
            discr.num_flux_local(lambda, qin,
              f_vec.subarray(degree, dg::all{}, std::make_tuple(1, 3)), normal,
              face_idcs, face_idcs, dg::get<1>(lift_matrix), aux_sys);
            // Jacobian
            discr.num_flux_jac(lambda, qin,
              A_mat.subarray(
                dg::all{}, dg::all{}, degree, dg::all{}, dg::all{}, degree),
              B_mat.subarray(
                dg::all{}, dg::all{}, degree, dg::all{}, right_face),
              C_vert.subarray(1, dg::all{}, dg::all{}, dg::all{}, dg::all{}),
              sctx.vschur.subarray(
                1, elem_x, elem_y, dg::all{}, dg::all{}, dg::all{}, dg::all{}),
              normal, face_idcs, face_idcs, dg::get<1>(lift_matrix), sys);
          }
        }
        // bottom face
        {
          std::array<double, 2> normal = {0, -1};
          auto qin = q.subarray(dg::all{}, 0, dg::all{});
          if (elem_y == 0)
          {
            // boundary condition
            auto lambda = lmbda.subarray(
              std::make_tuple(lmbda.size() - 1, lmbda.size()), dg::all{});
            discr.num_flux_local(lambda, qin,
              f_vec.subarray(dg::all{}, 0, std::make_tuple(0, 1)), normal,
              bc_idcs, face_idcs, dg::get<2>(lift_matrix), sys);
            discr.num_flux_local(lambda, qin,
              f_vec.subarray(dg::all{}, 0, std::make_tuple(1, 3)), normal,
              bc_idcs, face_idcs, dg::get<2>(lift_matrix), aux_sys);
            // Jacobian
            // B isn't actually used; being passed as a dummy
            discr.num_flux_local_jac(lambda, qin,
              A_mat.subarray(dg::all{}, 0, dg::all{}, dg::all{}, 0, dg::all{}),
              B_mat.subarray(dg::all{}, 0, dg::all{}, dg::all{}, bottom_face),
              normal, bc_idcs, face_idcs, dg::get<2>(lift_matrix), bc);
          }
          else
          {
            auto f_idx = (elem_x * (nelems_y - 1) + (elem_y - 1) +
                           (nelems_x - 1) * nelems_y) *
                         (degree + 1);
            auto lambda = lmbda.subarray(
              std::make_tuple(f_idx, f_idx + degree + 1), dg::all{});
            auto grhs = glob_rhs.subarray(
              std::make_tuple(f_idx, f_idx + degree + 1), dg::all{});
            discr.num_flux_hdg(lambda, qin, grhs,
              f_vec.subarray(dg::all{}, 0, std::make_tuple(0, 1)), normal,
              face_idcs, face_idcs, dg::get<2>(lift_matrix), sys);
            discr.num_flux_local(lambda, qin,
              f_vec.subarray(dg::all{}, 0, std::make_tuple(1, 3)), normal,
              face_idcs, face_idcs, dg::get<2>(lift_matrix), aux_sys);
            // Jacobian
            discr.num_flux_jac(lambda, qin,
              A_mat.subarray(dg::all{}, 0, dg::all{}, dg::all{}, 0, dg::all{}),
              B_mat.subarray(dg::all{}, 0, dg::all{}, dg::all{}, bottom_face),
              C_horiz.subarray(0, dg::all{}, dg::all{}, dg::all{}, dg::all{}),
              sctx.hschur.subarray(1, elem_x, elem_y - 1, dg::all{}, dg::all{},
                dg::all{}, dg::all{}),
              normal, face_idcs, face_idcs, dg::get<2>(lift_matrix), sys);
          }
        }

        // top face
        {
          std::array<double, 2> normal = {0, 1};
          auto qin = q.subarray(dg::all{}, degree, dg::all{});
          if (elem_y == nelems_y - 1)
          {
            // boundary condition
            auto lambda = lmbda.subarray(
              std::make_tuple(lmbda.size() - 1, lmbda.size()), dg::all{});
            discr.num_flux_local(lambda, qin,
              f_vec.subarray(dg::all{}, degree, std::make_tuple(0, 1)), normal,
              bc_idcs, face_idcs, dg::get<3>(lift_matrix), sys);
            discr.num_flux_local(lambda, qin,
              f_vec.subarray(dg::all{}, degree, std::make_tuple(1, 3)), normal,
              bc_idcs, face_idcs, dg::get<3>(lift_matrix), aux_sys);
            // Jacobian
            // B isn't actually used; being passed as a dummy
            discr.num_flux_local_jac(lambda, qin,
              A_mat.subarray(
                dg::all{}, degree, dg::all{}, dg::all{}, degree, dg::all{}),
              B_mat.subarray(dg::all{}, degree, dg::all{}, dg::all{}, top_face),
              normal, bc_idcs, face_idcs, dg::get<3>(lift_matrix), bc);
          }
          else
          {
            auto f_idx =
              (elem_x * (nelems_y - 1) + elem_y + (nelems_x - 1) * nelems_y) *
              (degree + 1);
            auto lambda = lmbda.subarray(
              std::make_tuple(f_idx, f_idx + degree + 1), dg::all{});
            auto grhs = glob_rhs.subarray(
              std::make_tuple(f_idx, f_idx + degree + 1), dg::all{});
            discr.num_flux_hdg(lambda, qin, grhs,
              f_vec.subarray(dg::all{}, degree, std::make_tuple(0, 1)), normal,
              face_idcs, face_idcs, dg::get<3>(lift_matrix), sys);
            discr.num_flux_local(lambda, qin,
              f_vec.subarray(dg::all{}, degree, std::make_tuple(1, 3)), normal,
              face_idcs, face_idcs, dg::get<3>(lift_matrix), aux_sys);
            // Jacobian
            discr.num_flux_jac(lambda, qin,
              A_mat.subarray(
                dg::all{}, degree, dg::all{}, dg::all{}, degree, dg::all{}),
              B_mat.subarray(dg::all{}, degree, dg::all{}, dg::all{}, top_face),
              C_horiz.subarray(1, dg::all{}, dg::all{}, dg::all{}, dg::all{}),
              sctx.hschur.subarray(
                1, elem_x, elem_y, dg::all{}, dg::all{}, dg::all{}, dg::all{}),
              normal, face_idcs, face_idcs, dg::get<3>(lift_matrix), sys);
          }
        }

        // perform local linear solves
        Eigen::PartialPivLU<decltype(A_map)> lu(A_map);
        {
          // solve for Xi
          Eigen::Map<Eigen::Matrix<double, 3 * nnodes, 1>> f_map(f_vec.data());
          Eigen::Map<Eigen::Matrix<double, 3 * nnodes, 1>> xi_map(
            &xi_vecs(elem_x, elem_y, 0, 0, 0));
          xi_map = lu.solve(f_map);
        }
        // update global RHS
        if (elem_x != 0)
        {
          // left face
          size_t f_idx = ((elem_x - 1) * nelems_y + elem_y) * (degree + 1);
          grhs_update_helper(
            C_vert.subarray(0, dg::all{}, dg::all{}, dg::all{}, dg::all{}),
            xi_vecs.subarray(elem_x, elem_y, 0, dg::all{}, dg::all{}),
            glob_rhs.subarray(
              std::make_tuple(f_idx, f_idx + degree + 1), dg::all{}));
        }
        if (elem_x != nelems_x - 1)
        {
          // right face
          size_t f_idx = (elem_x * nelems_y + elem_y) * (degree + 1);
          grhs_update_helper(
            C_vert.subarray(1, dg::all{}, dg::all{}, dg::all{}, dg::all{}),
            xi_vecs.subarray(elem_x, elem_y, degree, dg::all{}, dg::all{}),
            glob_rhs.subarray(
              std::make_tuple(f_idx, f_idx + degree + 1), dg::all{}));
        }
        if (elem_y != 0)
        {
          // bottom face
          size_t f_idx = (elem_x * (nelems_y - 1) + (elem_y - 1) +
                           (nelems_x - 1) * nelems_y) *
                         (degree + 1);
          grhs_update_helper(
            C_horiz.subarray(0, dg::all{}, dg::all{}, dg::all{}, dg::all{}),
            xi_vecs.subarray(elem_x, elem_y, dg::all{}, 0, dg::all{}),
            glob_rhs.subarray(
              std::make_tuple(f_idx, f_idx + degree + 1), dg::all{}));
        }
        if (elem_y != nelems_y - 1)
        {
          // top face
          size_t f_idx =
            (elem_x * (nelems_y - 1) + elem_y + (nelems_x - 1) * nelems_y) *
            (degree + 1);
          grhs_update_helper(
            C_horiz.subarray(1, dg::all{}, dg::all{}, dg::all{}, dg::all{}),
            xi_vecs.subarray(elem_x, elem_y, dg::all{}, degree, dg::all{}),
            glob_rhs.subarray(
              std::make_tuple(f_idx, f_idx + degree + 1), dg::all{}));
        }

        // solve for gamma and update contribution to schur compliment
        // assumes at least 2 elements in each dimension(?)
        if (elem_x != 0)
        {
          // left face couples to something
          {
            Eigen::Map<Eigen::Matrix<double, 3 * nnodes, degree + 1>> B_map(
              &B_mat(0, 0, 0, 0, 0));
            Eigen::Map<Eigen::Matrix<double, 3 * nnodes, degree + 1>> gamma_map(
              &gamma_mats(0, 0, 0, 0, 0, elem_x, elem_y));
            gamma_map = lu.solve(B_map);
          }

          auto gamma_left = gamma_mats.subarray(dg::all{}, dg::all{}, dg::all{},
            dg::all{}, left_face, elem_x, elem_y);

          // left self coupling
          schur_update_helper(
            gamma_left.subarray(dg::all{}, dg::all{}, 0, dg::all{}, dg::all{}),
            C_vert.subarray(0, dg::all{}, dg::all{}, dg::all{}, dg::all{}),
            sctx.vschur.subarray(1, elem_x - 1, elem_y, dg::all{}, dg::all{},
              dg::all{}, dg::all{}));
          if (elem_x != nelems_x - 1)
          {
            // left to right
            schur_update_helper(gamma_left.subarray(dg::all{}, dg::all{},
                                  degree, dg::all{}, dg::all{}),
              C_vert.subarray(1, dg::all{}, dg::all{}, dg::all{}, dg::all{}),
              sctx.vschur.subarray(
                0, elem_x, elem_y, dg::all{}, dg::all{}, dg::all{}, dg::all{}));
          }
          if (elem_y != 0)
          {
            // left to bottom right
            schur_update_helper(gamma_left.subarray(dg::all{}, 0, dg::all{},
                                  dg::all{}, dg::all{}),
              C_horiz.subarray(0, dg::all{}, dg::all{}, dg::all{}, dg::all{}),
              sctx.hschur.subarray(5, elem_x, elem_y - 1, dg::all{}, dg::all{},
                dg::all{}, dg::all{}));
          }
          if (elem_y != nelems_y - 1)
          {
            // left to top right
            schur_update_helper(gamma_left.subarray(dg::all{}, degree,
                                  dg::all{}, dg::all{}, dg::all{}),
              C_horiz.subarray(1, dg::all{}, dg::all{}, dg::all{}, dg::all{}),
              sctx.hschur.subarray(
                3, elem_x, elem_y, dg::all{}, dg::all{}, dg::all{}, dg::all{}));
          }
        }
        if (elem_x != nelems_x - 1)
        {
          // right face couples to something
          {
            Eigen::Map<Eigen::Matrix<double, 3 * nnodes, degree + 1>> B_map(
              &B_mat(0, 0, 0, 0, degree + 1));
            Eigen::Map<Eigen::Matrix<double, 3 * nnodes, degree + 1>> gamma_map(
              &gamma_mats(0, 0, 0, 0, degree + 1, elem_x, elem_y));
            gamma_map = lu.solve(B_map);
          }
          auto gamma_right = gamma_mats.subarray(dg::all{}, dg::all{},
            dg::all{}, dg::all{}, right_face, elem_x, elem_y);
          // right self coupling
          schur_update_helper(gamma_right.subarray(dg::all{}, dg::all{}, degree,
                                dg::all{}, dg::all{}),
            C_vert.subarray(1, dg::all{}, dg::all{}, dg::all{}, dg::all{}),
            sctx.vschur.subarray(
              1, elem_x, elem_y, dg::all{}, dg::all{}, dg::all{}, dg::all{}));
          if (elem_x != 0)
          {
            // right to left
            schur_update_helper(gamma_right.subarray(dg::all{}, dg::all{}, 0,
                                  dg::all{}, dg::all{}),
              C_vert.subarray(0, dg::all{}, dg::all{}, dg::all{}, dg::all{}),
              sctx.vschur.subarray(2, elem_x - 1, elem_y, dg::all{}, dg::all{},
                dg::all{}, dg::all{}));
          }
          if (elem_y != 0)
          {
            // right to bottom left
            schur_update_helper(gamma_right.subarray(dg::all{}, 0, dg::all{},
                                  dg::all{}, dg::all{}),
              C_horiz.subarray(0, dg::all{}, dg::all{}, dg::all{}, dg::all{}),
              sctx.hschur.subarray(6, elem_x, elem_y - 1, dg::all{}, dg::all{},
                dg::all{}, dg::all{}));
          }
          if (elem_y != nelems_y - 1)
          {
            // right to top left
            schur_update_helper(gamma_right.subarray(dg::all{}, degree,
                                  dg::all{}, dg::all{}, dg::all{}),
              C_horiz.subarray(1, dg::all{}, dg::all{}, dg::all{}, dg::all{}),
              sctx.hschur.subarray(
                4, elem_x, elem_y, dg::all{}, dg::all{}, dg::all{}, dg::all{}));
          }
        }

        if (elem_y != 0)
        {
          // bottom face couples to something
          {
            Eigen::Map<Eigen::Matrix<double, 3 * nnodes, degree + 1>> B_map(
              &B_mat(0, 0, 0, 0, 2 * (degree + 1)));
            Eigen::Map<Eigen::Matrix<double, 3 * nnodes, degree + 1>> gamma_map(
              &gamma_mats(0, 0, 0, 0, 2 * (degree + 1), elem_x, elem_y));
            gamma_map = lu.solve(B_map);
          }
          auto gamma_bottom = gamma_mats.subarray(dg::all{}, dg::all{},
            dg::all{}, dg::all{}, bottom_face, elem_x, elem_y);
          // bottom self coupling
          schur_update_helper(gamma_bottom.subarray(
                                dg::all{}, 0, dg::all{}, dg::all{}, dg::all{}),
            C_horiz.subarray(0, dg::all{}, dg::all{}, dg::all{}, dg::all{}),
            sctx.hschur.subarray(1, elem_x, elem_y - 1, dg::all{}, dg::all{},
              dg::all{}, dg::all{}));
          if (elem_x != 0)
          {
            // bottom to left top
            schur_update_helper(gamma_bottom.subarray(dg::all{}, dg::all{}, 0,
                                  dg::all{}, dg::all{}),
              C_vert.subarray(0, dg::all{}, dg::all{}, dg::all{}, dg::all{}),
              sctx.vschur.subarray(4, elem_x - 1, elem_y, dg::all{}, dg::all{},
                dg::all{}, dg::all{}));
          }
          if (elem_x != nelems_x - 1)
          {
            // bottom to right top
            schur_update_helper(gamma_bottom.subarray(dg::all{}, dg::all{},
                                  degree, dg::all{}, dg::all{}),
              C_vert.subarray(1, dg::all{}, dg::all{}, dg::all{}, dg::all{}),
              sctx.vschur.subarray(
                3, elem_x, elem_y, dg::all{}, dg::all{}, dg::all{}, dg::all{}));
          }
          if (elem_y != nelems_y - 1)
          {
            // bottom to top
            schur_update_helper(gamma_bottom.subarray(dg::all{}, degree,
                                  dg::all{}, dg::all{}, dg::all{}),
              C_horiz.subarray(1, dg::all{}, dg::all{}, dg::all{}, dg::all{}),
              sctx.hschur.subarray(
                0, elem_x, elem_y, dg::all{}, dg::all{}, dg::all{}, dg::all{}));
          }
        }

        if (elem_y != nelems_y - 1)
        {
          // top face couples to something
          {
            Eigen::Map<Eigen::Matrix<double, 3 * nnodes, degree + 1>> B_map(
              &B_mat(0, 0, 0, 0, 3 * (degree + 1)));
            Eigen::Map<Eigen::Matrix<double, 3 * nnodes, degree + 1>> gamma_map(
              &gamma_mats(0, 0, 0, 0, 3 * (degree + 1), elem_x, elem_y));
            gamma_map = lu.solve(B_map);
          }
          auto gamma_top = gamma_mats.subarray(dg::all{}, dg::all{}, dg::all{},
            dg::all{}, top_face, elem_x, elem_y);
          // top self coupling
          schur_update_helper(gamma_top.subarray(dg::all{}, degree, dg::all{},
                                dg::all{}, dg::all{}),
            C_horiz.subarray(1, dg::all{}, dg::all{}, dg::all{}, dg::all{}),
            sctx.hschur.subarray(
              1, elem_x, elem_y, dg::all{}, dg::all{}, dg::all{}, dg::all{}));
          if (elem_x != 0)
          {
            // top to left bottom
            schur_update_helper(
              gamma_top.subarray(dg::all{}, dg::all{}, 0, dg::all{}, dg::all{}),
              C_vert.subarray(0, dg::all{}, dg::all{}, dg::all{}, dg::all{}),
              sctx.vschur.subarray(6, elem_x - 1, elem_y, dg::all{}, dg::all{},
                dg::all{}, dg::all{}));
          }
          if (elem_x != nelems_x - 1)
          {
            // top to right bottom
            schur_update_helper(gamma_top.subarray(dg::all{}, dg::all{}, degree,
                                  dg::all{}, dg::all{}),
              C_vert.subarray(1, dg::all{}, dg::all{}, dg::all{}, dg::all{}),
              sctx.vschur.subarray(
                5, elem_x, elem_y, dg::all{}, dg::all{}, dg::all{}, dg::all{}));
          }
          if (elem_y != 0)
          {
            // top to bottom
            schur_update_helper(
              gamma_top.subarray(dg::all{}, 0, dg::all{}, dg::all{}, dg::all{}),
              C_horiz.subarray(0, dg::all{}, dg::all{}, dg::all{}, dg::all{}),
              sctx.hschur.subarray(2, elem_x, elem_y - 1, dg::all{}, dg::all{},
                dg::all{}, dg::all{}));
          }
        }
      }
    }

    // perform global solve
    // TODO: are these assembly calls required?
    VecAssemblyBegin(glob_vec);
    VecAssemblyEnd(glob_vec);

    KSPSolve(ksp, glob_vec, dlmbda_vec);

    // check for convergence
    {
      KSPConvergedReason reason;
      KSPGetConvergedReason(ksp, &reason);
      // if > 0, converged
      if (reason <= 0)
      {
        ADD_FAILURE() << "KSP didn't converge: "
                      << dg::petsc::to_string(reason);
      }
    }

    {
      size_t fidx = 0;
      // vertical faces
      for (size_t fx = 0; fx < nelems_x - 1; ++fx)
      {
        for (size_t fy = 0; fy < nelems_y; ++fy)
        {
          for (size_t j = 0; j < degree + 1; ++j, ++fidx)
          {
            lmbda(fidx, 0) += dlmbda(fidx, 0);
          }
        }
      }
      // horizontal faces
      for (size_t fx = 0; fx < nelems_x; ++fx)
      {
        for (size_t fy = 0; fy < nelems_y - 1; ++fy)
        {
          for (size_t j = 0; j < degree + 1; ++j, ++fidx)
          {
            lmbda(fidx, 0) += dlmbda(fidx, 0);
          }
        }
      }
    }

    for (size_t elem_x = 0; elem_x < nelems_x; ++elem_x)
    {
      for (size_t elem_y = 0; elem_y < nelems_y; ++elem_y)
      {
        // back-solve for local element solution
        // this is just a matrix multiply
        Eigen::Map<Eigen::Matrix<double, 3 * (degree + 1) * (degree + 1), 1>>
          xi_map(&xi_vecs(elem_x, elem_y, 0, 0, 0),
            3 * (degree + 1) * (degree + 1), 1);
        if (elem_x != 0)
        {
          // left face
          auto f_idx = ((elem_x - 1) * nelems_y + elem_y) * (degree + 1);
          Eigen::Map<Eigen::Matrix<double, degree + 1, 1>> dlmbda_map(
            &dlmbda(f_idx, 0));
          Eigen::Map<
            Eigen::Matrix<double, 3 * (degree + 1) * (degree + 1), degree + 1>>
            gamma_map(&gamma_mats(0, 0, 0, 0, 0, elem_x, elem_y));
          xi_map -= gamma_map * dlmbda_map;
        }
        if (elem_x != nelems_x - 1)
        {
          // right face
          auto f_idx = (elem_x * nelems_y + elem_y) * (degree + 1);
          Eigen::Map<Eigen::Matrix<double, degree + 1, 1>> dlmbda_map(
            &dlmbda(f_idx, 0));
          Eigen::Map<
            Eigen::Matrix<double, 3 * (degree + 1) * (degree + 1), degree + 1>>
            gamma_map(&gamma_mats(0, 0, 0, 0, degree + 1, elem_x, elem_y));
          xi_map -= gamma_map * dlmbda_map;
        }
        if (elem_y != 0)
        {
          // bottom face
          auto f_idx = (elem_x * (nelems_y - 1) + (elem_y - 1) +
                         (nelems_x - 1) * nelems_y) *
                       (degree + 1);
          Eigen::Map<Eigen::Matrix<double, degree + 1, 1>> dlmbda_map(
            &dlmbda(f_idx, 0));
          Eigen::Map<
            Eigen::Matrix<double, 3 * (degree + 1) * (degree + 1), degree + 1>>
            gamma_map(
              &gamma_mats(0, 0, 0, 0, 2 * (degree + 1), elem_x, elem_y));
          xi_map -= gamma_map * dlmbda_map;
        }
        if (elem_y != nelems_y - 1)
        {
          // top face
          auto f_idx =
            (elem_x * (nelems_y - 1) + elem_y + (nelems_x - 1) * nelems_y) *
            (degree + 1);
          Eigen::Map<Eigen::Matrix<double, degree + 1, 1>> dlmbda_map(
            &dlmbda(f_idx, 0));
          Eigen::Map<
            Eigen::Matrix<double, 3 * (degree + 1) * (degree + 1), degree + 1>>
            gamma_map(
              &gamma_mats(0, 0, 0, 0, 3 * (degree + 1), elem_x, elem_y));
          xi_map -= gamma_map * dlmbda_map;
        }
        // xi now contains du, manually unpack into solution vectors
        // have to manually unpack, since q and sigma are in different
        // arrays...
        for (size_t i = 0; i < degree + 1; ++i)
        {
          for (size_t j = 0; j < degree + 1; ++j)
          {
            qs[1](elem_x, elem_y, i, j, 0) += xi_vecs(elem_x, elem_y, i, j, 0);
            sigma(elem_x, elem_y, i, j, 0) += xi_vecs(elem_x, elem_y, i, j, 1);
            sigma(elem_x, elem_y, i, j, 1) += xi_vecs(elem_x, elem_y, i, j, 2);
          }
        }
      }
    }
  }

  sys.asol(dg::make_view_sequence(dg::view<double, 5>(expected_q),
             dg::view<double, 5>(expected_sigma)),
    expected_lmbda, int_absc, int_absc, xmin, xmax, ymin, ymax, 1);

  auto q_error = integrate_l2_error<degree, degree, int_degree, int_degree>(
    qs[1], expected_q, nelems_x, nelems_y, dx, dy);
  auto sigma_x_error =
    integrate_l2_error<degree, degree, int_degree, int_degree>(
      sigma.subarray(
        dg::all{}, dg::all{}, dg::all{}, dg::all{}, std::make_tuple(1)),
      expected_sigma.subarray(
        dg::all{}, dg::all{}, dg::all{}, dg::all{}, std::make_tuple(1)),
      nelems_x, nelems_y, dx, dy);
  auto sigma_y_error =
    integrate_l2_error<degree, degree, int_degree, int_degree>(
      sigma.subarray(
        dg::all{}, dg::all{}, dg::all{}, dg::all{}, std::make_tuple(1, 2)),
      expected_sigma.subarray(
        dg::all{}, dg::all{}, dg::all{}, dg::all{}, std::make_tuple(1, 2)),
      nelems_x, nelems_y, dx, dy);
  // TODO: how to properly compute error in lambda?
  return std::make_tuple(q_error, sigma_x_error, sigma_y_error);
}

TEST(test_hdg_rect_2d, poisson)
{
  // point sampling convergence testing
  constexpr size_t degree = 2;
  auto err_1 = poisson_sim<degree>(4, 5);
  auto err_2 = poisson_sim<degree>(8, 10);
  // q error
  EXPECT_NEAR(std::get<0>(err_1) / std::get<0>(err_2), 8, .9);
  // sigma error
  EXPECT_NEAR(std::get<1>(err_1) / std::get<1>(err_2), 8, .9);
  EXPECT_NEAR(std::get<2>(err_1) / std::get<2>(err_2), 8, .5);
  // TODO: what is the expected lambda error?
}

int main(int argc, char** argv)
{
  init_tutils(&argc, argv);
  dg::context ctx(0, nullptr);

  glob_context = &ctx;

  return run_tutils();
}
