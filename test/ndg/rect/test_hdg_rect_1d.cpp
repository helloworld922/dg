#include "dg/ndg/rect/rect_1d.hpp"
#include "dg/eqsets/temporal/dirk.hpp"
#include "dg/utils/hslab.hpp"

#include "dg/utils/view_utils.hpp"

#include "tutils/tutils.hpp"

#include "dg/context.hpp"

#include <array>
#include <cmath>
#include <numeric>

///
/// @brief Crux as a way to pass context created in main function to tests...
///
static dg::context* glob_context;

///
/// @brief Dirichlet BC's for diffusion system
///
struct diffusion_dirichlet_bc
{
  double coeff;
  double tau;
  template <class Lmbda, class QIn, class A, class B>
  void num_flux_local_jac(const Lmbda& lmbda, const QIn& qin, double mult,
    A&& A_mat, B&& B_mat, double normal) const
  {
    // system is responsible for all signs
    // local system
    A_mat(0, 0) += mult * coeff * tau;
    A_mat(0, 1) -= mult * coeff * normal;
    // actually ignores B since lmbda is not part of vector of unknowns
    // B_mat(0, 0) += mult * coeff * tau;
    // B_mat(1, 0) += mult * normal;
  }
};

///
/// @brief Helper for diffusion system. Helper for computing 1D gradient of a
/// scalar variable. This only has the num_flux part, since there's no way to
/// safely separate what parts contribute to the local RHS, and which parts to
/// the global RHS...
///
struct gradient_helper
{
  template <class Lmbda, class Q>
  auto num_flux(const Lmbda& lmbda_, const Q& q_, double normal) const
  {
    auto& q = dg::get<0>(q_);
    auto& sigma = dg::get<1>(q_);
    dg::shslab<double, 1, 1> res(1);
    // auxiliary PDE
    res[0] = -lmbda_[0] * normal;
    return res;
  }
};

///
/// @brief Implements:
/// \f{gather}{
/// d_t q - d_x (k \sigma) = 0\\
/// \sigma - d_x(q) = 0
/// \f}
/// Used for testing HDG. Used Implicit Euler for timestepping.
///
struct diffusion_system
{
  /// diffusion coefficient k
  double coeff;

  // HDG continuity penalty
  double tau;

  // TODO: doesn't seem to work with dirk3?
  dg::eqsets::dirk2<> tsys;

  template <class Q>
  auto int_flux(const Q& q_) const
  {
    auto& q = dg::get<0>(q_);
    auto& sigma = dg::get<1>(q_);
    dg::shslab<double, 2, 1> res(2);
    res[0] = -coeff * sigma[0];
    res[1] = -q[0];
    return res;
  }

  template <class Q, class D>
  void int_flux_jac(const Q& q_, double mult, D&& dst) const
  {
    // system is responsible for all signs
    dst(0, 1) += mult * coeff;
    dst(1, 0) += mult;
  }

  template <class Lmbda, class Q>
  auto num_flux(const Lmbda& lmbda_, const Q& q_, double normal) const
  {
    auto& q = dg::get<0>(q_);
    auto& sigma = dg::get<1>(q_);
    dg::shslab<double, 1, 1> res(1);
    // primary PDE
    res[0] = coeff * (tau * (q[0] - lmbda_[0]) - sigma[0] * normal);
    return res;
  }

  template <class Lmbda, class QIn, class A, class B, class C, class D>
  void num_flux_jac(const Lmbda& lmbda, const QIn& qin, double mult, A&& A_mat,
    B&& B_mat, C&& C_mat, D&& D_mat, double normal) const
  {
    // system is responsible for all signs
    // for interior faces
    // local system
    A_mat(0, 0) += mult * coeff * tau;
    A_mat(0, 1) -= mult * coeff * normal;
    B_mat(0, 0) -= mult * coeff * tau;
    B_mat(1, 0) -= mult * normal;
    // global system
    C_mat(0, 0) += mult * coeff * tau;
    C_mat(0, 1) -= mult * coeff * normal;
    D_mat(0, 0) -= mult * coeff * tau;
  }

  template <class Q>
  auto source(const Q& q_) const
  {
    auto& q = dg::get<0>(q_);
    auto& sigma = dg::get<1>(q_);
    auto& qprev = dg::get<2>(q_);
    //auto tmp = tsys.source(dg::make_view_sequence(q, dg::get<2>(q_)));
    dg::shslab<double, 2, 1> res(2);
    // Implicit Euler timestepping
    res[0] = tsys.idt * (qprev[0] - q[0]);
    res[1] = -sigma[0];
    return res;
  }

  template <class Q, class D>
  void source_jac(const Q& q, double mult, D&& dst) const
  {
    // system is responsible for all signs
    // backward euler time advance
#if 0
    tsys.source_jac(
      q, mult, dst.subarray(std::make_tuple(1), std::make_tuple(1)));
#else
    dst(0, 0) += mult * tsys.idt;
#endif
    // aux eq. identity
    dst(1, 1) += mult;
  }

  ///
  /// @brief Computes the analytical solution at a given time
  /// Assumes the initial conditions are a single period of a sin wave and
  /// domain has dirichlet zero boundary conditions.
  ///
  template <class Q, class L, class A>
  void asol(
    Q&& q_, L&& lmbda, const A& absc, double xmin, double xmax, double t)
  {
    auto& q = dg::get<0>(q_);
    auto& sigma = dg::get<1>(q_);
    auto dx = (xmax - xmin) / q.extent(0);
    for (size_t elem = 0; elem < q.extent(0); ++elem)
    {
      double x0 = dx * elem + xmin;
      for (size_t i = 0; i < q.extent(1); ++i)
      {
        double x = x0 + 0.5 * dx * (absc[i] + 1);
        q(elem, i, 0) =
          sin(6.283185307179586 * x) * exp(-39.47841760435743 * coeff * t);
        sigma(elem, i, 0) = 6.283185307179586 * cos(6.283185307179586 * x) *
                            exp(-39.47841760435743 * coeff * t);
      }
      lmbda(elem, 0) = q(elem, q.extent(1) - 1, 0);
    }
    lmbda(q.extent(0) - 1, 0) = 0;
  }
};

///
/// @brief A context to be used in PETSc's Shell Matrices to help with linear
/// solves of the Schur complement. Note: this implementation is not parallel!
/// It is tailored specifically for the 1D diffusion HDG test. It should be
/// replaced with a general helper in the future...
///
/// specific assumptions:
/// - 1D
/// - Has Dirichlet BC's on all boundaries, boundaries are excluded from
/// unknowns
/// - Uniform number of unknowns over whole mesh
///
struct hdg_schur_context
{
  // dim 0: block idx (0 is left, 1 is diagonal, 2 is right)
  // dim 1: face idx
  // dim 2: face node idx for row (specific to face)
  // dim 3: lambda for row
  // dim 4: face node idx for row (specific to face)
  // dim 5: lambda for col
  dg::hslab<double, 6> schur;

  hdg_schur_context(size_t nnodes, size_t nelems, size_t num_lambda)
    : schur(3, (nelems - 1), 1, num_lambda, 1, num_lambda)
  {
  }

  ///
  /// @brief Matrix transpose/vector multiplication: y = A^T x
  ///
  PetscErrorCode transpose_mult_vec(Vec x_, Vec y_)
  {
    VecSet(y_, 0);
    const PetscScalar* x_data;
    PetscScalar* y_data;
    VecGetArray(y_, &y_data);
    VecGetArrayRead(x_, &x_data);
    dg::view<const double, 2> x(x_data, schur.extent(1), schur.extent(2));
    dg::view<double, 2> y(y_data, schur.extent(1), schur.extent(2));

    for (size_t face = 0; face < schur.extent(1); ++face)
    {
      if (face != 0)
      {
        // left block
        for (size_t i = 0; i < schur.extent(2); ++i)
        {
          for (size_t j = 0; j < schur.extent(2); ++j)
          {
            y(face - 1, i) += schur(0, face, 0, j, 0, i) * x(face, j);
          }
        }
      }

      // diagonal block
      for (size_t i = 0; i < schur.extent(2); ++i)
      {
        for (size_t j = 0; j < schur.extent(2); ++j)
        {
          y(face, i) += schur(1, face, 0, j, 0, i) * x(face, j);
        }
      }

      if (face != schur.extent(1) - 1)
      {
        // right block
        for (size_t i = 0; i < schur.extent(2); ++i)
        {
          for (size_t j = 0; j < schur.extent(2); ++j)
          {
            y(face + 1, i) += schur(2, face, 0, j, 0, i) * x(face, j);
          }
        }
      }
    }

    VecRestoreArray(y_, &y_data);
    VecRestoreArrayRead(x_, &x_data);
    return 0;
  }

  ///
  /// @brief Matrix/vector multiplication: y = A x
  ///
  PetscErrorCode mult_vec(Vec x_, Vec y_)
  {
    VecSet(y_, 0);
    const PetscScalar* x_data;
    PetscScalar* y_data;
    VecGetArray(y_, &y_data);
    VecGetArrayRead(x_, &x_data);
    dg::view<const double, 2> x(x_data, schur.extent(1), schur.extent(2));
    dg::view<double, 2> y(y_data, schur.extent(1), schur.extent(2));

    for (size_t face = 0; face < schur.extent(1); ++face)
    {
      if (face != 0)
      {
        // left block
        for (size_t i = 0; i < schur.extent(2); ++i)
        {
          for (size_t j = 0; j < schur.extent(2); ++j)
          {
            y(face, i) += schur(0, face, 0, i, 0, j) * x(face - 1, j);
          }
        }
      }

      // diagonal block
      for (size_t i = 0; i < schur.extent(2); ++i)
      {
        for (size_t j = 0; j < schur.extent(2); ++j)
        {
          y(face, i) += schur(1, face, 0, i, 0, j) * x(face, j);
        }
      }

      if (face != schur.extent(1) - 1)
      {
        // right block
        for (size_t i = 0; i < schur.extent(2); ++i)
        {
          for (size_t j = 0; j < schur.extent(2); ++j)
          {
            y(face, i) += schur(2, face, 0, i, 0, j) * x(face + 1, j);
          }
        }
      }
    }

    VecRestoreArray(y_, &y_data);
    VecRestoreArrayRead(x_, &x_data);
    return 0;
  }
};

static PetscErrorCode shell_mult(Mat A, Vec x, Vec y)
{
  hdg_schur_context* ctx;
  MatShellGetContext(A, &ctx);
  return ctx->mult_vec(x, y);
}

static PetscErrorCode shell_transpose_mult(Mat A, Vec x, Vec y)
{
  hdg_schur_context* ctx;
  MatShellGetContext(A, &ctx);
  return ctx->transpose_mult_vec(x, y);
}

///
/// @brief Helper for integrating the L-2 error norm
/// @param sol computed solution, in base_degree
/// @param expected expected solution, evaluated in int_degree
///
template <size_t base_degree, size_t int_degree, class Q, class E>
static double integrate_l2_error(
  const Q& sol, const E& expected, size_t nelems, double dx)
{
  static dg::spatial::elements::lagrange<double, base_degree> base_element;
  static dg::spatial::elements::lagrange<double, int_degree> int_element;

  double res = 0;

  for (size_t comp = 0; comp < sol.extent(2); ++comp)
  {
    double tmp = 0;
    for (size_t elem = 0; elem < nelems; ++elem)
    {
      tmp += int_element.vol_integral(
        [dx, base = sol.subarray(elem, dg::all{}, comp),
          high = expected.subarray(elem, dg::all{}, comp)](
          const std::array<double, 1>& xi) {
          // interpolate base solution to xi
          auto val = base_element.interp(xi, base);
          auto exp_ = int_element.interp(xi, high);
          return (val - exp_) * (val - exp_) * 0.5 * dx;
        });
    }
    res += tmp;
  }
  return sqrt(res);
}

TEST(test_hdg_rect_1d, int_flux_jac)
{
  // TODO
  FAIL() << "not implemented yet";
}

TEST(test_hdg_rect_1d, num_flux_jac)
{
  // TODO
  FAIL() << "not implemented yet";
}

template <size_t degree>
auto implicit_diffusion_sim(size_t nelems, double dt)
{
  // Diffusion equation solver, uses IRK1
  // dirichlet boundary conditions are handled with lambda removed from vector
  // of unknowns
  dg::ndg::rect<degree> discr{};

  diffusion_system sys;
  gradient_helper aux_sys;
  diffusion_dirichlet_bc bc;

  // diffusion coeff.
  constexpr double kappa = 1e-2;
  constexpr size_t nsteps = 5;
  constexpr double xmin = 0;
  constexpr double xmax = 1;
  double dx = 1. / nelems;
  double jac = 0.5 * dx;

  sys.coeff = kappa;
  sys.tau = 1;
  sys.tsys.idt = 1 / dt;
  bc.coeff = kappa;
  bc.tau = 1;

  // hdg matrices
  Eigen::Matrix<double, degree + 1, degree + 1> mass_matrix;
  discr.calc_mass_matrix(mass_matrix, jac);
  Eigen::Matrix<double, degree + 1, degree + 1> amatrix;
  discr.calc_advect_matrix(amatrix);
  // Eigen's default layout is left
  dg::view<double, 2, dg::layout_left> advect_matrix(
    amatrix.data(), degree + 1, degree + 1);
  std::array<dg::shslab<double, 1, 2>, 2> lift_matrix = {
    dg::shslab<double, 1, 2>(1, 1), dg::shslab<double, 1, 2>(1, 1)};
  discr.calc_lift_matrix(lift_matrix, 1, 1);

  // solution space
  dg::view_array<dg::hslab<double, 3>, decltype(sys.tsys)::num_buffers()> qs;
  for (size_t i = 0; i < qs.size(); ++i)
  {
    qs[i] = dg::hslab<double, 3>(nelems, degree + 1, 1);
  }
  // also stash a temporary for boundary condition at the end
  dg::hslab<double, 2> lmbda(nelems, 1);
  dg::hslab<double, 2> dlmbda(nelems - 1, 1);
  dg::hslab<double, 3> sigma(nelems, degree + 1, 1);

  constexpr static dg::lobatto<double> quad{};
  auto absc = quad.abscissas(degree + 1);

  // initial conditions
  sys.asol(dg::make_view_sequence(qs[0].as_view(), sigma.as_view()), lmbda,
    absc, xmin, xmax, 0);

  // initialize qs to something
  for (size_t i = 1; i < qs.size(); ++i)
  {
    std::copy(qs[0].data(), qs[0].data() + qs[0].size(), qs[i].data());
  }

  // local A matrix
  dg::shslab<double, (degree + 1) * (degree + 1) * 2 * 2, 4, dg::layout_left>
    A_mat(2, degree + 1, 2, degree + 1);
  Eigen::Map<Eigen::Matrix<double, 2 * (degree + 1), 2 * (degree + 1)>> A_map(
    A_mat.data(), 2 * (degree + 1), 2 * (degree + 1));
  // one local C matrix per face in an element
  dg::shslab<double, 2, 4> C_left(1, 1, 1, 2);
  dg::shslab<double, 2, 4> C_right(1, 1, 1, 2);

  hdg_schur_context sctx(degree + 1, nelems, 1);
  // global storage for A^-1 B
  // TODO: how to treat gamma special for elements with a boundary face?
  // For now, this actually allocate the space for them, but never touch that
  // memory
  dg::shslab<double, 2 * (degree + 1) * 1 * 2, 4, dg::layout_left> B_mat(
    2, degree + 1, 1, 2);
  dg::hslab<double, 5, dg::layout_left> gamma_mats(2, degree + 1, 1, 2, nelems);
  // global storage for A^-1 f
  dg::hslab<double, 3> xi_vecs(nelems, degree + 1, 2);
  dg::shslab<double, 2 * (degree + 1), 2> f_vec(degree + 1, 2);
  // global RHS
  dg::hslab<double, 2> glob_rhs(nelems - 1, 1);

  // setup PETSc datastructures
  dg::petsc::matrix schur;
  // TODO: PETSc error checking?
  MatCreateShell(glob_context->comm, nelems - 1, nelems - 1, nelems - 1,
    nelems - 1, &sctx, &schur.reset());
  MatShellSetOperation(schur, MATOP_MULT, (void (*)(void))(&shell_mult));
  MatShellSetOperation(
    schur, MATOP_MULT_TRANSPOSE, (void (*)(void))(&shell_transpose_mult));

  dg::petsc::vector dlmbda_vec, glob_vec;
  VecCreateMPIWithArray(glob_context->comm, 1, glob_rhs.size(), glob_rhs.size(),
    glob_rhs.data(), &glob_vec.reset());
  VecCreateMPIWithArray(glob_context->comm, 1, dlmbda.size(), dlmbda.size(),
    dlmbda.data(), &dlmbda_vec.reset());

  dg::petsc::linear_solver ksp;
  KSPCreate(glob_context->comm, &ksp.reset());
  KSPSetFromOptions(ksp);
  KSPSetOperators(ksp, schur, schur);
  KSPSetType(ksp, KSPIBCGS);
  KSPSetInitialGuessNonzero(ksp, PETSC_FALSE);
  // no pre-conditioning
  {
    PC prec;
    KSPGetPC(ksp, &prec);
    PCSetType(prec, PCNONE);
  }

  std::array<size_t, 1> idcs_out = {0};

  // Technically only need to setup matrices once for linear problem, but this
  // test is written as if this was a non-linear problem
  for (size_t iter = 0; iter < nsteps; ++iter)
  {
    for (size_t stage = 0; stage < sys.tsys.num_stages(); ++stage)
    {
      // zero-out buffers
      dg::util::fill(sctx.schur, 0);
      dg::util::fill(glob_rhs, 0);

      for (size_t elem = 0; elem < nelems; ++elem)
      {
        // zero-out buffers
        dg::util::fill(A_mat, 0);
        dg::util::fill(B_mat, 0);
        dg::util::fill(f_vec, 0);
        dg::util::fill(C_left, 0);
        dg::util::fill(C_right, 0);

        // internal vars for the current element
        auto q =
          dg::make_view_sequence(qs[sys.tsys.stage_buffer(stage + 1)].subarray(
                                   elem, dg::all{}, dg::all{}),
            sigma.subarray(elem, dg::all{}, dg::all{}));
        // internal vars for the current element, including previous time (used
        // by source helper)
        auto qn =
          dg::make_view_sequence(qs[sys.tsys.stage_buffer(stage + 1)].subarray(
                                   elem, dg::all{}, dg::all{}),
            sigma.subarray(elem, dg::all{}, dg::all{}),
            qs[sys.tsys.stage_buffer(stage + 1)].subarray(
              elem, dg::all{}, dg::all{}));

        // internal flux
        discr.int_flux(q, f_vec, advect_matrix, sys);
        discr.int_flux_jac(q, A_mat, advect_matrix, sys);
        // source terms
        discr.source(qn, f_vec, sys, mass_matrix);
        discr.source_jac(qn, A_mat, sys, mass_matrix);

        // numerical flux
        // left face
        {
          std::array<size_t, 1> idcs_in = {0};
          if (elem == 0)
          {
            // boundary condition
            auto lambda =
              lmbda.subarray(std::make_tuple(nelems - 1, nelems), dg::all{});
            discr.num_flux_local(lambda, q,
              f_vec.subarray(dg::all{}, std::make_tuple(0, 1)), -1, idcs_out,
              idcs_in, lift_matrix[0], sys);
            discr.num_flux_local(lambda, q,
              f_vec.subarray(dg::all{}, std::make_tuple(1, 2)), -1, idcs_out,
              idcs_in, lift_matrix[0], aux_sys);
            // Jacobian
            // gamma isn't actually used; being passed as a dummy
            discr.num_flux_local_jac(lambda, q, A_mat,
              B_mat.subarray(
                dg::all{}, dg::all{}, dg::all{}, std::make_tuple(0, 1)),
              -1, idcs_out, idcs_in, lift_matrix[0], bc);
          }
          else
          {
            auto lambda =
              lmbda.subarray(std::make_tuple(elem - 1, elem), dg::all{});
            discr.num_flux_hdg(lambda, q,
              glob_rhs.subarray(std::make_tuple(elem - 1, elem), dg::all{}),
              f_vec.subarray(dg::all{}, std::make_tuple(0, 1)), -1, idcs_out,
              idcs_in, lift_matrix[0], sys);
            discr.num_flux_local(lambda, q,
              f_vec.subarray(dg::all{}, std::make_tuple(1, 2)), -1, idcs_out,
              idcs_in, lift_matrix[0], aux_sys);
            // Jacobian
            discr.num_flux_jac(lambda, q, A_mat,
              B_mat.subarray(
                dg::all{}, dg::all{}, dg::all{}, std::make_tuple(0, 1)),
              C_left,
              sctx.schur.subarray(
                1, elem - 1, dg::all{}, dg::all{}, dg::all{}, dg::all{}),
              -1, idcs_out, idcs_in, lift_matrix[0], sys);
          }
        }
        // right face
        {
          std::array<size_t, 1> idcs_in = {degree};
          if (elem == nelems - 1)
          {
            auto lambda =
              lmbda.subarray(std::make_tuple(nelems - 1, nelems), dg::all{});
            discr.num_flux_local(lambda, q,
              f_vec.subarray(dg::all{}, std::make_tuple(0, 1)), 1, idcs_out,
              idcs_in, lift_matrix[1], sys);
            discr.num_flux_local(lambda, q,
              f_vec.subarray(dg::all{}, std::make_tuple(1, 2)), 1, idcs_out,
              idcs_in, lift_matrix[1], aux_sys);
            // Jacobian
            // gamma isn't actually used; being passed as a dummy
            discr.num_flux_local_jac(lambda, q, A_mat,
              B_mat.subarray(
                dg::all{}, dg::all{}, dg::all{}, std::make_tuple(1, 2)),
              1, idcs_out, idcs_in, lift_matrix[1], bc);
          }
          else
          {
            auto lambda =
              lmbda.subarray(std::make_tuple(elem, elem + 1), dg::all{});
            discr.num_flux_hdg(lambda, q,
              glob_rhs.subarray(std::make_tuple(elem, elem + 1), dg::all{}),
              f_vec.subarray(dg::all{}, std::make_tuple(0, 1)), 1, idcs_out,
              idcs_in, lift_matrix[1], sys);
            discr.num_flux_local(lambda, q,
              f_vec.subarray(dg::all{}, std::make_tuple(1, 2)), 1, idcs_out,
              idcs_in, lift_matrix[1], aux_sys);
            // Jacobian
            discr.num_flux_jac(lambda, q, A_mat,
              B_mat.subarray(
                dg::all{}, dg::all{}, dg::all{}, std::make_tuple(1, 2)),
              C_right,
              sctx.schur.subarray(
                1, elem, dg::all{}, dg::all{}, dg::all{}, dg::all{}),
              1, idcs_out, idcs_in, lift_matrix[1], sys);
          }
        }

        // perform local linear solves
        Eigen::PartialPivLU<decltype(A_map)> lu(A_map);
        // solve for Xi
        Eigen::Map<Eigen::Matrix<double, 2 * (degree + 1), 1>> f_map(
          f_vec.data(), 2 * (degree + 1), 1);
        Eigen::Map<Eigen::Matrix<double, 2 * (degree + 1), 1>> xi_map(
          &xi_vecs(elem, 0, 0), 2 * (degree + 1), 1);
        xi_map = lu.solve(f_map);

        // solve for gamma
        // assumes at least 2 elements
        if (elem == 0)
        {
          // ignore left BC face
          Eigen::Map<Eigen::Matrix<double, 2 * (degree + 1), 1>> B_map(
            &B_mat(0, 0, 0, 1), 2 * (degree + 1), 1);
          Eigen::Map<Eigen::Matrix<double, 2 * (degree + 1), 1>> gamma_map(
            &gamma_mats(0, 0, 0, 1, elem), 2 * (degree + 1), 1);
          gamma_map = lu.solve(B_map);
          // right face coupling to itself
          {
            // update contribution to the Schur compliment
            sctx.schur(1, elem, 0, 0, 0, 0) -=
              C_right(0, 0, 0, 0) * gamma_mats(0, degree, 0, 1, elem) +
              C_right(0, 0, 0, 1) * gamma_mats(1, degree, 0, 1, elem);

            // update contribution to global RHS
            glob_rhs(elem, 0) -=
              C_right(0, 0, 0, 0) * xi_vecs(elem, degree, 0) +
              C_right(0, 0, 0, 1) * xi_vecs(elem, degree, 1);
          }
        }
        else if (elem == nelems - 1)
        {
          // ignore right BC face
          Eigen::Map<Eigen::Matrix<double, 2 * (degree + 1), 1>> B_map(
            &B_mat(0, 0, 0, 0), 2 * (degree + 1), 1);
          Eigen::Map<Eigen::Matrix<double, 2 * (degree + 1), 1>> gamma_map(
            &gamma_mats(0, 0, 0, 0, elem), 2 * (degree + 1), 1);
          gamma_map = lu.solve(B_map);
          // left face coupling with itself
          {
            // update contribution to the Schur compliment
            sctx.schur(1, elem - 1, 0, 0, 0, 0) -=
              C_left(0, 0, 0, 0) * gamma_mats(0, 0, 0, 0, elem) +
              C_left(0, 0, 0, 1) * gamma_mats(1, 0, 0, 0, elem);

            // update contribution to global RHS
            glob_rhs(elem - 1, 0) -= C_left(0, 0, 0, 0) * xi_vecs(elem, 0, 0) +
                                     C_left(0, 0, 0, 1) * xi_vecs(elem, 0, 1);
          }
        }
        else
        {
          // general case
          Eigen::Map<Eigen::Matrix<double, 2 * (degree + 1), 1 * 2>> B_map(
            B_mat.data(), 2 * (degree + 1), 1 * 2);
          Eigen::Map<Eigen::Matrix<double, 2 * (degree + 1), 1 * 2>> gamma_map(
            &gamma_mats(0, 0, 0, 0, elem), 2 * (degree + 1), 1 * 2);
          gamma_map = lu.solve(B_map);
          // left face coupling with itself
          {
            // update contribution to the Schur compliment
            sctx.schur(1, elem - 1, 0, 0, 0, 0) -=
              C_left(0, 0, 0, 0) * gamma_mats(0, 0, 0, 0, elem) +
              C_left(0, 0, 0, 1) * gamma_mats(1, 0, 0, 0, elem);
          }
          // left face coupling with right face
          {
            sctx.schur(2, elem - 1, 0, 0, 0, 0) -=
              C_left(0, 0, 0, 0) * gamma_mats(0, 0, 0, 1, elem) +
              C_left(0, 0, 0, 1) * gamma_mats(1, 0, 0, 1, elem);
          }
          // right face coupling with left face
          {
            sctx.schur(0, elem, 0, 0, 0, 0) -=
              C_right(0, 0, 0, 0) * gamma_mats(0, degree, 0, 0, elem) +
              C_right(0, 0, 0, 1) * gamma_mats(1, degree, 0, 0, elem);
          }
          // right face coupling with itself
          {
            sctx.schur(1, elem, 0, 0, 0, 0) -=
              C_right(0, 0, 0, 0) * gamma_mats(0, degree, 0, 1, elem) +
              C_right(0, 0, 0, 1) * gamma_mats(1, degree, 0, 1, elem);
          }

          // update contribution to global RHS
          glob_rhs(elem, 0) -= C_right(0, 0, 0, 0) * xi_vecs(elem, degree, 0) +
                               C_right(0, 0, 0, 1) * xi_vecs(elem, degree, 1);
          glob_rhs(elem - 1, 0) -= C_left(0, 0, 0, 0) * xi_vecs(elem, 0, 0) +
                                   C_left(0, 0, 0, 1) * xi_vecs(elem, 0, 1);
        }
      }
      // perform global solve
      // TODO: are these assembly calls required?
      VecAssemblyBegin(glob_vec);
      VecAssemblyEnd(glob_vec);
      KSPSolve(ksp, glob_vec, dlmbda_vec);
      // check for convergence
      {
        KSPConvergedReason reason;
        KSPGetConvergedReason(ksp, &reason);
        // if > 0, converged
        if (reason <= 0)
        {
          ADD_FAILURE() << "KSP didn't converge: "
                        << dg::petsc::to_string(reason);
        }
      }
      for (size_t i = 0; i < nelems - 1; ++i)
      {
        lmbda(i, 0) += dlmbda(i, 0);
      }

      for (size_t elem = 0; elem < nelems; ++elem)
      {
        // back-solve for local element solution
        // this is just a matrix multiply
        Eigen::Map<Eigen::Matrix<double, 2 * (degree + 1), 1>> xi_map(
          &xi_vecs(elem, 0, 0), 2 * (degree + 1), 1);
        // have to manually unpack, since q and sigma are in different arrays...
        if (elem == 0)
        {
          // ignore left BC
          Eigen::Map<Eigen::Matrix<double, 1, 1>> dlmbda_map(
            &dlmbda(elem, 0), 1, 1);
          Eigen::Map<Eigen::Matrix<double, 2 * (degree + 1), 1>> gamma_map(
            &gamma_mats(0, 0, 0, 1, elem), 2 * (degree + 1), 1);
          xi_map -= gamma_map * dlmbda_map;
        }
        else if (elem == nelems - 1)
        {
          // ignore right BC
          Eigen::Map<Eigen::Matrix<double, 1, 1>> dlmbda_map(
            &dlmbda(elem - 1, 0), 1, 1);
          Eigen::Map<Eigen::Matrix<double, 2 * (degree + 1), 1>> gamma_map(
            &gamma_mats(0, 0, 0, 0, elem), 2 * (degree + 1), 1);
          xi_map -= gamma_map * dlmbda_map;
        }
        else
        {
          // general case
          Eigen::Map<Eigen::Matrix<double, 2, 1>> dlmbda_map(
            &dlmbda(elem - 1, 0), 2, 1);
          Eigen::Map<Eigen::Matrix<double, 2 * (degree + 1), 1 * 2>> gamma_map(
            &gamma_mats(0, 0, 0, 0, elem), 2 * (degree + 1), 1 * 2);
          xi_map -= gamma_map * dlmbda_map;
        }
        // xi now contains du, manually unpack into solution vectors
        for (size_t i = 0; i < degree + 1; ++i)
        {
          qs[sys.tsys.stage_buffer(stage + 1)](elem, i, 0) +=
            xi_vecs(elem, i, 0);
          sigma(elem, i, 0) += xi_vecs(elem, i, 1);
        }
      }
      EXPECT_EQ(sys.tsys.advance_stage(qs), stage + 1 == sys.tsys.num_stages());
    }
    // swap to prepare for next timestep
    // only need to swap q, not sigma
    std::swap(qs[sys.tsys.stage_buffer<0>()].data(),
      qs[sys.tsys.stage_buffer<decltype(sys.tsys)::num_stages()>()].data());
  }
  // compute expected solution
  constexpr size_t int_degree = 12;

  dg::hslab<double, 3> expected_q(nelems, int_degree + 1, 1);
  dg::hslab<double, 3> expected_sigma(nelems, int_degree + 1, 1);
  dg::hslab<double, 2> expected_lmbda(nelems, 1);
  auto int_absc = quad.abscissas(int_degree + 1);

  sys.asol(dg::make_view_sequence(dg::view<double, 3>(expected_q),
             dg::view<double, 3>(expected_sigma)),
    expected_lmbda, int_absc, xmin, xmax, dt * nsteps);

  auto q_error = integrate_l2_error<degree, int_degree>(
    qs[sys.tsys.stage_buffer(0)], expected_q, nelems, dx);
  auto sigma_error =
    integrate_l2_error<degree, int_degree>(sigma, expected_sigma, nelems, dx);
  // just do a standard linear algebra L2 vector norm
  auto lmbda_error =
    sqrt(std::inner_product(lmbda.data(), lmbda.data() + lmbda.size(),
      expected_lmbda.data(), 0., [](double a, double b) { return a + b; },
      [](double a, double b) { return (a - b) * (a - b); }));
  return std::make_tuple(q_error, sigma_error, lmbda_error);
}

TEST(test_hdg_rect_1d, implicit_diffusion)
{
  // point sampling convergence testing
  constexpr size_t degree = 1;
  double dt = 1e-3;
  auto err_1 = implicit_diffusion_sim<degree>(8, dt);
  auto err_2 = implicit_diffusion_sim<degree>(16, dt);
  // q error
  EXPECT_NEAR(std::get<0>(err_1) / std::get<0>(err_2), 4, 0.2);
  // sigma error
  EXPECT_NEAR(std::get<1>(err_1) / std::get<1>(err_2), 4, 0.2);
  // TODO: what is the expected lambda error?
  // EXPECT_LT(std::get<2>(err_1), 1e-5);
  // EXPECT_LT(std::get<2>(err_2), 1e-5);
}

int main(int argc, char** argv)
{
  init_tutils(&argc, argv);
  dg::context ctx(0, nullptr);

  glob_context = &ctx;

  return run_tutils();
}
