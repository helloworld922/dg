#include "dg/ndg/rect/rect_2d.hpp"

#include "dg/utils/hslab.hpp"

#include "dg/utils/view_utils.hpp"

#include "tutils/tutils.hpp"

#include <Eigen/Sparse>
#include <Eigen/SparseLU>

#include <array>

struct advection_sys
{
  std::array<double, 2> a;

  template <class Q>
  auto int_flux(const Q& q)
  {
    dg::shslab<double, 2, 2> res(1, 2);
    res(0, 0) = a[0] * q[0];
    res(0, 1) = a[1] * q[0];
    return res;
  }

  template <class Q, class C>
  auto int_flux(const Q& q, const C& coords)
  {
    dg::shslab<double, 2, 2> res(1, 2);
    res(0, 0) = a[0] * q[0];
    res(0, 1) = a[1] * q[0];
    return res;
  }

  template <class F, class C>
  double asol(F&& fun, const C& coord, double t) const
  {
    std::array<double, 2> c2 = {coord[0] - a[0] * t, coord[1] - a[1] * t};
    return fun(c2);
  }

  template <class Ay, class Ax, class Q, class F>
  void asol(Q&& q, const Ax& absc_x, const Ay& absc_y, F&& fun, double xmin,
    double xmax, double ymin, double ymax, size_t nelems_x, size_t nelems_y,
    size_t degree_x, size_t degree_y, double t) const
  {
    auto dx = (xmax - xmin) / nelems_x;
    auto dy = (ymax - ymin) / nelems_y;
    std::array<double, 2> c = {0};
    for (size_t nx = 0; nx < nelems_x; ++nx)
    {
      auto x0 = dx * nx + xmin;
      for (size_t ny = 0; ny < nelems_y; ++ny)
      {
        auto y0 = dy * ny + ymin;
        for (size_t i_x = 0; i_x < degree_x + 1; ++i_x)
        {
          c[0] = x0 + 0.5 * dx * (absc_x[i_x] + 1);
          for (size_t i_y = 0; i_y < degree_y + 1; ++i_y)
          {
            c[1] = y0 + 0.5 * dy * (absc_y[i_y] + 1);
            q(nx, ny, i_x, i_y, 0) = asol(fun, c, t);
          }
        }
      }
    }
  }

  ///
  /// @brief Computes n.(Fin-F*) (strong form numerical flux)
  /// @param qin q inside the element
  /// @param qout q outside the element
  /// @param n outwards normal
  ///
  template <class QOUT, class QIN, class N>
  auto num_flux(QOUT&& qout, QIN&& qin, const N& n) const
  {
    std::array<double, 1> res = {
      0.5 * ((qin[0] + qout[0]) * (a[0] * n[0] + a[1] * n[1]) -
              fabs(a[0] * n[0] + a[1] * n[1]) * (qout[0] - qin[0]))};
    return res;
  }
};

struct non_conservation_system
{
  std::array<double, 2> n;

  template <class C>
  std::array<double, 2> a(const C& x) const
  {
    // rotate speed
    auto s = tanh(x[0] * n[0] + x[1] * n[1]);
    std::array<double, 2> res = {s * n[0], s * n[1]};
    return res;
  }

  template <class F, class C>
  double asol(F&& fun, const C& coord, double t) const
  {
    auto x = coord[0] * n[0] + coord[1] * n[1];
    return fun(asinh(sinh(x) * exp(-t)));
  }

  template <class Ay, class Ax, class Q, class F>
  void asol(Q&& q, const Ax& absc_x, const Ay& absc_y, F&& fun, double xmin,
    double xmax, double ymin, double ymax, size_t nelems_x, size_t nelems_y,
    size_t degree_x, size_t degree_y, double t) const
  {
    auto dx = (xmax - xmin) / nelems_x;
    auto dy = (ymax - ymin) / nelems_y;
    std::array<double, 2> c = {0};
    for (size_t nx = 0; nx < nelems_x; ++nx)
    {
      auto x0 = dx * nx + xmin;
      for (size_t ny = 0; ny < nelems_y; ++ny)
      {
        auto y0 = dy * ny + ymin;
        for (size_t i_x = 0; i_x < degree_x + 1; ++i_x)
        {
          c[0] = x0 + 0.5 * dx * (absc_x[i_x] + 1);
          for (size_t i_y = 0; i_y < degree_y + 1; ++i_y)
          {
            c[1] = y0 + 0.5 * dy * (absc_y[i_y] + 1);
            q(nx, ny, i_x, i_y, 0) = asol(fun, c, t);
          }
        }
      }
    }
  }

  template <class Qi, class Qj, class Ri, class Rj>
  auto int_nflux(const Qi& qi, const Qj& qj, const Ri& xi, const Rj& xj) const
  {
    auto s = a(xi);
    dg::shslab<double, 2, 2> res(1, 2);
    res(0, 0) = s[0] * qj[0];
    res(0, 1) = s[1] * qj[0];
    return res;
  }

  template <class QOut, class QIn, class N, class C>
  auto num_flux(
    const QOut& qout, const QIn& qin, const N& normal, const C& x) const
  {
    auto aout = a(x);
    auto aout_n = aout[0] * normal[0] + aout[1] * normal[1];
    double ao = 0;
    if (aout_n == 0)
    {
      // adjust x in direction of normal
      std::array<double, 2> xtmp = {x[0] + normal[0], x[1] + normal[1]};
      aout = a(xtmp);
      aout_n = aout[0] * normal[0] + aout[1] * normal[1];
    }
    ao = copysign(1., aout_n);
    // looks weird because even though we divided out a, it's still needs to be
    // accounted for
    std::array<double, 2> res = {
      0.5 * ((qout[0] + qin[0]) - ao * (qout[0] - qin[0])) * normal[0],
      0.5 * ((qout[0] + qin[0]) - ao * (qout[0] - qin[0])) * normal[1]};
    return res;
  }

  template <class Qi, class F, class Ri>
  auto num_nflux(const Qi& qi, const F& nflux, const Ri& xi) const
  {
    auto atmp = a(xi);
    std::array<double, 1> res = {atmp[0] * nflux[0] + atmp[1] * nflux[1]};
    return res;
  }
};

template <size_t base_x, size_t base_y, size_t int_x, size_t int_y, class Q,
  class E>
double integrate_l2_error(const Q& sol, const E& expected, size_t nelems_x,
  size_t nelems_y, double dx, double dy)
{
  static dg::spatial::elements::lagrange<double, 1, 1> flat_element;
  static dg::spatial::elements::lagrange<double, base_x, base_y> base_element;
  static dg::spatial::elements::lagrange<double, int_x, int_y> exp_element;
  static dg::spatial::elements::lagrange<double, 12, 12> int_element;

  double res = 0;

  std::array<double, 8> buffer = {0, 0, 0, dy, dx, 0, dx, dy};
  dg::view<double, 3> coords(buffer.data(), 2, 2, 2);

  for (size_t comp = 0; comp < sol.extent(4); ++comp)
  {
    double tmp = 0;
    for (size_t elem_x = 0; elem_x < nelems_x; ++elem_x)
    {
      for (size_t elem_y = 0; elem_y < nelems_y; ++elem_y)
      {
        tmp += int_element.vol_integral(
          [dx, base = sol.subarray(elem_x, elem_y, dg::all{}, dg::all{}, comp),
            high =
              expected.subarray(elem_x, elem_y, dg::all{}, dg::all{}, comp),
            &coords](const std::array<double, 2>& xi) {
            // interpolate base solution to xi
            auto val = base_element.interp(xi, base);
            auto exp_ = exp_element.interp(xi, high);
            return (val - exp_) * (val - exp_) *
                   flat_element.jacobian(xi, coords).determinant();
          });
      }
    }
    res += tmp;
  }
  return sqrt(res);
}

TEST(test_ndg_rect_2d, constructor)
{
  {
    dg::ndg::rect<1, 1> discr;
    // TODO: test any cache information
  }
  {
    dg::ndg::rect<2, 3> discr;
  }
}

TEST(test_ndg_rect_2d, imass_matrix)
{
  {
    // 4 node canonical square
    dg::ndg::rect<1, 1> discr;
    std::array<double, 8> buffer = {-1, -1, -1, 1, 1, -1, 1, 1};
    dg::view<double, 3> coords(buffer.data(), 2, 2, 2);
    auto imass = discr.calc_imass_matrix(coords);
    decltype(imass) expected;
    expected << 4, -2, -2, 1, -2, 4, 1, -2, -2, 1, 4, -2, 1, -2, -2, 4;
    for (size_t i = 0; i < 2 * 2; ++i)
    {
      SCOPED_TRACE(i);
      for (size_t j = 0; j < 2 * 2; ++j)
      {
        SCOPED_TRACE(j);
        EXPECT_NEAR(imass(i, j), expected(i, j), 1e-13);
      }
    }
  }
}

TEST(test_ndg_rect_2d, advect_matrix)
{
  constexpr double tol = 1e-13;
  {
    // 4 node canonical square
    dg::ndg::rect<1, 1> discr;
    std::array<double, 8> buffer = {-1, -1, -1, 1, 1, -1, 1, 1};
    dg::view<double, 3> coords(buffer.data(), 2, 2, 2);
    auto imass = discr.calc_imass_matrix(coords);
    EXPECT_EQ(
      imass.innerSize(), (discr.degree_x() + 1) * (discr.degree_y() + 1));
    EXPECT_EQ(
      imass.outerSize(), (discr.degree_x() + 1) * (discr.degree_y() + 1));

    dg::hslab<double, 3> advect(2,
      (discr.degree_x() + 1) * (discr.degree_y() + 1),
      (discr.degree_x() + 1) * (discr.degree_y() + 1));
    discr.calc_advect_matrix(advect, imass, coords);

    std::array<double, 2 * (discr.degree_x() + 1) * (discr.degree_y() + 1) *
                         (discr.degree_x() + 1) * (discr.degree_y() + 1)>
      expected = {-1.5, 0, -1.5, 0, 0, -1.5, 0, -1.5, 1.5, 0, 1.5, 0, 0, 1.5, 0,
        1.5, -1.5, -1.5, 0, 0, 1.5, 1.5, 0, 0, 0, 0, -1.5, -1.5, 0, 0, 1.5,
        1.5};
    for (size_t i = 0; i < 2; ++i)
    {
      SCOPED_TRACE(i);
      for (size_t row = 0; row < advect.extent(1); ++row)
      {
        SCOPED_TRACE(row);
        for (size_t col = 0; col < advect.extent(2); ++col)
        {
          SCOPED_TRACE(col);
          EXPECT_NEAR(
            advect(i, row, col), expected[advect.index(i, row, col)], tol);
        }
      }
    }
  }
}

TEST(test_ndg_rect_2d, lift_matrix)
{
  {
    // 4 node canonical square
    dg::ndg::rect<1, 1> discr;
    std::array<double, 8> buffer = {-1, -1, -1, 1, 1, -1, 1, 1};
    dg::view<double, 3> coords(buffer.data(), 2, 2, 2);
    auto imass = discr.calc_imass_matrix(coords);
    dg::hslab<double, 2, dg::layout_left> lift(4, 8);
    discr.calc_lift_matrix(lift, imass, coords);

    // pretty sure this is correct
    std::array<double, 4 * 8> expected = {2, 0, -1, 0, 2, 0, -1, 0, 0, 2, 0, -1,
      -1, 0, 2, 0, -1, 0, 2, 0, 0, 2, 0, -1, 0, -1, 0, 2, 0, -1, 0, 2};
    for (size_t i = 0; i < 4; ++i)
    {
      SCOPED_TRACE(i);
      for (size_t j = 0; j < 8; ++j)
      {
        SCOPED_TRACE(j);
        EXPECT_NEAR(lift(i, j), expected[i * 8 + j], 1e-13);
      }
    }
  }
}

TEST(test_ndg_rect_2d, int_flux)
{
  {
    // piecewise linear
    constexpr size_t degree_x = 1;
    constexpr size_t degree_y = 1;
    dg::ndg::rect<degree_x, degree_y> discr;
    std::array<double, 8> buffer = {0, 0, 0, 1, 1, 0, 1, 1};
    dg::view<double, 3> coords(buffer.data(), 2, 2, 2);
    auto imass = discr.calc_imass_matrix(coords);
    dg::hslab<double, 3, dg::layout_left> advect(
      2, (degree_x + 1) * (degree_y + 1), (degree_x + 1) * (degree_y + 1));
    discr.calc_advect_matrix(advect, imass, coords);

    // test without calculating coordinate
    dg::hslab<double, 3> q(degree_x + 1, degree_y + 1, 1);
    dg::hslab<double, 3> r(degree_x + 1, degree_y + 1, 1);
    advection_sys sys;
    sys.a[0] = 1;
    sys.a[1] = 0;
    // horizontal variation
    q(0, 0, 0) = 1;
    q(1, 0, 0) = 0;
    q(0, 1, 0) = 1;
    q(1, 1, 0) = 0;
    dg::util::fill(r, 0);
    discr.int_flux(q, r, advect, sys, coords);
    {
      std::array<double, 4> expected = {-3., -3., 3., 3.};
      for (size_t i_x = 0; i_x < 2; ++i_x)
      {
        SCOPED_TRACE(i_x);
        for (size_t i_y = 0; i_y < 2; ++i_y)
        {
          SCOPED_TRACE(i_y);
          EXPECT_NEAR(r(i_x, i_y, 0), expected[r.index(i_x, i_y, 0)], 1e-13);
        }
      }
    }
    // test with calculating coordinate
    // vertical variation
    q(0, 0, 0) = 1;
    q(1, 0, 0) = 1;
    q(0, 1, 0) = 0;
    q(1, 1, 0) = 0;
    dg::util::fill(r, 0);
    sys.a[0] = 0;
    sys.a[1] = 1;
    discr.int_flux(q, r, advect, sys, coords);
    {
      std::array<double, 4> expected = {-3., 3., -3., 3.};
      for (size_t i_x = 0; i_x < 2; ++i_x)
      {
        SCOPED_TRACE(i_x);
        for (size_t i_y = 0; i_y < 2; ++i_y)
        {
          SCOPED_TRACE(i_y);
          EXPECT_NEAR(r(i_x, i_y, 0), expected[r.index(i_x, i_y, 0)], 1e-13);
        }
      }
    }
  }
}

TEST(test_ndg_rect_2d, num_flux)
{
  {
    // piecewise linear
    constexpr size_t degree_x = 1, degree_y = 1;
    dg::ndg::rect<degree_y, degree_x> discr{};
    std::array<double, 8> buffer = {0, 0, 0, 1, 1, 0, 1, 1};
    dg::view<double, 3> coords(buffer.data(), 2, 2, 2);
    auto imass = discr.calc_imass_matrix(coords);
    dg::hslab<double, 2, dg::layout_left> lift(
      (degree_x + 1) * (degree_y + 1), 2 * (degree_x + 1) + 2 * (degree_y + 1));
    discr.calc_lift_matrix(lift, imass, coords);
    dg::hslab<double, 3> qin(degree_x + 1, degree_y + 1, 1);
    dg::hslab<double, 3> qout(degree_x + 1, degree_y + 1, 1);
    for (size_t i = 0; i < degree_x + 1; ++i)
    {
      for (size_t j = 0; j < degree_y + 1; ++j)
      {
        qin(i, j, 0) = i * (degree_y + 1) + j;
        qout(i, j, 0) = i * (degree_y + 1) + j;
      }
    }
    dg::hslab<double, 3> r(degree_x + 1, degree_y + 1, 1);
    
    advection_sys sys;
    sys.a[0] = 1;
    sys.a[1] = 1;
    // left face
    dg::util::fill(r, 0);
    {
      std::array<double, 2> normal = {-1, 0};
      std::array<size_t, 2> idcs_out = {0, 1};
      std::array<size_t, 2> idcs_in = {0, 1};
      discr.num_flux(qout.subarray(degree_x, dg::all{}, dg::all{}),
        qin.subarray(0, dg::all{}, dg::all{}), r, normal, idcs_out, idcs_in,
        lift.subarray(dg::all{}, std::make_tuple(degree_y + 1)), sys);
      std::array<double, 4> expected = {8, 12, -4, -6};
      for (size_t i_x = 0; i_x < 2; ++i_x)
      {
        SCOPED_TRACE(i_x);
        for (size_t i_y = 0; i_y < 2; ++i_y)
        {
          SCOPED_TRACE(i_y);
          EXPECT_NEAR(r(i_x, i_y, 0), expected[r.index(i_x, i_y, 0)], 1e-13);
        }
      }
    }
    // right face
    dg::util::fill(r, 0);
    {
      std::array<double, 2> normal = {1, 0};
      std::array<size_t, 2> idcs_out = {0, 1};
      std::array<size_t, 2> idcs_in = {0, 1};
      discr.num_flux(qout.subarray(0, dg::all{}, dg::all{}),
        qin.subarray(degree_x, dg::all{}, dg::all{}), r, normal, idcs_out,
        idcs_in,
        lift.subarray(
          dg::all{}, std::make_tuple(degree_y + 1, 2 * (degree_y + 1))),
        sys);
      std::array<double, 4> expected = {4, 6, -8, -12};
      for (size_t i_x = 0; i_x < 2; ++i_x)
      {
        SCOPED_TRACE(i_x);
        for (size_t i_y = 0; i_y < 2; ++i_y)
        {
          SCOPED_TRACE(i_y);
          EXPECT_NEAR(r(i_x, i_y, 0), expected[r.index(i_x, i_y, 0)], 1e-13);
        }
      }
    }
    // bottom face
    dg::util::fill(r, 0);
    {
      std::array<double, 2> normal = {0, -1};
      std::array<size_t, 2> idcs_out = {0, 1};
      std::array<size_t, 2> idcs_in = {0, 1};
      discr.num_flux(qout.subarray(dg::all{}, degree_y, dg::all{}),
        qin.subarray(dg::all{}, 0, dg::all{}), r, normal, idcs_out, idcs_in,
        lift.subarray(dg::all{}, std::make_tuple(2 * (degree_y + 1),
                                   2 * (degree_y + 1) + degree_x + 1)),
        sys);
      std::array<double, 4> expected = {4, -2, 12, -6};
      for (size_t i_x = 0; i_x < 2; ++i_x)
      {
        SCOPED_TRACE(i_x);
        for (size_t i_y = 0; i_y < 2; ++i_y)
        {
          SCOPED_TRACE(i_y);
          EXPECT_NEAR(r(i_x, i_y, 0), expected[r.index(i_x, i_y, 0)], 1e-13);
        }
      }
    }
    // top face
    dg::util::fill(r, 0);
    {
      std::array<double, 2> normal = {0, 1};
      std::array<size_t, 2> idcs_out = {0, 1};
      std::array<size_t, 2> idcs_in = {0, 1};
      discr.num_flux(qout.subarray(dg::all{}, 0, dg::all{}),
        qin.subarray(dg::all{}, degree_y, dg::all{}), r, normal, idcs_out,
        idcs_in,
        lift.subarray(
          dg::all{}, std::make_tuple(2 * (degree_y + 1) + degree_x + 1,
                       2 * (degree_y + 1) + 2 * (degree_x + 1))),
        sys);
      std::array<double, 4> expected = {2, -4, 6, -12};
      for (size_t i_x = 0; i_x < 2; ++i_x)
      {
        SCOPED_TRACE(i_x);
        for (size_t i_y = 0; i_y < 2; ++i_y)
        {
          SCOPED_TRACE(i_y);
          EXPECT_NEAR(r(i_x, i_y, 0), expected[r.index(i_x, i_y, 0)], 1e-13);
        }
      }
    }
  }
}

template <size_t degree_x, size_t degree_y>
double advection_sim(size_t nelems_x, size_t nelems_y, double dt)
{
  constexpr size_t nnodes = (degree_x + 1) * (degree_y + 1);
  dg::ndg::rect<degree_x, degree_y> discr{};

  constexpr double xmin = 0;
  constexpr double xmax = 1;
  constexpr double ymin = 0;
  constexpr double ymax = 1;
  double dx = (xmax - xmin) / nelems_x;
  double dy = (ymax - ymin) / nelems_y;
  std::array<double, 8> buffer = {0, 0, 0, dy, dx, 0, dx, dy};
  dg::view<double, 3> coords(buffer.data(), 2, 2, 2);

  std::array<size_t, degree_y + 1> vert_idcs;
  for (size_t i = 0; i < degree_y + 1; ++i)
  {
    vert_idcs[i] = i;
  }
  
  std::array<size_t, degree_x + 1> horiz_idcs;
  for (size_t i = 0; i < degree_x + 1; ++i)
  {
    horiz_idcs[i] = i;
  }

  constexpr size_t nsteps = 1;

  advection_sys sys;
  sys.a[0] = 1;
  sys.a[1] = 0.5;

  // storage space for solution vectors
  dg::hslab<double, 5> q0(nelems_x, nelems_y, degree_x + 1, degree_y + 1, 1);
  dg::hslab<double, 5> q1(nelems_x, nelems_y, degree_x + 1, degree_y + 1, 1);

  // ndg matrices
  auto imass_matrix = discr.calc_imass_matrix(coords);

  dg::hslab<double, 3, dg::layout_left> advect_matrix(2, nnodes, nnodes);
  discr.calc_advect_matrix(advect_matrix, imass_matrix, coords);

  dg::hslab<double, 2, dg::layout_left> lift_matrix(
    nnodes, 2 * (degree_x + degree_y + 2));
  discr.calc_lift_matrix(lift_matrix, imass_matrix, coords);

  // initial conditions
  constexpr static dg::lobatto<double> quad{};
  auto absc_x = quad.abscissas(degree_x + 1);
  auto absc_y = quad.abscissas(degree_y + 1);
  auto init_cond = [](auto& c) {
    return sin(6.283185307179586 * c[0]) + sin(6.283185307179586 * c[1]);
  };
  sys.asol(q0, absc_x, absc_y, init_cond, xmin, xmax, ymin, ymax, nelems_x,
    nelems_y, degree_x, degree_y, 0);

  // time advance with forward Euler
  for (size_t step = 0; step < nsteps; ++step)
  {
    // zero out RHS variable first
    dg::util::fill(q1, 0);

    for (size_t elem_x = 0; elem_x < nelems_x; ++elem_x)
    {
      for (size_t elem_y = 0; elem_y < nelems_y; ++elem_y)
      {
        auto q = q0.subarray(elem_x, elem_y, dg::all{}, dg::all{}, dg::all{});
        auto r = q1.subarray(elem_x, elem_y, dg::all{}, dg::all{}, dg::all{});
        // internal flux
        discr.int_flux(q, r, advect_matrix, sys);
        // numerical flux
        // left face
        {
          std::array<double, 2> normal = {-1, 0};
          if (elem_x == 0)
          {
            // periodic BC's
            auto qout =
              q0.subarray(nelems_x - 1, elem_y, degree_x, dg::all{}, dg::all{});
            discr.num_flux(qout, q.subarray(0, dg::all{}, dg::all{}), r, normal,
              vert_idcs, vert_idcs,
              lift_matrix.subarray(dg::all{}, std::make_tuple(degree_y + 1)),
              sys);
          }
          else
          {
            auto qout =
              q0.subarray(elem_x - 1, elem_y, degree_x, dg::all{}, dg::all{});
            discr.num_flux(qout, q.subarray(0, dg::all{}, dg::all{}), r, normal,
              vert_idcs, vert_idcs,
              lift_matrix.subarray(dg::all{}, std::make_tuple(degree_y + 1)),
              sys);
          }
        }
        // right face
        {
          std::array<double, 2> normal = {1, 0};
          if (elem_x == nelems_x - 1)
          {
            // periodic BC's
            auto qout = q0.subarray(0, elem_y, 0, dg::all{}, dg::all{});
            discr.num_flux(qout, q.subarray(degree_x, dg::all{}, dg::all{}), r,
              normal, vert_idcs, vert_idcs,
              lift_matrix.subarray(
                dg::all{}, std::make_tuple(degree_y + 1, 2 * (degree_y + 1))),
              sys);
          }
          else
          {
            auto qout =
              q0.subarray(elem_x + 1, elem_y, 0, dg::all{}, dg::all{});
            discr.num_flux(qout, q.subarray(degree_x, dg::all{}, dg::all{}), r,
              normal, vert_idcs, vert_idcs,
              lift_matrix.subarray(
                dg::all{}, std::make_tuple(degree_y + 1, 2 * (degree_y + 1))),
              sys);
          }
        }
        // bottom face
        {
          std::array<double, 2> normal = {0, -1};
          if (elem_y == 0)
          {
            // periodic BC's
            auto qout =
              q0.subarray(elem_x, nelems_y - 1, dg::all{}, degree_y, dg::all{});
            discr.num_flux(qout, q.subarray(dg::all{}, 0, dg::all{}), r, normal,
              horiz_idcs, horiz_idcs,
              lift_matrix.subarray(
                dg::all{}, std::make_tuple(2 * (degree_y + 1),
                             2 * (degree_y + 1) + degree_x + 1)),
              sys);
          }
          else
          {
            auto qout =
              q0.subarray(elem_x, elem_y - 1, dg::all{}, degree_y, dg::all{});
            discr.num_flux(qout, q.subarray(dg::all{}, 0, dg::all{}), r, normal,
              horiz_idcs, horiz_idcs,
              lift_matrix.subarray(
                dg::all{}, std::make_tuple(2 * (degree_y + 1),
                             2 * (degree_y + 1) + degree_x + 1)),
              sys);
          }
        }
        // top face
        {
          std::array<double, 2> normal = {0, 1};
          if (elem_y == nelems_y - 1)
          {
            // periodic BC's
            auto qout = q0.subarray(elem_x, 0, dg::all{}, 0, dg::all{});
            discr.num_flux(qout, q.subarray(dg::all{}, degree_y, dg::all{}), r,
              normal, horiz_idcs, horiz_idcs,
              lift_matrix.subarray(
                dg::all{}, std::make_tuple(2 * (degree_y + 1) + degree_x + 1,
                             2 * (degree_y + 1) + 2 * (degree_x + 1))),
              sys);
          }
          else
          {
            auto qout =
              q0.subarray(elem_x, elem_y + 1, dg::all{}, 0, dg::all{});
            discr.num_flux(qout, q.subarray(dg::all{}, degree_y, dg::all{}), r,
              normal, horiz_idcs, horiz_idcs,
              lift_matrix.subarray(
                dg::all{}, std::make_tuple(2 * (degree_y + 1) + degree_x + 1,
                             2 * (degree_y + 1) + 2 * (degree_x + 1))),
              sys);
          }
        }
        // time advance
        for (size_t i_x = 0; i_x < (degree_x + 1); ++i_x)
        {
          for (size_t i_y = 0; i_y < (degree_y + 1); ++i_y)
          {
            r(i_x, i_y, 0) = q(i_x, i_y, 0) + dt * r(i_x, i_y, 0);
          }
        }
      }
    }
    std::swap(q0.data(), q1.data());
  }
  // expected solution
  constexpr size_t int_degree = 12;
  dg::hslab<double, 5> expected(
    nelems_x, nelems_y, (int_degree + 1), (int_degree + 1), 1);

  auto absc_int = quad.abscissas(int_degree + 1);
  sys.asol(expected, absc_int, absc_int, init_cond, xmin, xmax, ymin, ymax,
    nelems_x, nelems_y, int_degree, int_degree, dt * nsteps);

  return integrate_l2_error<degree_x, degree_y, int_degree, int_degree>(
    q0, expected, nelems_x, nelems_y, dx, dy);
}

TEST(test_ndg_rect_2d, advection)
{
  // point sampling convergence test
  constexpr size_t degree_x = 2, degree_y = 2;
  double dt = .25 / (100 * (2 * degree_x + 1));
  auto err1 = advection_sim<degree_x, degree_y>(5, 6, dt);
  auto err2 = advection_sim<degree_x, degree_y>(10, 12, dt);
  EXPECT_NEAR(err1 / err2, 8, 0.2);
}

TEST(test_ndg_rect_2d, int_nflux)
{
  {
    // piecewise linear
    constexpr size_t degree_x = 1;
    constexpr size_t degree_y = 1;
    dg::ndg::rect<degree_x, degree_y> discr;
    std::array<double, 8> buffer = {0.5, 0.5, 0.5, 1, 1, 0.5, 1, 1};
    dg::view<double, 3> coords(buffer.data(), 2, 2, 2);
    auto imass = discr.calc_imass_matrix(coords);
    dg::hslab<double, 3, dg::layout_left> advect(
      2, (degree_x + 1) * (degree_y + 1), (degree_x + 1) * (degree_y + 1));
    discr.calc_advect_matrix(advect, imass, coords);

    // test without calculating coordinate
    dg::hslab<double, 3> q(degree_x + 1, degree_y + 1, 1);
    dg::hslab<double, 3> r(degree_x + 1, degree_y + 1, 1);
    non_conservation_system sys;
    sys.n[0] = 1;
    sys.n[1] = 0;
    // horizontal variation
    q(0, 0, 0) = 1;
    q(1, 0, 0) = 0;
    q(0, 1, 0) = 1;
    q(1, 1, 0) = 0;
    dg::util::fill(r, 0);
    discr.int_nflux(q, r, advect, sys, coords);
    {
      std::array<double, 4> expected = {-2.7727029435600583,
        -2.7727029435600583, 4.5695649357345891, 4.5695649357345891};
      for (size_t i_x = 0; i_x < 2; ++i_x)
      {
        SCOPED_TRACE(i_x);
        for (size_t i_y = 0; i_y < 2; ++i_y)
        {
          SCOPED_TRACE(i_y);
          EXPECT_NEAR(r(i_x, i_y, 0), expected[r.index(i_x, i_y, 0)], 1e-13);
        }
      }
    }
    // vertical variation
    q(0, 0, 0) = 1;
    q(1, 0, 0) = 1;
    q(0, 1, 0) = 0;
    q(1, 1, 0) = 0;
    dg::util::fill(r, 0);
    sys.n[0] = 0;
    sys.n[1] = 1;
    discr.int_nflux(q, r, advect, sys, coords);
    {
      std::array<double, 4> expected = {-2.7727029435600583, 4.5695649357345891,
        -2.7727029435600583, 4.5695649357345891};
      for (size_t i_x = 0; i_x < 2; ++i_x)
      {
        SCOPED_TRACE(i_x);
        for (size_t i_y = 0; i_y < 2; ++i_y)
        {
          SCOPED_TRACE(i_y);
          EXPECT_NEAR(r(i_x, i_y, 0), expected[r.index(i_x, i_y, 0)], 1e-13);
        }
      }
    }
  }
}

TEST(test_ndg_rect_2d, num_nflux)
{
  {
    // piecewise linear
    constexpr size_t degree_x = 1, degree_y = 1;
    dg::ndg::rect<degree_y, degree_x> discr{};
    double dx = 0.5, dy = 0.5;
    std::array<double, 8> buffer = {
      0.5, 0.5, 0.5, 0.5 + dy, 0.5 + dx, 0.5, 0.5 + dx, 0.5 + dy};
    dg::view<double, 3> coords(buffer.data(), 2, 2, 2);
    auto imass = discr.calc_imass_matrix(coords);
    dg::hslab<double, 2, dg::layout_left> lift(4, 8);
    discr.calc_lift_matrix(lift, imass, coords);
    dg::hslab<double, 3> q(degree_x + 1, degree_y + 1, 1);
    dg::hslab<double, 3> r(degree_x + 1, degree_y + 1, 1);
    for (size_t i_x = 0; i_x < degree_x + 1; ++i_x)
    {
      for (size_t i_y = 0; i_y < degree_y + 1; ++i_y)
      {
        q(i_x, i_y, 0) = 1;
      }
    }
    std::array<double, 1> qin = {1};
    std::array<double, 1> qout = {0};

    std::array<double, 2> x;
    std::array<double, 2> n;
    non_conservation_system sys;
    sys.n[0] = sqrt(2) * .5;
    sys.n[1] = sqrt(2) * .5;

    constexpr static dg::lobatto<double> quad{};
    auto absc_x = quad.abscissas(degree_x + 1);
    auto absc_y = quad.abscissas(degree_y + 1);

    // left face
    x[0] = coords(0, 0, 0);
    n[0] = -1;
    n[1] = 0;
    dg::util::fill(r, 0);
    for (size_t i = 0; i < 2; ++i)
    {
      x[1] = coords(0, 0, 1) + 0.5 * dy * absc_y[i];
      auto fluxes = sys.num_flux(qout, qin, n, x);
      discr.num_nflux(q, r, lift.subarray(dg::all{}, i), fluxes, sys, coords);
    }
    {
      std::array<double, 4> expected = {0, 0, 0, 0};
      for (size_t i_x = 0; i_x < 2; ++i_x)
      {
        SCOPED_TRACE(i_x);
        for (size_t i_y = 0; i_y < 2; ++i_y)
        {
          SCOPED_TRACE(i_y);
          EXPECT_NEAR(r(i_x, i_y, 0), expected[r.index(i_x, i_y, 0)], 1e-13);
        }
      }
    }

    // right face
    x[0] = coords(0, 0, 0) + dx;
    n[0] = 1;
    n[1] = 0;
    dg::util::fill(r, 0);
    for (size_t i = 0; i < 2; ++i)
    {
      x[1] = coords(0, 0, 1) + 0.5 * dy * absc_y[i];
      auto fluxes = sys.num_flux(qout, qin, n, x);
      discr.num_nflux(
        q, r, lift.subarray(dg::all{}, 2 + i), fluxes, sys, coords);
    }
    {
      std::array<double, 4> expected = {1.7221143431610955, 2.2229072552546216,
        -4.4458145105092433, -5.0254676392435194};
      for (size_t i_x = 0; i_x < 2; ++i_x)
      {
        SCOPED_TRACE(i_x);
        for (size_t i_y = 0; i_y < 2; ++i_y)
        {
          SCOPED_TRACE(i_y);
          EXPECT_NEAR(r(i_x, i_y, 0), expected[r.index(i_x, i_y, 0)], 1e-13);
        }
      }
    }
    // bottom face
    x[1] = coords(0, 0, 1);
    n[0] = 0;
    n[1] = -1;
    dg::util::fill(r, 0);
    for (size_t i = 0; i < 2; ++i)
    {
      x[0] = coords(0, 0, 0) + 0.5 * dy * absc_y[i];
      auto fluxes = sys.num_flux(qout, qin, n, x);
      discr.num_nflux(
        q, r, lift.subarray(dg::all{}, 2 * 2 + i), fluxes, sys, coords);
    }
    {
      std::array<double, 4> expected = {0, 0, 0, 0};
      for (size_t i_x = 0; i_x < 2; ++i_x)
      {
        SCOPED_TRACE(i_x);
        for (size_t i_y = 0; i_y < 2; ++i_y)
        {
          SCOPED_TRACE(i_y);
          EXPECT_NEAR(r(i_x, i_y, 0), expected[r.index(i_x, i_y, 0)], 1e-13);
        }
      }
    }
    // top face
    x[1] = coords(0, 0, 1) + dy;
    n[0] = 0;
    n[1] = 1;
    dg::util::fill(r, 0);
    for (size_t i = 0; i < 2; ++i)
    {
      x[0] = coords(0, 0, 0) + 0.5 * dy * absc_y[i];
      auto fluxes = sys.num_flux(qout, qin, n, x);
      discr.num_nflux(
        q, r, lift.subarray(dg::all{}, 2 + 2 * 2 + i), fluxes, sys, coords);
    }
    {
      std::array<double, 4> expected = {1.7221143431610955, -4.4458145105092433,
        2.2229072552546216, -5.0254676392435194};
      for (size_t i_x = 0; i_x < 2; ++i_x)
      {
        SCOPED_TRACE(i_x);
        for (size_t i_y = 0; i_y < 2; ++i_y)
        {
          SCOPED_TRACE(i_y);
          EXPECT_NEAR(r(i_x, i_y, 0), expected[r.index(i_x, i_y, 0)], 1e-13);
        }
      }
    }
  }
}

#if 0

template <size_t degree_x, size_t degree_y>
double nadvection_sim(size_t nelems_x, size_t nelems_y, double dt)
{
  constexpr size_t nnodes = (degree_x + 1) * (degree_y + 1);
  dg::ndg::rect<degree_x, degree_y> discr{};

  constexpr double xmin = -1, xmax = 1, ymin = -1, ymax = 1;

  double dx = (xmax - xmin) / nelems_x, dy = (ymax - ymin) / nelems_y;
  std::array<double, 8> buffer = {0, 0, 0, dy, dx, 0, dx, dy};
  dg::view<double, 3> coords(buffer.data(), 2, 2, 2);

  constexpr size_t nsteps = 1;

  non_conservation_system sys;
  sys.n[0] = sqrt(2) * 0.5;
  sys.n[1] = sqrt(2) * 0.5;

  // storage space for solution vectors
  dg::hslab<double, 5> q0(nelems_x, nelems_y, degree_x + 1, degree_y + 1, 1);
  dg::hslab<double, 5> q1(nelems_x, nelems_y, degree_x + 1, degree_y + 1, 1);

  // ndg matrices
  auto imass_matrix = discr.calc_imass_matrix(coords);

  dg::hslab<double, 3, dg::layout_left> advect_matrix(2, nnodes, nnodes);
  discr.calc_advect_matrix(advect_matrix, imass_matrix, coords);

  dg::hslab<double, 2, dg::layout_left> lift_matrix(
    nnodes, 2 * (degree_x + degree_y + 2));
  discr.calc_lift_matrix(lift_matrix, imass_matrix, coords);

  // construct face node maps
  face_node_maps map(degree_x + 1, degree_y + 1);

  // initial conditions
  constexpr static dg::lobatto<double> quad{};
  auto absc_x = quad.abscissas(degree_x + 1);
  auto absc_y = quad.abscissas(degree_y + 1);

  auto init_cond = [](double x) { return cos(3.141592653589793 * x); };

  sys.asol(q0, absc_x, absc_y, init_cond, xmin, xmax, ymin, ymax, nelems_x,
    nelems_y, degree_x, degree_y, 0);

  // time advance with forward Euler
  for (size_t step = 0; step < nsteps; ++step)
  {
    auto t = dt * step;
    // zero out RHS variable first
    dg::util::fill(q1, 0);

    for (size_t elem_x = 0; elem_x < nelems_x; ++elem_x)
    {
      // update coords x
      coords(0, 0, 0) = elem_x * dx + xmin;
      coords(0, 1, 0) = elem_x * dx + xmin;
      coords(1, 0, 0) = (elem_x + 1) * dx + xmin;
      coords(1, 1, 0) = (elem_x + 1) * dx + xmin;
      for (size_t elem_y = 0; elem_y < nelems_y; ++elem_y)
      {
        // update coords y
        coords(0, 0, 1) = elem_y * dy + ymin;
        coords(0, 1, 1) = (elem_y + 1) * dy + ymin;
        coords(1, 0, 1) = elem_y * dy + ymin;
        coords(1, 1, 1) = (elem_y + 1) * dy + ymin;
        auto q = q0.subarray(elem_x, elem_y, dg::all{}, dg::all{}, dg::all{});
        auto r = q1.subarray(elem_x, elem_y, dg::all{}, dg::all{}, dg::all{});
        // internal flux
        discr.int_nflux(q, r, advect_matrix, sys, coords);
        // numerical flux
        {
          for (size_t i = 0; i < lift_matrix.extent(1); ++i)
          {
            // face node coordinate
            std::array<double, 2> x = {
              coords(0, 0, 0) + (1 + map.normals[i][0]) * 0.5 * dx,
              coords(0, 0, 1) + (1 + map.normals[i][1]) * 0.5 * dy};
            bool is_bc = false;
            auto out_x = elem_x + map.outside_element_offset[i][0];
            auto out_y = elem_y + map.outside_element_offset[i][1];
            if (map.outside_element_offset[i][0] == -1)
            {
              // left face
              x[1] = coords(0, 0, 1) + dy * (absc_y[i] + 1) * 0.5;
              if (elem_x == 0)
              {
                is_bc = true;
              }
            }
            else if (map.outside_element_offset[i][0] == 1)
            {
              // right face
              x[1] =
                coords(0, 0, 1) + dy * (absc_y[i - (degree_y + 1)] + 1) * 0.5;
              if (elem_x == nelems_x - 1)
              {
                is_bc = true;
              }
            }
            else if (map.outside_element_offset[i][1] == -1)
            {
              // bottom face
              x[0] = coords(0, 0, 0) +
                     dx * (absc_x[i - 2 * (degree_y + 1)] + 1) * 0.5;
              if (elem_y == 0)
              {
                is_bc = true;
              }
            }
            else if (map.outside_element_offset[i][1] == 1)
            {
              // top face
              x[0] = coords(0, 0, 0) +
                     dx *
                       (absc_x[i - 2 * (degree_y + 1) - (degree_x + 1)] + 1) *
                       0.5;
              if (elem_y == nelems_y - 1)
              {
                is_bc = true;
              }
            }

            auto qin = q.subarray(map.inside_node_index[i][0],
              map.inside_node_index[i][1], dg::all{});

            if (is_bc)
            {
              std::array<double, 1> qout = {sys.asol(init_cond, x, t)};
              auto num_flux = sys.num_flux(qout, qin, map.normals[i], x);
              discr.num_nflux(q, r, lift_matrix.subarray(dg::all{}, i),
                num_flux, sys, coords);
            }
            else
            {
              auto qout =
                q0.subarray(out_x, out_y, map.outside_node_index[i][0],
                  map.outside_node_index[i][1], dg::all{});
              auto num_flux = sys.num_flux(qout, qin, map.normals[i], x);
              discr.num_nflux(q, r, lift_matrix.subarray(dg::all{}, i),
                num_flux, sys, coords);
            }
          }
        }
        // time advance
        for (size_t i_x = 0; i_x < (degree_x + 1); ++i_x)
        {
          for (size_t i_y = 0; i_y < (degree_y + 1); ++i_y)
          {
            r(i_x, i_y, 0) = q(i_x, i_y, 0) + dt * r(i_x, i_y, 0);
          }
        }
      }
    }
    std::swap(q0.data(), q1.data());
  }
  // expected solution
  constexpr size_t int_degree = 12;
  dg::hslab<double, 5> expected(
    nelems_x, nelems_y, (int_degree + 1), (int_degree + 1), 1);

  auto absc_int = quad.abscissas(int_degree + 1);
  sys.asol(expected, absc_int, absc_int, init_cond, xmin, xmax, ymin, ymax,
    nelems_x, nelems_y, int_degree, int_degree, dt * nsteps);

  return integrate_l2_error<degree_x, degree_y, int_degree, int_degree>(
    q0, expected, nelems_x, nelems_y, dx, dy);
}

TEST(test_ndg_rect_2d, nadvection)
{
  // point sampling convergence test
  constexpr size_t degree_x = 2, degree_y = 2;
  double dt = .25 / (100 * (2 * degree_x + 1));
  auto err1 = nadvection_sim<degree_x, degree_y>(5, 6, dt);
  auto err2 = nadvection_sim<degree_x, degree_y>(10, 12, dt);
  EXPECT_NEAR(err1 / err2, 8, 0.2);
}
#endif

int main(int argc, char** argv)
{
  init_tutils(&argc, argv);
  return run_tutils();
}
