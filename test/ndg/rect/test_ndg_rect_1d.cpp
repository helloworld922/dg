#include "dg/ndg/rect/rect_1d.hpp"

#include "dg/utils/hslab.hpp"

#include "dg/utils/view_utils.hpp"

#include "tutils/tutils.hpp"

#include <Eigen/Sparse>
#include <Eigen/SparseLU>

#include <array>
#include <cmath>

///
/// @brief Implements d_t q + d_x(a q) = 0
/// Used for testing traditional DG
///
struct advection_system
{
  double a;

  template <class Q>
  auto int_flux(const Q& q) const
  {
    std::array<double, 1> res = {a * q[0]};
    return res;
  }

  template <class Q, class R>
  auto int_flux(const Q& q, const R& pos) const
  {
    std::array<double, 1> res = {a * q[0]};
    return res;
  }

  ///
  /// @brief Computes n.F* (weak form numerical flux)
  /// @param qin q inside the element
  /// @param qout q outside the element
  /// @param normal outward pointing normal
  ///
  template <class QOUT, class QIN>
  auto num_flux(const QOUT& qout, const QIN& qin, double normal) const
  {
    std::array<double, 1> res = {
      0.5 * (a * (qout[0] + qin[0]) * normal - fabs(a) * (qout[0] - qin[0]))};
    return res;
  }
};

///
/// @brief Implements d_t q + tanh(x) d_x q = 0
///
struct non_conservation_system
{
  double a(double x) const
  {
    return tanh(x);
  }

  /// @param fun initial condition
  template <class F>
  double asol(F&& fun, double x, double t) const
  {
    return fun(asinh(sinh(x) * exp(-t)));
  }

  ///
  /// @brief analytical solution
  /// @param fun initial condition
  ///
  template <class A, class Q, class F>
  void asol(
    Q&& q, const A& absc, F&& fun, double xmin, double xmax, double t) const
  {
    auto nelems = q.shape()[0];
    auto dx = (xmax - xmin) / nelems;
    for (size_t elem = 0; elem < nelems; ++elem)
    {
      auto x0 = dx * elem;
      for (size_t i = 0; i < q.shape()[1]; ++i)
      {
        double x = x0 + 0.5 * dx * (absc[i] + 1);
        // slightly tweak the edge nodes towards the center
        if (i == q.shape()[1] - 1)
        {
          x = nextafter(x, x0);
        }
        else if (i == 0)
        {
          x = nextafter(x, dx * (elem + 1));
        }
        q(elem, i, 0) = asol(fun, x, t);
      }
    }
  }

  template <class Qi, class Qj, class Ri, class Rj>
  auto int_nflux(const Qi& qi, const Qj& qj, const Ri& xi, const Rj& xj) const
  {
    std::array<double, 1> res = {a(xi) * qj[0]};
    return res;
  }

  ///
  /// @brief Computes n.F* (weak form numerical flux). This is only for the
  /// stuff inside the surface integral.
  /// @param qin q inside the element
  /// @param qout q outside the element
  /// @param normal outward pointing normal
  /// @param x face node position
  ///
  template <class QOUT, class QIN>
  auto num_flux(const QOUT& qout, const QIN& qin, double normal, double x) const
  {
    auto aout = a(x);
    if (aout == 0)
    {
      aout = copysign(1., a(x + normal));
    }
    else
    {
      aout = copysign(1., aout);
    }
    // looks weird because even though we divided out a, it's still needs to be
    // accounted for
    std::array<double, 1> res = {
      0.5 * ((qout[0] + qin[0]) * normal - aout * (qout[0] - qin[0]))};
    return res;
  }

  ///
  /// @brief the total combined non-conservation form surface flux tanh(x)*oint
  /// q n dx
  ///
  template <class Q, class F>
  auto num_nflux(const Q& qi, const F& nflux, double xi) const
  {
    std::array<double, 1> res = {a(xi) * nflux[0]};
    return res;
  }
};

///
/// @brief Helper for integrating the L-2 error norm
/// @param sol computed solution, in base_degree
/// @param expected expected solution, evaluated in int_degree
///
template <size_t base_degree, size_t int_degree, class Q, class E>
static double integrate_l2_error(
  const Q& sol, const E& expected, size_t nelems, double dx)
{
  static dg::spatial::elements::lagrange<double, base_degree> base_element;
  static dg::spatial::elements::lagrange<double, int_degree> int_element;

  double res = 0;

  for (size_t comp = 0; comp < sol.extent(2); ++comp)
  {
    double tmp = 0;
    for (size_t elem = 0; elem < nelems; ++elem)
    {
      tmp += int_element.vol_integral(
        [dx, base = sol.subarray(elem, dg::all{}, comp),
          high = expected.subarray(elem, dg::all{}, comp)](
          const std::array<double, 1>& xi) {
          // interpolate base solution to xi
          auto val = base_element.interp(xi, base);
          auto exp_ = int_element.interp(xi, high);
          return (val - exp_) * (val - exp_) * 0.5 * dx;
        });
    }
    res += tmp;
  }
  return sqrt(res);
}

TEST(test_ndg_rect_1d, mass_matrix)
{
  {
    dg::ndg::rect<1> ndg{};
    Eigen::Matrix<dg::real256, 2, 2> mass_matrix;
    ndg.calc_mass_matrix(mass_matrix);
    Eigen::Matrix<dg::real256, 2, 2> expected_mass_matrix;
    expected_mass_matrix << 2, -1, -1, 2;

    expected_mass_matrix = expected_mass_matrix.inverse().eval();

    for (size_t i = 0; i < 2; ++i)
    {
      SCOPED_TRACE(i);
      for (size_t j = 0; j < 2; ++j)
      {
        SCOPED_TRACE(j);
        EXPECT_NEAR(mass_matrix(i, j), expected_mass_matrix(i, j), 1e-13);
      }
    }
  }
  {
    dg::ndg::rect<2> ndg{};
    Eigen::Matrix<dg::real256, 3, 3> mass_matrix;
    ndg.calc_mass_matrix(mass_matrix);
    Eigen::Matrix<dg::real256, 3, 3> expected_mass_matrix;
    expected_mass_matrix << 4.5, -.75, 1.5, -.75, 1.125, -.75, 1.5, -.75, 4.5;

    expected_mass_matrix = expected_mass_matrix.inverse().eval();
    for (size_t i = 0; i < 3; ++i)
    {
      SCOPED_TRACE(i);
      for (size_t j = 0; j < 3; ++j)
      {
        SCOPED_TRACE(j);
        EXPECT_NEAR(mass_matrix(i, j), expected_mass_matrix(i, j), 1e-13);
      }
    }
  }
  {
    dg::ndg::rect<3> ndg{};
    Eigen::Matrix<dg::real256, 4, 4> mass_matrix;
    ndg.calc_mass_matrix(mass_matrix);
    Eigen::Matrix<dg::real256, 4, 4> expected_mass_matrix;
    expected_mass_matrix << 8, -0.89442719099991588, 0.89442719099991588, -2,
      -0.89442719099991588, 1.6, -.4, 0.89442719099991588, 0.89442719099991588,
      -.4, 1.6, -0.89442719099991588, -2, 0.89442719099991588,
      -0.89442719099991588, 8;

    expected_mass_matrix = expected_mass_matrix.inverse().eval();
    for (size_t i = 0; i < 4; ++i)
    {
      SCOPED_TRACE(i);
      for (size_t j = 0; j < 4; ++j)
      {
        SCOPED_TRACE(j);
        EXPECT_NEAR(mass_matrix(i, j), expected_mass_matrix(i, j), 1e-13);
      }
    }
  }
}

TEST(test_ndg_rect_1d, cadvect_matrix)
{
  {
    dg::ndg::rect<1> ndg{};
    Eigen::Matrix<dg::real256, 2, 2> cadvect_matrix;
    ndg.calc_advect_matrix(cadvect_matrix);
    Eigen::Matrix<dg::real256, 2, 2> expected_cadvect_matrix;
    expected_cadvect_matrix << -0.5, -0.5, 0.5, 0.5;
    for (size_t i = 0; i < 2; ++i)
    {
      SCOPED_TRACE(i);
      for (size_t j = 0; j < 2; ++j)
      {
        SCOPED_TRACE(j);
        EXPECT_NEAR(cadvect_matrix(i, j), expected_cadvect_matrix(i, j), 1e-13);
      }
    }
  }
  {
    dg::ndg::rect<2> ndg{};
    Eigen::Matrix<dg::real256, 3, 3> cadvect_matrix;
    ndg.calc_advect_matrix(cadvect_matrix);

    Eigen::Matrix<dg::real256, 3, 3> expected_cadvect_matrix;
    expected_cadvect_matrix << -0.5, -2. / 3., 1. / 6., 2. / 3., 0, -2. / 3.,
      -1. / 6., 2. / 3., 0.5;
    for (size_t i = 0; i < 3; ++i)
    {
      SCOPED_TRACE(i);
      for (size_t j = 0; j < 3; ++j)
      {
        SCOPED_TRACE(j);
        EXPECT_NEAR(cadvect_matrix(i, j), expected_cadvect_matrix(i, j), 1e-13);
      }
    }
  }
}

TEST(test_ndg_rect_1d, advect_matrix)
{
  dg::ndg::rect<3> ndg{};
  Eigen::Matrix<double, 4, 4> mass_matrix;
  ndg.calc_mass_matrix(mass_matrix);
  Eigen::Matrix<double, 4, 4> advect_matrix = mass_matrix.inverse();
  Eigen::Matrix<double, 4, 4> cadvect_matrix;

  ndg.calc_advect_matrix(cadvect_matrix);

  advect_matrix *= cadvect_matrix;

  Eigen::Matrix<double, 4, 4> expected_advect_matrix;

  expected_advect_matrix << -5, -4.045084971874737, 1.545084971874737, -2.5,
    1.703444185374863, 0, -1.118033988749895, 1.203444185374863,
    -1.203444185374863, 1.118033988749895, 0, -1.703444185374863, 2.5,
    -1.545084971874737, 4.045084971874737, 5;

  for (size_t i = 0; i < 4; ++i)
  {
    SCOPED_TRACE(i);
    for (size_t j = 0; j < 4; ++j)
    {
      SCOPED_TRACE(j);
      EXPECT_NEAR(advect_matrix(i, j), expected_advect_matrix(i, j), 1e-13);
    }
  }
}

TEST(test_ndg_rect_1d, lift_matrix)
{
  constexpr size_t Degree = 3;
  dg::ndg::rect<Degree> ndg{};
  constexpr double tol = 1e-16;

  {
    // just the lift matrix
    std::array<dg::shslab<double, 1, 2>, 2> lift_matrix = {
      dg::shslab<double, 1, 2>(1, 1), dg::shslab<double, 1, 2>(1, 1)};

    ndg.calc_lift_matrix(lift_matrix, 1, 2);
    EXPECT_NEAR(lift_matrix[0](0, 0), 1, tol);
    EXPECT_NEAR(lift_matrix[1](0, 0), 2, tol);
  }
  {
    // multiplied by a mass matrix
    Eigen::Matrix<double, Degree + 1, Degree + 1> imass_matrix;
    ndg.calc_mass_matrix(imass_matrix);
    imass_matrix = imass_matrix.inverse().eval();
    std::array<dg::shslab<double, Degree + 1, 2>, 2> lift_matrix = {
      dg::shslab<double, Degree + 1, 2>(1, Degree + 1),
      dg::shslab<double, Degree + 1, 2>(1, Degree + 1)};

    ndg.calc_lift_matrix(lift_matrix, imass_matrix, 1, 2);
    for (size_t i = 0; i < Degree + 1; ++i)
    {
      // left wall
      EXPECT_NEAR(lift_matrix[0](0, i), imass_matrix(i, 0), tol);
      // right wall
      EXPECT_NEAR(lift_matrix[1](0, i), 2 * imass_matrix(i, Degree), tol);
    }
  }
}

TEST(test_ndg_rect_1d, int_flux)
{
  advection_system sys;
  sys.a = 1;

  {
    // piecewise linear
    dg::ndg::rect<1> discr{};
    constexpr double jac = 0.5;
    // calculate advection matrix
    Eigen::Matrix<double, 2, 2> mass_matrix;
    discr.calc_mass_matrix(mass_matrix, jac);
    Eigen::Matrix<double, 2, 2> amatrix = mass_matrix.inverse();
    Eigen::Matrix<double, 2, 2> cadvect_matrix;
    discr.calc_advect_matrix(cadvect_matrix);

    amatrix *= cadvect_matrix;

    // Eigen's default layout is left
    dg::view<double, 2, dg::layout_left> advect_matrix(amatrix.data(), 2, 2);

    // test without calculating x coordinate
    std::vector<double> q_buf(2);
    std::vector<double> r_buf(2);
    dg::view<double, 2> q(q_buf.data(), 2, 1);
    dg::view<double, 2> r(r_buf.data(), 2, 1);
    q(0, 0) = 1;
    q(1, 0) = 0;
    discr.int_flux(q, r, advect_matrix, sys);

    {
      std::vector<double> expected_r = {-3., 3.};
      EXPECT_RANGE_NEAR(r_buf, expected_r, 1e-14);
    }
    // test with calculating x coordinate
    q(0, 0) = 1;
    q(1, 0) = 0;
    r(0, 0) = 0;
    r(1, 0) = 0;
    discr.int_flux(q, r, advect_matrix, sys, 0., 1.);

    {
      std::vector<double> expected_r = {-3., 3.};
      EXPECT_RANGE_NEAR(r_buf, expected_r, 1e-14);
    }
  }
  {
    // piecewise parabolic
    dg::ndg::rect<2> discr{};
    constexpr double jac = 0.5;
    // calculate advection matrix
    Eigen::Matrix<double, 3, 3> mass_matrix;
    discr.calc_mass_matrix(mass_matrix, jac);
    Eigen::Matrix<double, 3, 3> amatrix = mass_matrix.inverse();
    Eigen::Matrix<double, 3, 3> cadvect_matrix;
    discr.calc_advect_matrix(cadvect_matrix);

    amatrix *= cadvect_matrix;

    // Eigen's default layout is left
    dg::view<double, 2, dg::layout_left> advect_matrix(amatrix.data(), 3, 3);

    // test without calculating x coordinate
    std::vector<double> q_buf(3);
    std::vector<double> r_buf(3);
    dg::view<double, 2> q(q_buf.data(), 3, 1);
    dg::view<double, 2> r(r_buf.data(), 3, 1);
    q(0, 0) = 1;
    q(1, 0) = 0.5;
    q(2, 0) = 0;
    discr.int_flux(q, r, advect_matrix, sys);

    {
      std::vector<double> expected_r = {-8, 2.5, -2};
      EXPECT_RANGE_NEAR(r_buf, expected_r, 1e-14);
    }
  }
}

TEST(test_ndg_rect_1d, num_flux)
{
  {
    // piecewise linear
    constexpr size_t degree = 1;
    dg::ndg::rect<degree> discr{};
    constexpr double jac = 0.5;

    // calculate lift matrix
    Eigen::Matrix<double, degree + 1, degree + 1> imass_matrix;
    discr.calc_mass_matrix(imass_matrix, jac);
    imass_matrix = imass_matrix.inverse().eval();
    std::array<dg::shslab<double, degree + 1, 2>, 2> lift_matrix = {
      dg::shslab<double, degree + 1, 2>(1, degree + 1),
      dg::shslab<double, degree + 1, 2>(1, degree + 1)};
    discr.calc_lift_matrix(lift_matrix, imass_matrix, 1, 1);

    advection_system sys;
    sys.a = 1;
    std::vector<double> r_buf(degree + 1);
    dg::shslab<double, degree + 1, 2> qin(degree + 1, 1);
    dg::shslab<double, degree + 1, 2> qout(degree + 1, 1);
    qin(0, 0) = 1;
    qin(degree, 0) = 1;
    qout(0, 0) = 0;
    qout(degree, 0) = 0;
    // right face
    std::array<size_t, 1> idcs_in = {degree};
    std::array<size_t, 1> idcs_out = {0};
    double normal = 1;
    dg::view<double, 2> r(r_buf.data(), 2, 1);
    discr.num_flux(
      qout, qin, r, normal, idcs_out, idcs_in, lift_matrix[1], sys);

    {
      std::vector<double> expected_r = {2, -4};
      EXPECT_RANGE_NEAR(r_buf, expected_r, 1e-14);
    }

    for (size_t i = 0; i < degree + 1; ++i)
    {
      r(i, 0) = 0;
    }
    // left face
    idcs_in[0] = degree;
    idcs_out[0] = 0;
    normal = -1;
    discr.num_flux(
      qout, qin, r, normal, idcs_out, idcs_in, lift_matrix[0], sys);

    {
      std::vector<double> expected_r = {0, 0};
      EXPECT_RANGE_NEAR(r_buf, expected_r, 1e-14);
    }
  }

  {
    // piecewise parabolic
    constexpr size_t degree = 2;
    dg::ndg::rect<degree> discr{};
    constexpr double jac = 0.5;

    // calculate lift matrix
    Eigen::Matrix<double, degree + 1, degree + 1> imass_matrix;
    discr.calc_mass_matrix(imass_matrix, jac);
    imass_matrix = imass_matrix.inverse().eval();
    std::array<dg::shslab<double, degree + 1, 2>, 2> lift_matrix = {
      dg::shslab<double, degree + 1, 2>(1, degree + 1),
      dg::shslab<double, degree + 1, 2>(1, degree + 1)};
    discr.calc_lift_matrix(lift_matrix, imass_matrix, 1, 1);

    advection_system sys;
    sys.a = 1;
    std::vector<double> r_buf(degree + 1);
    dg::shslab<double, degree + 1, 2> qin(degree + 1, 1);
    dg::shslab<double, degree + 1, 2> qout(degree + 1, 1);
    qin(0, 0) = 1;
    qin(degree, 0) = 1;
    qout(0, 0) = 0;
    qout(degree, 0) = 0;
    // right face
    std::array<size_t, 1> idcs_in = {degree};
    std::array<size_t, 1> idcs_out = {0};
    double normal = 1;
    dg::view<double, 2> r(r_buf.data(), 2, 1);
    discr.num_flux(
      qout, qin, r, normal, idcs_out, idcs_in, lift_matrix[1], sys);

    {
      std::vector<double> expected_r = {-3, 1.5, -9};
      EXPECT_RANGE_NEAR(r_buf, expected_r, 1e-14);
    }

    for (size_t i = 0; i < degree + 1; ++i)
    {
      r(i, 0) = 0;
    }
    // left face
    idcs_in[0] = 0;
    idcs_out[0] = degree;
    normal = -1;
    discr.num_flux(
      qout, qin, r, normal, idcs_out, idcs_in, lift_matrix[0], sys);

    {
      std::vector<double> expected_r = {0, 0, 0};
      EXPECT_RANGE_NEAR(r_buf, expected_r, 1e-14);
    }
  }
}

template <size_t degree>
double advection_sim(size_t nelems, double dt)
{
  dg::ndg::rect<degree> discr{};
  // wave speed
  constexpr double a = 1;
  constexpr size_t nsteps = 1;
  double dx = 1. / nelems;
  double jac = 0.5 * dx;
  // ndg matrices

  // calculate advection and lift matrices
  Eigen::Matrix<double, degree + 1, degree + 1> mass_matrix;
  discr.calc_mass_matrix(mass_matrix, jac);
  Eigen::Matrix<double, degree + 1, degree + 1> amatrix = mass_matrix.inverse();
  std::array<dg::shslab<double, degree + 1, 2>, 2> lift_matrix = {
    dg::shslab<double, degree + 1, 2>(1, degree + 1),
    dg::shslab<double, degree + 1, 2>(1, degree + 1)};
  discr.calc_lift_matrix(lift_matrix, amatrix, 1, 1);

  Eigen::Matrix<double, degree + 1, degree + 1> cadvect_matrix;
  discr.calc_advect_matrix(cadvect_matrix);

  amatrix *= cadvect_matrix;

  // Eigen's default layout is left
  dg::view<double, 2, dg::layout_left> advect_matrix(
    amatrix.data(), degree + 1, degree + 1);

  dg::hslab<double, 3> q0(nelems, degree + 1, 1);
  dg::hslab<double, 3> q1(nelems, degree + 1, 1);

  constexpr static dg::lobatto<double> quad{};
  auto absc = quad.abscissas(degree + 1);

  // initial conditions:
  // q = sin(2 pi x)
  for (size_t elem = 0; elem < nelems; ++elem)
  {
    double x0 = dx * elem;
    for (size_t i = 0; i < degree + 1; ++i)
    {
      double x = x0 + 0.5 * dx * (absc[i] + 1);
      q0(elem, i, 0) = sin(6.283185307179586 * x);
    }
  }

  advection_system sys;
  sys.a = a;
  std::array<size_t, 1> left_face = {0};
  std::array<size_t, 1> right_face = {degree};

  // time advance with forward Euler
  for (size_t step = 0; step < nsteps; ++step)
  {
    // zero out RHS variable first
    dg::util::fill(q1, 0);

    for (size_t elem = 0; elem < nelems; ++elem)
    {
      auto r = q1.subarray(elem, dg::all{}, dg::all{});
      // internal flux
      discr.int_flux(q0.subarray(elem, dg::all{}, dg::all{}), r, advect_matrix,
        sys, elem * dx, (elem + 1) * dx);
      // left face
      if (elem == 0)
      {
        // periodic BC
        discr.num_flux(q0.subarray(nelems - 1, dg::all{}, dg::all{}),
          q0.subarray(elem, dg::all{}, dg::all{}), r, -1, right_face, left_face,
          lift_matrix[0], sys);
      }
      else
      {
        discr.num_flux(q0.subarray(elem - 1, dg::all{}, dg::all{}),
          q0.subarray(elem, dg::all{}, dg::all{}), r, -1, right_face, left_face,
          lift_matrix[0], sys);
      }
      // right face
      if (elem == nelems - 1)
      {
        // periodic BC
        discr.num_flux(q0.subarray(0, dg::all{}, dg::all{}),
          q0.subarray(elem, dg::all{}, dg::all{}), r, 1, left_face, right_face,
          lift_matrix[1], sys);
      }
      else
      {
        discr.num_flux(q0.subarray(elem + 1, dg::all{}, dg::all{}),
          q0.subarray(elem, dg::all{}, dg::all{}), r, 1, left_face, right_face,
          lift_matrix[1], sys);
      }
      // time advance: forward Euler
      for (size_t i = 0; i < degree + 1; ++i)
      {
        r(i, 0) = q0(elem, i, 0) + dt * r(i, 0);
      }
    }
    std::swap(q0.data(), q1.data());
  }

  // compute expected solution
  constexpr size_t int_degree = 12;

  dg::hslab<double, 3> expected(nelems, int_degree + 1, 1);
  auto int_absc = quad.abscissas(int_degree + 1);

  for (size_t elem = 0; elem < nelems; ++elem)
  {
    double x0 = dx * elem;
    for (size_t i = 0; i < int_degree + 1; ++i)
    {
      double x = x0 + 0.5 * dx * (int_absc[i] + 1);
      expected(elem, i, 0) = sin(6.283185307179586 * (x - a * dt * nsteps));
    }
  }
  return integrate_l2_error<degree, int_degree>(q0, expected, nelems, dx);
}

TEST(test_ndg_rect_1d, advection)
{
  // point sampling convergence testing
  constexpr size_t degree = 2;
  double dt = .5 / (100 * (2 * degree + 1));
  auto err_1 = advection_sim<degree>(10, dt);
  auto err_2 = advection_sim<degree>(20, dt);
  EXPECT_NEAR(err_1 / err_2, 8, 0.2);
}

TEST(test_ndg_rect_1d, int_nflux)
{
  non_conservation_system sys;
  {
    // piecewise linear
    dg::ndg::rect<1> discr{};

    constexpr double jac = 0.5;
    // calculate advection matrix
    Eigen::Matrix<double, 2, 2> mass_matrix;
    discr.calc_mass_matrix(mass_matrix, jac);
    Eigen::Matrix<double, 2, 2> amatrix = mass_matrix.inverse();
    Eigen::Matrix<double, 2, 2> cadvect_matrix;
    discr.calc_advect_matrix(cadvect_matrix);

    amatrix *= cadvect_matrix;

    // Eigen's default layout is left
    dg::view<double, 2, dg::layout_left> advect_matrix(amatrix.data(), 2, 2);

    std::vector<double> q_buf(2);
    std::vector<double> r_buf(2);
    dg::view<double, 2> q(q_buf.data(), 2, 1);
    dg::view<double, 2> r(r_buf.data(), 2, 1);
    q(0, 0) = 0;
    q(1, 0) = 1;
    discr.int_nflux(q, r, advect_matrix, sys, 0., 1.);
    {
      std::vector<double> expected_r = {0., 2.2847824678672946};
      EXPECT_RANGE_NEAR(r_buf, expected_r, 1e-14);
    }
  }
  {
    // piecewise cubic
    dg::ndg::rect<3> discr{};

    constexpr double jac = 0.5;
    // calculate advection matrix
    Eigen::Matrix<double, 4, 4> mass_matrix;
    discr.calc_mass_matrix(mass_matrix, jac);
    Eigen::Matrix<double, 4, 4> amatrix = mass_matrix.inverse();
    Eigen::Matrix<double, 4, 4> cadvect_matrix;
    discr.calc_advect_matrix(cadvect_matrix);

    amatrix *= cadvect_matrix;

    // Eigen's default layout is left
    dg::view<double, 2, dg::layout_left> advect_matrix(amatrix.data(), 4, 4);

    std::vector<double> q_buf(4);
    std::vector<double> r_buf(4);
    dg::view<double, 2> q(q_buf.data(), 4, 1);
    dg::view<double, 2> r(r_buf.data(), 4, 1);
    q(0, 0) = 0;
    q(1, 0) = static_cast<double>(0.5 * (1 + discr.element.absc[0][1]));
    q(2, 0) = static_cast<double>(0.5 * (1 + discr.element.absc[0][2]));
    q(3, 0) = 1;
    discr.int_nflux(q, r, advect_matrix, sys, 0., 1.);
    {
      std::vector<double> expected_r = {
        0., 0.2126464526516782, -1.7266870249108639, 11.423912339336471};
      EXPECT_RANGE_NEAR(r_buf, expected_r, 1e-14);
    }
  }
}

TEST(test_ndg_rect_1d, num_nflux)
{
  non_conservation_system sys;
  {
    // piecewise cubic
    constexpr size_t degree = 3;
    constexpr double jac = 0.5;

    dg::ndg::rect<degree> discr{};

    // calculate lift matrix
    Eigen::Matrix<double, degree + 1, degree + 1> imass_matrix;
    discr.calc_mass_matrix(imass_matrix, jac);
    imass_matrix = imass_matrix.inverse().eval();
    std::array<dg::shslab<double, degree + 1, 2>, 2> lift_matrix = {
      dg::shslab<double, degree + 1, 2>(1, degree + 1),
      dg::shslab<double, degree + 1, 2>(1, degree + 1)};
    discr.calc_lift_matrix(lift_matrix, imass_matrix, 1, 1);

    dg::shslab<double, degree + 1, 2> qin(degree + 1, 1);
    dg::shslab<double, degree + 1, 2> qout(degree + 1, 1);

    {
      // element to the left of origin
      std::vector<double> r_buf(degree + 1);
      dg::view<double, 2> r(r_buf.data(), degree + 1, 1);

      // right face
      qin(0, 0) = -1;
      qin(1, 0) = static_cast<double>(0.5 * (1 + discr.element.absc[0][1])) - 1;
      qin(2, 0) = static_cast<double>(0.5 * (1 + discr.element.absc[0][2])) - 1;
      qin(3, 0) = 0;
      qout(0, 0) = 1;
      qout(degree, 0) = -2;
      std::array<size_t, 1> idcs_in = {degree};
      std::array<size_t, 1> idcs_out = {0};

      discr.num_nflux(
        qout, qin, r, 1, idcs_out, idcs_in, lift_matrix[1], sys, -1., 0.);
      {
        std::vector<double> expected_r = {0., 0., 0., 0.};
        EXPECT_RANGE_NEAR(r_buf, expected_r, 1e-14);
      }
      // left face
      idcs_in[0] = 0;
      idcs_out[0] = degree;

      discr.num_nflux(
        qout, qin, r, -1, idcs_out, idcs_in, lift_matrix[0], sys, -1., 0.);
      {
        std::vector<double> expected_r = {
          12.185506495292234, -1.1075485585730511, 0.48221008505820212, 0.};
        EXPECT_RANGE_NEAR(r_buf, expected_r, 1e-14);
      }
    }
    {
      // element to right of origin
      std::vector<double> r_buf(degree + 1);
      dg::view<double, 2> r(r_buf.data(), degree + 1, 1);

      qin(0, 0) = 0;
      qin(1, 0) = static_cast<double>(0.5 * (1 + discr.element.absc[0][1]));
      qin(2, 0) = static_cast<double>(0.5 * (1 + discr.element.absc[0][2]));
      qin(3, 0) = 1;
      qout(0, 0) = -3;
      qout(degree, 0) = 1;
      // left face
      std::array<size_t, 1> idcs_in = {0};
      std::array<size_t, 1> idcs_out = {degree};

      discr.num_nflux(
        qout, qin, r, -1, idcs_out, idcs_in, lift_matrix[0], sys, 0., 1.);
      {
        std::vector<double> expected_r = {0., 0., 0., 0.};
        EXPECT_RANGE_NEAR(r_buf, expected_r, 1e-14);
      }
      // right face
      idcs_in[0] = degree;
      idcs_out[0] = 0;

      discr.num_nflux(
        qout, qin, r, 1, idcs_out, idcs_in, lift_matrix[1], sys, 0., 1.);
      {
        std::vector<double> expected_r = {
          0., -0.48221008505820212, 1.1075485585730511, -12.185506495292234};
        EXPECT_RANGE_NEAR(r_buf, expected_r, 1e-14);
      }
    }
  }
}

template <size_t degree>
double nadvection_sim(size_t nelems, double dt)
{
  dg::ndg::rect<degree> discr{};
  constexpr size_t nsteps = 1;
  constexpr double xmin = -2;
  constexpr double xmax = 2;
  double dx = 4. / nelems;
  double jac = 0.5 * dx;

  non_conservation_system sys;

  // ndg matrices
  // calculate advection and lift matrices
  Eigen::Matrix<double, degree + 1, degree + 1> mass_matrix;
  discr.calc_mass_matrix(mass_matrix, jac);
  Eigen::Matrix<double, degree + 1, degree + 1> amatrix = mass_matrix.inverse();
  std::array<dg::shslab<double, degree + 1, 2>, 2> lift_matrix = {
    dg::shslab<double, degree + 1, 2>(1, degree + 1),
    dg::shslab<double, degree + 1, 2>(1, degree + 1)};
  discr.calc_lift_matrix(lift_matrix, amatrix, 1, 1);

  Eigen::Matrix<double, degree + 1, degree + 1> cadvect_matrix;
  discr.calc_advect_matrix(cadvect_matrix);

  amatrix *= cadvect_matrix;

  // Eigen's default layout is left
  dg::view<double, 2, dg::layout_left> advect_matrix(
    amatrix.data(), degree + 1, degree + 1);

  dg::hslab<double, 3> q0(nelems, degree + 1, 1);
  dg::hslab<double, 3> q1(nelems, degree + 1, 1);

  constexpr static dg::lobatto<double> quad{};
  auto absc = quad.abscissas(degree + 1);

  auto init_cond = [](double x) { return cos(3.141592653589793 * x); };

  // initial conditions
  sys.asol(q0, absc, init_cond, xmin, xmax, 0);

  dg::shslab<double, 1, 2> bc(1, 1);
  std::array<size_t, 1> left_face = {0};
  std::array<size_t, 1> right_face = {degree};

  // time advance with forward Euler
  for (size_t step = 0; step < nsteps; ++step)
  {
    // zero out RHS variable first
    dg::util::fill(q1, 0);
    auto t = dt * step;

    for (size_t elem = 0; elem < nelems; ++elem)
    {
      auto qelem = q0.subarray(elem, dg::all{}, dg::all{});
      auto r = q1.subarray(elem, dg::all{}, dg::all{});
      // internal flux
      discr.int_nflux(qelem, r, advect_matrix, sys, elem * dx, (elem + 1) * dx);
      // left face
      {
        auto x = nextafter(dx * elem, dx * (elem + 1));
        if (elem == 0)
        {
          // boundary conditions (assume infinite domain)
          bc(0, 0) = sys.asol(init_cond, x, t);
          // TODO: update using correct coordinates?
          discr.num_nflux(q0.subarray(elem - 1, dg::all{}, dg::all{}),
            q0.subarray(elem, dg::all{}, dg::all{}), r, -1, left_face,
            left_face, lift_matrix[0], sys, x, dx * (elem + 1));
#if 0
          num_flux = sys.num_flux(qout, qelem.subarray(0, dg::all{}), -1, x);
          discr.num_nflux(qelem, r, lift.subarray(dg::all{}, 0), num_flux, sys,
            dx * elem, dx * (elem + 1));
#endif
        }
        else
        {
          // TODO: correct x needed for num_flux?
          discr.num_nflux(q0.subarray(elem - 1, dg::all{}, dg::all{}),
            q0.subarray(elem, dg::all{}, dg::all{}), r, -1, right_face,
            left_face, lift_matrix[0], sys, x, dx * (elem + 1));
#if 0
          num_flux = sys.num_flux(q0.subarray(elem - 1, degree, dg::all{}),
            qelem.subarray(0, dg::all{}), -1, x);
          discr.num_nflux(qelem, r, lift.subarray(dg::all{}, 0), num_flux, sys,
            dx * elem, dx * (elem + 1));
#endif
        }
      }
      // right face
      {
        auto x = nextafter(dx * (elem + 1), dx * elem);
        if (elem == nelems - 1)
        {
          // boundary conditions (assume infinite domain)
          bc(0, 0) = sys.asol(init_cond, x, t);
          discr.num_nflux(bc, q0.subarray(elem, dg::all{}, dg::all{}), r, 1,
            left_face, right_face, lift_matrix[1], sys, dx * elem,
            dx * (elem + 1));
          // TODO: update using correct coordinates?
#if 0
          num_flux =
            sys.num_flux(qout, qelem.subarray(degree, dg::all{}), 1, x);
          discr.num_nflux(qelem, r, lift.subarray(dg::all{}, 1), num_flux, sys,
            dx * elem, dx * (elem + 1));
#endif
        }
        else
        {
          discr.num_nflux(q0.subarray(elem + 1, dg::all{}, dg::all{}),
            q0.subarray(elem, dg::all{}, dg::all{}), r, 1, left_face,
            right_face, lift_matrix[1], sys, dx * elem, dx * (elem + 1));
          // TODO: update using correct coordinates?
#if 0
          num_flux = sys.num_flux(q0.subarray(elem + 1, 0, dg::all{}),
            q0.subarray(elem, degree, dg::all{}), 1, x);
          discr.num_nflux(qelem, r, lift.subarray(dg::all{}, 1), num_flux, sys,
            dx * elem, dx * (elem + 1));
#endif
        }
      }
      // time advance: forward Euler
      for (size_t i = 0; i < degree + 1; ++i)
      {
        r(i, 0) = q0(elem, i, 0) + dt * r(i, 0);
      }
    }
    std::swap(q0.data(), q1.data());
  }

  // compute expected solution
  constexpr size_t int_degree = 12;

  dg::hslab<double, 3> expected(nelems, int_degree + 1, 1);
  auto int_absc = quad.abscissas(int_degree + 1);

  // expected results
  sys.asol(expected, int_absc, init_cond, xmin, xmax, dt * nsteps);

  return integrate_l2_error<degree, int_degree>(q0, expected, nelems, dx);
}

TEST(test_ndg_rect_1d, nadvection)
{
  // point sampling convergence testing
  constexpr size_t degree = 2;
  double dt = .5 / (100 * (2 * degree + 1));
  auto err_1 = nadvection_sim<degree>(10, dt);
  auto err_2 = nadvection_sim<degree>(21, dt);
  EXPECT_NEAR(err_1 / err_2, 9, 0.2);
}

int main(int argc, char** argv)
{
  init_tutils(&argc, argv);
  return run_tutils();
}
