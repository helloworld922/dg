#include "dg/eqsets/temporal/dirk.hpp"

#include "tutils/tutils.hpp"

#include <cmath>

template <class Dirk>
static double scalar_sim(size_t nsteps, double dt)
{
  // d_t q + k q = 0
  Dirk sys;
  constexpr double k = 1;
  sys.idt = 1 / dt;
  dg::view_array<dg::shslab<double, 1, 1>, Dirk::num_buffers()> qs;
  for (size_t i = 0; i < qs.size(); ++i)
  {
    qs[i] = dg::shslab<double, 1, 1>(1);
    EXPECT_EQ(qs[i].size(), 1);
    EXPECT_EQ(qs[i].shape()[0], 1);
    qs[i][0] = 1;
  }
  // residual
  dg::shslab<double, 1, 1> rhs(1);
  dg::shslab<double, 1, 2> jac(1, 1);
  for (size_t i = 0; i < nsteps; ++i)
  {
    for (size_t stage = 0; stage < sys.num_stages(); ++stage)
    {
      // clear buffers
      jac(0, 0) = 0;
      rhs[0] = 0;

      auto qstage =
        dg::make_view_array(qs[sys.stage_buffer(stage + 1)].as_view(),
          qs[sys.stage_buffer(stage)].as_view());
      {
        auto r = sys.source(qstage);
        rhs[0] -= k * qstage[0][0];
        rhs[0] += r[0];
      }
      sys.source_jac(qstage, 1, jac);
      jac(0, 0) += k;
      // solve
      qstage[0][0] += rhs[0] / jac(0, 0);
      EXPECT_EQ(sys.advance_stage(qs), stage == sys.num_stages() - 1);
    }
    std::swap(qs[sys.stage_buffer(0)], qs[sys.stage_buffer(sys.num_stages())]);
  }
  return fabs(qs[sys.stage_buffer(0)][0] - exp(-k * nsteps * dt));
}

template <class Dirk>
static double system_sim(size_t nsteps, double dt)
{
  Dirk sys;
  constexpr double k0 = 1;
  constexpr double k1 = .1;
  sys.idt = 1 / dt;
  dg::view_array<dg::shslab<double, 2, 1>, Dirk::num_buffers()> qs;
  for (size_t i = 0; i < qs.size(); ++i)
  {
    qs[i] = dg::shslab<double, 2, 1>(2);
    EXPECT_EQ(qs[i].size(), 2);
    EXPECT_EQ(qs[i].shape()[0], 2);
    dg::util::fill(qs[i], 1);
  }
  // residual
  dg::shslab<double, 2, 1> rhs(2);
  dg::shslab<double, 4, 2> jac(2, 2);
  for (size_t i = 0; i < nsteps; ++i)
  {
    for (size_t stage = 0; stage < sys.num_stages(); ++stage)
    {
      // clear buffers
      dg::util::fill(rhs, 0);
      dg::util::fill(jac, 0);

      auto qstage =
        dg::make_view_array(qs[sys.stage_buffer(stage + 1)].as_view(),
          qs[sys.stage_buffer(stage)].as_view());
      {
        auto r = sys.source(qstage);
        rhs[0] -= k0 * qstage[0][0];
        rhs[0] += r[0];
        rhs[1] -= k1 * qstage[0][1];
        rhs[1] += r[1];
      }
      sys.source_jac(qstage, 1, jac);
      jac(0, 0) += k0;
      jac(1, 1) += k1;
      // solve
      qstage[0][0] += rhs[0] / jac(0, 0);
      qstage[0][1] += rhs[1] / jac(1, 1);
      EXPECT_EQ(sys.advance_stage(qs), stage == sys.num_stages() - 1);
    }
    std::swap(qs[sys.stage_buffer(0)], qs[sys.stage_buffer(sys.num_stages())]);
  }
  return sqrt(pow(qs[sys.stage_buffer(0)][0] - exp(-k0 * nsteps * dt), 2) +
              pow(qs[sys.stage_buffer(0)][1] - exp(-k1 * nsteps * dt), 2));
}

TEST(test_dirk, irk1_scalar)
{
  // basic scalar ODE test
  {
    dg::eqsets::dirk1<> sys;
    EXPECT_EQ(sys.num_stages(), 1);
    EXPECT_EQ(sys.num_buffers(), 2);
  }
  {
    auto err1 = scalar_sim<dg::eqsets::dirk1<>>(1, 0.1);
    auto err2 = scalar_sim<dg::eqsets::dirk1<>>(2, 0.05);
    EXPECT_NEAR(err1 / err2, 2, 0.1);
  }
#if 0
  {
    auto err1 = scalar_sim<dg::eqsets::dirk1<>>(1, 0.2);
    auto err2 = scalar_sim<dg::eqsets::dirk1<>>(2, 0.1);
    EXPECT_NEAR(err1 / err2, 2, 0.25);
  }
#endif
}

TEST(test_dirk, irk2_scalar)
{
  // basic scalar ODE test
  {
    dg::eqsets::dirk2<> sys;
    EXPECT_EQ(sys.num_stages(), 2);
    EXPECT_EQ(sys.num_buffers(), 3);
  }
  {
    auto err1 = scalar_sim<dg::eqsets::dirk2<>>(1, .1);
    auto err2 = scalar_sim<dg::eqsets::dirk2<>>(2, 0.05);
    EXPECT_NEAR(err1 / err2, 4, 0.1);
  }
  {
    auto err1 = scalar_sim<dg::eqsets::dirk2<>>(2, 0.2);
    auto err2 = scalar_sim<dg::eqsets::dirk2<>>(4, 0.1);
    EXPECT_NEAR(err1 / err2, 4, 0.1);
  }
}

TEST(test_dirk, irk3_scalar)
{
  // basic scalar ODE test
  {
    dg::eqsets::dirk3<> sys;
    EXPECT_EQ(sys.num_stages(), 4);
    EXPECT_EQ(sys.num_buffers(), 3);
  }
  {
    auto err1 = scalar_sim<dg::eqsets::dirk3<>>(2, 0.5);
    auto err2 = scalar_sim<dg::eqsets::dirk3<>>(4, 0.25);
    EXPECT_NEAR(err1 / err2, 8, .75);
  }
  {
    auto err1 = scalar_sim<dg::eqsets::dirk3<>>(2, 0.2);
    auto err2 = scalar_sim<dg::eqsets::dirk3<>>(4, 0.1);
    EXPECT_NEAR(err1 / err2, 8, 0.3);
  }
}

TEST(test_dirk, irk1_system)
{
  // basic system ODE test
  {
    dg::eqsets::dirk1<2> sys;
    EXPECT_EQ(sys.num_stages(), 1);
    EXPECT_EQ(sys.num_buffers(), 2);
  }
  {
    auto err1 = system_sim<dg::eqsets::dirk1<2>>(1, 0.1);
    auto err2 = system_sim<dg::eqsets::dirk1<2>>(2, 0.05);
    EXPECT_NEAR(err1 / err2, 2, 0.1);
  }
  {
    auto err1 = system_sim<dg::eqsets::dirk1<2>>(1, 0.2);
    auto err2 = system_sim<dg::eqsets::dirk1<2>>(2, 0.1);
    EXPECT_NEAR(err1 / err2, 2, 0.25);
  }
}

TEST(test_dirk, irk2_system)
{
  // basic system ODE test
  {
    dg::eqsets::dirk2<2> sys;
    EXPECT_EQ(sys.num_stages(), 2);
    EXPECT_EQ(sys.num_buffers(), 3);
  }
  {
    auto err1 = system_sim<dg::eqsets::dirk2<2>>(1, .1);
    auto err2 = system_sim<dg::eqsets::dirk2<2>>(2, 0.05);
    EXPECT_NEAR(err1 / err2, 4, 0.1);
  }
  {
    auto err1 = system_sim<dg::eqsets::dirk2<2>>(2, 0.2);
    auto err2 = system_sim<dg::eqsets::dirk2<2>>(4, 0.1);
    EXPECT_NEAR(err1 / err2, 4, 0.1);
  }
}

TEST(test_dirk, irk3_system)
{
  // basic system ODE test
  {
    dg::eqsets::dirk3<2> sys;
    EXPECT_EQ(sys.num_stages(), 4);
    EXPECT_EQ(sys.num_buffers(), 3);
  }
  {
    auto err1 = system_sim<dg::eqsets::dirk3<2>>(2, 0.5);
    auto err2 = system_sim<dg::eqsets::dirk3<2>>(4, 0.25);
    EXPECT_NEAR(err1 / err2, 8, .75);
  }
  {
    auto err1 = system_sim<dg::eqsets::dirk3<2>>(2, 0.2);
    auto err2 = system_sim<dg::eqsets::dirk3<2>>(4, 0.1);
    EXPECT_NEAR(err1 / err2, 8, 0.3);
  }
}

int main(int argc, char** argv)
{
  init_tutils(&argc, argv);
  return run_tutils();
}
