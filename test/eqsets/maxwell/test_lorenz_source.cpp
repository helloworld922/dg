#include "dg/eqsets/maxwell/lorenz_source.hpp"

#include "dg/utils/hslab.hpp"

#include "tutils/tutils.hpp"

#include <Eigen/Sparse>
#include <Eigen/SparseLU>

#include <array>

TEST(test_lorenz_source, source)
{
  FAIL() << "not implemented yet";
}

TEST(test_lorenz_source, source_jac)
{
  FAIL() << "not implemented yet";
}

int main(int argc, char** argv)
{
  init_tutils(&argc, argv);
  return run_tutils();
}
