#include "dg/eqsets/five_moment/em_force.hpp"

#include "dg/utils/hslab.hpp"

#include "tutils/tutils.hpp"

#include <Eigen/Sparse>
#include <Eigen/SparseLU>

TEST(test_five_moment_em_force_1d, source)
{
  FAIL() << "not implemented yet";
}

TEST(test_five_moment_em_force_1d, source_jac)
{
  FAIL() << "not implemented yet";
}

TEST(test_five_moment_em_force_2d, source)
{
  FAIL() << "not implemented yet";
}

TEST(test_five_moment_em_force_2d, source_jac)
{
  FAIL() << "not implemented yet";
}

TEST(test_five_moment_em_force_3d, source)
{
  FAIL() << "not implemented yet";
}

TEST(test_five_moment_em_force_3d, source_jac)
{
  FAIL() << "not implemented yet";
}

int main(int argc, char** argv)
{
  init_tutils(&argc, argv);
  return run_tutils();
}
