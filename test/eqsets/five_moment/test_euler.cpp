#include "dg/eqsets/five_moment/euler.hpp"

#include "dg/ndg/rect/rect_1d.hpp"

#include "dg/utils/hslab.hpp"
#include "dg/utils/view_utils.hpp"

#include "dg/utils/instructions_list.hpp"

#include "tutils/tutils.hpp"

#include <Eigen/Sparse>
#include <Eigen/SparseLU>

#include <iomanip>

class test_euler_1d : public ::testing::Test
{
public:
  dg::ndg::rect<1> discr_1;
  dg::eqsets::euler<1> sys;

  dg::hslab<double, 2, dg::layout_left> advect_matrix_1;

  // allocations which have space for 1 elements
  dg::hslab<double, 2> q;
  dg::hslab<double, 2> rhs;

  double ijac = 2.;

  test_euler_1d()
    : discr_1(), advect_matrix_1(2, 2), q(2, 4),
      rhs(2, 5)
  {
  }

  void SetUp() override
  {
    discr_1.calc_advect_matrix(advect_matrix_1, ijac);
    sys.gamma = 5. / 3.;
  }

  void TearDown() override
  {
  }
};

TEST_F(test_euler_1d, int_flux)
{
  // set q
  q(0, 0) = 1;
  q(0, 1) = 1;
  q(0, 2) = -.25;
  q(0, 3) = .5;
  q(0, 4) =
    1 / (sys.gamma - 1) +
    0.5 * (q(0, 1) * q(0, 1) + q(0, 2) * q(0, 2) + q(0, 3) * q(0, 3)) / q(0, 0);

  q(1, 0) = 2;
  q(1, 1) = 1.5;
  q(1, 2) = -.125;
  q(1, 3) = .75;
  q(1, 4) =
    1.5 / (sys.gamma - 1) +
    0.5 * (q(1, 1) * q(1, 1) + q(1, 2) * q(1, 2) + q(1, 3) * q(1, 3)) / q(1, 0);
  // fill RHS to 0
  dg::util::fill(rhs, 0);

  discr_1.int_flux(q, rhs, advect_matrix_1, sys);

  std::array<double, 10> expected = {-0.5, -0.7291666666666665, -0.15625,
    -0.0625, -0.4469401041666656, -0.5, -0.7291666666666665, -0.15625, -0.0625,
    -0.4469401041666656};

  for (size_t i = 0; i < 2; ++i)
  {
    SCOPED_TRACE(i);
    for (size_t j = 0; j < 5; ++j)
    {
      SCOPED_TRACE(j);
      EXPECT_NEAR(rhs(i,j), expected[i*5+j], 1e-13);
    }
  }
}

TEST_F(test_euler_1d, int_flux_jac)
{
  // set q
  q(0, 0) = 1;
  q(0, 1) = 1;
  q(0, 2) = -.25;
  q(0, 3) = .5;
  q(0, 4) =
    1 / (sys.gamma - 1) +
    0.5 * (q(0, 1) * q(0, 1) + q(0, 2) * q(0, 2) + q(0, 3) * q(0, 3)) / q(0, 0);

  q(1, 0) = 2;
  q(1, 1) = 1.5;
  q(1, 2) = -.125;
  q(1, 3) = .75;
  q(1, 4) =
    1.5 / (sys.gamma - 1) +
    0.5 * (q(1, 1) * q(1, 1) + q(1, 2) * q(1, 2) + q(1, 3) * q(1, 3)) / q(1, 0);

  dg::instructions_list instrs;

  discr_1.int_flux_jac(q, q, advect_matrix_1, sys, instrs);

  instrs.compress();

  std::vector<PetscInt> expected_rows = {0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2,
    2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5,
    5, 5, 5, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8, 8, 8, 8};

  std::vector<PetscInt> expected_cols = {1, 5, 0, 1, 2, 3, 4, 5, 6, 7, 8, 0, 1,
    2, 4, 5, 6, 0, 1, 3, 4, 5, 7, 0, 1, 2, 3, 4, 5, 6, 7, 8, 0, 1, 2, 3, 4, 5,
    6, 7, 8, 0, 1, 2, 4, 5, 6, 0, 1, 3, 4, 5, 7, 0, 1, 2, 3, 4, 5, 6, 7, 8};

  std::vector<PetscScalar> expected_values = {-1, 1, .5625, -1.3333333333333333,
    -0.16666666666666669, 0.33333333333333337, -0.99348958333333348, 1,
    0.04166666666666667, -0.25, 0.6666666666666667, -0.25, 0.25, -1, 0.046875,
    -0.0625, 0.75, 0.5, -0.5, -1, -0.28125, 0.375, 0.75, 2.458333333333333,
    -3.229166666666667, -0.1666666666666667, 0.3333333333333334,
    -3.161295572916667, 2.853515625, 0.03125, -0.1875, 1.25, 0.5625,
    -1.333333333333333, -0.1666666666666667, 0.3333333333333334,
    -0.9934895833333335, 1, 0.04166666666666667, -0.25, 0.6666666666666667,
    -0.25, 0.25, -1, 0.046875, -0.0625, 0.75, 0.5, -0.5, -1, -0.28125, 0.375,
    0.75, 2.458333333333333, -2.229166666666667, -0.1666666666666667,
    0.3333333333333334, -3.161295572916667, 1.853515625, 0.03125, -0.1875,
    1.25};

  EXPECT_RANGE_EQ(instrs.rows, expected_rows);
  EXPECT_RANGE_EQ(instrs.cols, expected_cols);
  EXPECT_RANGE_NEAR(instrs.values, expected_values, 1e-13);
}

TEST(test_euler_2d, int_flux)
{
  FAIL() << "not implemented yet";
}

TEST(test_euler_2d, int_flux_jac)
{
  FAIL() << "not implemented yet";
}

TEST(test_euler_3d, int_flux)
{
  FAIL() << "not implemented yet";
}

TEST(test_euler_3d, int_flux_jac)
{
  FAIL() << "not implemented yet";
}

int main(int argc, char** argv)
{
  init_tutils(&argc, argv);
  return run_tutils();
}

