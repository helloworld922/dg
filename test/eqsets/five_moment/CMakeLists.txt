add_executable(test_euler test_euler.cpp)
target_link_libraries(test_euler dg ${TUTILS_LIBRARIES})
ADD_GTESTS(test_euler ""
  test_euler_1d.int_flux
  test_euler_1d.int_flux_jac
  #test_euler_2d.int_flux
  #test_euler_2d.int_flux_jac
  #test_euler_3d.int_flux
  #test_euler_3d.int_flux_jac
  )

#add_executable(test_euler_roe test_euler_roe.cpp)
#target_link_libraries(test_euler_roe dg ${TUTILS_LIBRARIES})
#ADD_GTESTS(test_euler_roe ""
#  test_euler_roe.num_flux_1d)

add_executable(test_euler_rusanov test_euler_rusanov.cpp)
target_link_libraries(test_euler_rusanov dg ${TUTILS_LIBRARIES})
ADD_GTESTS(test_euler_rusanov ""
  test_euler_rusanov_1d.num_flux
  test_euler_rusanov_1d.num_flux_jac
  #test_euler_rusanov_2d.num_flux
  #test_euler_rusanov_2d.num_flux_jac
  #test_euler_rusanov_3d.num_flux
  #test_euler_rusanov_3d.num_flux_jac
  )

if(0)
  add_executable(test_five_moment_em_force test_em_force.cpp)
  target_link_libraries(test_five_moment_em_force dg ${TUTILS_LIBRARIES})
  ADD_GTESTS(test_five_moment_em_force ""
    test_five_moment_em_force_1d.source
    test_five_moment_em_force_1d.source_jac
    test_five_moment_em_force_2d.source
    test_five_moment_em_force_2d.source_jac
    test_five_moment_em_force_3d.source
    test_five_moment_em_force_3d.source_jac)
endif()
