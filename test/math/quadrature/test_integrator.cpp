#include "dg/math/quadrature/quadrature.hpp"
#include "tutils/tutils.hpp"

TEST(test_integrator, basic_legendre_1d)
{
  constexpr real tol=1e-13;
  constexpr dg::legendre quad{};

  auto absc_data = quad.abscissas(4);
  typedef dg::view<real, dg::extents<4, 1>> absc_type;
  typedef dg::view<real, dg::extents<4>> wghts_type;
  typedef dg::view<real, dg::extents<1>> res_type;
  absc_type absc(absc_data.data());
  auto wghts_data = quad.weights(wghts_type(absc.data()));
  wghts_type wghts(wghts_data.data());
  std::array<real, 1> res_buf{0};

  dg::integrator<absc_type, wghts_type> integrator(absc, wghts);
  res_type res_view(res_buf.data());
  integrator(
    res_view, [](res_type& res,
                typename dg::integrator<absc_type, wghts_type>::xi_type xi,
                real w) -> void { res(0) += xi(0) * xi(0) * w; });
  EXPECT_NEAR(res_buf[0], 2./3., tol);
}

TEST(test_integrator, basic_legendre_2d)
{
  constexpr real tol = 1e-13;
  constexpr dg::legendre quad{};

  constexpr size_t N = 4;

  auto absc_data = quad.abscissas(N);
  auto wghts_data =
    quad.weights(dg::view<real, dg::extents<N>>(absc_data.data()));
  for(size_t i = 0; i < N; ++i)
  {
    absc_data.push_back(absc_data[i]);
    wghts_data.push_back(wghts_data[i]);
  }

  typedef dg::view<real, dg::extents<N, N, 1>, dg::layout_cprod> absc_type;
  typedef dg::view<real, dg::extents<2*N>> wghts_type;
  typedef dg::view<real, dg::extents<1>> res_type;
  absc_type absc(absc_data.data());
  wghts_type wghts(wghts_data.data());
  std::array<real, 1> res_buf{0};

  dg::integrator<absc_type, wghts_type> integrator(absc, wghts);
  res_type res_view(res_buf.data());
  integrator(
    res_view,
    [](res_type& res,
      typename dg::integrator<absc_type, wghts_type>::xi_type xi,
      real w) -> void { res(0) += (xi(1) * xi(1) + xi(0) * xi(0)) * w; });
  EXPECT_NEAR(res_buf[0], 8./3., tol);
}

TEST(test_integrator, basic_legendre_3d)
{
  constexpr real tol = 1e-13;
  constexpr dg::legendre quad{};

  constexpr size_t N = 4;

  auto absc_data = quad.abscissas(N);
  auto wghts_data =
    quad.weights(dg::view<real, dg::extents<N>>(absc_data.data()));
  for(size_t j = 0; j < 2; ++j)
  {
    for (size_t i = 0; i < N; ++i)
    {
      absc_data.push_back(absc_data[i]);
      wghts_data.push_back(wghts_data[i]);
    }
  }

  typedef dg::view<real, dg::extents<N, N, N, 1>, dg::layout_cprod> absc_type;
  typedef dg::view<real, dg::extents<3 * N>> wghts_type;
  typedef dg::view<real, dg::extents<1>> res_type;
  absc_type absc(absc_data.data());
  wghts_type wghts(wghts_data.data());
  std::array<real, 1> res_buf{0};

  dg::integrator<absc_type, wghts_type> integrator(absc, wghts);
  res_type res_view(res_buf.data());
  integrator(res_view,
      [](res_type& res,
          typename dg::integrator<absc_type, wghts_type>::xi_type xi, real w)
          -> void
      {
        res(0) += (xi(2) * xi(2) + xi(1) * xi(1) + xi(0) * xi(0)) * w;
      });
  EXPECT_NEAR(res_buf[0], 8, tol);
}

int main(int argc, char** argv)
{
  init_tutils(&argc, argv);
  return run_tutils();
}

