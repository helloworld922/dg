#include "dg/math/quadrature/legendre.hpp"
#include "tutils/tutils.hpp"

#include <array>

TEST(test_legendre, abcissas_real128)
{
  dg::real128 tol{"1e-31"};
  constexpr dg::legendre<dg::real128> quad{};
  for (size_t i = 1; i <= 64; ++i)
  {
    auto absc = quad.abscissas(i);
    SCOPED_TRACE(i);
    EXPECT_EQ(absc.size(), i);
    for (auto& v : absc)
    {
      EXPECT_NEAR(quad(v, i), 0, tol);
    }
    // abscissa should be in sorted order
    EXPECT_TRUE(std::is_sorted(absc.begin(), absc.end()));
    // abscissa should be unique
    EXPECT_EQ(std::unique(absc.begin(), absc.end()), absc.end());
  }
}

TEST(test_legendre, abcissas_double)
{
  double tol = 1e-10;
  constexpr dg::legendre<double> quad{};
  for (size_t i = 1; i <= 64; ++i)
  {
    auto absc = quad.abscissas(i);
    SCOPED_TRACE(i);
    EXPECT_EQ(absc.size(), i);
    for (auto& v : absc)
    {
      EXPECT_NEAR(quad(v, i), 0, tol);
    }
    // abscissa should be in sorted order
    EXPECT_TRUE(std::is_sorted(absc.begin(), absc.end()));
    // abscissa should be unique
    EXPECT_EQ(std::unique(absc.begin(), absc.end()), absc.end());
  }
}

TEST(test_legendre, eval_real128)
{
  dg::real128 tol{"1e-32"};
  constexpr static dg::legendre<dg::real128> quad{};
  for (size_t i = 0; i <= 100; ++i)
  {
    auto x = dg::real128{0.02} * i - 1;
    SCOPED_TRACE(x);
    EXPECT_NEAR(quad(x, 0), 1, tol);
    EXPECT_NEAR(quad(x, 1), x, tol);
    EXPECT_NEAR(quad(x, 2), 0.5 * (3 * x * x - 1), tol);
    EXPECT_NEAR(quad(x, 3), 0.5 * (5 * x * x * x - 3 * x), tol);
    EXPECT_NEAR(quad(x, 4), 0.125 * (35 * x * x * x * x - 30 * x * x + 3), tol);
    EXPECT_NEAR(quad(x, 9), (12155 * pow(x, 9) - 25740 * pow(x, 7) +
                              18018 * pow(x, 5) - 4620 * x * x * x + 315 * x) /
                              128.,
      tol);
  }
}

TEST(test_legendre, deriv_real128)
{
  dg::real128 tol{"1e-31"};
  constexpr static dg::legendre<dg::real128> quad{};
  for (size_t i = 0; i <= 100; ++i)
  {
    auto x = dg::real128{"0.02"} * i - 1;
    SCOPED_TRACE(x);
    EXPECT_NEAR(quad.deriv(x, 0), 0, tol);
    EXPECT_NEAR(quad.deriv(x, 1), 1, tol);
    EXPECT_NEAR(quad.deriv(x, 2), 3 * x, tol);
    EXPECT_NEAR(quad.deriv(x, 3), 0.5 * (5 * 3 * x * x - 3), tol);
    EXPECT_NEAR(
      quad.deriv(x, 4), 0.125 * (35 * 4 * x * x * x - 30 * 2 * x), tol);
    EXPECT_NEAR(quad.deriv(x, 9),
      (12155 * 9 * pow(x, 8) - 25740 * 7 * pow(x, 6) + 18018 * 5 * pow(x, 4) -
        4620 * 3 * x * x + 315) /
        128.,
      tol);
  }
}
#if 0
TEST(test_legendre, weights)
{
  // integration convergence testing
  // should converge at O(2 n)
  constexpr real tol = 1e-1;
  constexpr size_t nelems = 2;
  constexpr static dg::legendre quad{};
  for (size_t i = 1; i < 5; ++i)
  {
    auto absc = quad.abscissas(i);
    auto wghts = quad.weights(absc);
    std::array<real, 8> errs = {0};
    // convergence testing
    // integrate sin(x) from 0 to 1
    for (size_t n = 0; n < errs.size(); ++n)
    {
      real dx = 1. / (n + nelems);
      for(size_t k = 0; k < n+nelems; ++k)
      {
        for (size_t j = 0; j < absc.size(); ++j)
        {
          auto x = dx * (absc[j] + 1) * 0.5 + dx * k;
          errs[n] += sin(x) * wghts[j];
        }
      }
      errs[n] *= 0.5*dx;
      errs[n] = fabs(errs[n] - (1 - cos(1)));
    }
    // should probably do a better least-squares fit...
    for (size_t n = 1; n < errs.size(); ++n)
    {
      if(errs[n] > 1e-15)
      {
        EXPECT_NEAR(
          errs[n] / errs[n - 1], pow((n +nelems-1.) / (n + nelems), 2 * i), tol);
      }
    }
  }
}
#endif
int main(int argc, char** argv)
{
  init_tutils(&argc, argv);
  return run_tutils();
}
