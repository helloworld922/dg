#include "dg/math/quadrature/chebyshev.hpp"
#include "tutils/tutils.hpp"

#include <array>

TEST(test_chebyshev1, eval_real128)
{
  dg::real128 tol{"1e-31"};
  constexpr static dg::chebyshev<dg::real128, 1> quad{};
  for (size_t i = 0; i <= 100; ++i)
  {
    auto x = dg::real128{"0.02"} * i - 1;
    SCOPED_TRACE(x);
    EXPECT_NEAR(quad(x, 0), 1, tol);
    EXPECT_NEAR(quad(x, 1), x, tol);
    EXPECT_NEAR(quad(x, 2), 2 * x * x - 1, tol);
    EXPECT_NEAR(quad(x, 3), 4 * x * x * x - 3 * x, tol);
    EXPECT_NEAR(quad(x, 4), 8 * x * x * x * x - 8 * x * x + 1, tol);
    EXPECT_NEAR(quad(x, 9), 256 * pow(x, 9) - 576 * pow(x, 7) +
                              432 * pow(x, 5) - 120 * x * x * x + 9 * x,
      tol);
  }
}

TEST(test_chebyshev1, deriv_real128)
{
  dg::real128 tol{"1e-30"};
  constexpr static dg::chebyshev<dg::real128, 1> quad{};
  for (size_t i = 0; i <= 100; ++i)
  {
    auto x = dg::real128{"0.02"} * i - 1;
    SCOPED_TRACE(x);
    EXPECT_NEAR(quad.deriv(x, 0), 0, tol);
    EXPECT_NEAR(quad.deriv(x, 1), 1, tol);
    EXPECT_NEAR(quad.deriv(x, 2), 4 * x, tol);
    EXPECT_NEAR(quad.deriv(x, 3), 4 * 3 * x * x - 3, tol);
    EXPECT_NEAR(quad.deriv(x, 4), 8 * 4 * x * x * x - 8 * 2 * x, tol);
    EXPECT_NEAR(quad.deriv(x, 9),
      256 * 9 * pow(x, 8) - 576 * 7 * pow(x, 6) + 432 * 5 * pow(x, 4) -
        120 * 3 * x * x + 9,
      tol);
  }
}

TEST(test_chebyshev1, weights_real128)
{
  dg::real128 pi = boost::math::constants::pi<dg::real128>();
  // integration convergence testing
  // should converge at O(n)?
  dg::real128 tol = 1e-1;
  constexpr size_t nelems = 1;
  constexpr static dg::chebyshev<dg::real128, 1> quad{};
  for (size_t i = 1; i < 5; ++i)
  {
    auto absc = quad.abscissas(i);
    auto wghts = quad.weights(absc);

    std::array<dg::real128, 1> errs = {0};
    // convergence testing
    // integrate cos(x) from -1 to 1
    for (size_t n = 0; n < errs.size(); ++n)
    {
      dg::real128 dx = dg::real128{1} / (n + nelems);
      for(size_t k = 0; k < n+nelems; ++k)
      {
        for (size_t j = 0; j < absc.size(); ++j)
        {
          auto x = dx * (absc[j] + 1) * 0.5 + dx * k;
          errs[n] += exp(-x*x)*sqrt(1-absc[j]*absc[j]) * wghts[j];
        }
      }
      errs[n] *= dx * 0.5;
      errs[n] = abs(errs[n] - 0.5 * sqrt(pi) * erf(1));
    }
    // should probably do a better least-squares fit...
    // TODO: what rate should this really be converging at?
#if 0
    for (size_t n = 1; n < errs.size(); ++n)
    {
      if(errs[n] > 1e-15)
      {
        EXPECT_NEAR(errs[n] / errs[n - 1],
          pow((n + nelems - 1.) / (n + nelems), i), tol);
      }
    }
#endif
  }
}

TEST(test_chebyshev2, eval_real128)
{
  dg::real128 tol{"1e-31"};
  constexpr static dg::chebyshev<dg::real128, 2> quad{};
  for (size_t i = 0; i <= 100; ++i)
  {
    auto x = dg::real128{"0.02"} * i - 1;
    EXPECT_NEAR(quad(x, 0), 1, tol);
    EXPECT_NEAR(quad(x, 1), 2 * x, tol);
    EXPECT_NEAR(quad(x, 2), 4 * x * x - 1, tol);
    EXPECT_NEAR(quad(x, 3), 8 * x * x * x - 4 * x, tol);
    EXPECT_NEAR(quad(x, 4), 16 * x * x * x * x - 12 * x * x + 1, tol);
    EXPECT_NEAR(quad(x, 9), 512 * pow(x, 9) - 1024 * pow(x, 7) +
                              672 * pow(x, 5) - 160 * x * x * x + 10 * x,
      tol);
  }
}

TEST(test_chebyshev2, deriv_real128)
{
  dg::real128 tol{"1e-30"};
  constexpr static dg::chebyshev<dg::real128, 2> quad{};
  for (size_t i = 0; i <= 100; ++i)
  {
    auto x = dg::real128{"0.02"} * i - 1;
    EXPECT_NEAR(quad.deriv(x, 0), 0, tol);
    EXPECT_NEAR(quad.deriv(x, 1), 2, tol);
    EXPECT_NEAR(quad.deriv(x, 2), 4 * 2 * x, tol);
    EXPECT_NEAR(quad.deriv(x, 3), 8 * 3 * x * x - 4, tol);
    EXPECT_NEAR(quad.deriv(x, 4), 16 * 4 * x * x * x - 12 * 2 * x, tol);
    EXPECT_NEAR(quad.deriv(x, 9),
      512 * 9 * pow(x, 8) - 1024 * 7 * pow(x, 6) + 672 * 5 * pow(x, 4) -
        160 * 3 * x * x + 10,
      tol);
  }
}

TEST(test_chebyshev2, weights_real128)
{
  dg::real128 pi = boost::math::constants::pi<dg::real128>();
  // integration convergence testing
  // should converge at O(n)
  dg::real128 tol = 1e-1;
  constexpr size_t nelems = 1;
  constexpr static dg::chebyshev<dg::real128, 2> quad{};
  for (size_t i = 1; i < 8; ++i)
  {
    auto absc = quad.abscissas(i);
    auto wghts = quad.weights(absc);
    std::array<dg::real128, 1> errs = {0};
    // convergence testing
    // integrate sin(x) from 0 to 1
    for (size_t n = 0; n < errs.size(); ++n)
    {
      dg::real128 dx = dg::real128{1} / (n + nelems);
      for(size_t k = 0; k < n+nelems; ++k)
      {
        for (size_t j = 0; j < absc.size(); ++j)
        {
          auto x = dx * (absc[j] + 1) * 0.5 + dx * k;
          errs[n] += exp(-x * x) / sqrt(1 - absc[j] * absc[j]) * wghts[j];
        }
      }
      errs[n] *= 0.5 * dx;
      //errs[n] = fabs(errs[n] - 0.5 * sqrt(pi) * erf(1));
      //std::cout << errs[n] << '\n';
    }
    // should probably do a better least-squares fit...
    // TODO: what rate should this really be converging at?
#if 0
    for (size_t n = 1; n < errs.size(); ++n)
    {
      if(errs[n] > 1e-15)
      {
        EXPECT_NEAR(errs[n] / errs[n - 1],
          pow((n + nelems - 1.) / (n + nelems), i), tol);
      }
    }
#endif
  }
}

int main(int argc, char** argv)
{
  init_tutils(&argc, argv);
  return run_tutils();
}
