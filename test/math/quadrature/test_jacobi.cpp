#include "dg/math/quadrature/quadrature.hpp"
#include "tutils/tutils.hpp"

#include "dg/dg_config.hpp"

#include <array>

TEST(test_jacobi, cheb1_eval_real128)
{
  dg::chebyshev<dg::real128, 1> quad1{};
  dg::real128 tol{"1e-33"};
  dg::jacobi<dg::real128> quad2(-0.5, -0.5);
  for (size_t i = 0; i <= 100; ++i)
  {
    auto x = dg::real128("0.02") * i - 1;
    EXPECT_NEAR(quad1(x, 0), quad2(x, 0) / quad2(1, 0), tol);
    EXPECT_NEAR(quad1(x, 1), quad2(x, 1) / quad2(1, 1), tol);
    EXPECT_NEAR(quad1(x, 2), quad2(x, 2) / quad2(1, 2), tol);
    EXPECT_NEAR(quad1(x, 3), quad2(x, 3) / quad2(1, 3), tol);
    EXPECT_NEAR(quad1(x, 4), quad2(x, 4) / quad2(1, 4), tol);
    EXPECT_NEAR(quad1(x, 9), quad2(x, 9) / quad2(1, 9), tol);
  }
}

TEST(test_jacobi, cheb1_eval_double)
{
  dg::chebyshev<double, 1> quad1{};
  double tol=1e-13;
  dg::jacobi<double> quad2(-0.5, -0.5);
  for (size_t i = 0; i <= 100; ++i)
  {
    auto x = 0.02 * i - 1;
    EXPECT_NEAR(quad1(x, 0), quad2(x, 0) / quad2(1, 0), tol);
    EXPECT_NEAR(quad1(x, 1), quad2(x, 1) / quad2(1, 1), tol);
    EXPECT_NEAR(quad1(x, 2), quad2(x, 2) / quad2(1, 2), tol);
    EXPECT_NEAR(quad1(x, 3), quad2(x, 3) / quad2(1, 3), tol);
    EXPECT_NEAR(quad1(x, 4), quad2(x, 4) / quad2(1, 4), tol);
    EXPECT_NEAR(quad1(x, 9), quad2(x, 9) / quad2(1, 9), tol);
  }
}

TEST(test_jacobi, cheb1_deriv_real128)
{
  dg::real128 tol{"1e-31"};
  dg::chebyshev<dg::real128, 1> quad1{};
  dg::jacobi<dg::real128> quad2(-0.5, -0.5);
  for (size_t i = 0; i <= 100; ++i)
  {
    auto x = 0.02 * i - 1;
    EXPECT_NEAR(quad1.deriv(x, 0), quad2.deriv(x, 0) / quad2(1, 0), tol);
    EXPECT_NEAR(quad1.deriv(x, 1), quad2.deriv(x, 1) / quad2(1, 1), tol);
    EXPECT_NEAR(quad1.deriv(x, 2), quad2.deriv(x, 2) / quad2(1, 2), tol);
    EXPECT_NEAR(quad1.deriv(x, 3), quad2.deriv(x, 3) / quad2(1, 3), tol);
    EXPECT_NEAR(quad1.deriv(x, 4), quad2.deriv(x, 4) / quad2(1, 4), tol);
    EXPECT_NEAR(quad1.deriv(x, 9), quad2.deriv(x, 9) / quad2(1, 9), tol);
  }
}

TEST(test_jacobi, cheb1_deriv_double)
{
  double tol = 1e-12;
  dg::chebyshev<double, 1> quad1{};
  dg::jacobi<double> quad2(-0.5,-0.5);
  for (size_t i = 0; i <= 100; ++i)
  {
    auto x = 0.02 * i - 1;
    EXPECT_NEAR(quad1.deriv(x, 0), quad2.deriv(x, 0) / quad2(1, 0), tol);
    EXPECT_NEAR(quad1.deriv(x, 1), quad2.deriv(x, 1) / quad2(1, 1), tol);
    EXPECT_NEAR(quad1.deriv(x, 2), quad2.deriv(x, 2) / quad2(1, 2), tol);
    EXPECT_NEAR(quad1.deriv(x, 3), quad2.deriv(x, 3) / quad2(1, 3), tol);
    EXPECT_NEAR(quad1.deriv(x, 4), quad2.deriv(x, 4) / quad2(1, 4), tol);
    EXPECT_NEAR(quad1.deriv(x, 9), quad2.deriv(x, 9) / quad2(1, 9), tol);
  }
}

TEST(test_jacobi, cheb2_eval_real128)
{
  dg::real128 tol{"1e-32"};
  dg::chebyshev<dg::real128, 2> quad1{};
  dg::jacobi<dg::real128> quad2(0.5, 0.5);
  for (size_t i = 0; i <= 100; ++i)
  {
    auto x = 0.02 * i - 1;
    EXPECT_NEAR(quad1(x, 0), quad2(x, 0) / quad2(1, 0), tol);
    EXPECT_NEAR(quad1(x, 1), 2 * quad2(x, 1) / quad2(1, 1), tol);
    EXPECT_NEAR(quad1(x, 2), 3 * quad2(x, 2) / quad2(1, 2), tol);
    EXPECT_NEAR(quad1(x, 3), 4 * quad2(x, 3) / quad2(1, 3), tol);
    EXPECT_NEAR(quad1(x, 4), 5 * quad2(x, 4) / quad2(1, 4), tol);
    EXPECT_NEAR(quad1(x, 9), 10 * quad2(x, 9) / quad2(1, 9), tol);
  }
}

TEST(test_jacobi, cheb2_eval_double)
{
  double tol = 1e-12;
  dg::chebyshev<double, 2> quad1{};
  dg::jacobi<double> quad2(0.5, 0.5);
  for (size_t i = 0; i <= 100; ++i)
  {
    auto x = 0.02 * i - 1;
    EXPECT_NEAR(quad1(x, 0), quad2(x, 0) / quad2(1, 0), tol);
    EXPECT_NEAR(quad1(x, 1), 2 * quad2(x, 1) / quad2(1, 1), tol);
    EXPECT_NEAR(quad1(x, 2), 3 * quad2(x, 2) / quad2(1, 2), tol);
    EXPECT_NEAR(quad1(x, 3), 4 * quad2(x, 3) / quad2(1, 3), tol);
    EXPECT_NEAR(quad1(x, 4), 5 * quad2(x, 4) / quad2(1, 4), tol);
    EXPECT_NEAR(quad1(x, 9), 10 * quad2(x, 9) / quad2(1, 9), tol);
  }
}

TEST(test_jacobi, cheb2_deriv_real128)
{
  dg::real128 tol{"1e-31"};
  dg::chebyshev<dg::real128, 2> quad1{};
  dg::jacobi<dg::real128> quad2(0.5, 0.5);
  for (size_t i = 0; i <= 100; ++i)
  {
    auto x = 0.02 * i - 1;
    EXPECT_NEAR(quad1.deriv(x, 0), quad2.deriv(x, 0) / quad2(1, 0), tol);
    EXPECT_NEAR(quad1.deriv(x, 1), 2 * quad2.deriv(x, 1) / quad2(1, 1), tol);
    EXPECT_NEAR(quad1.deriv(x, 2), 3 * quad2.deriv(x, 2) / quad2(1, 2), tol);
    EXPECT_NEAR(quad1.deriv(x, 3), 4 * quad2.deriv(x, 3) / quad2(1, 3), tol);
    EXPECT_NEAR(quad1.deriv(x, 4), 5 * quad2.deriv(x, 4) / quad2(1, 4), tol);
    EXPECT_NEAR(quad1.deriv(x, 9), 10 * quad2.deriv(x, 9) / quad2(1, 9), tol);
  }
}

TEST(test_jacobi, cheb2_deriv_double)
{
  double tol = 1e-12;
  dg::chebyshev<double, 2> quad1{};
  dg::jacobi<double> quad2(0.5, 0.5);
  for (size_t i = 0; i <= 100; ++i)
  {
    auto x = 0.02 * i - 1;
    EXPECT_NEAR(quad1.deriv(x, 0), quad2.deriv(x, 0) / quad2(1, 0), tol);
    EXPECT_NEAR(quad1.deriv(x, 1), 2 * quad2.deriv(x, 1) / quad2(1, 1), tol);
    EXPECT_NEAR(quad1.deriv(x, 2), 3 * quad2.deriv(x, 2) / quad2(1, 2), tol);
    EXPECT_NEAR(quad1.deriv(x, 3), 4 * quad2.deriv(x, 3) / quad2(1, 3), tol);
    EXPECT_NEAR(quad1.deriv(x, 4), 5 * quad2.deriv(x, 4) / quad2(1, 4), tol);
    EXPECT_NEAR(quad1.deriv(x, 9), 10 * quad2.deriv(x, 9) / quad2(1, 9), tol);
  }
}

TEST(test_jacobi, leg_eval_real128)
{
  dg::real128 tol{"1e-33"};
  dg::legendre<dg::real128> quad1{};
  dg::jacobi<dg::real128> quad2(0, 0);
  for (size_t i = 0; i <= 100; ++i)
  {
    auto x = 0.02 * i - 1;
    EXPECT_NEAR(quad1(x, 0), quad2(x, 0), tol);
    EXPECT_NEAR(quad1(x, 1), quad2(x, 1), tol);
    EXPECT_NEAR(quad1(x, 2), quad2(x, 2), tol);
    EXPECT_NEAR(quad1(x, 3), quad2(x, 3), tol);
    EXPECT_NEAR(quad1(x, 4), quad2(x, 4), tol);
    EXPECT_NEAR(quad1(x, 9), quad2(x, 9), tol);
  }
}

TEST(test_jacobi, leg_eval_double)
{
  double tol = 1e-12;
  dg::legendre<double> quad1{};
  dg::jacobi<double> quad2(0, 0);
  for (size_t i = 0; i <= 100; ++i)
  {
    auto x = 0.02 * i - 1;
    EXPECT_NEAR(quad1(x, 0), quad2(x, 0), tol);
    EXPECT_NEAR(quad1(x, 1), quad2(x, 1), tol);
    EXPECT_NEAR(quad1(x, 2), quad2(x, 2), tol);
    EXPECT_NEAR(quad1(x, 3), quad2(x, 3), tol);
    EXPECT_NEAR(quad1(x, 4), quad2(x, 4), tol);
    EXPECT_NEAR(quad1(x, 9), quad2(x, 9), tol);
  }
}

TEST(test_jacobi, leg_deriv_real128)
{
  dg::real128 tol{"1e-32"};
  dg::legendre<dg::real128> quad1{};
  dg::jacobi<dg::real128> quad2(0, 0);
  for (size_t i = 0; i <= 100; ++i)
  {
    auto x = 0.02 * i - 1;
    EXPECT_NEAR(quad1.deriv(x, 0), quad2.deriv(x, 0), tol);
    EXPECT_NEAR(quad1.deriv(x, 1), quad2.deriv(x, 1), tol);
    EXPECT_NEAR(quad1.deriv(x, 2), quad2.deriv(x, 2), tol);
    EXPECT_NEAR(quad1.deriv(x, 3), quad2.deriv(x, 3), tol);
    EXPECT_NEAR(quad1.deriv(x, 4), quad2.deriv(x, 4), tol);
    EXPECT_NEAR(quad1.deriv(x, 9), quad2.deriv(x, 9), tol);
  }
}

TEST(test_jacobi, leg_deriv_double)
{
  double tol = 1e-12;
  dg::legendre<double> quad1{};
  dg::jacobi<double> quad2(0, 0);
  for (size_t i = 0; i <= 100; ++i)
  {
    auto x = 0.02 * i - 1;
    EXPECT_NEAR(quad1.deriv(x, 0), quad2.deriv(x, 0), tol);
    EXPECT_NEAR(quad1.deriv(x, 1), quad2.deriv(x, 1), tol);
    EXPECT_NEAR(quad1.deriv(x, 2), quad2.deriv(x, 2), tol);
    EXPECT_NEAR(quad1.deriv(x, 3), quad2.deriv(x, 3), tol);
    EXPECT_NEAR(quad1.deriv(x, 4), quad2.deriv(x, 4), tol);
    EXPECT_NEAR(quad1.deriv(x, 9), quad2.deriv(x, 9), tol);
  }
}

int main(int argc, char** argv)
{
  init_tutils(&argc, argv);
  return run_tutils();
}
