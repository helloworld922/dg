#include "dg/math/quadrature/lobatto.hpp"
#include "tutils/tutils.hpp"

#include <Eigen/Dense>

#include <array>

TEST(test_lobatto, eval_real128)
{
  dg::real128 tol{"1e-31"};
  constexpr static dg::lobatto<dg::real128> quad{};
  for (size_t i = 0; i <= 100; ++i)
  {
    auto x = dg::real128{"0.02"} * i - 1;
    EXPECT_NEAR(quad(x, 1), 0, tol);
    EXPECT_NEAR(quad(x, 2), 1, tol);
    EXPECT_NEAR(quad(x, 3), 3 * x, tol);
    EXPECT_NEAR(quad(x, 4), 0.5 * (5 * 3 * x * x - 3), tol);
    EXPECT_NEAR(quad(x, 5), 0.125 * (35 * 4 * x * x * x - 30 * 2 * x), tol);
    EXPECT_NEAR(quad(x, 10),
      (12155 * 9 * pow(x, 8) - 25740 * 7 * pow(x, 6) + 18018 * 5 * pow(x, 4) -
        4620 * 3 * x * x + 315) /
        128.,
      tol);
  }
}

TEST(test_lobatto, deriv_real128)
{
  dg::real128 tol{"1e-30"};
  constexpr static dg::lobatto<dg::real128> quad{};
  for (size_t i = 0; i <= 100; ++i)
  {
    auto x = dg::real128{"0.02"} * i - 1;
    EXPECT_NEAR(quad.deriv(x, 1), 0, tol);
    EXPECT_NEAR(quad.deriv(x, 2), 0, tol);
    EXPECT_NEAR(quad.deriv(x, 3), 3, tol);
    EXPECT_NEAR(quad.deriv(x, 4), 0.5 * (5 * 3 * 2 * x), tol);
    EXPECT_NEAR(quad.deriv(x, 5), 0.125 * (35 * 4 * 3 * x * x - 30 * 2), tol);
    EXPECT_NEAR(quad.deriv(x, 10),
      (12155 * 9 * 8 * pow(x, 7) - 25740 * 7 * 6 * pow(x, 5) +
        18018 * 5 * 4 * pow(x, 3) - 4620 * 3 * 2 * x) /
        128.,
      tol);
  }
}

TEST(test_lobatto, abcissas_real128)
{
  dg::real128 tol{"1e-29"};
  constexpr dg::lobatto<dg::real128> quad{};
  for (size_t i = 1; i <= 64; ++i)
  {
    auto absc = quad.abscissas(i);
    EXPECT_EQ(absc.size(), i);
    if (i > 1)
    {
      EXPECT_NEAR(absc[0], -1, tol);
      EXPECT_NEAR(absc[absc.size() - 1], 1, tol);
    }
    else
    {
      EXPECT_NEAR(absc[0], 0, tol);
    }
    for (size_t j = 1; j < absc.size() - 1; ++j)
    {
      EXPECT_NEAR(quad(absc[j], i), 0, tol);
    }
    // abscissa should be in sorted order
    EXPECT_TRUE(std::is_sorted(absc.begin(), absc.end()));
    // abscissa should be unique
    EXPECT_EQ(std::unique(absc.begin(), absc.end()), absc.end());
  }
}

TEST(test_lobatto, weights_real256)
{
  // integration convergence testing
  // should converge at O(2 n - 2)
  // minimum error limit
  dg::real256 tol{"1e-65"};
  dg::real256 slope_tol{"2e-2"};
  // integrate sin(x) from [0,1]
  // expected result: 1-cos(1)
  dg::real256 expected = 1 - cos(dg::real256{1});
  constexpr double elem_growth = 1.1;
  constexpr static dg::lobatto<dg::real256> quad{};
  constexpr size_t max_elems = 100;
  // checking any higher orders is too difficult with real256
  for (size_t i = 2; i <= 14; ++i)
  {
    auto absc = quad.abscissas(i);
    auto weights = quad.weights(absc);
    size_t nelems = 1;
    bool num_limit = false;
    std::vector<dg::real256> xs;
    std::vector<dg::real256> ys;
    while (!num_limit && nelems < max_elems)
    {
      dg::real256 err = 0;
      dg::real256 dx = dg::real256(1) / nelems;
      for (size_t elem = 0; elem < nelems; ++elem)
      {
        for (size_t node = 0; node < absc.size(); ++node)
        {
          dg::real256 x = (elem + .5 * (1 + absc[node])) * dx;
          err += weights[node] * sin(x);
        }
      }
      err *= dx * 0.5;
      err = fabs(err - expected);
      // detect if error is still in the starting off "bad" rate, converging
      // region, or numerical limits region
      num_limit |= err < tol;
      xs.emplace_back(nelems);
      ys.emplace_back(log(err));

      nelems = std::max<size_t>(nelems + 1, elem_growth * nelems);
    }
    // check convergence rate
    Eigen::Matrix<dg::real256, Eigen::Dynamic, 2> A(xs.size(), 2);
    ASSERT_EQ(xs.size(), ys.size());
    Eigen::Map<Eigen::Matrix<dg::real256, Eigen::Dynamic, 1>> b(
      ys.data(), ys.size());
    for (size_t j = 0; j < xs.size(); ++j)
    {
      A(j, 0) = 1;
      A(j, 1) = log(xs[j]);
    }
    Eigen::Matrix<dg::real256, 2, 1> sol = A.colPivHouseholderQr().solve(b);
    dg::real256 slope = -sol(1);
    dg::real256 slope_err = fabs(slope - (2 * i - 2));
    EXPECT_NEAR(slope, 2 * i - 2, slope_tol);
  }
}

int main(int argc, char** argv)
{
  init_tutils(&argc, argv);
  return run_tutils();
}
