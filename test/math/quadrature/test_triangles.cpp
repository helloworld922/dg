#include "dg/math/quadrature/triangle.hpp"
#include "tutils/tutils.hpp"

#include <array>
#include <numeric>

TEST(test_triangles_modes, eval_real128)
{
  dg::real128 tol{"1e-31"};
  constexpr dg::triangle_modes<dg::real128> quad{};
  for (size_t i = 0; i <= 100; ++i)
  {
    auto x = dg::real128{"0.02"} * i - 1;
    for (size_t j = 0; j <= 100 - i; ++j)
    {
      auto y = dg::real128{"0.02"} * j - 1;
      {
        dg::jacobi<dg::real128> jac(1, 0);
        EXPECT_NEAR(quad(x, y, 0, 0), 1, tol);
      }
      {
        dg::jacobi<dg::real128> jac(1, 0);
        EXPECT_NEAR(quad(x, y, 0, 1), (1 + 3 * y) / 2, tol);
      }
      {
        dg::jacobi<dg::real128> jac(2 * 1 + 1, 0);
        EXPECT_NEAR(quad(x, y, 1, 0), 1 + 2 * x + y, tol);
      }
      {
        dg::jacobi<dg::real128> jac(2 * 1 + 1, 0);
        EXPECT_NEAR(quad(x, y, 1, 1), (1 + 2 * x + y) * (3 + 5 * y) / 2, tol);
      }
    }
  }
}

TEST(test_triangles_modes, norm)
{
  constexpr double tol = 1e-14;
  std::vector<double> norms = {2., 1., 2. / 3., 0.5, 2. / 5., 1. / 3., 4. / 3.,
    8. / 9., 2. / 3., 8. / 15., 4. / 9., 8. / 21., 32. / 15., 8. / 5.,
    32. / 25., 16. / 15., 32. / 35., 4. / 5., 32. / 7., 128. / 35., 64. / 21.,
    128. / 49., 16. / 7., 128. / 63., 512. / 45., 256. / 27., 512. / 63.,
    64. / 9., 512. / 81., 256. / 45., 1024. / 33., 2048. / 77., 256. / 11.,
    2048. / 99., 1024. / 55., 2048. / 121.};
  constexpr size_t order = 6;
  constexpr dg::triangle_modes<double> quad{};

  for (size_t i = 0; i < order; ++i)
  {
    for (size_t j = 0; j < order; ++j)
    {
      EXPECT_NEAR(quad.norm(i, j), norms[i * order + j], tol);
    }
  }
}

TEST(test_triangles_modes, deriv_x_real128)
{
  dg::real128 tol{"1e-31"};
  constexpr dg::triangle_modes<dg::real128> quad{};
  for (size_t i = 0; i <= 100; ++i)
  {
    auto x = dg::real128{"0.01"} * i;
    for (size_t j = 0; j <= 100 - i; ++j)
    {
      auto y = dg::real128{"0.01"} * j;
      EXPECT_NEAR(quad.deriv_x(x, y, 0, 0), 0, tol);
      {
        dg::jacobi<dg::real128> jac(1, 0);
        EXPECT_NEAR(quad.deriv_x(x, y, 0, 1), 0, tol);
      }
      {
        dg::jacobi<dg::real128> jac(2 * 1 + 1, 0);
        EXPECT_NEAR(quad.deriv_x(x, y, 1, 0), 2 * jac(y, 0), tol);
      }
      {
        dg::jacobi<dg::real128> jac(2 * 1 + 1, 0);
        EXPECT_NEAR(quad.deriv_x(x, y, 1, 1), 2 * jac(y, 1), tol);
      }
    }
  }
}

TEST(test_triangles_modes, deriv_y_real128)
{
  dg::real128 tol{"1e-31"};
  constexpr dg::triangle_modes<dg::real128> quad{};
  for (size_t i = 0; i <= 100; ++i)
  {
    auto x = dg::real128{"0.01"} * i;
    for (size_t j = 0; j <= 100 - i; ++j)
    {
      auto y = dg::real128{"0.01"} * j;
      EXPECT_NEAR(quad.deriv_y(x, y, 0, 0), 0, tol);
      {
        dg::jacobi<dg::real128> jac(1, 0);
        EXPECT_NEAR(quad.deriv_y(x, y, 0, 1), jac.deriv(y, 1), tol);
      }
      {
        dg::jacobi<dg::real128> jac(2 * 1 + 1, 0);
        EXPECT_NEAR(quad.deriv_y(x, y, 1, 0),
          (jac(y, 0) + (2 * x + y + 1) * jac.deriv(y, 0)), tol);
      }
      {
        dg::jacobi<dg::real128> jac(2 * 1 + 1, 0);
        EXPECT_NEAR(quad.deriv_y(x, y, 1, 1),
          (jac(y, 1) + (2 * x + y + 1) * jac.deriv(y, 1)), tol);
      }
    }
  }
}

TEST(test_triangles_nodes, eval_real128)
{
  dg::real128 tol{"1e-31"};
  {
    // constant
    std::vector<dg::real128> absc_ = {0, 0};
    dg::view<dg::real128, 2> absc(absc_.data(), 1, 2);
    dg::triangle_nodes<dg::real128> quad(absc);
    EXPECT_NEAR(quad(0, 0, 0, 0), 1, tol);
    for (size_t i = 0; i <= 100; ++i)
    {
      auto x = dg::real128{"0.02"} * i - 1;
      for (size_t j = 0; j <= 100 - i; ++j)
      {
        auto y = dg::real128{"0.02"} * j - 1;
        EXPECT_NEAR(quad(x, y, 0, 0), 1, tol);
      }
    }
  }
  {
    // linear
    std::vector<dg::real128> absc_ = {-1, -1, 1, -1, -1, 1};
    dg::view<dg::real128, 2> absc(
      absc_.data(), 3, 2);
    dg::triangle_nodes<dg::real128> quad(absc);
    {
      auto basis = quad(-1, -1);
      EXPECT_NEAR(basis(0), 1, tol);
      EXPECT_NEAR(basis(1), 0, tol);
      EXPECT_NEAR(basis(2), 0, tol);
    }

    {
      auto basis = quad(1, -1);
      EXPECT_NEAR(basis(0), 0, tol);
      EXPECT_NEAR(basis(1), 1, tol);
      EXPECT_NEAR(basis(2), 0, tol);
    }

    {
      auto basis = quad(-1, 1);
      EXPECT_NEAR(basis(0), 0, tol);
      EXPECT_NEAR(basis(1), 0, tol);
      EXPECT_NEAR(basis(2), 1, tol);
    }

    for (size_t i = 0; i <= 50; ++i)
    {
      auto x = dg::real128{"0.04"} * i - 1;
      for (size_t j = 0; j <= 50 - i; ++j)
      {
        auto y = dg::real128{"0.04"} * j - 1;
        auto basis = quad(x, y);
        EXPECT_EQ(basis.rows(), 3);
        auto sum = basis(0) + basis(1) + basis(2);
        EXPECT_NEAR(sum, 1, tol);
      }
    }
  }
  {
    // quadratic
    std::vector<dg::real128> absc_ = {-1, -1, 0, -1, 1, -1, -1, 0, 0, 0, -1, 1};
    dg::view<dg::real128, 2> absc(
      absc_.data(), 6, 2);
    dg::triangle_nodes<dg::real128> quad(absc);

    for(size_t i = 0; i < 6; ++i)
    {
      auto basis = quad(absc(i, 0), absc(i, 1));
      for(size_t j = 0; j < 6; ++j)
      {
        if(i == j)
        {
          EXPECT_NEAR(basis(j), 1, tol);
        }
        else
        {
          EXPECT_NEAR(basis(j), 0, tol);
        }
      }
    }

    for (size_t i = 0; i <= 50; ++i)
    {
      auto x = dg::real128{"0.04"} * i - 1;
      for (size_t j = 0; j <= 50 - i; ++j)
      {
        auto y = dg::real128{"0.04"} * j - 1;
        auto basis = quad(x, y);
        EXPECT_EQ(basis.rows(), 6);
        auto sum =
          basis(0) + basis(1) + basis(2) + basis(3) + basis(4) + basis(5);
        EXPECT_NEAR(sum, 1, tol);
      }
    }
  }
}

TEST(test_triangles_nodes, deriv_x_real128)
{
  dg::real128 tol{"1e-31"};
  {
    // constant
    std::vector<dg::real128> absc_ = {0, 0};
    dg::view<dg::real128, 2> absc(
      absc_.data(), 1, 2);
    dg::triangle_nodes<dg::real128> quad(absc);
    EXPECT_NEAR(quad.deriv_x(0, 0, 0, 0), 0, tol);
    for (size_t i = 0; i <= 100; ++i)
    {
      auto x = dg::real128{"0.02"} * i - 1;
      for (size_t j = 0; j <= 100 - i; ++j)
      {
        auto y = dg::real128{"0.02"} * j - 1;
        EXPECT_NEAR(quad.deriv_x(x, y, 0, 0), 0, tol);
      }
    }
  }
  {
    // linear
    std::vector<dg::real128> absc_ = {-1, -1, 1, -1, -1, 1};
    dg::view<dg::real128, 2> absc(
      absc_.data(), 3, 2);
    dg::triangle_nodes<dg::real128> quad(absc);
    {
      auto db = quad.deriv_x(-1, -1);
      EXPECT_NEAR(db(0), dg::real128("-.5"), tol);
      EXPECT_NEAR(db(1), dg::real128("0.5"), tol);
      EXPECT_NEAR(db(2), dg::real128("0"), tol);
    }

    {
      auto db = quad.deriv_x(1, -1);
      EXPECT_NEAR(db(0), dg::real128("-.5"), tol);
      EXPECT_NEAR(db(1), dg::real128("0.5"), tol);
      EXPECT_NEAR(db(2), dg::real128("0"), tol);
    }

    {
      auto db = quad.deriv_x(-1, 1);
      EXPECT_NEAR(db(0), dg::real128("-.5"), tol);
      EXPECT_NEAR(db(1), dg::real128("0.5"), tol);
      EXPECT_NEAR(db(2), dg::real128("0"), tol);
    }
  }
}

TEST(test_triangles_nodes, deriv_y_real128)
{
  dg::real128 tol{"1e-31"};
  {
    // constant
    std::vector<dg::real128> absc_ = {0, 0};
    dg::view<dg::real128, 2> absc(
      absc_.data(), 1, 2);
    dg::triangle_nodes<dg::real128> quad(absc);
    EXPECT_NEAR(quad.deriv_y(0, 0, 0, 0), 0, tol);
    for (size_t i = 0; i <= 100; ++i)
    {
      auto x = dg::real128{"0.02"} * i - 1;
      for (size_t j = 0; j <= 100 - i; ++j)
      {
        auto y = dg::real128{"0.02"} * j - 1;
        EXPECT_NEAR(quad.deriv_y(x, y, 0, 0), 0, tol);
      }
    }
  }
  {
    // linear
    std::vector<dg::real128> absc_ = {-1, -1, 1, -1, -1, 1};
    dg::view<dg::real128, 2> absc(
      absc_.data(), 3, 2);
    dg::triangle_nodes<dg::real128> quad(absc);
    {
      auto db = quad.deriv_y(-1, -1);
      EXPECT_NEAR(db(0), dg::real128("-.5"), tol);
      EXPECT_NEAR(db(1), dg::real128("0"), tol);
      EXPECT_NEAR(db(2), dg::real128("0.5"), tol);
    }

    {
      auto db = quad.deriv_y(1, -1);
      EXPECT_NEAR(db(0), dg::real128("-.5"), tol);
      EXPECT_NEAR(db(1), dg::real128("0"), tol);
      EXPECT_NEAR(db(2), dg::real128("0.5"), tol);
    }

    {
      auto db = quad.deriv_y(-1, 1);
      EXPECT_NEAR(db(0), dg::real128("-.5"), tol);
      EXPECT_NEAR(db(1), dg::real128("0"), tol);
      EXPECT_NEAR(db(2), dg::real128("0.5"), tol);
    }
  }
}

TEST(test_triangles_nodes, weights_real128)
{
  dg::real128 tol{"1e-31"};
  typedef dg::triangle_nodes<dg::real128> quad_type;
  {
    // constant
    std::vector<dg::real128> absc_ = {0, 0};
    dg::view<dg::real128, 2> absc(
      absc_.data(), 1, 2);
    auto weights = quad_type::weights(absc);
    std::vector<dg::real128> expected = {2};
    EXPECT_RANGE_NEAR(weights, expected, tol);
  }
  {
    // linear
    std::vector<dg::real128> absc_ = {-1, -1, 1, -1, -1, 1};
    dg::view<dg::real128, 2> absc(
      absc_.data(), 3, 2);
    auto weights = quad_type::weights(absc);
    std::vector<dg::real128> expected = {2, 2, 2};
    for (auto& v : expected)
    {
      v /= 3;
    }
    EXPECT_RANGE_NEAR(weights, expected, tol);
  }
  {
    // quadratic
    std::vector<dg::real128> absc_ = {-1, -1, 0, -1, 1, -1, -1, 0, 0, 0, -1, 1};
    dg::view<dg::real128, 2> absc(
      absc_.data(), 6, 2);
    auto weights = quad_type::weights(absc);
    std::vector<dg::real128> expected = {0, 2, 0, 2, 2, 0};
    for (auto& v : expected)
    {
      v /= 3;
    }
    EXPECT_RANGE_NEAR(weights, expected, tol);
  }
  {
    // cubic
    std::vector<dg::real128> absc_ = {-1, -1, -sqrt(dg::real128{1} / 5), -1,
      sqrt(dg::real128{1} / 5), -1, 1, -1,

      -1, -sqrt(dg::real128{1} / 5), -1, sqrt(dg::real128{1} / 5), -1, 1,

      -1 + dg::real128{2} / 3, -1 + dg::real128{2} / 3,
      -sqrt(dg::real128{1} / 5), sqrt(dg::real128{1} / 5),
      sqrt(dg::real128{1} / 5), -sqrt(dg::real128{1} / 5)};
    dg::view<dg::real128, 2> absc(
      absc_.data(), 10, 2);
    auto weights = quad_type::weights(absc);
    std::vector<dg::real128> expected = {1, 5, 5, 1, 5, 5, 1, 27, 5, 5};
    for (auto& v : expected)
    {
      v /= 30;
    }
    EXPECT_RANGE_NEAR(weights, expected, tol);
    // should have 10 weights
    EXPECT_EQ(weights.size(), 10);
    // sum of weights should be 2
    auto total =
      std::accumulate(weights.begin(), weights.end(), dg::real128{0});
    EXPECT_NEAR(total, 2, tol);
    // all weights should be non-negative
    for (auto& v : weights)
    {
      EXPECT_GE(v, 0);
    }
  }
}

int main(int argc, char** argv)
{
  init_tutils(&argc, argv);
  return run_tutils();
}
