#include "dg/ndg/rect/rect_2d.hpp"

namespace dg
{
  namespace ndg
  {
    namespace detail
    {
      dg::spatial::elements::lagrange<dg::real256, 1, 1>&
      rect_2d_base::flat_element()
      {
        static dg::spatial::elements::lagrange<dg::real256, 1, 1> instance;
        return instance;
      }
    } // namespace detail
  }   // namespace ndg
} // namespace dg
