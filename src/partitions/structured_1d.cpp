#include "dg/partitions/structured.hpp"
#include <vector>

namespace dg
{
  namespace partitions
  {
    static int get_rank(MPI_Comm comm)
    {
      int res;
      MPI_Comm_rank(comm, &res);
      return res;
    }

    static int get_nprocs(MPI_Comm comm)
    {
      int res;
      MPI_Comm_size(comm, &res);
      return res;
    }

    structured::structured(size_t rank, size_t nprocs, size_t nelems,
      size_t degree, size_t num_comps, bool periodic)
      : global_idxr(dg::layout_offset<>(
                      ((!periodic) * num_comps +
                        nelems * rank / nprocs * (degree + 1) * num_comps)),
          nelems * (rank + 1) / nprocs - nelems * rank / nprocs, degree + 1,
          num_comps),
        local_idxr(global_idxr.shape(), dg::layout_offset<>(0)),
        bc_L_global(dg::layout_offset<>(0), num_comps),
        bc_R_global(
          dg::layout_offset<>(num_comps + nelems * (degree + 1) * num_comps),
          num_comps),
        global_left(dg::layout_offset<>(global_idxr.index(0, 0, 0) - num_comps),
          num_comps),
        global_right(
          dg::layout_offset<>(global_idxr.layout().offset + local_idxr.size()),
          num_comps),
        local_left(dg::layout_offset<>(local_idxr.size()), num_comps),
        local_right(
          dg::layout_offset<>(local_idxr.size() + num_comps), num_comps),
        local_elem_start(nelems * rank / nprocs),
        local_elem_stop(nelems * (rank + 1) / nprocs), nelems(nelems),
        degree(degree), num_comps(num_comps), rank(rank), nprocs(nprocs),
        periodic(periodic)
    {
      // modifications to offsets
      if (rank == 0)
      {
        if (periodic)
        {
          global_left.layout().offset =
            (nelems - 1) * (degree + 1) * num_comps + degree * num_comps;
          if (nprocs == 1)
          {
            local_left.layout().offset = global_left.layout().offset;
            local_right.layout().offset = 0;
          }
        }
        else
        {
          local_left.layout().offset = 0;
          local_idxr.layout().offset = num_comps;
          local_right.layout().offset -= num_comps;
        }
      }
      if (rank == nprocs - 1)
      {
        if (periodic)
        {
          global_right.layout().offset = 0;
        }
        else if (rank != 0)
        {
          std::swap(local_left.layout().offset, local_right.layout().offset);
        }
      }
    }

    std::vector<PetscInt> structured::calc_ghost_idcs() const
    {
      std::vector<PetscInt> ghost_idcs;
      ghost_idcs.reserve(2 * num_comps);

      if (nprocs > 1)
      {
        // left
        if (rank != 0 || periodic)
        {
          for (size_t i = 0; i < num_comps; ++i)
          {
            ghost_idcs.push_back(global_left.index(i));
          }
        }

        // right
        if (rank != nprocs - 1 || periodic)
        {
          for (size_t i = 0; i < num_comps; ++i)
          {
            ghost_idcs.push_back(global_right.index(i));
          }
        }
      }

      return ghost_idcs;
    }

    dg::petsc::vector structured::alloc(MPI_Comm comm) const
    {
      dg::petsc::vector res;
      auto ghost_idcs = calc_ghost_idcs();

      if (periodic)
      {
        VecCreateGhost(comm, local_idxr.size(), PETSC_DECIDE, ghost_idcs.size(),
          ghost_idcs.data(), &res.reset());
      }
      else
      {
        auto size = local_idxr.size();
        if (rank == 0)
        {
          size += num_comps;
        }
        if (rank == nprocs - 1)
        {
          size += num_comps;
        }
        VecCreateGhost(comm, size, PETSC_DECIDE, ghost_idcs.size(),
          ghost_idcs.data(), &res.reset());
      }
      return res;
    }
  }
}
