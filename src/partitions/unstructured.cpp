#include "dg/partitions/unstructured.hpp"

#include <algorithm>
#include <utility>
#include <numeric>
#include <unordered_set>

#include <iostream>

namespace dg
{
  namespace partitions
  {
    ///
    /// @param elem_partition which elements are owned by which MPI rank
    /// (CSR-style)
    ///
    void mesh_dual(std::vector<idx_t>& elem_partition,
      std::vector<idx_t>& elem_starts, std::vector<idx_t>& elem_nodes,
      std::vector<idx_t>& xadj, std::vector<idx_t>& adjncy, idx_t ncommon,
      MPI_Comm comm)
    {
      // TODO:
      // - more than one patch per MPI rank?
      // - insert faces for HDG?
      int rank, nprocs;
      MPI_Comm_rank(comm, &rank);
      MPI_Comm_size(comm, &nprocs);

      // calculate number of nodes globally
      // assumes: all node ids [0-node_max] are used
      idx_t num_nodes;
      {
        auto local_max =
          *std::max_element(elem_nodes.begin(), elem_nodes.end());
        MPI_Allreduce(&local_max, &num_nodes, 1,
          dg::mpi::mpi_type<idx_t>::type(), MPI_MAX, comm);
        ++num_nodes;
      }

      // node distribution array
      // equally distribute nodes
      // slightly weird code to deal with overflow?
      std::vector<idx_t> node_dist(nprocs + 1);
      for (size_t i = 0, j = num_nodes; i < nprocs; ++i)
      {
        auto k = j / (nprocs - i);
        node_dist[i + 1] = node_dist[i] + k;
        j -= k;
      }

      // number of entries in elem_nodes
      std::vector<std::pair<idx_t, idx_t>> node_list(elem_starts.back());
      std::vector<idx_t> aux_array(elem_starts.back());
      // TODO: this doesn't seem to be an actual hash set?
      // index is key
      // 1: value is in
      // 0: value is not present
      // TODO: htable also seems to be used for something else involving mask?
      constexpr idx_t mask = (1 << 11) - 1;
      std::vector<int> htable(
        std::max<idx_t>(node_dist[rank + 1] - node_dist[rank], mask + 1));

      // find a local numbering for the nodes
      // for each element
      for (idx_t i = 0; i < elem_starts.size() - 1; ++i)
      {
        // for each node in the element
        for (idx_t j = elem_starts[i]; j < elem_starts[i + 1]; ++j)
        {
          node_list[j].first = elem_nodes[j];
          node_list[j].second = j;
          // keep track of which local element id the node belongs to
          aux_array[j] = i;
        }
      }
      // sort node_list by key in increasing order
      std::sort(node_list.begin(), node_list.end(),
        [](const auto& a, const auto& b) { return a.first < b.first; });

      // find number of unique nodes locally
      idx_t num_local_nodes = 1;
      for (idx_t i = 1; i < node_list.size(); ++i)
      {
        if (node_list[i].first > node_list[i - 1].first)
        {
          ++num_local_nodes;
        }
      }
      std::vector<idx_t> nmap;
      nmap.reserve(num_local_nodes);
      // re-number nodes (and elements?)
      nmap.push_back(node_list[0].first);
      elem_nodes[node_list[0].second] = 0;
      node_list[0].second = aux_array[node_list[0].second];
      for (idx_t i = 1; i < node_list.size(); ++i)
      {
        if (node_list[i].first > node_list[i - 1].first)
        {
          // unique node
          nmap.push_back(node_list[i].first);
        }
        elem_nodes[node_list[i].second] = nmap.size() - 1;
        node_list[i].second = aux_array[node_list[i].second];
      }
      // TODO: is this required?
      // MPI_Barrier(comm);

      // for MPI_Alltoallv
      std::vector<int> scounts(nprocs);
      std::vector<int> rcounts(nprocs);
      std::vector<int> sdispl(nprocs + 1);
      std::vector<int> rdispl(nprocs + 1);

      // transmit parts of node_list to all other ranks
      // TODO: does this mean we have a global list of nodes on every rank?
      for (int pe = 0, i = 0; i < node_list.size(); ++i)
      {
        while (node_list[i].first >= node_dist[pe + 1])
        {
          ++pe;
        }
        scounts[pe] += 2;
      }

      MPI_Alltoall(scounts.data(), 1, dg::mpi::mpi_type<idx_t>::type(),
        rcounts.data(), 1, dg::mpi::mpi_type<idx_t>::type(), comm);

      std::partial_sum(scounts.begin(), scounts.end(), sdispl.begin() + 1);
      std::partial_sum(rcounts.begin(), rcounts.end(), rdispl.begin() + 1);

      idx_t nrecv = rdispl[nprocs] / 2;
      std::vector<std::pair<idx_t, idx_t>> recv_buffer(std::max(1, nrecv));
      MPI_Alltoallv(node_list.data(), scounts.data(), sdispl.data(),
        dg::mpi::mpi_type<idx_t>::type(), recv_buffer.data(), rcounts.data(),
        rdispl.data(), dg::mpi::mpi_type<idx_t>::type(), comm);

      // TODO: global node-element list?
      std::vector<idx_t> gnodes_starts(
        node_dist[rank + 1] - node_dist[rank] + 1);
      for (idx_t i = 0; i < nprocs; ++i)
      {
        for (idx_t j = rdispl[i] / 2; j < rdispl[i + 1] / 2; ++j)
        {
          auto lnode = recv_buffer[j].first - node_dist[rank];
          // this avoids aliasing issues later
          ++gnodes_starts[lnode + 1];
        }
      }
      std::partial_sum(gnodes_starts.begin() + 1, gnodes_starts.end(),
        gnodes_starts.begin() + 1);

      std::vector<idx_t> gnodes_data(std::max(1, gnodes_starts.back()));
      for (int pe = 0; pe < nprocs; ++pe)
      {
        for (idx_t j = rdispl[pe] / 2; j < rdispl[pe + 1] / 2; ++j)
        {
          auto lnode = recv_buffer[j].first - node_dist[rank];
          gnodes_data[gnodes_starts[lnode]] =
            recv_buffer[j].second + elem_partition[pe];
          gnodes_starts[lnode]++;
        }
      }
      std::copy_backward(
        gnodes_starts.begin(), gnodes_starts.end() - 1, gnodes_starts.end());
      gnodes_starts[0] = 0;

      // send out global node data?
      std::fill(scounts.begin(), scounts.end(), 0);
      for (int pe = 0; pe < nprocs; ++pe)
      {
        for (int j = rdispl[pe] / 2; j < rdispl[pe + 1] / 2; ++j)
        {
          auto lnode = recv_buffer[j].first - node_dist[rank];
          // pretty sure this has no artificial hash collisions?
          if (!htable[lnode])
          {
            scounts[pe] += gnodes_starts[lnode + 1] - gnodes_starts[lnode];
            htable[lnode] = 1;
          }
        }
        // clear htable to 0
        // TODO: is this better than just doing a fill?
        for (auto j = rdispl[pe] / 2; j < rdispl[pe + 1] / 2; ++j)
        {
          auto lnode = recv_buffer[j].first - node_dist[rank];
          htable[lnode] = 0;
        }
      }

      MPI_Alltoall(scounts.data(), 1, dg::mpi::mpi_type<idx_t>::type(),
        rcounts.data(), 1, dg::mpi::mpi_type<idx_t>::type(), comm);

      std::partial_sum(scounts.begin(), scounts.end(), sdispl.begin() + 1);

      auto nsend = sdispl[nprocs];
      std::vector<idx_t> send_buffer;
      send_buffer.reserve(std::max(1, nsend));

      // TODO: what does this do to setup send_buffer?
      for (int pe = 0; pe < nprocs; ++pe)
      {
        for (idx_t j = rdispl[pe] / 2; j < rdispl[pe + 1] / 2; ++j)
        {
          auto lnode = recv_buffer[j].first - node_dist[rank];
          if (!htable[lnode])
          {
            for (auto k = gnodes_starts[lnode]; k < gnodes_starts[lnode + 1];
                 ++k)
            {
              if (k == gnodes_starts[lnode])
              {
                send_buffer.push_back(-(gnodes_data[k] + 1));
              }
              else
              {
                send_buffer.push_back(gnodes_data[k]);
              }
            }
            htable[lnode] = 1;
          }
        }
        // clear htable to 0
        // TODO: is this better than just doing a fill?
        for (auto j = rdispl[pe] / 2; j < rdispl[pe + 1] / 2; ++j)
        {
          auto lnode = recv_buffer[j].first - node_dist[rank];
          htable[lnode] = 0;
        }
      }

      if (!send_buffer.size())
      {
        send_buffer.push_back(0);
      }

      std::partial_sum(rcounts.begin(), rcounts.end(), rdispl.begin() + 1);

      nrecv = rdispl[nprocs];
      std::vector<idx_t> node_data(std::max(1, nrecv));
      MPI_Alltoallv(send_buffer.data(), scounts.data(), sdispl.data(),
        dg::mpi::mpi_type<idx_t>::type(), node_data.data(), rcounts.data(),
        rdispl.data(), dg::mpi::mpi_type<idx_t>::type(), comm);

      std::vector<idx_t> node_starts(num_local_nodes + 1);

      for (int pe = 0, k = -1; pe < nprocs; ++pe)
      {
        for (auto j = rdispl[pe]; j < rdispl[pe + 1]; ++j)
        {
          if (node_data[j] < 0)
          {
            ++k;
            node_data[j] = (-node_data[j]) - 1;
          }
          ++node_starts[k + 1];
        }
      }
      std::partial_sum(
        node_starts.begin() + 1, node_starts.end(), node_starts.begin() + 1);

      xadj.clear();
      xadj.resize(elem_starts.size());
      adjncy.clear();
      //std::vector<idx_t> xadj(elem_starts.size());
      // size determined in pass 2
      //std::vector<idx_t> adjncy;

      // TODO: is this required?
      // std::fill(htable.begin(), htable.end(), 0);

      // TODO: what is this section doing?
      // two passes
      // idx_t first_elem = elem_partition[rank];
      std::vector<idx_t> ind;
      std::vector<idx_t> wgt;
      for (auto pass = 0; pass < 2; ++pass)
      {
        for (size_t i = 0; i < elem_starts.size() - 1; ++i)
        {
          idx_t count = 0;
          for (idx_t j = elem_starts[i]; j < elem_starts[i + 1]; ++j)
          {
            auto node = elem_nodes[j];
            for (auto k = node_starts[node]; k < node_starts[node + 1]; ++k)
            {
              auto kk = node_data[k];
              if (kk == elem_partition[rank] + i)
              {
                continue;
              }
              auto m = htable[kk & mask];
              if (!m)
              {
                while (ind.size() <= count)
                {
                  ind.push_back(0);
                  wgt.push_back(0);
                }
                ind[count] = kk;
                wgt[count] = 1;
                ++count;
                htable[kk & mask] = count;
              }
              else
              {
                if (ind[m - 1] == kk)
                {
                  ++wgt[m - 1];
                }
                else
                {
                  idx_t jj = 0;
                  for (; jj < count; ++jj)
                  {
                    if (ind[jj] == kk)
                    {
                      ++wgt[jj];
                      break;
                    }
                  }
                  if (jj == count)
                  {
                    while (ind.size() <= count)
                    {
                      ind.push_back(0);
                      wgt.push_back(0);
                    }
                    ind[count] = kk;
                    wgt[count] = 1;
                    ++count;
                  }
                }
              }
            }
          }
          for (idx_t j = 0; j < count; ++j)
          {
            htable[(ind[j] & mask)] = 0;
            if (wgt[j] >= ncommon)
            {
              if (pass == 0)
              {
                // i+1 is to make sure partial_sum can fulfill the roll of
                // MAKECSR in-place
                ++xadj[i + 1];
              }
              else
              {
                adjncy[xadj[i]++] = ind[j];
              }
            }
          }
        }
        if (pass == 0)
        {
          std::partial_sum(xadj.begin() + 1, xadj.end(), xadj.begin() + 1);
          adjncy.resize(xadj.back());
        }
        else
        {
          // unfortunately, still need SHIFTCSR...
          std::copy_backward(xadj.begin(), xadj.end() - 1, xadj.end());
          xadj[0] = 0;
        }
      }

      // re-number elements
      for (idx_t i = 0; i < elem_starts.back(); ++i)
      {
        elem_nodes[i] = nmap[elem_nodes[i]];
      }
    }
  } // namespace partitions
} // namespace dg
