#include "dg/context.hpp"
#include <cassert>

static bool mpi_is_initialized()
{
  int flag;
  MPI_Initialized(&flag);
  return flag;
}

static bool petsc_is_initialized()
{
  PetscBool flag;
  PetscInitialized(&flag);
  return flag;
}

namespace dg
{
  context::context(int argn, char** argv, const char* petsc_db,
    const char* petsc_help, int thread_required)
    : context()
  {
    if (!mpi_is_initialized())
    {
      int provided;
      auto err = MPI_Init_thread(&argn, &argv, thread_required, &provided);
      assert(err == MPI_SUCCESS);
      initialized_mpi_ = true;
      assert(provided >= thread_required);
    }
    if (!petsc_is_initialized())
    {
      auto err = PetscInitialize(&argn, &argv, petsc_db, petsc_help);
      assert(!err);
      CHKERRV(err);
      initialized_petsc_ = true;
    }
    MPI_Comm_dup(MPI_COMM_WORLD, &comm.reset());
  }

  context::context(MPI_Comm c)
  {
    assert(mpi_is_initialized());
    assert(petsc_is_initialized());
    MPI_Comm_dup(c, &comm.reset());
  }

  context::~context()
  {
    if (initialized_petsc_)
    {
      PetscFinalize();
    }
    // delete comm
    comm.reset();
    if (initialized_mpi_)
    {
      MPI_Finalize();
    }
  }
}
