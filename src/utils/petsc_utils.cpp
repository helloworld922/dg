#include "dg/utils/petsc_utils.hpp"

namespace dg
{
  namespace petsc
  {
    const char* to_string(SNESConvergedReason res)
    {
      switch (res)
      {
      case SNES_CONVERGED_FNORM_ABS:
        return "converged (||F|| < atol)";
      case SNES_CONVERGED_FNORM_RELATIVE:
        return "converged (||F|| < rtol * ||F_initial||)";
      case SNES_CONVERGED_SNORM_RELATIVE:
        return "converged (||delta x|| < stol * ||x||)";
      case SNES_CONVERGED_ITS:
        return "converged (max iterations)";
      case SNES_CONVERGED_TR_DELTA:
        return "converged (trust region delta)";
      case SNES_DIVERGED_FUNCTION_DOMAIN:
        return "diverged (function domain)";
      case SNES_DIVERGED_FUNCTION_COUNT:
        return "diverged (function count)";
      case SNES_DIVERGED_LINEAR_SOLVE:
        return "diverged (linear solve)";
      case SNES_DIVERGED_FNORM_NAN:
        return "diverged (||F|| is NaN)";
      case SNES_DIVERGED_MAX_IT:
        return "diverged (max iterations)";
      case SNES_DIVERGED_LINE_SEARCH:
        return "diverged (line search)";
      case SNES_DIVERGED_LOCAL_MIN:
        return "||J^T b|| is small, local minimum of F";
      case SNES_DIVERGED_DTOL:
        return "||F|| > divtol * ||F_initial||";
      case SNES_CONVERGED_ITERATING:
      default:
        return "iterating";
      }
    }

    const char* to_string(KSPConvergedReason res)
    {
      switch (res)
      {
      case KSP_CONVERGED_RTOL_NORMAL:
      case KSP_CONVERGED_RTOL:
        return "converged (||F x - b|| < rtol * ||F x - b||_initial)";
      case KSP_CONVERGED_ATOL_NORMAL:
      case KSP_CONVERGED_ATOL:
        return "converged (||F x - b|| < atol)";
      case KSP_CONVERGED_ITS:
        return "converged (max iterations)";
      case KSP_CONVERGED_CG_NEG_CURVE:
        return "converged (CG negative curvature)";
      case KSP_CONVERGED_CG_CONSTRAINED:
        return "converged (CG constrained step)";
      case KSP_CONVERGED_STEP_LENGTH:
        return "converged (step length)";
      case KSP_CONVERGED_HAPPY_BREAKDOWN:
        return "converged (happy breakdown)";
      case KSP_DIVERGED_NULL:
        return "diverged (null)";
      case KSP_DIVERGED_ITS:
        return "diverged (max iterations)";
      case KSP_DIVERGED_DTOL:
        return "diverged (||F x - b|| > divtol * ||F x - b||_initial)";
      case KSP_DIVERGED_BREAKDOWN:
        return "diverged (breakdown)";
      case KSP_DIVERGED_BREAKDOWN_BICG:
        return "diverged (BICG breakdown)";
      case KSP_DIVERGED_NONSYMMETRIC:
        return "diverged (non-symmetric)";
      case KSP_DIVERGED_INDEFINITE_PC:
        return "diverged (indefinite pre-conditioner)";
      case KSP_DIVERGED_NANORINF:
        return "diverged (NaN or inf)";
      case KSP_DIVERGED_INDEFINITE_MAT:
        return "diverged (indefinite matrix)";
      case KSP_DIVERGED_PC_FAILED:
        return "diverged (pre-conditioner setup failed)";
      case KSP_CONVERGED_ITERATING:
        return "iterating";
      }
    }

    const char* to_string(SNESLineSearchReason res)
    {
      switch(res)
      {
      default:
      case SNES_LINESEARCH_SUCCEEDED:
        return "succeeded";
      case SNES_LINESEARCH_FAILED_NANORINF:
        return "failed (NaN or inf)";
      case SNES_LINESEARCH_FAILED_DOMAIN:
        return "failed (domain)";
      case SNES_LINESEARCH_FAILED_REDUCT:
        return "failed (insufficient reduction)";
      case SNES_LINESEARCH_FAILED_USER:
        return "failed (user)";
      case SNES_LINESEARCH_FAILED_FUNCTION:
        return "failed (function)";
      }
    }
  }
}
