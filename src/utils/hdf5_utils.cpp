#include "dg/utils/hdf5_utils.hpp"

namespace dg
{
  namespace hdf5
  {
    void identifier::dec()
    {
      auto p = get();
      if (H5Iis_valid(p) > 0)
      {
        auto err = H5Idec_ref(p);
        assert(err >= 0);
      }
    }

    void identifier::inc()
    {
      auto p = get();
      if (H5Iis_valid(p) > 0)
      {
        H5Iinc_ref(p);
      }
    }

    namespace plists
    {
      file_access::file_access() : identifier(H5Pcreate(H5P_FILE_ACCESS))
      {
      }

      file_access::file_access(MPI_Comm comm, MPI_Info info)
        : identifier(H5Pcreate(H5P_FILE_ACCESS))
      {
        set_mpio(comm, info);
      }

      file_access::file_access(const location& loc)
        : identifier(H5Fget_access_plist(H5Iget_file_id(loc)))
      {
      }

      void file_access::set_mpio(MPI_Comm comm, MPI_Info info)
      {
        H5Pset_fapl_mpio(get(), comm, info);
      }

      void file_access::get_mpio(MPI_Comm* comm, MPI_Info* info) const
      {
        auto driver = H5Pget_driver(get());
        if (driver == H5FD_MPIO)
        {
          H5Pget_fapl_mpio(get(), comm, info);
        }
      }
    } // namespace plists

    file object::get_file() const
    {
      return file(*this);
    }

    std::string object::path() const
    {
      auto len = H5Iget_name(get(), nullptr, 0);
      assert(len >= 0);
      std::string res(len, '\0');
      H5Iget_name(get(), const_cast<char*>(res.data()), len + 1);
      return res;
    }

    bool location::exists(const char* name) const
    {
      auto res = H5Oexists_by_name(get(), name, H5P_DEFAULT);
      assert(res >= 0);
      return res > 0;
    }

    bool location::has_attribute(const char* name) const
    {
      auto res = H5Aexists(get(), name);
      assert(res >= 0);
      return res > 0;
    }

    bool location::exists(const char* name, const plist& link_access) const
    {
      auto res = H5Oexists_by_name(get(), name, link_access);
      assert(res >= 0);
      return res > 0;
    }

    file::file(const object& o) : location(H5Iget_file_id(o))
    {
    }

    file::file(const char* fname, unsigned flags)
    {
      if ((flags & H5F_ACC_TRUNC) || (flags & H5F_ACC_EXCL))
      {
        // create
        reset(H5Fcreate(fname, flags, H5P_DEFAULT, H5P_DEFAULT));
      }
      else
      {
        // open
        reset(H5Fopen(fname, flags, H5P_DEFAULT));
      }
    }

    file::file(
      const char* fname, unsigned flags, const plists::file_access& access)
    {
      if ((flags & H5F_ACC_TRUNC) || (flags & H5F_ACC_EXCL))
      {
        // create
        reset(H5Fcreate(fname, flags, H5P_DEFAULT, access));
      }
      else
      {
        // open
        reset(H5Fopen(fname, flags, access));
      }
    }

    std::string file::name() const
    {
      auto len = H5Fget_name(get(), nullptr, 0);
      assert(len >= 0);
      std::string res(len, '\0');
      H5Fget_name(get(), const_cast<char*>(res.data()), len + 1);
      return res;
    }

    void file::flush() const
    {
      H5Fflush(get(), H5F_SCOPE_LOCAL);
    }

    group::group(const location& parent, const char* path)
    {
      // check if group exists
      if (H5Lexists(parent, path, H5P_DEFAULT) > 0)
      {
        reset(H5Gopen2(parent, path, H5P_DEFAULT));
      }
      else
      {
        reset(H5Gcreate2(parent, path, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT));
      }
    }

    attribute::attribute(const location& loc, const char* name)
      : object(H5Aopen(loc, name, H5P_DEFAULT))
    {
    }

    attribute::attribute(
      const location& loc, const char* name, hid_t type, const dataspace& space)
      : object(H5Acreate2(loc, name, type, space, H5P_DEFAULT, H5P_DEFAULT))
    {
    }

    std::vector<hsize_t> dataspace::shape() const
    {
      std::vector<hsize_t> res(ndims());
      H5Sget_simple_extent_dims(get(), res.data(), nullptr);
      return res;
    }

    void dataspace::select_hyperslab(const hsize_t* start, const hsize_t* count,
      const hsize_t* stride, const hsize_t* block, H5S_seloper_t op)
    {
      H5Sselect_hyperslab(get(), op, start, stride, count, block);
    }

    dataset::dataset(const location& parent, const char* path)
    {
      reset(H5Dopen2(parent, path, H5P_DEFAULT));
    }

    dataset::dataset(
      const location& parent, const char* path, const plist& access)
    {
      reset(H5Dopen2(parent, path, access));
    }

    dataset::dataset(const location& parent, const char* path, hid_t type,
      const dataspace& space)
    {
      reset(H5Dcreate2(
        parent, path, type, space, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT));
    }
  } // namespace hdf5
} // namespace dg
