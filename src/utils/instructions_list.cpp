#include "dg/utils/instructions_list.hpp"

#include <algorithm>
#include <numeric>

namespace dg
{
  template <typename T>
  static std::vector<size_t> sort_permutation(const std::vector<T>& rows,
    const std::vector<T>& cols, std::vector<size_t>& p)
  {
    p.resize(rows.size());
    std::iota(p.begin(), p.end(), 0);
    std::sort(p.begin(), p.end(), [&](size_t i, size_t j) {
      if (rows[i] == rows[j])
      {
        return cols[i] < cols[j];
      }
      else
      {
        return rows[i] < rows[j];
      }
    });
    return p;
  }

  template <typename T>
  static void apply_permutation_in_place(
    std::vector<T>& vec, const std::vector<size_t>& p)
  {
    std::vector<bool> done(vec.size());
    for (size_t i = 0; i < vec.size(); ++i)
    {
      if (done[i])
      {
        continue;
      }
      done[i] = true;
      size_t prev_j = i;
      size_t j = p[i];
      while (i != j)
      {
        std::swap(vec[prev_j], vec[j]);
        done[j] = true;
        prev_j = j;
        j = p[j];
      }
    }
  }

  void instructions_list::compress()
  {
    // sort by row then column
    sort_permutation(rows, cols, permute_);
    apply_permutation_in_place(rows, permute_);
    apply_permutation_in_place(cols, permute_);
    apply_permutation_in_place(values, permute_);
    // remove duplicate entries
    size_t row = 0;
    size_t pos = 0;
    for (size_t i = 1; i < rows.size(); ++i)
    {
      if (!(rows[i] == rows[pos] && cols[i] == cols[pos]))
      {
        if((++pos) != i)
        {
          rows[pos] = rows[i];
          cols[pos] = cols[i];
          values[pos] = values[i];
        }
      }
      else
      {
        values[pos] += values[i];
      }
    }
    ++pos;
    rows.erase(rows.begin() + pos, rows.end());
    cols.erase(cols.begin() + pos, cols.end());
    values.erase(values.begin() + pos, values.end());
  }

  PetscErrorCode instructions_list::apply(Mat mat, InsertMode mode)
  {
    compress();
    // apply MatSetValues row by row
    auto iter = rows.begin();
    PetscErrorCode res = 0;
    while (!res && iter != rows.end())
    {
      auto stop = std::upper_bound(iter, rows.end(), *iter);
      res = MatSetValues(mat, 1, &(*iter), stop - iter,
        &cols[iter - rows.begin()], &values[iter - rows.begin()], mode);
      iter = stop;
    }
    return res;
  }
}
