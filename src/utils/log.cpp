#include "dg/utils/log.hpp"

#include <boost/log/expressions.hpp>
#include <boost/log/attributes.hpp>
#include <boost/log/utility/setup.hpp>
#include <boost/log/support/date_time.hpp>

namespace dg
{
  namespace log
  {
    static const char* to_string(severity_level lvl)
    {
      const char* str = nullptr;
      switch (lvl)
      {
      case severity_level::trace:
        str = "TRACE";
        break;
      case severity_level::debug:
        str = "DEBUG";
        break;
      case severity_level::info:
        str = "INFO";
        break;
      case severity_level::warning:
        str = "WARNING";
        break;
      case severity_level::error:
        str = "ERROR";
        break;
      case severity_level::fatal:
        str = "FATAL";
        break;
      case severity_level::quiet:
        str = "QUIET";
        break;
      default:
        break;
      }
      return str;
    }

    //! Outputs stringized representation of the severity level to the stream
    template <typename CharT, typename TraitsT>
    inline std::basic_ostream<CharT, TraitsT>& operator<<(
      std::basic_ostream<CharT, TraitsT>& strm, dg::log::severity_level lvl)
    {
      const char* str = dg::log::to_string(lvl);
      if (str)
      {
        strm << str;
      }
      else
      {
        strm << static_cast<int>(lvl);
      }
      return strm;
    }

    logger::logger(
      severity_level strm_sev, severity_level log_sev, const std::string& fname)
    {
      namespace expr = boost::log::expressions;

      auto global = boost::log::core::get();

      static auto timestamp = global->add_global_attribute(
        "TimeStamp", boost::log::attributes::local_clock());

      const auto format =
        (expr::stream
          << expr::if_(expr::attr<bool>("RawMsg") == false)[(
               expr::stream
               << "[" << expr::attr<severity_level>("Severity") << "] "
               << expr::if_(expr::has_attr("Trace"))
                    [expr::stream << "[" << expr::attr<std::string>("Trace")
                                  << "] "]
               << "[" << expr::format_date_time<boost::posix_time::ptime>(
                           "TimeStamp", "%Y-%m-%d, %H:%M:%S.%f")
               << "] : ")]
          << expr::message);

      if (!fname.empty())
      {
        log_file =
          boost::log::add_file_log(boost::log::keywords::format = format,
            boost::log::keywords::filter =
              (expr::attr<severity_level>("Severity") >= log_sev),
            boost::log::keywords::file_name = fname);
      }

      term_strm = boost::log::add_console_log(std::clog,
        boost::log::keywords::format = format,
        boost::log::keywords::filter =
          (expr::attr<severity_level>("Severity") >= strm_sev));
    }

    logger::~logger()
    {
      boost::log::core::get()->remove_sink(log_file);
      boost::log::core::get()->remove_sink(term_strm);
    }
  }
}
