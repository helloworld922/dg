# Hybridizable Discontinuous Galerkin (HDG) {#hdg}

Consider the advection-diffusion-reaction (ADR) system

\f{gather}{
S_i + \partial_{j}\left(F_{ij} - D_{ijkl} \sigma_{kl} \right) = 0\\
\sigma_{ij} - \partial_{j} G_{i} = 0\\
  \left(F_{ij} n_{j} - D_{ijkl} \sigma_{kl} n_{j}\right)_{\epsilon^{N}} = g^{N}_{i}\\
  \left(q_{i}\right)_{\epsilon^{D}} = g^{D}_{i}
\f}

An HDG formulation seeks to find a solution (TODO: implement llbracket/rrbracket and link to a local mathjax install)

\f{align}{
\int \left(
  v_{i} S_i - \left(
  F_{ij} - D_{ijkl} \sigma_{kl} \right) \partial_{j} v_{i}
\right) dV
  + \oint
  \left(
    \mu_i (F_{ij} n_{j} - D_{ijkl} \sigma_{kl} n_{j})^{*}
  \right) dS &= 0 & \forall \vec{v},\vec{\mu} &\in \vec{V},\vec{M}\\
  \int \left(
     v_{i} \sigma_{ij} + G_{i} \partial_{j} v_{i}
  \right) dV
  - \oint
  \left(
    \mu_{i} (G_{i} n_{j})^*
  \right) dS
  &= 0 & \forall \vec{v}, \vec{\mu} &\in \vec{V}, \vec{M}\\
  \int_{\epsilon}
  \left[
  (F_{ij} n_{j} - D_{ijkl} \sigma_{kl} n_{j})^{*}
  \right] \mu_{i} dS
  + \int_{\epsilon^{N}} (F_{ij} n_{j} - D_{ijkl} \sigma_{kl} n_{j})^{*} \mu_{i} dS
  &= \int_{\epsilon^{N}} g^{N}_{i} \mu_{i} dS & \forall \vec{\mu} &\in \vec{M}
\f}

The Jacobian of this entire system can be written in matrix form as

\f{gather}{
  \begin{bmatrix}
    A & B\\
    C & D
  \end{bmatrix}
  \begin{bmatrix}
    x\\
    \lambda
  \end{bmatrix}
  =
  \begin{bmatrix}
    r\\
    s
  \end{bmatrix}
\f}

Where \f$x\f$ and \f$r\f$ are defined inside elements, and \f$\lambda\f$ and \f$s\f$ are defined on element boundaries. A key important thing to note is that \f$A\f$ is block-diagonal, thus it can efficiently be inverted. This means a Schur decomposition approach can be implemented efficiently.

\f{gather}{
  \begin{bmatrix}
  A & 0\\
  0 & D - C A^{-1} B
  \end{bmatrix} \begin{bmatrix}
    x\\
    \lambda
  \end{bmatrix}
  =
  \begin{bmatrix}
    r - B \lambda\\
    s - C A^{-1} r
  \end{bmatrix}
\f}

The solve process then proceeds in two steps:

1. Solve for \f$\lambda\f$ using the Schur complement and modified RHS.
2. Use the solution \f$\lambda\f$ to modify the RHS and solve for \f$x\f$.

The only global linear solve required is on the sparse Schur Complement system to find \f$\lambda\f$. Modifications to non-linear systems would still perform this two step linear solve process, but what is being solved for (\f$x\f$ and \f$\lambda\f$) are the "search direction vectors" of a Newton-Raphson method. This can be complemented with a line search and repeated iterations to solve the non-linear system.

Local diagonal blocks of \f$A\f$ are assumed to be dense. Local blocks of \f$B\f$ may be sparse, however for now the requirements of the local solver make storing it in dense form advantageous.
Local blocks of \f$C\f$ are sparse, and \f$D\f$ is globally sparse.

## Implementation Notes (unstructured)

1. Metis/ParMetis partitions the elements into patches.
2. Classify faces as completely internal to a patch, on a domain boundary, or shared between patches.
  - Internal and domain boundary faces are owned locally. TODO: determine some way to figure out who "owns" faces shared between patches?
3. Create local storage for unknowns, one for q on elements, and one for lambda on faces (including those shared, need to move shared faces to end, with locally owned ones first).
  - Is there some efficient way to use VecScatter for shared faces?
4. VecCreateNest to create a global Petsc vector of unique unknowns
5. Do something similar to 3-4 for full RHS? Also need a smaller RHS for the Schur complement solve.
6. Create specialized matrix class for \f$\Gamma\f$ and the Schur complement. TODO: how to deal with overlapping data dependencies writing to Schur complement/RHS?
7. Create KSP and SNESLineSearch for the solver.

- VecScatter might be more useful than trying to create Vec's with ghosts, though this might require an absurd number of sub-communicators... maybe manual MPI commands?
- Preconditioners?
- Linesearch and search direction vector? SNESLineSearchCreate?