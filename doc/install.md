# Install instructions

## Building from source

### Dependencies

- C++14 compatible compiler
- [CMake 3.1+](http://www.cmake.org/)
- [Git](https://git-scm.com/)
- [optional] [Doxygen](http://www.doxygen.nl/): for building the documentation.
- [optional] [Google Test](https://code.google.com/p/googletest/): for building unit tests. Note: because of the [special requirements](https://code.google.com/p/googletest/wiki/FAQ#Why_is_it_not_recommended_to_install_a_pre-compiled_copy_of_Goog) for how Google Test should be built, this is included as a submodule.
- [optional] [GCov](https://gcc.gnu.org/onlinedocs/gcc/Gcov.html): for generating test coverage results.
- [optional] PThreads: used by Google Test to build thread-safe tests. At the moment this is not required. Enabling `gtest_disable_pthreads` removes the PThreads dependency.
- [Eigen3](http://eigen.tuxfamily.org/index.php?title=Main_Page)
- [Boost 1.62+](http://www.boost.org/) (note: previous versions have bugs with self-assignment of multi-precision values.)
- [GNU MPFR](http://www.mpfr.org/)
- [HDF5](https://support.hdfgroup.org/HDF5/)
- [MPI](https://www.open-mpi.org/): Any version of in theory should work
- [ParMETIS 4.0.3](http://glaros.dtc.umn.edu/gkhome/metis/parmetis/overview)
- [pkg-config](https://www.freedesktop.org/wiki/Software/pkg-config/)
- [PETSc](https://www.mcs.anl.gov/petsc/)

### Basic Building

It is highly recommended to [build out-of-source](http://voices.canonical.com/jussi.pakkanen/2013/04/16/why-you-should-consider-using-separate-build-directories/). This project uses CMake to configure. Important parameters:

- `CMAKE_BUILD_TYPE`: Which build configuration to use (Release, Debug, etc.). Defaults to Debug build.
- `CMAKE_CXX_FLAGS`: Additional C++ compiler flags
- `CMAKE_INSTALL_PREFIX`: Where to install project to
- `BUILD_DOCUMENTATION`: Whether doxygen documentation should be built
- `CMAKE_LIBRARY_PATH`: List of additional directories to try and find libraries in
- `CMAKE_PREFIX_PATH`: List of additional prefix directories to try and find dependencies in
- `BUILD_EXAMPLES`: Whether example should be built
- `DG_BUILD_TESTING`: Whether tests should be built
- `BUILD_COVERAGE`: Whether tests should be built with code coverage output
- `DG_INTERNAL_DOCS`: Build internal docs useful for developers.

Example configuration for Unix:

~~~{.sh}
# Assuming root project directory is PROJ_ROOT:
mkdir ${PROJ_ROOT}/../build
cd ${PROJ_ROOT}/../build
cmake ${PROJ_ROOT}
make

# run tests
make test

# install project (currently not necessary for testing)
make install
~~~

Additional info on using CMake can be found on their [Wiki](http://www.cmake.org/Wiki/CMake).

### IDE Project Generation

CMake is capable of producing projects for various common IDE's, notably
[XCode](https://developer.apple.com/xcode/) ([see here for tutorial](http://outcoldman.com/en/archive/2014/05/02/xcode-and-cmake/)) and
[Visual Studio](https://www.visualstudio.com/) ([see here for tutorial](https://cognitivewaves.wordpress.com/cmake-and-visual-studio/)).
