# Discontinuous Galerkin (DG) {#dg_method}

The DG helpers implement the machinery to discretize a coupled system of first order PDE's. It is capable of handling PDE's in conservation or non-conservation form.

## Conservation Form

Consider the conservation form advection-diffusion-reaction system
\f{gather}{
  \partial_{j} F_{ij} - S_i = 0\\
  \sigma_{ij} - \partial_{j} G_i = 0
\f}

where \f$S_i\f$ and \f$G_i\f$ are functions of the conserved variables \f$q_i\f$, position \f$x_i\f$, time \f$t\f$, and any temporal derivatives. Notably they cannot contain any direct spatial derivatives or any component of \f$\sigma_{ij}\f$. \f$F_{ij}\f$ is a function of the conserved variables \f$q_i\f$,
the auxiliary gradient variables \f$\sigma_{ij}\f$, position \f$x_i\f$, time \f$t\f$, and any temporal derivatives. Similar to \f$S_i\f$, \f$F_{ij}\f$ cannot contain any direct spatial derivatives.

For example, the linear advection system has \f$S = -\partial_t q\f$ and \f$F_{j} = a_j q\f$, producing the system
\f{gather}{
\partial_t q + \partial_j \left(a_j q\right) = 0
\f}
The linear diffusion equation uses \f$S = -\partial_t q\f$, \f$F_{j} = - \kappa \sigma_{ij}\f$, and \f$G = q\f$.

Integrating over test functions \f$\phi\f$ gives the semi-discrete form
\f{gather}{
\oint_{\partial K} (F_{ij} n_j)^* \phi_i dS - \int_K \left(\phi_i S_i + F_{ij} \partial_j \phi_i\right) dV = 0\\
\int_K \left(\phi_{ij} \sigma_{ij} + q_i \partial_j \phi_{ij}\right) dV - \oint_{\partial K} (G_i n_j)^* \phi_{ij} dS = 0
\f}

There are 3 types of terms the helpers implement for conservation form systems: internal flux, numerical flux, and source terms. The helpers accumulate the individual operators into a residual. The chosen sign conventions are:

1. Internal flux terms are added: \f$R_K \leftarrow R_K + \int_K F_{ij} \partial_j \phi_i dV\f$
2. Numerical flux terms are subtracted: \f$R_K \leftarrow R_K - \oint_{\partial K} (F_{ij} n_j)^* \phi_i dV\f$
3. Source terms are added: \f$R_K \leftarrow R_K + \int_K S_i \phi_i dV\f$

There are two primary usages for these helpers: in implementing implicit time stepping, and implementing explicit time stepping. ImEx methods can be implemented using a combination of these two base methods. PDE's without any time derivatives are implemented using similar techniques to implicit time stepping.

### Explicit Time Stepping

Explicit time stepping can be implemented by re-writing the discretized form with only the time derivative term on the LHS,
\f{gather}{
\int_K \phi_i \partial_t q_i dV = \int_K \left(\phi_i S'_i + F_{ij} \partial_j \phi_i\right) dV - \oint_{\partial K} (F_{ij} n_j)^* \phi_i dS
\f}

where \f$S'\f$ are the remaining non time dependent source terms. Note that all terms on the RHS are the same sign convention of the helpers, so they can be used directly.

The general strategy for implementing an explicit method are then:

- Use the helpers for the RHS terms
- Apply the time step advance \f$q^{(n+1)}_i \leftarrow q^{(n)}_i + a^{(n)} \Delta t R_{i}(t + c^{(n)} \Delta t)\f$

For the cases implementing higher order derivatives using traditional DG (such as LDG or interior penalty), the auxiliary equation would perform a similar re-ordering,
\f{gather}{
\int_K \phi_{ij} \sigma_{ij} dV = \oint_{\partial K} (G_i n_j)^* \phi_{ij} dS - \int_K \phi_{ij} G_i \partial_j \phi_{ij} dV
\f}

The RHS in this case has the opposite sign convention as the helpers. Directly using the helpers with unmodified systems would result in computing \f$-\sigma_{ij}\f$. This can be adjusted for in the system directly, or by negating the result afterwards.

### Implicit Time Stepping

Implicit time stepping is designed to work with Newton-Raphson style non-linear solvers, utilizing an optional analytical Jacobian. This has the form

\f{gather}{
J^{(n)} \frac{\Delta \vec{x}^{(n+1)}}{\alpha^{(n+1)}} + \vec{R}(\vec{x}^{(n)}) = 0\\
J^{(n)}_{ij} = \frac{\partial R_{i}}{\partial x_{j}}
\f}

where \f$\vec{x}^{(n)}\f$ is the previous guess for the solution of all unknowns, \f$\vec{R}\f$ is the residual of the discretized form, \f$\alpha^{(n+1)} \in (0, 1]\f$ is an optional line search step size, and \f$J^{(n)}\f$ is the analytical Jacobian. A key thing to note is that if we want to form a traditional linear system \f$A \vec{x} = \vec{b}\f$, the residual needs to be negated. Thus we have

\f{gather}{
\vec{b} = -\vec{R}^{(n)} =
\begin{bmatrix}
\int_K \left(\phi_i S_i + F_{ij} \partial_j \phi_i\right) dV - \oint_{\partial K} (F_{ij} n_j)^* \phi_i dS\\
\oint_{\partial K} (G_i n_j)^* \phi_{ij} dS - \int_K \left(\phi_{ij} \sigma_{ij} + q_i \partial_j \phi_{ij}\right) dV
\end{bmatrix}
\f}

Notice that for the primary PDE, the sign conventions again match that of the helpers. Thus, the same system helpers can be used for explicit and implicit time stepping without modification.
Similarly, the auxiliary equation signs are again negated.

The helpers for computing the analytical Jacobian assumes the system is responsible for taking care of all signs; they are passed in the raw mass, advection, or lift matrix.

## Non-Conservation Form

TODO
