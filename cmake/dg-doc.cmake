find_package(Doxygen 1.8)
option(BUILD_DOCUMENTATION "Create and install the HTML based documentation (requires Doxygen)" DOXYGEN_FOUND)
if(BUILD_DOCUMENTATION)
  if(NOT DOXYGEN_FOUND)
    message(FATAL_ERROR "Doxygen is needed to build the documentation.")
  endif()

  option(DG_INTERNAL_DOCS "Build internal docs" No)

  set(doxyfile "${CMAKE_CURRENT_BINARY_DIR}/doxyfile")
  if(DG_INTERNAL_DOCS)
    set(DG_DOXY_INTERNAL_DOCS "YES")
  else()
    set(DG_DOXY_INTERNAL_DOCS "NO")
  endif()
  configure_file("${CMAKE_CURRENT_SOURCE_DIR}/doxyfile.in"
    ${doxyfile} @ONLY)
  configure_file("${CMAKE_CURRENT_SOURCE_DIR}/doxylayout.in.xml"
    "${CMAKE_CURRENT_BINARY_DIR}/doxylayout.xml" COPYONLY)
  add_custom_target(DG_DOC
    COMMAND ${DOXYGEN_EXECUTABLE} ${doxyfile}
    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
    COMMENT "Generating documentation with Doxygen"
    VERBATIM)
  install(DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}"
    DESTINATION "${DOC_INSTALL_DIR}/${PROJECT_NAME}"
    COMPONENT docs
    PATTERN "html/*"
    )
  install(FILES "${CMAKE_CURRENT_SOURCE_DIR}/LICENSE.md"
    DESTINATION "${DOC_INSTALL_DIR}/${PROJECT_NAME}"
    )
endif()
