find_package(Threads REQUIRED)

find_package(MPFR REQUIRED)
include_directories(SYSTEM "${MPFR_INCLUDES}")

list(APPEND DG_LINK_LIBS "${MPFR_LIBRARIES}")

find_package(Boost REQUIRED COMPONENTS log system date_time log_setup filesystem)
list(APPEND DG_LINK_LIBS "${Boost_LIBRARIES}")
include_directories(SYSTEM "${Boost_INCLUDE_DIRS}")
#add_definitions(-DBOOST_LOG_DYN_LINK)

find_package(MPI REQUIRED)
list(APPEND DG_LINK_LIBS "${MPI_LIBRARIES}")
include_directories(SYSTEM "${MPI_C_INCLUDE_PATH}")
include_directories(SYSTEM "${MPI_CXX_INCLUDE_PATH}")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${MPI_C_COMPILE_FLAGS}")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${MPI_CXX_COMPILE_FLAGS}")

find_package(HDF5 REQUIRED)
list(APPEND DG_LINK_LIBS "${HDF5_C_LIBRARIES}")
list(APPEND DG_LINK_LIBS "${HDF5_C_LIBRARY}")
include_directories(SYSTEM "${HDF5_INCLUDE_DIRS}")
include_directories(SYSTEM "${HDF5_INCLUDE_DIR}")

find_package(ParMETIS 4.0.3 REQUIRED)
list(APPEND DG_LINK_LIBS "${ParMETIS_LIBRARIES}")
include_directories(SYSTEM "${ParMETIS_INCLUDE_DIRS}")

find_package(PkgConfig REQUIRED)

find_package(Eigen3 REQUIRED)
include("${EIGEN3_USE_FILE}")

pkg_check_modules(PETSC PETSc REQUIRED)
include_directories(SYSTEM ${PETSC_INCLUDE_DIRS})
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${PETSC_CFLAGS_OTHER}")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${PETSC_CFLAGS_OTHER}")
list(APPEND DG_LINK_LIBS "${PETSC_LIBRARIES}")

link_directories("${PETSC_LIBRARY_DIRS}")
link_directories("${Boost_LIBRARY_DIRS}")

message(STATUS "Linking with: ${DG_LINK_LIBS}")
