option(DG_BUILD_TESTING "Build Tests" ON)

# add google test subdir
add_subdirectory(deps/tutils EXCLUDE_FROM_ALL)

##
# @brief Add a python script as a test.
# @param name
# @param script test script
# @param extra_args extra arguments to pass to the executable
function(PY_ADD_TESTS name script extra_args)
  add_test(NAME ${name}
    COMMAND "${PYTHON_EXECUTABLE}" "${script}"
    WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}")
  message(STATUS "Running Py test in ${CMAKE_CURRENT_BINARY_DIR}")
endfunction()

# determine if we can build coverage
function(detect_coverage _cxx_flags _support)
  set(${_support} "NO" PARENT_SCOPE)
  if(NOT CMAKE_COMPILER_IS_GNUCXX)
    # not gcc, check if clang 3.0.0 or greater
    if(${CMAKE_CXX_COMPILER_ID} STREQUAL "Clang" AND NOT ${CMAKE_CXX_COMPILER_VERSION} LESS "3.0.0")
      set(${_cxx_flags} "--coverage -Xclang -coverage-cfg-checksum -Xclang -coverage-no-function-names-in-data" PARENT_SCOPE)
    else()
      message(STATUS "${CMAKE_CXX_COMPILER_ID} ${CMAKE_CXX_COMPILER_VERSION} does not support code coverage.")
      return()
    endif()
  else()
    set(${_cxx_flags} "--coverage" PARENT_SCOPE)
  endif()
  set(${_support} "YES" PARENT_SCOPE)
  message(STATUS "${CMAKE_CXX_COMPILER_ID} ${CMAKE_CXX_COMPILER_VERSION} supports code coverage.")
endfunction()

detect_coverage(CMAKE_CXX_COVERAGE_FLAGS SUPPORT_COVERAGE)

CMAKE_DEPENDENT_OPTION(BUILD_COVERAGE "Build test code coverage" Off
  "SUPPORT_COVERAGE" Off)

if(DG_BUILD_TESTING)
  enable_testing()
  # configure ctest custom options
  configure_file("${PROJECT_SOURCE_DIR}/CTestCustom.cmake"
    "${PROJECT_BINARY_DIR}/CTestCustom.cmake")
  include(CTest)

  if(BUILD_COVERAGE)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${CMAKE_CXX_COVERAGE_FLAGS}")
  endif()
  
  # TODO: some workaround for this?
  include_directories(deps/tutils/include)
  include_directories(deps/tutils/deps/googletest/googletest/include)
  link_directories(${CMAKE_CURRENT_BINARY_DIR}/deps/tutils/deps/googletest)
  
  add_subdirectory(${PROJECT_SOURCE_DIR}/test)
endif()
