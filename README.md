# About

DG is a C++ library for working with Discontinuous Galkerin and Hybridizable Discontinuous Galkerin methods.

It supports handling conservation form and non-conservation form PDE's.

# Install

See [this page](doc/install.md) for build/install instructions.

# Contributing

TODO

# License

DG is not [licensed](LICENSE.md) for public release yet.

