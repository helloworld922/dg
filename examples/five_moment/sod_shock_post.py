import h5py
from numpy import *
from matplotlib.pyplot import *

def plot_dg(xs, ys, degree, *args, **kwargs):
    nelems = len(xs) // (degree + 1)
    x = zeros(len(xs) + nelems-1)
    y = zeros(len(ys) + nelems-1)
    for i in range(degree+1):
        x[i::degree+2] = xs[i::degree+1]
        y[i::degree+2] = ys[i::degree+1]
    x[degree+1::degree+2] = nan
    y[degree+1::degree+2] = nan
    plot(x, y, *args, **kwargs)

with h5py.File('sod_shock_explicit.h5','r') as f:
    xs = f['mesh'][:].flatten()
    degree = f['0']['rho'].shape[1] - 1
    
    figure()
    for frame in range(0,101,10):
        if(str(frame) in f):
            rho = f[str(frame)]['rho'][:].flatten()
            plot_dg(xs, rho, degree)
    grid(True)
    #ylim([0,2.5])
    tight_layout()
