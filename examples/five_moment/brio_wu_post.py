import h5py
from numpy import *
from matplotlib.pyplot import *
import matplotlib.animation as animation

end_frame = 1001

def make_dg(v, degree, res=None):
    nelems = len(v) // (degree + 1)
    if(res is None):
        res = zeros(len(v)+nelems-1)
    for i in range(degree+1):
        res[i::degree+2] = v[i::degree+1]
    res[degree+1::degree+2] = nan
    return res

def extract_comp(dset, comp):
    return dset[comp][:]

ffmpeg_writer = animation.writers['ffmpeg']

with h5py.File('tf_brio_wu_erk1.h5','r') as f:
    ioff()
    xs = f['mesh'][:].flatten()
    degree = f['0']['elec rho'].shape[1] - 1

    comps = ['ion rho', 'elec rho', 'ion px', 'elec px', 'Ex']
    funs = [lambda dset: extract_comp(dset, 'ion rho'),
            lambda dset: extract_comp(dset, 'elec rho'),
            lambda dset: extract_comp(dset, 'ion px'),
            lambda dset: extract_comp(dset, 'elec px'),
            lambda dset: extract_comp(dset, 'Ex')]

    comps = ['ion rho','elec rho','Ex']
    funs = [lambda dset: extract_comp(dset, 'ion rho'),
            lambda dset: extract_comp(dset, 'elec rho'),
            lambda dset: extract_comp(dset, 'Ex')]
    ylims = [None,]*3#[0.999,1.001],[-0.02,0.02]]

    xs = make_dg(xs, degree)
    ys = None

    for c,fun,yl in zip(comps,funs,ylims):
        metadata = dict(title='tf_brio_wu_%s'%c)
        writer = ffmpeg_writer(fps=60, metadata=metadata, bitrate=6000)
        dpi = 150
        fig = figure(dpi=dpi)
        line = None

        with writer.saving(fig, 'tf_brio_wu_%s.mp4'%c, dpi):
            for frame in range(end_frame):
                if(str(frame) in f):
                    print(frame)
                    v = fun(f[str(frame)]).flatten()
                    ys = make_dg(v, degree, ys)
                    if(line is None):
                        line = plot(xs, ys)[0]
                        grid(True)
                        #xlim([0, 1])
                        if(yl is not None):
                            ylim(yl)
                        else:
                            minv = amin(v)
                            maxv = amax(v)
                            dv = maxv - minv
                            if(dv < 1e-10):
                                dv = 1
                            ylim([minv-dv, maxv+dv])
                        tight_layout()
                    else:
                        line.set_data(xs, ys)
                    writer.grab_frame()

