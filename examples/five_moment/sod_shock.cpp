///
/// @file sod_shock.cpp
/// @brief This example implements a basic 1D Sod shock tube.
///

#include "dg/ndg/rect/rect_1d.hpp"

#include "dg/eqsets/five_moment/euler_rusanov.hpp"

#include "dg/context.hpp"
#include "dg/utils/hslab.hpp"
#include "dg/utils/hdf5_utils.hpp"
#include "dg/utils/view_utils.hpp"
#include "dg/utils/log.hpp"

#include "dg/io/structured/structured_1d.hpp"

#include <array>
#include <functional>
#include <cmath>
#include <vector>

///
/// @brief Initializes a shock tube
/// @param q vector of unknowns with 5 components
/// @param shock_pos what element is the first element of the right state
/// @param gamma gas gamma
/// @param rho_L
/// @param rho_R
/// @param u_L
/// @param u_R
/// @param PL
/// @param PR
///
template <class V>
void init_shock(V& q, size_t shock_pos, double gamma, double rho_L = 1,
  double rho_R = 4, double u_L = 0, double u_R = 0, double PL = 1,
  double PR = 4)
{
  for (size_t elem = 0; elem < shock_pos; ++elem)
  {
    for (size_t node = 0; node < q.extent(1); ++node)
    {
      q(elem, node, 0) = rho_L;
      q(elem, node, 1) = rho_L * u_L;
      q(elem, node, 2) = 0;
      q(elem, node, 3) = 0;
      q(elem, node, 4) = PL / (gamma - 1) + 0.5 * rho_L * u_L * u_L;
    }
  }

  for (size_t elem = shock_pos; elem < q.extent(0); ++elem)
  {
    for (size_t node = 0; node < q.extent(1); ++node)
    {
      q(elem, node, 0) = rho_R;
      q(elem, node, 1) = rho_R * u_R;
      q(elem, node, 2) = 0;
      q(elem, node, 3) = 0;
      q(elem, node, 4) = PR / (gamma - 1) + 0.5 * rho_R * u_R * u_R;
    }
  }
}

///
/// @brief Uses forwards Euler time stepping to solve a 1D Euler fluid system
///
template <size_t Degree>
void erk1(dg::hslab<double, 3>& q0, double gamma, double xmin, double xmax,
  double dt, size_t nout, size_t writeout_period, dg::log::logger& log,
  double rho_L, double rho_R, double PL, double PR)
{
  DG_LOG(log, info) << "Constructing DG matrices...";

  auto nelems = q0.extent(0);
  auto dx = (xmax - xmin) / nelems;
  auto jac = 0.5 * dx;
  dg::ndg::rect<Degree> discr;

  // calculate advection matrix
  Eigen::Matrix<double, Degree + 1, Degree + 1> mass_matrix;
  discr.calc_mass_matrix(mass_matrix, jac);
  Eigen::Matrix<double, Degree + 1, Degree + 1> amatrix = mass_matrix.inverse();

  std::array<dg::shslab<double, Degree + 1, 2>, 2> lift_matrix = {
    dg::shslab<double, Degree + 1, 2>(1, Degree + 1),
    dg::shslab<double, Degree + 1, 2>(1, Degree + 1)};

  discr.calc_lift_matrix(lift_matrix, amatrix, 1, 1);

  Eigen::Matrix<double, Degree + 1, Degree + 1> cadvect_matrix;
  discr.calc_advect_matrix(cadvect_matrix);

  amatrix *= cadvect_matrix;
  // Eigen's default layout is left
  dg::view<double, 2, dg::layout_left> advect_matrix(
    amatrix.data(), Degree + 1, Degree + 1);

  // write out mesh, initial conditions
  DG_LOG(log, info) << "Writing out mesh and initial conditions...";
  dg::eqsets::euler_rusanov<1, double> sys;
  sys.gamma = gamma;
  dg::hdf5::file f("sod_shock_explicit.h5", H5F_ACC_TRUNC);
  dg::io::structured<1> out(f);
  out.write_uniform_mesh<Degree>(xmin, xmax, nelems);
  std::vector<std::pair<std::string, size_t>> components = {
    {"rho", 0}, {"px", 1}, {"py", 2}, {"pz", 3}, {"e", 4}};
  out.write_solution(0, 0, q0, nelems, 0, components);

  // initialize working buffer for q1
  dg::hslab<double, 3> q1(q0.shape());

  // boundary conditions
  dg::shslab<double, 5, 2> qleft(1, 5);
  qleft(0, 0) = rho_L;
  qleft(0, 1) = qleft(0, 2) = qleft(0, 3) = 0;
  qleft(0, 4) = PL / (sys.gamma - 1);

  dg::shslab<double, 5, 2> qright(1, 5);
  qright(0, 0) = rho_R;
  qright(0, 1) = qright(0, 2) = qright(0, 3) = 0;
  qright(0, 4) = PR / (sys.gamma - 1);

  DG_LOG(log, info) << "Simulation start";
  for (size_t frame = 0; frame < nout; ++frame)
  {
    DG_LOG(log, info) << "frame " << frame + 1 << ", t = ["
                      << frame * writeout_period * dt << ", "
                      << (frame + 1) * writeout_period * dt << "]";
    for (size_t step = 0; step < writeout_period; ++step)
    {
      // zero out RHS
      dg::util::fill(q1, 0);
      // iterate through every element
      for (size_t elem = 0; elem < nelems; ++elem)
      {
        auto q = q0.subarray(elem, dg::all{}, dg::all{});
        auto r = q1.subarray(elem, dg::all{}, dg::all{});

        // internal flux
        discr.int_flux(q, r, advect_matrix, sys);
        // numerical flux
        // left face
        {
          std::array<size_t, 1> idcs_in = {0};
          if (elem == 0)
          {
            std::array<size_t, 1> idcs_out = {0};
            // boundary condition
            discr.num_flux(
              qleft, q, r, -1, idcs_out, idcs_in, lift_matrix[0], sys);
          }
          else
          {
            std::array<size_t, 1> idcs_out = {Degree};
            discr.num_flux(q0.subarray(elem - 1, dg::all{}, dg::all{}), q, r,
              -1, idcs_out, idcs_in, lift_matrix[0], sys);
          }
        }
        // right face
        {
          std::array<size_t, 1> idcs_in = {Degree};
          std::array<size_t, 1> idcs_out = {0};
          if (elem == q0.extent(0) - 1)
          {
            // boundary condition
            discr.num_flux(
              qright, q, r, 1, idcs_out, idcs_in, lift_matrix[1], sys);
          }
          else
          {
            discr.num_flux(q0.subarray(elem + 1, dg::all{}, dg::all{}), q, r, 1,
              idcs_out, idcs_in, lift_matrix[1], sys);
          }
        }
        // time advance
        for (size_t i = 0; i <= Degree; ++i)
        {
          for (size_t comp = 0; comp < 5; ++comp)
          {
            r(i, comp) = q(i, comp) + dt * r(i, comp);
          }
        }
      }
      // swap buffers
      std::swap(q0.data(), q1.data());
    }
    // write out current state of solution
    out.write_solution(
      frame + 1, (frame + 1) * writeout_period * dt, q0, nelems, 0, components);
  }
  DG_LOG(log, info) << "Done";
}

int main(int argc, char** argv)
{
  dg::log::logger log(dg::log::severity_level::info);

  // Gas gamma for diatomic molecules
  constexpr double gamma = 1.4;
  constexpr double rho_L = 1.225;
  constexpr double rho_R = 1.225 * 2;
  constexpr double P_L = 101325;
  constexpr double P_R = 101325 * 2;

  constexpr size_t nelems = 100;
  constexpr size_t Degree = 2;
  constexpr double xmin = 0, xmax = 1;
  constexpr double dx = (xmax - xmin) / nelems;

  double dt =
    1e-7; // 0.025 * dx / (sqrt(gamma * P_R / rho_R) * (2 * Degree + 1));

  // Run sim to a time of about 1e-3
  constexpr size_t nout = 100;
  size_t writeout_period = std::max<size_t>(round(1e-3 / (nout * dt)), 1);
  DG_LOG(log, info) << "dt = " << dt
                    << ", writeout_period = " << writeout_period;

  // initial conditions
  DG_LOG(log, info) << "Applying initial conditions...";
  dg::hslab<double, 3> q(nelems, Degree + 1, 5);
  init_shock(q, nelems / 2, gamma, rho_L, rho_R, 0, 0, P_L, P_R);

  DG_LOG(log, info) << "Using forwards Euler timestepping";
  // do simulation
  erk1<Degree>(q, gamma, xmin, xmax, dt, nout, writeout_period, log, rho_L,
    rho_R, P_L, P_R);

  return 0;
}
