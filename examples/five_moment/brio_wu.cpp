///
/// @file brio_wu.cpp
/// @brief This example implements Langmuir oscillations propogating from a
/// small jump discontinuity using the two-fluid model.
/// It implements parallelism via MPI
///

#include "dg/ndg/rect/rect_1d.hpp"

#include "dg/eqsets/systems/two_fluid.hpp"

#include "dg/context.hpp"
#include "dg/utils/hslab.hpp"
#include "dg/utils/hdf5_utils.hpp"
#include "dg/utils/view_utils.hpp"
#include "dg/utils/log.hpp"

#include "dg/io/structured/structured_1d.hpp"

#include <array>
#include <functional>
#include <cmath>
#include <vector>

#include <fenv.h>

#define EVOLVE_IONS

constexpr size_t rho_i = 0;
constexpr size_t px_i = 1;
constexpr size_t py_i = 2;
constexpr size_t pz_i = 3;
constexpr size_t e_i = 4;
constexpr size_t rho_e = 5;
constexpr size_t px_e = 6;
constexpr size_t py_e = 7;
constexpr size_t pz_e = 8;
constexpr size_t e_e = 9;
constexpr size_t Ex = 10;
constexpr size_t Ey = 11;
constexpr size_t Ez = 12;
constexpr size_t Bx = 13;
constexpr size_t By = 14;
constexpr size_t Bz = 15;

///
/// @brief Initializes a small jump discontinuity
/// @param q vector of unknowns with 16 components
/// @param jump_pos what element is the first element of the right state
/// @param gamma gas gamma
/// @param rho_L
/// @param rho_R
/// @param PL
/// @param PR
///
template <class V>
void initial_conds(V& q, size_t start, size_t jump_pos, double gamma, double Ai,
  double Ae, double n_L = 1, double n_R = 2, double T_L = 1, double T_R = 1,
  double Bx = 1, double By_L = 1, double By_R = -1, double Bz_L = 0,
  double Bz_R = 0)
{
  if (start < jump_pos)
  {
    for (size_t elem = 0;
         elem < std::min<size_t>(jump_pos - start, q.extent(0)); ++elem)
    {
      for (size_t node = 0; node < q.extent(1); ++node)
      {
        q(elem, node, rho_i) = Ai * n_L;
        q(elem, node, px_i) = 0;
        q(elem, node, py_i) = 0;
        q(elem, node, pz_i) = 0;
        q(elem, node, e_i) = n_L * T_L / (gamma - 1);

        q(elem, node, rho_e) = Ae * n_L;
        q(elem, node, px_e) = 0;
        q(elem, node, py_e) = 0;
        q(elem, node, pz_e) = 0;
        q(elem, node, e_e) = n_L * T_L / (gamma - 1);

        for (size_t comp = Ex; comp <= Ez; ++comp)
        {
          q(elem, node, comp) = 0;
        }
        // B field
        q(elem, node, Bx) = Bx;
        q(elem, node, By) = By_L;
        q(elem, node, Bz) = Bz_L;
      }
    }
  }

  for (size_t elem = std::max<size_t>(jump_pos, start);
       elem < q.extent(0) + start; ++elem)
  {
    for (size_t node = 0; node < q.extent(1); ++node)
    {
      q(elem - start, node, rho_i) = Ai * n_R;
      q(elem - start, node, px_i) = 0;
      q(elem - start, node, py_i) = 0;
      q(elem - start, node, pz_i) = 0;
      q(elem - start, node, e_i) = n_R * T_R / (gamma - 1);

      q(elem - start, node, rho_e) = Ae * n_R;
      q(elem - start, node, px_e) = 0;
      q(elem - start, node, py_e) = 0;
      q(elem - start, node, pz_e) = 0;
      q(elem - start, node, e_e) = n_R * T_R / (gamma - 1);

      for (size_t comp = Ex; comp <= Ez; ++comp)
      {
        q(elem - start, node, comp) = 0;
      }
      // B field
      q(elem - start, node, Bx) = Bx;
      q(elem - start, node, By) = By_R;
      q(elem - start, node, Bz) = Bz_R;
    }
  }
}

/// Forwards Euler timestepping
template <size_t Degree>
struct erk1
{
  dg::log::logger& log;
  dg::context& mctx;
  dg::systems::ideal_two_fluid<1> sys;

  size_t rank, nprocs;
  double xmin, xmax;
  size_t nelems;

  // local q0
  dg::hslab<double, 3> q0;

  // ghost cell data
  dg::shslab<double, 16, 2> qleft;
  dg::shslab<double, 16, 2> qright;

  // local q1
  dg::hslab<double, 3> q1;

  dg::ndg::rect<Degree> discr;

  // DG discretization matrices
  Eigen::Matrix<double, Degree + 1, Degree + 1> amatrix;

  dg::view<double, 2, dg::layout_left> advect_matrix;

  std::array<dg::shslab<double, Degree + 1, 2>, 2> lift_matrix;

  double n_L, n_R, T_L, T_R, Bx, By_L, By_R, Bz_L, Bz_R;

  erk1(dg::log::logger& log, dg::context& mctx, double omega_p_norm,
    double skin_depth_norm, double gamma, double Ai, double Zi, double Ae,
    double Ze, double xmin, double xmax, size_t nelems, size_t rank,
    size_t nprocs, double n_L, double n_R, double T_L, double T_R, double Bx,
    double By_L, double By_R, double Bz_L, double Bz_R)
    : log(log), mctx(mctx),
      sys(omega_p_norm, skin_depth_norm, gamma, gamma, Ai, Zi, Ae, Ze),
      rank(rank), nprocs(nprocs), xmin(xmin), xmax(xmax), nelems(nelems),
      q0(nelems * (rank + 1) / nprocs - nelems * rank / nprocs, Degree + 1, 16),
      qleft(1, 16), qright(1, 16),
      q1(nelems * (rank + 1) / nprocs - nelems * rank / nprocs, Degree + 1, 16),
      advect_matrix(nullptr, Degree + 1, Degree + 1),
      lift_matrix({dg::shslab<double, Degree + 1, 2>(1, Degree + 1),
        dg::shslab<double, Degree + 1, 2>(1, Degree + 1)}),
      n_L(n_L), n_R(n_R), T_L(T_L), T_R(T_R), Bx(Bx), By_L(By_L), By_R(By_R),
      Bz_L(Bz_L), Bz_R(Bz_R)
  {
    auto dx = (xmax - xmin) / nelems;
    auto jac = 0.5 * dx;

    DG_LOG(log, info) << "c0: " << sys.msys.c0;
    DG_LOG(log, info) << "ion_fsource: " << sys.ion_fsource.norm_factor;
    DG_LOG(log, info) << "elec_fsource: " << sys.elec_fsource.norm_factor;
    DG_LOG(log, info) << "ion_em_force: " << sys.ion_em_force.norm_factor;
    DG_LOG(log, info) << "elec_em_force: " << sys.elec_em_force.norm_factor;

    DG_LOG(log, info) << "Calculating DG Matrices...";
    // calculate advection matrix
    Eigen::Matrix<double, Degree + 1, Degree + 1> mass_matrix;
    discr.calc_mass_matrix(mass_matrix, jac);
    amatrix = mass_matrix.inverse();

    discr.calc_lift_matrix(lift_matrix, amatrix, 1, 1);

    Eigen::Matrix<double, Degree + 1, Degree + 1> cadvect_matrix;
    discr.calc_advect_matrix(cadvect_matrix);
    amatrix *= cadvect_matrix;
    advect_matrix.data() = amatrix.data();

    if (rank == 0)
    {
      DG_LOG(log, info) << "Applying initial conditions...";
    }
    initial_conds(q0, nelems * rank / nprocs, nelems / 2, gamma, Ai, Ae, n_L,
      n_R, T_L, T_R, Bx, By_L, By_R, Bz_L, Bz_R);

    // left BC
    qleft(0, 0) = Ai * n_L;
    qleft(0, 1) = qleft(0, 2) = qleft(0, 3) = 0;
    qleft(0, 4) = n_L * T_L / (gamma - 1);
    qleft(0, 5) = Ae * n_L;
    qleft(0, 6) = qleft(0, 7) = qleft(0, 8) = 0;
    qleft(0, 9) = n_L * T_L / (gamma - 1);
    for (size_t comp = 10; comp <= 15; ++comp)
    {
      qleft(0, comp) = 0;
    }

    qleft(0, 13) = Bx;
    qleft(0, 14) = By_L;
    qleft(0, 15) = Bz_L;
    // right BC
    qright(0, 0) = Ai * n_R;
    qright(0, 1) = qright(0, 2) = qright(0, 3) = 0;
    qright(0, 4) = n_R * T_R / (gamma - 1);
    qright(0, 5) = Ae * n_R;
    qright(0, 6) = qright(0, 7) = qright(0, 8) = 0;
    qright(0, 9) = n_R * T_R / (gamma - 1);
    for (size_t comp = 10; comp <= 15; ++comp)
    {
      qright(0, comp) = 0;
    }
    qright(0, 13) = Bx;
    qright(0, 14) = By_R;
    qright(0, 15) = Bz_R;
  }

  ///
  /// @brief Sync ghost cell information
  ///
  void sync()
  {
    MPI_Request requests[4] = {
      MPI_REQUEST_NULL, MPI_REQUEST_NULL, MPI_REQUEST_NULL, MPI_REQUEST_NULL};

    size_t num_requests = 0;
    if (rank != 0)
    {
      // sink the left face
      MPI_Isend(&q0(0, 0, 0), 16, dg::mpi::mpi_type<double>::type(), rank - 1,
        1, mctx.comm, &requests[num_requests]);
      MPI_Irecv(&qleft(0, 0), 16, dg::mpi::mpi_type<double>::type(), rank - 1,
        0, mctx.comm, &requests[num_requests + 1]);
      num_requests += 2;
    }
    if (rank != nprocs - 1)
    {
      // sink the right wall
      MPI_Isend(&q0(q0.shape()[0] - 1, Degree, 0), 16,
        dg::mpi::mpi_type<double>::type(), rank + 1, 0, mctx.comm,
        &requests[num_requests]);
      MPI_Irecv(&qright(0, 0), 16, dg::mpi::mpi_type<double>::type(), rank + 1,
        1, mctx.comm, &requests[num_requests + 1]);
      num_requests += 2;
    }
    MPI_Waitall(num_requests, requests, MPI_STATUSES_IGNORE);
  }

  void step(double dt)
  {
    sync();
    // zero out RHS
    dg::util::fill(q1, 0);

    for (size_t elem = 0; elem < q0.shape()[0]; ++elem)
    {
      // internal fluxes
      auto ion_q = q0.subarray(elem, dg::all{}, std::make_tuple(0, 5));
      auto ion_r = q1.subarray(elem, dg::all{}, std::make_tuple(0, 5));

      auto elec_q = q0.subarray(elem, dg::all{}, std::make_tuple(5, 10));
      auto elec_r = q1.subarray(elem, dg::all{}, std::make_tuple(5, 10));

      auto efield_q = q0.subarray(elem, dg::all{}, std::make_tuple(10, 13));
      auto efield_r = q1.subarray(elem, dg::all{}, std::make_tuple(10, 13));

      auto bfield_q = q0.subarray(elem, dg::all{}, std::make_tuple(13, 16));
      auto bfield_r = q1.subarray(elem, dg::all{}, std::make_tuple(13, 16));

      auto fields_q = dg::make_view_sequence(efield_q, bfield_q);
      auto fields_r = dg::make_view_sequence(efield_r, bfield_r);
#ifdef EVOLVE_IONS
      {
        // ions
        discr.int_flux(ion_q, ion_r, advect_matrix, sys.ion_fluid);
      }
#endif
      {
        // electrons
        discr.int_flux(elec_q, elec_r, advect_matrix, sys.elec_fluid);
      }
      {
        // maxwell
        discr.int_flux(fields_q, fields_r, advect_matrix, sys.msys);
      }
      // source terms
      // ion E&M force
#ifdef EVOLVE_IONS
      discr.source(dg::make_view_sequence(ion_q, efield_q, bfield_q),
        ion_r.subarray(dg::all{}, std::make_tuple(1, 5)), sys.ion_em_force);
#endif
      discr.source(dg::make_view_sequence(elec_q, efield_q, bfield_q),
        elec_r.subarray(dg::all{}, std::make_tuple(1, 5)), sys.elec_em_force);

      discr.source(ion_q.subarray(dg::all{}, std::make_tuple(1, 4)), efield_r,
        sys.ion_fsource);
      discr.source(elec_q.subarray(dg::all{}, std::make_tuple(1, 4)), efield_r,
        sys.elec_fsource);

      // numerical fluxes
      // left face
      {
        std::array<size_t, 1> idcs_in = {0};
        if (elem == 0)
        {
          std::array<size_t, 1> idcs_out = {0};
#ifdef EVOLVE_IONS
          // ion fluid
          discr.num_flux(qleft.subarray(dg::all{}, std::make_tuple(0, 5)),
            ion_q, ion_r, -1, idcs_out, idcs_in, lift_matrix[0], sys.ion_fluid);
#endif
          // electron fluid
          discr.num_flux(qleft.subarray(dg::all{}, std::make_tuple(5, 10)),
            elec_q, elec_r, -1, idcs_out, idcs_in, lift_matrix[0],
            sys.elec_fluid);
          // maxwell fields
          discr.num_flux(dg::make_view_sequence(
                           qleft.subarray(dg::all{}, std::make_tuple(10, 13)),
                           qleft.subarray(dg::all{}, std::make_tuple(13, 16))),
            fields_q, fields_r, -1, idcs_out, idcs_in, lift_matrix[0],
            sys.msys);
        }
        else
        {
          std::array<size_t, 1> idcs_out = {Degree};
#ifdef EVOLVE_IONS
          // ion fluid
          discr.num_flux(
            q0.subarray(elem - 1, dg::all{}, std::make_tuple(0, 5)), ion_q,
            ion_r, -1, idcs_out, idcs_in, lift_matrix[0], sys.ion_fluid);
#endif
          // electron fluid
          discr.num_flux(
            q0.subarray(elem - 1, dg::all{}, std::make_tuple(5, 10)), elec_q,
            elec_r, -1, idcs_out, idcs_in, lift_matrix[0], sys.elec_fluid);
          // maxwell fields
          discr.num_flux(
            dg::make_view_sequence(
              q0.subarray(elem - 1, dg::all{}, std::make_tuple(10, 13)),
              q0.subarray(elem - 1, dg::all{}, std::make_tuple(13, 16))),
            fields_q, fields_r, -1, idcs_out, idcs_in, lift_matrix[0],
            sys.msys);
        }
      }
      // right face
      {
        std::array<size_t, 1> idcs_in = {Degree};
        std::array<size_t, 1> idcs_out = {0};
        if (elem == q0.shape()[0] - 1)
        {
#ifdef EVOLVE_IONS
          // ion fluid
          discr.num_flux(qright.subarray(dg::all{}, std::make_tuple(0, 5)),
            ion_q, ion_r, 1, idcs_out, idcs_in, lift_matrix[1], sys.ion_fluid);
#endif
          // electron fluid
          discr.num_flux(qright.subarray(dg::all{}, std::make_tuple(5, 10)),
            elec_q, elec_r, 1, idcs_out, idcs_in, lift_matrix[1],
            sys.elec_fluid);
          // maxwell fields
          discr.num_flux(dg::make_view_sequence(
                           qright.subarray(dg::all{}, std::make_tuple(10, 13)),
                           qright.subarray(dg::all{}, std::make_tuple(13, 16))),
            fields_q, fields_r, 1, idcs_out, idcs_in, lift_matrix[1], sys.msys);
        }
        else
        {
#ifdef EVOLVE_IONS
          // ion fluid
          discr.num_flux(
            q0.subarray(elem + 1, dg::all{}, std::make_tuple(0, 5)), ion_q,
            ion_r, 1, idcs_out, idcs_in, lift_matrix[1], sys.ion_fluid);
#endif
          // electron fluid
          discr.num_flux(
            q0.subarray(elem + 1, dg::all{}, std::make_tuple(5, 10)), elec_q,
            elec_r, 1, idcs_out, idcs_in, lift_matrix[1], sys.elec_fluid);
          // maxwell fields
          discr.num_flux(
            dg::make_view_sequence(
              q0.subarray(elem + 1, dg::all{}, std::make_tuple(10, 13)),
              q0.subarray(elem + 1, dg::all{}, std::make_tuple(13, 16))),
            fields_q, fields_r, 1, idcs_out, idcs_in, lift_matrix[1], sys.msys);
        }
      }
      // time advance
      for (size_t i = 0; i < q0.shape()[1]; ++i)
      {
        for (size_t comp = 0; comp < 16; ++comp)
        {
          q1(elem, i, comp) = q0(elem, i, comp) + dt * q1(elem, i, comp);
        }
      }
    }
    // finished computation, swap it into q0
    std::swap(q0.data(), q1.data());
  }
};

int main(int argc, char** argv)
{
  feenableexcept(FE_INVALID);
  dg::log::logger log(dg::log::severity_level::info);

  // simulation parameters
  constexpr double Ai = 1836;
  constexpr double Ae = 1;
  constexpr double Zi = 1;
  constexpr double Ze = -1;

  constexpr double gamma = 5. / 3.;
  constexpr double omega_p_norm = 1000;
  constexpr double skin_depth_norm = .01;

  constexpr double n_L = 4;
  constexpr double n_R = 1;
  constexpr double T_L = 1;
  constexpr double T_R = 1;
  constexpr double Bx = 1; //.75;
  constexpr double By_L = -.5;
  constexpr double By_R = .5;
  constexpr double Bz_L = 0;
  constexpr double Bz_R = 0;

  constexpr size_t nelems = 500;
  constexpr size_t Degree = 1;
  constexpr double xmin = -.5;
  constexpr double xmax = .5;
  constexpr double dx = (xmax - xmin) / nelems;

  constexpr double dt = 1e-6;

  constexpr size_t nout = 1000;

  constexpr double t_end = 2;

  size_t writeout_period = std::max<size_t>(round(t_end / (nout * dt)), 1);

  dg::context mctx(argc, argv);
  int rank, nprocs;
  MPI_Comm_rank(mctx.comm, &rank);
  MPI_Comm_size(mctx.comm, &nprocs);

  // not using PETSc
  PetscPopSignalHandler();

  // create simulation context
  if (rank == 0)
  {
    DG_LOG(log, info) << "Setting up simulation context...";
  }
  erk1<Degree> ctx(log, mctx, omega_p_norm, skin_depth_norm, gamma, Ai, Zi, Ae,
    Ze, xmin, xmax, nelems, rank, nprocs, n_L, n_R, T_L, T_R, Bx, By_L, By_R,
    Bz_L, Bz_R);

  dg::hdf5::file f("tf_brio_wu_erk1.h5", H5F_ACC_TRUNC,
    dg::hdf5::plists::file_access(mctx.comm));
  dg::io::structured<1> out(f);

  if (rank == 0)
  {
    DG_LOG(log, info) << "Writing out mesh and initial conditions...";
  }
  // write out mesh
  out.write_uniform_mesh<Degree>(xmin, xmax, nelems);
  std::vector<std::pair<std::string, size_t>> components = {{"ion rho", 0},
    {"ion px", 1}, {"ion py", 2}, {"ion pz", 3}, {"ion e", 4}, {"elec rho", 5},
    {"elec px", 6}, {"elec py", 7}, {"elec pz", 8}, {"elec e", 9}, {"Ex", 10},
    {"Ey", 11}, {"Ez", 12}, {"Bx", 13}, {"By", 14}, {"Bz", 15}};
  // write out initial conditions
  out.write_solution(0, 0, ctx.q0, nelems, nelems * rank / nprocs, components);

  if (rank == 0)
  {
    DG_LOG(log, info) << "Simulation start";
  }
  try
  {
    for (size_t frame = 0; frame < nout; ++frame)
    {
      auto t = frame * writeout_period * dt;
      if (rank == 0)
      {
        DG_LOG(log, info) << "frame " << frame + 1 << ", t = [" << t << ", "
                          << (frame + 1) * writeout_period * dt << "]";
      }
      for (size_t step = 0; step < writeout_period; ++step)
      {
        ctx.step(dt);
      }
      // write out current state of solution
      out.write_solution(frame + 1, (frame + 1) * writeout_period * dt, ctx.q0,
        nelems, nelems * rank / nprocs, components);
    }
  }
  catch (...)
  {
  }
  if (rank == 0)
  {
    DG_LOG(log, info) << "Done";
  }

  return 0;
}
