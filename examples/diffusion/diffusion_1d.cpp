#include "dg/ndg/rect/rect_1d.hpp"

#include "dg/utils/hslab.hpp"

#include "dg/context.hpp"
#include "dg/utils/hdf5_utils.hpp"

#include "dg/partitions/unstructured.hpp"

#include "dg/utils/log.hpp"

#include <Eigen/StdVector>

#include <array>
#include <functional>
#include <cmath>
#include <vector>

#include <iostream>

#include <cassert>

// basic run parameters
constexpr size_t nelems = 1;

constexpr size_t Degree = 2;

#define MODEL 2
#define PSEUDO_TIMESTEP

double conductivity(double r)
{
#if MODEL == 0
  // 1D un-uniformity
  return 1. / (0.25 * sin(6.283185307179586 * r) + 1);
#elif MODEL == 1
  return 1. / (.5 + r);
#else
  // simple test
  return 1;
#endif
}

struct laplace_system
{
};

int main(int argc, char** argv)
{
#if 0
  dg::log::logger log(dg::log::severity_level::info);

  dg::ndg::rect<Degree> discr{};
  constexpr double dx = 1. / nelems;
  constexpr double jac = 0.5 * dx;
  DG_LOG(log, info) << "Computing HDG matrices";
  // calculate advection matrix
  Eigen::Matrix<double, Degree + 1, Degree + 1> mass_matrix;
  discr.calc_mass_matrix(mass_matrix, jac);
  Eigen::Matrix<double, Degree + 1, Degree + 1> amatrix = mass_matrix.inverse();
  Eigen::Matrix<double, Degree + 1, Degree + 1> cadvect_matrix;
  discr.calc_advect_matrix(cadvect_matrix);

  amatrix *= cadvect_matrix;

  // Eigen's default layout is left
  dg::view<double, 2, dg::layout_left> advect_matrix(
    amatrix.data(), Degree + 1, Degree + 1);

  Eigen::SparseMatrix<double> clift_matrix(Degree + 1, 2);
  discr.calc_lift_matrix(clift_matrix);

  laplace_system sys;
  DG_LOG(log, info) << "allocating datastructures";

  // technically don't need to duplicate grad_q into q0/q1, but this makes it
  // easier to implement
  dg::petsc::vector q0_vec;
  dg::petsc::vector q1_vec;
  Eigen::Matrix<double, 2 * (Degree + 1), 2 * (Degree + 1)> A_mat;
  // only need one A matrix, we're going to intermediately store A^{-1} B
  // globally and A^{-1} f
  typedef Eigen::Matrix<double, 2 * (Degree + 1), 2> B_mat_type;
  std::vector<B_mat_type, Eigen::aligned_allocator<B_mat_type>> B_mats(nelems);
  // Need C for an entire element
  Eigen::Matrix<double, 2, 2 * (Degree + 1)> C_mat;

  // data pointers will be assigned later
  dg::view<double, 3> q0(nullptr, nelems, Degree + 1, 2);
  dg::view<double, 3> q1(nullptr, nelems, Degree + 1, 2);

  dg::view<double, 2> q0_lmbda(nullptr, nelems + 1, 1);
  dg::view<double, 2> q1_lmbda(nullptr, nelems + 1, 1);

  DG_LOG(log, info) << "problem setup";

  // source terms/internal fluxes
  for (size_t elem = 0; elem < nelems; ++elem)
  {
  }

  // numerical fluxes

  DG_LOG(log, info) << "global solve";

  DG_LOG(log, info) << "post local solve";
#endif
}
