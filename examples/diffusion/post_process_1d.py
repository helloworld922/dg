import h5py
from numpy import *
from matplotlib.pyplot import *
import scipy.sparse
import scipy.sparse.linalg

with h5py.File('diffusion_1d.h5','r') as f:
    xs = f['mesh'][:].flatten()
    phi = f['phi'][:].flatten()
    phi_x = f['phi_x'][:].flatten()
    
    ia = f['matA']['ia'][:]
    ja = f['matA']['ja'][:]
    data = f['matA']['data'][:]
    matA = scipy.sparse.csr_matrix((data, ja, ia))

    figure()
    plot(xs, phi, 'o-', label='sim')
    xlim([0,1])
    #ylim([0,1])
    grid(True)
    tight_layout()
    
    #figure()
    #plot(xs, jx)
    #ylim(min(amin(jx), -1), max(amax(jx), 1))
    #tight_layout()
