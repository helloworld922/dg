///
/// @file plane_wave.cpp
/// @brief This example implements a basic 1D E&M traveling plane wave.
///

#include "dg/ndg/rect/rect_1d.hpp"

#include "dg/eqsets/maxwell/maxwell.hpp"

#include "dg/context.hpp"
#include "dg/utils/hslab.hpp"
#include "dg/utils/hdf5_utils.hpp"
#include "dg/utils/view_utils.hpp"
#include "dg/utils/log.hpp"

#include <array>
#include <functional>
#include <cmath>
#include <vector>

template <class V>
void init_wave(V& q, double c0, double dx)
{
  constexpr static dg::lobatto<double> quad{};
  auto absc = quad.abscissas(q.extent(1));

  auto freq = 6.283185307179586 / (q.extent(0) * dx);
  for (size_t elem = 0; elem < q.extent(0); ++elem)
  {
    double x0 = elem * dx;
    for (size_t node = 0; node < q.extent(1); ++node)
    {
      // compute position
      double x = x0 + 0.5 * dx * (absc[node] + 1);
      // E
      q(elem, node, 0) = 0;
      q(elem, node, 1) = sin(6.283185307179586 * x);
      q(elem, node, 2) = //sin(6.283185307179586 * x);
      // B
      q(elem, node, 3) = 0;
      q(elem, node, 4) = 0;//sin(6.283185307179586 * x);
      q(elem, node, 5) = sin(6.283185307179586 * x);
    }
  }
}

struct exporter_1d
{
  dg::hdf5::file out;
  exporter_1d(const char* fname) : out(fname, H5F_ACC_TRUNC)
  {
  }

  template <class Q, class I>
  PetscErrorCode write_sol(size_t frame, double t, Q&& q,
    const std::vector<std::pair<std::string, I>>& components)
  {
    dg::hdf5::group g(out, std::to_string(frame).c_str());
    {
      // write out the time of the
      dg::view<double, 1> tv(&t, dg::extents<1>{1});
      dg::hdf5::attribute attr(g, "time", tv);
    }
    auto fspace = dg::hdf5::dataspace::simple(q.extent(0), q.extent(1));
    // write out each component
    for (auto& c : components)
    {
      auto comp = q.subarray(dg::all{}, dg::all{}, c.second);
      dg::hdf5::dataset d(
        g, c.first.c_str(), dg::hdf5::type_id<double>::value(), fspace);
      d.write(comp);
    }
    return 0;
  }

  template <size_t degree>
  void write_mesh(size_t nelems, double dx)
  {
    auto fspace = dg::hdf5::dataspace::simple(nelems * (degree + 1));
    dg::hdf5::dataset d(
      out, "mesh", dg::hdf5::type_id<double>::value(), fspace);

    static dg::spatial::elements::lagrange<double, degree> hi_elem;
    static dg::spatial::elements::lagrange<double, 1> flat_elem;

    dg::hslab<double, 1> coords(dg::extents<1>{2});
    std::array<double, 1> xi = {0};
    dg::shslab<double, 1, 1> pos(dg::extents<1>{1});

    hsize_t starts[] = {0};
    hsize_t counts[] = {1};

    for (size_t elem = 0; elem < nelems; ++elem)
    {
      coords(0) = elem * dx;
      coords(1) = (elem + 1) * dx;

      for (size_t i = 0; i <= degree; ++i)
      {
        xi[0] = hi_elem.absc[0][i];
        starts[0] = elem * (degree + 1) + i;
        pos[0] = flat_elem.interp(xi, coords);
        fspace.select_hyperslab(starts, counts);
        d.write(pos, fspace);
      }
    }
  }
};

///
/// @brief Uses forwards Euler time stepping to solve a 1D Euler fluid system
///
template <size_t Degree>
void erk1(dg::hslab<double, 3>& q0, double c0, double dx, double dt,
  size_t nout, size_t writeout_period, dg::log::logger& log)
{
  DG_LOG(log, info) << "Constructing DG matrices... ";

  auto jac = 0.5 * dx;
  dg::ndg::rect<Degree> discr;

  // calculate advection matrix
  Eigen::Matrix<double, Degree + 1, Degree + 1> mass_matrix;
  discr.calc_mass_matrix(mass_matrix, jac);
  Eigen::Matrix<double, Degree + 1, Degree + 1> amatrix = mass_matrix.inverse();

  std::array<dg::shslab<double, Degree + 1, 2>, 2> lift_matrix = {
    dg::shslab<double, Degree + 1, 2>(1, Degree + 1),
    dg::shslab<double, Degree + 1, 2>(1, Degree + 1)};

  discr.calc_lift_matrix(lift_matrix, amatrix, 1, 1);

  Eigen::Matrix<double, Degree + 1, Degree + 1> cadvect_matrix;
  discr.calc_advect_matrix(cadvect_matrix);

  amatrix *= cadvect_matrix;

  // Eigen's default layout is left
  dg::view<double, 2, dg::layout_left> advect_matrix(
    amatrix.data(), dg::extents<2>{Degree + 1, Degree + 1});

  // write out mesh, initial conditions
  DG_LOG(log, info) << "Writing out mesh and initial conditions...";
  dg::eqsets::maxwell<1, double> sys;
  sys.c0 = c0;
  exporter_1d out("plane_wave_explicit.h5");
  out.write_mesh<Degree>(q0.extent(0), dx);
  std::vector<std::pair<std::string, size_t>> components = {
    {"Ey", 1}, {"Ez", 2}, {"By", 4}, {"Bz", 5}};
  out.write_sol(0, 0, q0, components);

  // initialize working buffer for q1
  dg::hslab<double, 3> q1(q0.shape());

  DG_LOG(log, info) << "Simulation start";
  for (size_t frame = 0; frame < nout; ++frame)
  {
    DG_LOG(log, info) << "frame " << frame + 1 << ", t = ["
                      << frame * writeout_period * dt << ", "
                      << (frame + 1) * writeout_period * dt << "]";
    for (size_t step = 0; step < writeout_period; ++step)
    {
      dg::util::fill(q1, 0);
      // iterate through every element
      for (size_t elem = 0; elem < q0.extent(0); ++elem)
      {
        auto q = dg::make_view_sequence(
          q0.subarray(elem, dg::all{}, std::make_tuple(0, 3)),
          q0.subarray(elem, dg::all{}, std::make_tuple(3, 6)));
        auto r = dg::make_view_sequence(
          q1.subarray(elem, dg::all{}, std::make_tuple(0, 3)),
          q1.subarray(elem, dg::all{}, std::make_tuple(3, 6)));

        // internal flux
        discr.int_flux(q, r, advect_matrix, sys);
        // numerical flux
        // left face
        {
          std::array<size_t, 1> idcs_in = {0};
          std::array<size_t, 1> idcs_out = {Degree};
          if (elem == 0)
          {
            // periodic boundary condition
            auto qout = dg::make_view_sequence(
              q0.subarray(q0.extent(0) - 1, dg::all{}, std::make_tuple(0, 3)),
              q0.subarray(q0.extent(0) - 1, dg::all{}, std::make_tuple(3, 6)));
            discr.num_flux(
              qout, q, r, -1, idcs_out, idcs_in, lift_matrix[0], sys);
          }
          else
          {
            auto qout = dg::make_view_sequence(
              q0.subarray(elem - 1, dg::all{}, std::make_tuple(0, 3)),
              q0.subarray(elem - 1, dg::all{}, std::make_tuple(3, 6)));
            discr.num_flux(
              qout, q, r, -1, idcs_out, idcs_in, lift_matrix[0], sys);
          }
        }
        // right face
        {
          std::array<size_t, 1> idcs_in = {Degree};
          std::array<size_t, 1> idcs_out = {0};
          if (elem == q0.extent(0) - 1)
          {
            // periodic boundary condition
            auto qout = dg::make_view_sequence(
              q0.subarray(0, dg::all{}, std::make_tuple(0, 3)),
              q0.subarray(0, dg::all{}, std::make_tuple(3, 6)));
            discr.num_flux(
              qout, q, r, 1, idcs_out, idcs_in, lift_matrix[1], sys);
          }
          else
          {
            auto qout = dg::make_view_sequence(
              q0.subarray(elem + 1, dg::all{}, std::make_tuple(0, 3)),
              q0.subarray(elem + 1, dg::all{}, std::make_tuple(3, 6)));
            discr.num_flux(
              qout, q, r, 1, idcs_out, idcs_in, lift_matrix[1], sys);
          }
        }
        // time advance
        for (size_t i = 0; i <= Degree; ++i)
        {
          for (size_t comp = 0; comp < 3; ++comp)
          {
            dg::get<0>(r)(i, comp) =
              dg::get<0>(q)(i, comp) + dt * dg::get<0>(r)(i, comp);
          }
          for (size_t comp = 0; comp < 3; ++comp)
          {
            dg::get<1>(r)(i, comp) =
              dg::get<1>(q)(i, comp) + dt * dg::get<1>(r)(i, comp);
          }
        }
      }
      // swap buffers
      std::swap(q0.data(), q1.data());
    }
    // write out current state of solution
    out.write_sol(
      frame + 1, (frame + 1) * writeout_period * dt, q0, components);
  }
  DG_LOG(log, info) << "Done";
}

int main(int argc, char** argv)
{
  dg::log::logger log(dg::log::severity_level::info);

  constexpr double c0 = 1;
  constexpr size_t nelems = 100;
  constexpr size_t Degree = 2;
  constexpr double dx = 1. / nelems;

  double dt = 1e-4;

  // Run sim to a time of about 1
  constexpr size_t nout = 100;
  size_t writeout_period = std::max<size_t>(round(1. / (nout * dt)), 1);
  DG_LOG(log, info) << "dt = " << dt
                    << ", writeout_period = " << writeout_period;

  // initial conditions
  DG_LOG(log, info) << "Applying initial conditions...";
  dg::hslab<double, 3> q(nelems, Degree + 1, 6);
  init_wave(q, c0, dx);

  DG_LOG(log, info) << "Using forwards Euler timestepping";
  erk1<Degree>(q, c0, dx, dt, nout, writeout_period, log);

  return 0;
}
