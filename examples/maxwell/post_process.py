import h5py
from numpy import *
from matplotlib.pyplot import *

def plot_dg(xs, ys, degree, *args, **kwargs):
    nelems = len(xs) // (degree + 1)
    x = zeros(len(xs) + nelems-1)
    y = zeros(len(ys) + nelems-1)
    for i in range(degree+1):
        x[i::degree+2] = xs[i::degree+1]
        y[i::degree+2] = ys[i::degree+1]
    x[degree+1::degree+2] = nan
    y[degree+1::degree+2] = nan
    plot(x, y, *args, **kwargs)

try:
    with h5py.File('plane_wave_explicit.h5','r') as f:
        xs = f['mesh'][:].flatten()
        degree = f['0']['Ey'].shape[1] - 1
    
        figure()
        for frame in range(0,101,20):
            Ey = f[str(frame)]['Ey'][:].flatten()
            plot_dg(xs, Ey, degree)
        grid(True)
        #ylim([0,2.5])

        tight_layout()
except:
    pass
