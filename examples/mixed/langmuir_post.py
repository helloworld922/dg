import h5py
from numpy import *
from matplotlib.pyplot import *
import matplotlib.animation as animation

end_frame = 401

# TODO: change these to read in from the output file
c0 = 100
Ai = 100
Ae = 1
Zi = 1
Ze = -1

def left_bound(nelems):
    return nelems * 2 // 5

def right_bound(nelems):
    return nelems * 3 // 5

def make_dg(v, degree, res=None):
    nelems = len(v) // (degree + 1)
    if(res is None):
        res = zeros(len(v)+nelems-1)
    for i in range(degree+1):
        res[i::degree+2] = v[i::degree+1]
    res[degree+1::degree+2] = nan
    return res

def extract_comp(dset, comp):
    return dset[comp][:]

def extract_rho(dset,Zi,Ai,Ze,Ae):
    # extract MHD properties required
    mhd_rho = extract_comp(dset, 'rho')*(1-Zi*Ae/(Ai*Ze))

    # two-fluid Ex
    ion_rho = extract_comp(dset, 'ion rho')
    elec_rho = extract_comp(dset, 'elec rho')
    
    # total number of elements
    nelems = mhd_rho.shape[0]+ion_rho.shape[0]
    nleft = left_bound(nelems)
    nright = right_bound(nelems)
    nmhd = mhd_rho.shape[0]
    
    rho = zeros((nelems, ion_rho.shape[1]))
    rho[:nleft,:] = mhd_rho[:nleft]
    rho[nright:,:] = mhd_rho[nleft:]
    rho[nleft:nright,:] = ion_rho+elec_rho
    return rho

def extract_p(dset):
    # extract MHD properties required
    mhd_px = extract_comp(dset, 'px')
    mhd_py = extract_comp(dset, 'py')
    mhd_pz = extract_comp(dset, 'pz')
    # total number of elements

    # two-fluid Ex
    ion_px = extract_comp(dset, 'ion px')
    elec_px = extract_comp(dset, 'elec px')
    ion_py = extract_comp(dset, 'ion py')
    elec_py = extract_comp(dset, 'elec py')
    ion_pz = extract_comp(dset, 'ion pz')
    elec_pz = extract_comp(dset, 'elec pz')
    
    nelems = mhd_px.shape[0]+ion_px.shape[0]
    nleft = left_bound(nelems)
    nright = right_bound(nelems)
    nmhd = mhd_px.shape[0]
    p = zeros((3,nelems, ion_px.shape[1]))
    p[0,:nleft,:] = mhd_px[:nleft]
    p[1,:nleft,:] = mhd_py[:nleft]
    p[2,:nleft,:] = mhd_pz[:nleft]
    p[0,nright:,:] = mhd_px[nleft:]
    p[1,nright:,:] = mhd_py[nleft:]
    p[2,nright:,:] = mhd_pz[nleft:]
    p[0,nleft:nright,:] = ion_px+elec_px
    p[1,nleft:nright,:] = ion_px+elec_py
    p[2,nleft:nright,:] = ion_px+elec_pz
    return p[0]

def extract_e(dset):
    # extract MHD properties required
    mhd_e = extract_comp(dset, 'e')
    # total number of elements

    # two-fluid Ex
    ion_e = extract_comp(dset, 'ion e')
    elec_e = extract_comp(dset, 'elec e')
    tf_Ex = extract_comp(dset, 'Ex')
    tf_Bx = extract_comp(dset, 'tf Bx')
    tf_Ey = extract_comp(dset, 'Ey')
    tf_By = extract_comp(dset, 'tf By')
    tf_Ez = extract_comp(dset, 'Ez')
    tf_Bz = extract_comp(dset, 'tf Bz')
    
    nelems = mhd_e.shape[0]+ion_e.shape[0]
    nleft = left_bound(nelems)
    nright = right_bound(nelems)
    nmhd = mhd_e.shape[0]
    e = zeros((nelems, ion_e.shape[1]))
    e[:nleft,:] = mhd_e[:nleft]
    e[nright:,:] = mhd_e[nleft:]
    e[nleft:nright,:] = ion_e+elec_e + 0.5*(tf_Bx**2+tf_By**2+tf_Bz**2) +\
                        0.5/c0**2*(tf_Ex**2 + tf_Ey**2 + tf_Ez**2)
    return e

def extract_ni(dset):
    global Ai, Ae
    # extract MHD properties required
    mhd_rho = extract_comp(dset, 'rho')
    # total number of elements

    # two-fluid Ex
    ion_rho = extract_comp(dset, 'ion rho')
    nelems = mhd_rho.shape[0]+ion_rho.shape[0]
    nleft = left_bound(nelems)
    nright = right_bound(nelems)
    nmhd = mhd_rho.shape[0]
    rho = zeros((nelems, ion_rho.shape[1]))
    rho[:nleft,:] = mhd_rho[:nleft]/(Ai+Ae)
    rho[nright:,:] = mhd_rho[nleft:]/(Ai+Ae)
    rho[nleft:nright,:] = ion_rho/Ai
    return rho

def extract_ne(dset):
    global Ai,Ae
    # extract MHD properties required
    mhd_rho = extract_comp(dset, 'rho')
    # total number of elements

    # two-fluid Ex
    elec_rho = extract_comp(dset, 'elec rho')
    nelems = mhd_rho.shape[0]+elec_rho.shape[0]
    nleft = left_bound(nelems)
    nright = right_bound(nelems)
    nmhd = mhd_rho.shape[0]
    rho = zeros((nelems, elec_rho.shape[1]))
    rho[:nleft,:] = mhd_rho[:nleft]/(Ai+Ae)
    rho[nright:,:] = mhd_rho[nleft:]/(Ai+Ae)
    rho[nleft:nright,:] = elec_rho/Ae
    return rho

def extract_Ex(dset):
    # extract MHD properties required
    mhd_rho = extract_comp(dset, 'rho')
    mhd_uy = extract_comp(dset, 'py')/mhd_rho
    mhd_uz = extract_comp(dset, 'pz')/mhd_rho
    mhd_By = extract_comp(dset, 'mhd By')
    mhd_Bz = extract_comp(dset, 'mhd Bz')
    # total number of elements
    nmhd = mhd_rho.shape[0]
    
    # two-fluid Ex
    tf_Ex = extract_comp(dset, 'Ex')
    nelems = mhd_rho.shape[0]+tf_Ex.shape[0]
    nleft = left_bound(nelems)
    nright = right_bound(nelems)
    Ex = zeros((nelems, tf_Ex.shape[1]))
    Ex[:nleft,:] = mhd_uy[:nleft]*mhd_Bz[:nleft]-mhd_uz[:nleft]*mhd_By[:nleft]
    Ex[nright:,:] = mhd_uy[nleft:]*mhd_Bz[nleft:]-mhd_uz[nleft:]*mhd_By[nleft:]
    Ex[nleft:nright,:] = tf_Ex
    return Ex

ffmpeg_writer = animation.writers['ffmpeg']

ioff()
comps = ['rho','e','Ex','ni', 'ne']
#comps = ['rho','e','Ex']
comps = []
funs = [lambda dset: extract_rho(dset, Zi, Ai, Ze, Ae),
        extract_e, extract_Ex, extract_ni, extract_ne]
ylims = [[.99*(Ae+Ai), 1.01*(Ae+Ai)],None,[-0.1,0.1], [0.99,1.01],[0.99,1.01]]
suffix = ''#'_good'

with h5py.File('mixed_langmuir%s.h5'%suffix,'r') as f:
    with h5py.File('mixed_langmuir%s_raw.h5'%suffix,'r') as fraw:
        xs = f['mesh'][:].flatten()
        degree = f['0']['elec rho'].shape[1] - 1
        dx = xs[degree] - xs[0]

        # plot Ex, use Ohm's law for mhd region
        xs = make_dg(xs, degree)
        ys = None
        ysraw = None

        # animated plots
        for c,fun,yl in zip(comps,funs,ylims):
            metadata = dict(title='mixed_langmuir%s_%s'%(suffix,c))
            writer = ffmpeg_writer(fps=60, metadata=metadata, bitrate=6000)
            dpi = 150
            fig = figure(dpi=dpi)
            #fig = figure()
            line = None
            line_raw = None

            with writer.saving(fig, 'mixed_langmuir%s_%s.mp4'%(suffix,c), dpi):
                print('%s animation'%c)
                for frame in range(end_frame):
                    if(str(frame) in f and str(frame) in fraw):
                        print(frame)
                        v = fun(f[str(frame)]).flatten()
                        ys = make_dg(v, degree, ys)
                        vraw = fun(fraw[str(frame)]).flatten()
                        ysraw = make_dg(vraw, degree, ysraw)
                        if(line is None):
                            line = plot(xs, ys, label='conservative')[0]
                            line_raw = plot(xs, ysraw, label='direct')[0]
                            grid(True)
                            xlim([-2,2])
                            if(yl is not None):
                                ylim(yl)
                            else:
                                minv = amin(v)
                                maxv = amax(v)
                                dv = maxv - minv
                                if(dv < 1e-10):
                                    dv = 1
                                ylim([minv-dv, maxv+dv])
                            legend(loc='upper left')
                            tight_layout()
                        else:
                            line.set_data(xs, ys)
                            line_raw.set_data(xs, ysraw)
                        writer.grab_frame()

        #close('all')
        ion()

        # conservation testing
        ts = empty(end_frame)
        mass_cons = empty(end_frame)
        mass_noncons = empty(end_frame)
        p_cons = empty(end_frame)
        p_noncons = empty(end_frame)
        e_cons = empty(end_frame)
        e_noncons = empty(end_frame)
        
        print('conservation checking')
        for frame in range(end_frame):
            if(str(frame) in f and str(frame) in fraw):
                print(frame)
                v = extract_rho(f[str(frame)],Zi,Ai,Ze,Ae)
                vraw = extract_rho(fraw[str(frame)],Zi,Ai,Ze,Ae)
                # assume Degree 1, just do a direct sum and average
                # TODO: generalize?
                ts[frame] = f[str(frame)].attrs['time'][0]
                mass_cons[frame] = sum(v)*0.5*dx
                mass_noncons[frame] = sum(vraw)*0.5*dx

                v = extract_p(f[str(frame)])
                vraw = extract_p(fraw[str(frame)])
                # assume Degree 1, just do a direct sum and average
                # TODO: generalize?
                p_cons[frame] = sum(v)*0.5*dx
                p_noncons[frame] = sum(vraw)*0.5*dx

                v = extract_e(f[str(frame)])
                vraw = extract_e(fraw[str(frame)])
                # assume Degree 1, just do a direct sum and average
                # TODO: generalize?
                e_cons[frame] = sum(v)*0.5*dx
                e_noncons[frame] = sum(vraw)*0.5*dx
        figure()
        semilogy(ts, abs(mass_cons-mass_cons[0]), label='conservative')
        semilogy(ts, abs(mass_noncons-mass_noncons[0]), label='direct')
        #gca().ticklabel_format(axis='y',style='sci',scilimits=(-2,2))
        xlabel('t')
        ylabel(r'$|\Delta m|$')
        legend(loc='best')
        grid(True)
        tight_layout()

        figure()
        semilogy(ts, abs(e_cons-e_cons[0]), label='conservative')
        semilogy(ts, abs(e_noncons-e_noncons[0]), label='direct')
        #gca().ticklabel_format(axis='y',style='sci',scilimits=(-2,2))
        xlabel('t')
        ylabel(r'$|\Delta e|$')
        legend(loc='best')
        grid(True)
        tight_layout()
