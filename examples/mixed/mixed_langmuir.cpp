///
/// @file mixed_langmuir.cpp
/// @brief This example implements Langmuir oscillations propogating from a
/// small jump discontinuity using the two-fluid model. The langmuir wave is
/// allowed to
/// propogate through an MHD region.
/// It implements parallelism via MPI
///

#include "dg/ndg/rect/rect_1d.hpp"

#include "dg/eqsets/systems/two_fluid.hpp"
#include "dg/eqsets/mhd/imhd_rusanov.hpp"
#include "dg/eqsets/mhd/current_density_ip.hpp"
#include "dg/eqsets/transition/tf_to_mhd.hpp"
#include "dg/eqsets/transition/tf_to_mhd_raw.hpp"
#include "dg/eqsets/transition/mhd_to_tf.hpp"

#include "dg/context.hpp"
#include "dg/utils/hslab.hpp"
#include "dg/utils/hdf5_utils.hpp"
#include "dg/utils/view_utils.hpp"
#include "dg/utils/log.hpp"

#include "dg/io/structured/structured_1d.hpp"

#include <array>
#include <functional>
#include <cmath>
#include <vector>

#include <xmmintrin.h>

// define to use raw "non-conservative" numerical flux
#define USE_VAR_TRANSLATION
//#define GOOD_MATCHING

///
/// MHD initial conditions
///
template <class V>
void mhd_initial_conds(V&& q, size_t jump_pos, double gamma, double Ai,
  double Ae, double n_L, double n_R, double T,
  const std::vector<size_t>& elem_idcs)
{
  auto& fluid = dg::get<0>(q);
  auto& B = dg::get<1>(q);
  for (size_t i = 0; i < elem_idcs.size(); ++i)
  {
    for (size_t node = 0; node < fluid.extent(1); ++node)
    {
      if (elem_idcs[i] < jump_pos)
      {
        // left state
        fluid(i, node, 0) = Ai * n_L;
        fluid(i, node, 4) = 2 * n_L * T / (gamma - 1);
      }
      else
      {
        // right state
        fluid(i, node, 0) = Ai * n_R;
        fluid(i, node, 4) = 2 * n_R * T / (gamma - 1);
      }

      fluid(i, node, 1) = 0;
      fluid(i, node, 2) = 0;
      fluid(i, node, 3) = 0;

      B(i, node, 0) = 0;
      B(i, node, 1) = 0;
      B(i, node, 2) = 0;
    }
  }
}

///
/// Two-fluid initial conditions
///
template <class V>
void tf_initial_conds(V&& q, size_t jump_pos, double gamma, double Ai,
  double Ae, double n_L, double n_R, double T,
  const std::vector<size_t>& elem_idcs)
{
  auto& ifluid = dg::get<0>(q);
  auto& efluid = dg::get<1>(q);
  auto& E = dg::get<2>(q);
  auto& B = dg::get<3>(q);
  for (size_t i = 0; i < elem_idcs.size(); ++i)
  {
    for (size_t node = 0; node < ifluid.extent(1); ++node)
    {
      if (elem_idcs[i] < jump_pos)
      {
        // left state
        ifluid(i, node, 0) = Ai * n_L;
        efluid(i, node, 0) = Ae * n_L;
        ifluid(i, node, 4) = n_L * T / (gamma - 1);
        efluid(i, node, 4) = n_L * T / (gamma - 1);
      }
      else
      {
        // right state
        ifluid(i, node, 0) = Ai * n_R;
        efluid(i, node, 0) = Ae * n_R;
        ifluid(i, node, 4) = n_R * T / (gamma - 1);
        efluid(i, node, 4) = n_R * T / (gamma - 1);
      }

      ifluid(i, node, 1) = 0;
      ifluid(i, node, 2) = 0;
      ifluid(i, node, 3) = 0;
      efluid(i, node, 1) = 0;
      efluid(i, node, 2) = 0;
      efluid(i, node, 3) = 0;

      E(i, node, 0) = 0;
      E(i, node, 1) = 0;
      E(i, node, 2) = 0;

      B(i, node, 0) = 0;
      B(i, node, 1) = 0;
      B(i, node, 2) = 0;
    }
  }
}

///
/// interior face
///
struct interior_face
{
  size_t in_elem;
  size_t in_node;
  size_t local_in;
  size_t out_elem;
  size_t out_node;
  size_t local_out;
  size_t face_idx;
  double normal;

  interior_face(size_t in_elem, size_t in_node, size_t local_in,
    size_t out_elem, size_t out_node, size_t local_out, size_t face_idx,
    double normal)
    : in_elem(in_elem), in_node(in_node), local_in(local_in),
      out_elem(out_elem), out_node(out_node), local_out(local_out),
      face_idx(face_idx), normal(normal)
  {
  }

  interior_face(const interior_face& o) = default;
};

struct partition
{
  int rank, nprocs;
  size_t nelems;

  // faces which need to transition from two-fluid to MHD (MHD is interior
  // element)
  std::vector<interior_face> mhd_transition_int_faces;
  // faces which need to transition from MHD to two-fluid (two-fluid is interior
  // element)
  std::vector<interior_face> tf_transition_int_faces;

  // global element indices of locally owned elements
  std::vector<size_t> mhd_elems;
  // list of all MHD/MHD element interior faces
  std::vector<interior_face> mhd_int_faces;

  std::vector<size_t> tf_elems;
  std::vector<interior_face> tf_int_faces;

  size_t element_left, element_right;
  std::array<size_t, 2> left_bounds, right_bounds, mid_bounds;

  partition(int rank, int nprocs, size_t nelems, size_t degree)
    : rank(rank), nprocs(nprocs), nelems(nelems)
  {
    // TODO: how to properly classify faces?
    // left quarter/right quarter are to be solved with MHD
    left_bounds[0] = 0;
    left_bounds[1] = nelems * 2 / 5;
    right_bounds[0] = nelems * 3 / 5;
    right_bounds[1] = nelems;

    // middle half to be solved with two-fluid
    mid_bounds[0] = nelems * 2 / 5;
    mid_bounds[1] = nelems * 3 / 5;

    element_left = nelems * rank / nprocs;
    element_right = nelems * (rank + 1) / nprocs;

    // local mhd/tf element
    size_t mhd_elem = 0;
    size_t tf_elem = 0;
    size_t mid_mhd_elem = 0;

    for (size_t i = element_left; i < std::min(left_bounds[1], element_right);
         ++i, ++mhd_elem)
    {
      mhd_elems.push_back(i);
      // left face
      if (i != 0)
      {
        mhd_int_faces.emplace_back(
          i, 0, mhd_elem, i - 1, degree, mhd_elem - 1, 0, -1);
      }
      // right face
      if (i + 1 == left_bounds[1])
      {
        mhd_transition_int_faces.emplace_back(
          i, degree, mhd_elem, i + 1, 0, 0, 1, 1);
      }
      else
      {
        mhd_int_faces.emplace_back(
          i, degree, mhd_elem, i + 1, 0, mhd_elem + 1, 1, 1);
      }
    }
    mid_mhd_elem = mhd_elem - 1;
    for (size_t i = std::max(element_left, mid_bounds[0]);
         i < std::min(mid_bounds[1], element_right); ++i, ++tf_elem)
    {
      tf_elems.push_back(i);
      // left face
      if (i != mid_bounds[0])
      {
        tf_int_faces.emplace_back(
          i, 0, tf_elem, i - 1, degree, tf_elem - 1, 0, -1);
      }
      else
      {
        tf_transition_int_faces.emplace_back(
          i, 0, tf_elem, i - 1, degree, mid_mhd_elem, 0, -1);
      }
      // right face
      if (i + 1 != mid_bounds[1])
      {
        tf_int_faces.emplace_back(
          i, degree, tf_elem, i + 1, 0, tf_elem + 1, 1, 1);
      }
      else
      {
        tf_transition_int_faces.emplace_back(
          i, degree, tf_elem, i + 1, 0, mid_mhd_elem + 1, 1, 1);
      }
    }

    for (size_t i = std::max(element_left, right_bounds[0]);
         i < std::min(right_bounds[1], element_right); ++i, ++mhd_elem)
    {
      mhd_elems.push_back(i);
      // left face
      if (i == right_bounds[0])
      {
        // out local is supposed to be # tf elemens owned
        mhd_transition_int_faces.emplace_back(
          i, 0, mhd_elem, i - 1, degree, tf_elem - 1, 0, -1);
      }
      else
      {
        mhd_int_faces.emplace_back(
          i, 0, mhd_elem, i - 1, degree, mhd_elem - 1, 0, -1);
      }
      // right face
      if (i + 1 != nelems)
      {
        mhd_int_faces.emplace_back(
          i, degree, mhd_elem, i + 1, 0, mhd_elem + 1, 1, 1);
      }
    }
  }
};

///
/// @brief Heun's method (TVD RK2)
///
template <size_t Degree>
struct erk2
{
  dg::log::logger& log;
  dg::context& mctx;
  partition& part;

  dg::systems::ideal_two_fluid<1> tf_sys;
  dg::eqsets::imhd_rusanov<1> mhd_sys;
  dg::eqsets::current_density_ip<1> mhd_j;

#ifdef USE_VAR_TRANSLATION
  dg::eqsets::tf_to_mhd_raw<dg::systems::ideal_two_fluid<1>,
    dg::eqsets::imhd_rusanov<1>>
    mhd_trans_sys;
#else
  dg::eqsets::tf_to_mhd<dg::systems::ideal_two_fluid<1>,
    dg::eqsets::imhd_rusanov<1>>
    mhd_trans_sys;
#endif

  dg::eqsets::mhd_to_tf<dg::systems::ideal_two_fluid<1>,
    dg::eqsets::imhd_rusanov<1>>
    tf_trans_sys;

  double xmin, xmax;

  // local data allocation
  std::vector<double> alloc_;

  // MHD fluid variables only
  dg::view<double, 3> q0_mhd;
  dg::view<double, 3> q1_mhd;
  dg::view<double, 3> q2_mhd;

  dg::view<double, 3> q0_ion;
  dg::view<double, 3> q1_ion;
  dg::view<double, 3> q2_ion;

  dg::view<double, 3> q0_elec;
  dg::view<double, 3> q1_elec;
  dg::view<double, 3> q2_elec;

  dg::view<double, 3> q0_E;
  dg::view<double, 3> q1_E;
  dg::view<double, 3> q2_E;

  dg::view<double, 3> q0_B;
  dg::view<double, 3> q1_B;
  dg::view<double, 3> q2_B;

  // ghost cell data
  dg::shslab<double, 16, 2> qleft;
  dg::shslab<double, 16, 2> qright;

  // working space to compute j
  dg::hslab<double, 2> j_buf;

  dg::ndg::rect<Degree> discr;

  // DG discretization matrices
  Eigen::Matrix<double, Degree + 1, Degree + 1> amatrix;

  dg::view<double, 2, dg::layout_left> advect_matrix;
  std::array<dg::shslab<double, Degree + 1, 2>, 2> lift_matrix;

  double n_L, n_R, T;

  erk2(dg::log::logger& log, dg::context& mctx, partition& part,
    double omega_p_norm, double skin_depth_norm, double gamma, double Ai,
    double Zi, double Ae, double Ze, double xmin, double xmax, double n_L,
    double n_R, double T)
    : log(log), mctx(mctx), part(part), mhd_sys(gamma),
      tf_sys(omega_p_norm, skin_depth_norm, gamma, gamma, Ai, Zi, Ae, Ze),
      mhd_trans_sys(tf_sys, mhd_sys), tf_trans_sys(tf_sys, mhd_sys),
      alloc_(3 * (Degree + 1) *
             (part.tf_elems.size() * 16 + part.mhd_elems.size() * 8)),
      xmin(xmin), xmax(xmax),
      q0_mhd(alloc_.data(), part.mhd_elems.size(), Degree + 1, 5),
      q1_mhd(
        q0_mhd.data() + q0_mhd.size(), part.mhd_elems.size(), Degree + 1, 5),
      q2_mhd(
        q1_mhd.data() + q1_mhd.size(), part.mhd_elems.size(), Degree + 1, 5),
      q0_ion(
        q2_mhd.data() + q2_mhd.size(), part.tf_elems.size(), Degree + 1, 5),
      q1_ion(
        q0_ion.data() + q0_ion.size(), part.tf_elems.size(), Degree + 1, 5),
      q2_ion(
        q1_ion.data() + q1_ion.size(), part.tf_elems.size(), Degree + 1, 5),
      q0_elec(
        q2_ion.data() + q2_ion.size(), part.tf_elems.size(), Degree + 1, 5),
      q1_elec(
        q0_elec.data() + q0_elec.size(), part.tf_elems.size(), Degree + 1, 5),
      q2_elec(
        q1_elec.data() + q1_elec.size(), part.tf_elems.size(), Degree + 1, 5),
      q0_E(
        q2_elec.data() + q2_elec.size(), part.tf_elems.size(), Degree + 1, 3),
      q1_E(q0_E.data() + q0_E.size(), part.tf_elems.size(), Degree + 1, 3),
      q2_E(q1_E.data() + q1_E.size(), part.tf_elems.size(), Degree + 1, 3),
      q0_B(q2_E.data() + q2_E.size(),
        part.mhd_elems.size() + part.tf_elems.size(), Degree + 1, 3),
      q1_B(q0_B.data() + q0_B.size(),
        part.mhd_elems.size() + part.tf_elems.size(), Degree + 1, 3),
      q2_B(q1_B.data() + q1_B.size(),
        part.mhd_elems.size() + part.tf_elems.size(), Degree + 1, 3),
      qleft(1, 16), qright(1, 16), j_buf(Degree + 1, 3),
      advect_matrix(nullptr, Degree + 1, Degree + 1),
      lift_matrix({dg::shslab<double, Degree + 1, 2>(1, Degree + 1),
        dg::shslab<double, Degree + 1, 2>(1, Degree + 1)}),
      n_L(n_L), n_R(n_R), T(T)
  {
    auto dx = (xmax - xmin) / part.nelems;
    auto jac = 0.5 * dx;

    DG_LOG(log, info) << "c0: " << tf_sys.msys.c0;
    DG_LOG(log, info) << "ion_fsource: " << tf_sys.ion_fsource.norm_factor;
    DG_LOG(log, info) << "elec_fsource: " << tf_sys.elec_fsource.norm_factor;
    DG_LOG(log, info) << "ion_em_force: " << tf_sys.ion_em_force.norm_factor;
    DG_LOG(log, info) << "elec_em_force: " << tf_sys.elec_em_force.norm_factor;

    DG_LOG(log, info) << "Calculating DG Matrices...";
    // calculate advection matrix
    Eigen::Matrix<double, Degree + 1, Degree + 1> mass_matrix;
    discr.calc_mass_matrix(mass_matrix, jac);
    amatrix = mass_matrix.inverse();

    discr.calc_lift_matrix(lift_matrix, amatrix, 1, 1);

    Eigen::Matrix<double, Degree + 1, Degree + 1> cadvect_matrix;
    discr.calc_advect_matrix(cadvect_matrix);
    amatrix *= cadvect_matrix;
    advect_matrix.data() = amatrix.data();

    DG_LOG(log, info) << "Applying initial conditions...";

    mhd_initial_conds(dg::make_view_sequence(q0_mhd,
                        q0_B.subarray(std::make_tuple(0, q0_mhd.extent(0)),
                          dg::all{}, dg::all{})),
      part.nelems / 2, gamma, Ai, Ae, n_L, n_R, T, part.mhd_elems);

    tf_initial_conds(
      dg::make_view_sequence(q0_ion, q0_elec, q0_E,
        q0_B.subarray(std::make_tuple(q0_mhd.extent(0), q0_B.extent(0)),
          dg::all{}, dg::all{})),
      part.nelems / 2, gamma, Ai, Ae, n_L, n_R, T, part.tf_elems);

    // left BC
    qleft(0, 0) = Ai * n_L;
    qleft(0, 1) = qleft(0, 2) = qleft(0, 3) = 0;
    qleft(0, 4) = 2 * n_L * T / (gamma - 1);
    qleft(0, 5) = 0;
    qleft(0, 6) = 0;
    qleft(0, 7) = 0;
    // right BC
    qright(0, 0) = Ai * n_R;
    qright(0, 1) = qright(0, 2) = qright(0, 3) = 0;
    qright(0, 4) = 2 * n_R * T / (gamma - 1);
    qright(0, 5) = 0;
    qright(0, 6) = 0;
    qright(0, 7) = 0;
  }

  template <class Q, class QLeft, class QRight, class ILeft, class IRight>
  void compute_j(const Q& q_elem, const QLeft& qleft, const QRight& qright,
    const ILeft& idcs_left, const IRight& idcs_right)
  {
    dg::util::fill(j_buf, 0);
    discr.int_flux(q_elem, j_buf, advect_matrix, mhd_j);
    // left face
    {
      std::array<size_t, 1> idcs_in = {0};
      discr.num_flux(
        qleft, q_elem, j_buf, -1, idcs_left, idcs_in, lift_matrix[0], mhd_j);
    }
    // right face
    {
      // TODO: idcs_out comes from where?
      std::array<size_t, 1> idcs_in = {Degree};
      discr.num_flux(
        qright, q_elem, j_buf, 1, idcs_right, idcs_in, lift_matrix[1], mhd_j);
    }
  }

  template <class QMHD, class QTF>
  void compute_j_in(const interior_face& face, const QMHD& qmhd, const QTF& qtf)
  {
    auto& bmhd = dg::get<0>(qmhd);
    auto& btf = dg::get<1>(dg::get<2>(qtf));
    // assumes MHD is the inside element
    if (face.out_elem < part.element_left)
    {
      // don't own the two-fluid element, and it's on the left
      if (face.in_elem + 1 >= part.element_right)
      {
        // don't own the MHD element on the right
        std::array<size_t, 1> idcs_left = {0};
        std::array<size_t, 1> idcs_right = {0};
        compute_j(bmhd.subarray(face.local_in, dg::all{}, dg::all{}),
          qleft.subarray(dg::all{}, std::make_tuple(13, 16)),
          qright.subarray(dg::all{}, std::make_tuple(5, 8)), idcs_left,
          idcs_right);
      }
      else
      {
        // own the MHD element on the right
        std::array<size_t, 1> idcs_left = {0};
        std::array<size_t, 1> idcs_right = {0};
        compute_j(bmhd.subarray(face.local_in, dg::all{}, dg::all{}),
          qleft.subarray(dg::all{}, std::make_tuple(13, 16)),
          bmhd.subarray(face.local_in + 1, dg::all{}, dg::all{}), idcs_left,
          idcs_right);
      }
    }
    else if (face.out_elem >= part.element_right)
    {
      // don't own the two-fluid element, and it's on the right
      if (face.in_elem <= part.element_left)
      {
        // don't own the MHD element on the left
        std::array<size_t, 1> idcs_left = {0};
        std::array<size_t, 1> idcs_right = {0};
        compute_j(bmhd.subarray(face.local_in, dg::all{}, dg::all{}),
          qleft.subarray(dg::all{}, std::make_tuple(5, 8)),
          qright.subarray(dg::all{}, std::make_tuple(13, 16)), idcs_left,
          idcs_right);
      }
      else
      {
        // own the MHD element on the left
        std::array<size_t, 1> idcs_left = {Degree};
        std::array<size_t, 1> idcs_right = {0};
        compute_j(bmhd.subarray(face.local_in, dg::all{}, dg::all{}),
          bmhd.subarray(face.local_in - 1, dg::all{}, dg::all{}),
          qright.subarray(dg::all{}, std::make_tuple(13, 16)), idcs_left,
          idcs_right);
      }
    }
    else if (face.out_elem < face.in_elem)
    {
      // own the two-fluid element, and it's on the left
      if (face.in_elem + 1 >= part.element_right)
      {
        // don't own the MHD element on the right
        std::array<size_t, 1> idcs_left = {Degree};
        std::array<size_t, 1> idcs_right = {0};
        compute_j(bmhd.subarray(face.local_in, dg::all{}, dg::all{}),
          btf.subarray(face.local_out, dg::all{}, dg::all{}),
          qright.subarray(dg::all{}, std::make_tuple(5, 8)), idcs_left,
          idcs_right);
      }
      else
      {
        // own the MHD element on the right
        std::array<size_t, 1> idcs_left = {Degree};
        std::array<size_t, 1> idcs_right = {0};
        compute_j(bmhd.subarray(face.local_in, dg::all{}, dg::all{}),
          btf.subarray(face.local_out, dg::all{}, dg::all{}),
          bmhd.subarray(face.local_in + 1, dg::all{}, dg::all{}), idcs_left,
          idcs_right);
      }
    }
    else
    {
      // own the two-fluid element, and it's on the right
      if (face.in_elem <= part.element_left)
      {
        // don't own the MHD element on the left
        std::array<size_t, 1> idcs_left = {0};
        std::array<size_t, 1> idcs_right = {0};
        compute_j(bmhd.subarray(face.local_in, dg::all{}, dg::all{}),
          qleft.subarray(dg::all{}, std::make_tuple(5, 8)),
          btf.subarray(face.local_out, dg::all{}, dg::all{}), idcs_left,
          idcs_right);
      }
      else
      {
        // own the MHD element on the left
        std::array<size_t, 1> idcs_left = {Degree};
        std::array<size_t, 1> idcs_right = {0};
        compute_j(bmhd.subarray(face.local_in, dg::all{}, dg::all{}),
          bmhd.subarray(face.local_in - 1, dg::all{}, dg::all{}),
          btf.subarray(face.local_out, dg::all{}, dg::all{}), idcs_left,
          idcs_right);
      }
    }
  }

  template <class QMHD, class QTF>
  void compute_j_out(
    const interior_face& face, const QMHD& qmhd, const QTF& qtf)
  {
    // assumes MHD is the outside element
    // only call if we own the MHD element on the outside!
    auto& bmhd = dg::get<0>(qmhd);
    auto& btf = dg::get<1>(dg::get<2>(qtf));
    if (face.out_elem < face.in_elem)
    {
      // MHD element on the left
      if (face.out_elem <= part.element_left)
      {
        // don't own the MHD element on the left
        std::array<size_t, 1> idcs_left = {0};
        std::array<size_t, 1> idcs_right = {0};
        compute_j(bmhd.subarray(face.local_out, dg::all{}, dg::all{}),
          qleft.subarray(dg::all{}, std::make_tuple(5, 8)),
          btf.subarray(face.local_in, dg::all{}, dg::all{}), idcs_left,
          idcs_right);
      }
      else
      {
        // own the MHD element on the left
        std::array<size_t, 1> idcs_left = {Degree};
        std::array<size_t, 1> idcs_right = {0};
        compute_j(bmhd.subarray(face.local_out, dg::all{}, dg::all{}),
          bmhd.subarray(face.local_out - 1, dg::all{}, dg::all{}),
          btf.subarray(face.local_in, dg::all{}, dg::all{}), idcs_left,
          idcs_right);
      }
    }
    else
    {
      // MHD element on the right
      if (face.out_elem + 1 >= part.element_right)
      {
        // don't own the MHD element on the right
        std::array<size_t, 1> idcs_left = {Degree};
        std::array<size_t, 1> idcs_right = {0};
        compute_j(bmhd.subarray(face.local_out, dg::all{}, dg::all{}),
          btf.subarray(face.local_in, dg::all{}, dg::all{}),
          qright.subarray(dg::all{}, std::make_tuple(5, 8)), idcs_left,
          idcs_right);
      }
      else
      {
        // own the MHD element on the right
        std::array<size_t, 1> idcs_left = {Degree};
        std::array<size_t, 1> idcs_right = {0};
        compute_j(bmhd.subarray(face.local_out, dg::all{}, dg::all{}),
          btf.subarray(face.local_in, dg::all{}, dg::all{}),
          bmhd.subarray(face.local_out + 1, dg::all{}, dg::all{}), idcs_left,
          idcs_right);
      }
    }
  }

  ///
  /// @brief Compute the RHS for all MHD elements
  ///
  template <class QMHD, class RMHD, class QTF>
  void rhs_mhd(const QMHD& qmhd, RMHD&& rmhd, const QTF& qtf)
  {
    for (size_t i = 0; i < part.mhd_elems.size(); ++i)
    {
      // interior flux
      auto q_elem = qmhd.subarray(i, dg::all{}, dg::all{});
      auto r_elem = rmhd.subarray(i, dg::all{}, dg::all{});
      discr.int_flux(q_elem, r_elem, advect_matrix, mhd_sys);
    }
    // interior faces fluxes
    for (auto& face : part.mhd_int_faces)
    {
      auto q_elem = qmhd.subarray(face.local_in, dg::all{}, dg::all{});
      std::array<size_t, 1> idcs_in = {face.in_node};
      auto r_elem = rmhd.subarray(face.local_in, dg::all{}, dg::all{});
      // TODO: smarter way to detect whether to use ghost data or not?
      if (face.out_elem < part.element_left)
      {
        auto q_out = dg::make_view_sequence(
          qleft.subarray(dg::all{}, std::make_tuple(0, 5)),
          qleft.subarray(dg::all{}, std::make_tuple(5, 8)));
        std::array<size_t, 1> idcs_out = {0};
        discr.num_flux(q_out, q_elem, r_elem, face.normal, idcs_out, idcs_in,
          lift_matrix[face.face_idx], mhd_sys);
      }
      else if (face.out_elem >= part.element_right)
      {
        auto q_out = dg::make_view_sequence(
          qright.subarray(dg::all{}, std::make_tuple(0, 5)),
          qright.subarray(dg::all{}, std::make_tuple(5, 8)));
        std::array<size_t, 1> idcs_out = {0};
        discr.num_flux(q_out, q_elem, r_elem, face.normal, idcs_out, idcs_in,
          lift_matrix[face.face_idx], mhd_sys);
      }
      else
      {
        // true interior face
        std::array<size_t, 1> idcs_out = {face.out_node};
        discr.num_flux(qmhd.subarray(face.local_out, dg::all{}, dg::all{}),
          q_elem, r_elem, face.normal, idcs_out, idcs_in,
          lift_matrix[face.face_idx], mhd_sys);
      }
    }
    // transition faces
    for (auto& face : part.mhd_transition_int_faces)
    {
      compute_j_in(face, qmhd, qtf);

// extend q_in to include current density
#ifdef USE_VAR_TRANSLATION
      auto q_in = qmhd.subarray(face.local_in, dg::all{}, dg::all{});
#else
      auto q_in = dg::make_view_sequence(
        qmhd.subarray(face.local_in, dg::all{}, dg::all{}),
        j_buf.subarray(dg::all{}, dg::all{}));
#endif
      std::array<size_t, 1> idcs_in = {face.in_node};
      auto r_elem = rmhd.subarray(face.local_in, dg::all{}, dg::all{});
      if (face.out_elem < part.element_left)
      {
        // needed to sync j
        // TODO: this send needs to happen somewhere smarter
        MPI_Send(&j_buf(face.in_node, 0), 3, dg::mpi::mpi_type<double>::type(),
          part.rank - 1, 2, mctx.comm);
        // left ghost
        auto q_out = dg::make_view_sequence(
          qleft.subarray(dg::all{}, std::make_tuple(0, 5)),
          qleft.subarray(dg::all{}, std::make_tuple(5, 10)),
          dg::make_view_sequence(
            qleft.subarray(dg::all{}, std::make_tuple(10, 13)),
            qleft.subarray(dg::all{}, std::make_tuple(13, 16))));
        std::array<size_t, 1> idcs_out = {0};
        discr.num_flux(q_out, q_in, r_elem, face.normal, idcs_out, idcs_in,
          lift_matrix[face.face_idx], mhd_trans_sys);
      }
      else if (face.out_elem >= part.element_right)
      {
        // needed to sync j
        // TODO: this send needs to happen somewhere smarter
        MPI_Send(&j_buf(face.in_node, 0), 3, dg::mpi::mpi_type<double>::type(),
          part.rank + 1, 3, mctx.comm);
        // right ghost
        auto q_out = dg::make_view_sequence(
          qright.subarray(dg::all{}, std::make_tuple(0, 5)),
          qright.subarray(dg::all{}, std::make_tuple(5, 10)),
          dg::make_view_sequence(
            qright.subarray(dg::all{}, std::make_tuple(10, 13)),
            qright.subarray(dg::all{}, std::make_tuple(13, 16))));
        std::array<size_t, 1> idcs_out = {0};
        discr.num_flux(q_out, q_in, r_elem, face.normal, idcs_out, idcs_in,
          lift_matrix[face.face_idx], mhd_trans_sys);
      }
      else
      {
        // our partition owns both elements
        std::array<size_t, 1> idcs_out = {face.out_node};
        auto q_out = qtf.subarray(face.local_out, dg::all{}, dg::all{});
        discr.num_flux(q_out, q_in, r_elem, face.normal, idcs_out, idcs_in,
          lift_matrix[face.face_idx], mhd_trans_sys);
      }
    }
    // boundary conditions
    if (part.rank == 0)
    {
      // left BC
      auto q_elem = qmhd.subarray(0, dg::all{}, dg::all{});
      auto r_elem = rmhd.subarray(0, dg::all{}, dg::all{});

      auto q_out =
        dg::make_view_sequence(qleft.subarray(dg::all{}, std::make_tuple(0, 5)),
          qleft.subarray(dg::all{}, std::make_tuple(5, 8)));
      std::array<size_t, 1> idcs_out = {0};
      std::array<size_t, 1> idcs_in = {0};
      discr.num_flux(
        q_out, q_elem, r_elem, -1, idcs_out, idcs_in, lift_matrix[0], mhd_sys);
    }
    if (part.rank == part.nprocs - 1)
    {
      // right BC
      auto q_elem =
        qmhd.subarray(part.mhd_elems.size() - 1, dg::all{}, dg::all{});
      auto r_elem =
        rmhd.subarray(part.mhd_elems.size() - 1, dg::all{}, dg::all{});

      auto q_out = dg::make_view_sequence(
        qright.subarray(dg::all{}, std::make_tuple(0, 5)),
        qright.subarray(dg::all{}, std::make_tuple(5, 8)));

      std::array<size_t, 1> idcs_out = {0};
      std::array<size_t, 1> idcs_in = {Degree};

      discr.num_flux(
        q_out, q_elem, r_elem, 1, idcs_out, idcs_in, lift_matrix[1], mhd_sys);
    }
  }

  ///
  /// @brief Compute the RHS for all two-fluid elements
  ///
  template <class QTF, class RTF, class QMHD>
  void rhs_tf(const QTF& qtf, RTF&& rtf, const QMHD& qmhd)
  {
    for (size_t i = 0; i < part.tf_elems.size(); ++i)
    {
      // interior flux
      auto q_elem = qtf.subarray(i, dg::all{}, dg::all{});
      auto r_elem = rtf.subarray(i, dg::all{}, dg::all{});
      discr.int_flux(q_elem, r_elem, advect_matrix, tf_sys);
      // source terms
      {
        auto& q_ion = dg::get<0>(q_elem);
        auto& q_elec = dg::get<1>(q_elem);
        auto& q_E = dg::get<0>(dg::get<2>(q_elem));
        auto& q_B = dg::get<1>(dg::get<2>(q_elem));
        auto& ion_r = dg::get<0>(r_elem);
        auto& elec_r = dg::get<1>(r_elem);
        auto& efield_r = dg::get<0>(dg::get<2>(r_elem));
        discr.source(
          dg::make_view_sequence(dg::make_view_sequence(q_ion, q_E, q_B),
            dg::make_view_sequence(q_elec, q_E, q_B),
            q_ion.subarray(dg::all{}, std::make_tuple(1, 4)),
            q_elec.subarray(dg::all{}, std::make_tuple(1, 4))),
          dg::make_view_sequence(
            ion_r.subarray(dg::all{}, std::make_tuple(1, 5)),
            elec_r.subarray(dg::all{}, std::make_tuple(1, 5)), efield_r,
            efield_r),
          tf_sys);
      }
    }
    // interior face fluxes
    for (auto& face : part.tf_int_faces)
    {
      auto q_elem = qtf.subarray(face.local_in, dg::all{}, dg::all{});
      std::array<size_t, 1> idcs_in = {face.in_node};
      auto r_elem = rtf.subarray(face.local_in, dg::all{}, dg::all{});
      // TODO: smarter way to detect whether to use ghost data or not?
      if (face.out_elem < part.element_left)
      {
        auto q_out = dg::make_view_sequence(
          qleft.subarray(dg::all{}, std::make_tuple(0, 5)),
          qleft.subarray(dg::all{}, std::make_tuple(5, 10)),
          dg::make_view_sequence(
            qleft.subarray(dg::all{}, std::make_tuple(10, 13)),
            qleft.subarray(dg::all{}, std::make_tuple(13, 16))));
        std::array<size_t, 1> idcs_out = {0};
        discr.num_flux(q_out, q_elem, r_elem, face.normal, idcs_out, idcs_in,
          lift_matrix[face.face_idx], tf_sys);
      }
      else if (face.out_elem >= part.element_right)
      {
        auto q_out = dg::make_view_sequence(
          qright.subarray(dg::all{}, std::make_tuple(0, 5)),
          qright.subarray(dg::all{}, std::make_tuple(5, 10)),
          dg::make_view_sequence(
            qright.subarray(dg::all{}, std::make_tuple(10, 13)),
            qright.subarray(dg::all{}, std::make_tuple(13, 16))));
        std::array<size_t, 1> idcs_out = {0};
        discr.num_flux(q_out, q_elem, r_elem, face.normal, idcs_out, idcs_in,
          lift_matrix[face.face_idx], tf_sys);
      }
      else
      {
        // true interior face
        auto q_out = qtf.subarray(face.local_out, dg::all{}, dg::all{});
        std::array<size_t, 1> idcs_out = {face.out_node};
        discr.num_flux(q_out, q_elem, r_elem, face.normal, idcs_out, idcs_in,
          lift_matrix[face.face_idx], tf_sys);
      }
    }
    // transition faces
    for (auto& face : part.tf_transition_int_faces)
    {
      auto q_elem = qtf.subarray(face.local_in, dg::all{}, dg::all{});
      std::array<size_t, 1> idcs_in = {face.in_node};
      auto r_elem = rtf.subarray(face.local_in, dg::all{}, dg::all{});

      // extend q_out to include current density

      if (face.out_elem < part.element_left)
      {
        // left ghost
        // TODO: this recv needs to happen somewhere smarter
        MPI_Recv(&qleft(0, 8), 3, dg::mpi::mpi_type<double>::type(),
          part.rank - 1, 3, mctx.comm, MPI_STATUS_IGNORE);
        // augment q_out with j
        auto q_out = dg::make_view_sequence(
          dg::make_view_sequence(
            qleft.subarray(dg::all{}, std::make_tuple(0, 5)),
            qleft.subarray(dg::all{}, std::make_tuple(5, 8))),
          qleft.subarray(dg::all{}, std::make_tuple(8, 11)));
        std::array<size_t, 1> idcs_out = {0};
        discr.num_flux(q_out, q_elem, r_elem, face.normal, idcs_out, idcs_in,
          lift_matrix[face.face_idx], tf_trans_sys);
      }
      else if (face.out_elem >= part.element_right)
      {
        // right ghost
        // TODO: this recv needs to happen somewhere smarter
        MPI_Recv(&qright(0, 8), 3, dg::mpi::mpi_type<double>::type(),
          part.rank + 1, 2, mctx.comm, MPI_STATUS_IGNORE);
        // augment q_out with j
        auto q_out = dg::make_view_sequence(
          dg::make_view_sequence(
            qright.subarray(dg::all{}, std::make_tuple(0, 5)),
            qright.subarray(dg::all{}, std::make_tuple(5, 8))),
          qright.subarray(dg::all{}, std::make_tuple(8, 11)));
        std::array<size_t, 1> idcs_out = {0};
        discr.num_flux(q_out, q_elem, r_elem, face.normal, idcs_out, idcs_in,
          lift_matrix[face.face_idx], tf_trans_sys);
      }
      else
      {
        // our partition owns both elements
        // compute j outside
        compute_j_out(face, qmhd, qtf);
        // augment q_out with j
        auto q_out = dg::make_view_sequence(
          qmhd.subarray(face.local_out, dg::all{}, dg::all{}),
          j_buf.subarray(dg::all{}, dg::all{}));
        std::array<size_t, 1> idcs_out = {face.out_node};
        discr.num_flux(q_out, q_elem, r_elem, face.normal, idcs_out, idcs_in,
          lift_matrix[face.face_idx], tf_trans_sys);
      }
    }
    // no boundary condition faces
  }

  template <class Q0, class Q1>
  void stage0(const Q0& q0, Q1&& q1, double dt)
  {
    for (size_t i = 0; i < q0.extent(0); ++i)
    {
      for (size_t j = 0; j < q0.extent(1); ++j)
      {
        for (size_t k = 0; k < q0.extent(2); ++k)
        {
          q1(i, j, k) = q0(i, j, k) + dt * q1(i, j, k);
        }
      }
    }
  }

  template <class QMHD, class QIon, class QElec, class QE, class QB>
  void sync(QMHD&& qmhd, QIon&& qion, QElec&& qelec, QE&& qe, QB&& qb)
  {
    MPI_Request requests[4] = {
      MPI_REQUEST_NULL, MPI_REQUEST_NULL, MPI_REQUEST_NULL, MPI_REQUEST_NULL};
    size_t num_requests = 0;
    std::array<double, 16 * 2> msg_buf;
    bool sleft_is_tf = part.element_left >= part.mid_bounds[0] &&
                       part.element_left < part.mid_bounds[1];
    bool sright_is_tf = part.element_right > part.mid_bounds[0] &&
                        part.element_right <= part.mid_bounds[1];
    bool left_is_tf = part.element_left > part.mid_bounds[0] &&
                      part.element_left <= part.mid_bounds[1];
    bool right_is_tf = part.element_right >= part.mid_bounds[0] &&
                       part.element_right < part.mid_bounds[1];
    if (part.rank != 0)
    {
      // sink the left face
      if (sleft_is_tf)
      {
        // send left face two-fluid data
        for (size_t i = 0; i < 5; ++i)
        {
          msg_buf[i] = qion(0, 0, i);
        }
        for (size_t i = 0; i < 5; ++i)
        {
          msg_buf[i + 5] = qelec(0, 0, i);
        }
        for (size_t i = 0; i < 3; ++i)
        {
          msg_buf[i + 10] = qe(0, 0, i);
        }
        for (size_t i = 0; i < 3; ++i)
        {
          msg_buf[i + 13] = qb(q0_mhd.extent(0), 0, i);
        }
        MPI_Isend(&msg_buf[0], 16, dg::mpi::mpi_type<double>::type(),
          part.rank - 1, 1, mctx.comm, &requests[num_requests]);
      }
      else
      {
        // send left face MHD data
        for (size_t i = 0; i < 5; ++i)
        {
          msg_buf[i] = qmhd(0, 0, i);
        }
        for (size_t i = 0; i < 3; ++i)
        {
          msg_buf[i + 5] = qb(0, 0, i);
        }
        MPI_Isend(&msg_buf[0], 8, dg::mpi::mpi_type<double>::type(),
          part.rank - 1, 1, mctx.comm, &requests[num_requests]);
      }
      if (left_is_tf)
      {
        // recv left face two-fluid data
        MPI_Irecv(&qleft(0, 0), 16, dg::mpi::mpi_type<double>::type(),
          part.rank - 1, 0, mctx.comm, &requests[num_requests + 1]);
      }
      else
      {
        // recv right face mhd data
        MPI_Irecv(&qleft(0, 0), 8, dg::mpi::mpi_type<double>::type(),
          part.rank - 1, 0, mctx.comm, &requests[num_requests + 1]);
      }
      num_requests += 2;
    }
    if (part.rank != part.nprocs - 1)
    {
      // sink the right wall
      if (sright_is_tf)
      {
        // send right face two-fluid data
        for (size_t i = 0; i < 5; ++i)
        {
          msg_buf[i + 16] = qion(qion.extent(0) - 1, Degree, i);
        }
        for (size_t i = 0; i < 5; ++i)
        {
          msg_buf[i + 5 + 16] = qelec(qelec.extent(0) - 1, Degree, i);
        }
        for (size_t i = 0; i < 3; ++i)
        {
          msg_buf[i + 10 + 16] = qe(qe.extent(0) - 1, Degree, i);
        }
        for (size_t i = 0; i < 3; ++i)
        {
          msg_buf[i + 13 + 16] = qb(qb.extent(0) - 1, Degree, i);
        }
        MPI_Isend(&msg_buf[16], 16, dg::mpi::mpi_type<double>::type(),
          part.rank + 1, 0, mctx.comm, &requests[num_requests]);
      }
      else
      {
        // send right face MHD data
        for (size_t i = 0; i < 5; ++i)
        {
          msg_buf[i + 16] = qmhd(qmhd.extent(0) - 1, Degree, i);
        }
        for (size_t i = 0; i < 3; ++i)
        {
          msg_buf[i + 5 + 16] = qb(qmhd.extent(0) - 1, Degree, i);
        }
        MPI_Isend(&msg_buf[16], 8, dg::mpi::mpi_type<double>::type(),
          part.rank + 1, 0, mctx.comm, &requests[num_requests]);
      }
      if (right_is_tf)
      {
        // recv right face two-fluid data
        MPI_Irecv(&qright(0, 0), 16, dg::mpi::mpi_type<double>::type(),
          part.rank + 1, 1, mctx.comm, &requests[num_requests + 1]);
      }
      else
      {
        // recv right face mhd data
        MPI_Irecv(&qright(0, 0), 8, dg::mpi::mpi_type<double>::type(),
          part.rank + 1, 1, mctx.comm, &requests[num_requests + 1]);
      }
      num_requests += 2;
    }
    MPI_Waitall(num_requests, requests, MPI_STATUSES_IGNORE);
  }

  template <class Q0, class Q1, class Q2>
  void stage1(const Q0& q0, const Q1& q1, Q2&& q2, double dt)
  {
    for (size_t i = 0; i < q0.extent(0); ++i)
    {
      for (size_t j = 0; j < q0.extent(1); ++j)
      {
        for (size_t k = 0; k < q0.extent(2); ++k)
        {
          q2(i, j, k) = 0.5 * (q0(i, j, k) + q1(i, j, k) + dt * q2(i, j, k));
        }
      }
    }
  }

  void step(double dt)
  {
    // stage 1
    sync(q0_mhd, q0_ion, q0_elec, q0_E, q0_B);
    dg::util::fill(q1_mhd, 0);
    dg::util::fill(q1_ion, 0);
    dg::util::fill(q1_elec, 0);
    dg::util::fill(q1_E, 0);
    dg::util::fill(q1_B, 0);
    {
      auto qmhd = dg::make_view_sequence(
        q0_mhd, q0_B.subarray(
                  std::make_tuple(0, q0_mhd.extent(0)), dg::all{}, dg::all{}));
      auto rmhd = dg::make_view_sequence(
        q1_mhd, q1_B.subarray(
                  std::make_tuple(0, q1_mhd.extent(0)), dg::all{}, dg::all{}));
      auto qtf = dg::make_view_sequence(q0_ion, q0_elec,
        dg::make_view_sequence(
          q0_E, q0_B.subarray(std::make_tuple(q0_mhd.extent(0), q0_B.extent(0)),
                  dg::all{}, dg::all{})));
      auto rtf = dg::make_view_sequence(q1_ion, q1_elec,
        dg::make_view_sequence(
          q1_E, q1_B.subarray(std::make_tuple(q1_mhd.extent(0), q1_B.extent(0)),
                  dg::all{}, dg::all{})));
      rhs_mhd(qmhd, rmhd, qtf);
      rhs_tf(qtf, rtf, qmhd);
      // time advance
      stage0(q0_mhd, q1_mhd, dt);
      stage0(q0_ion, q1_ion, dt);
      stage0(q0_elec, q1_elec, dt);
      stage0(q0_E, q1_E, dt);
      stage0(q0_B, q1_B, dt);
    }
    // stage 2
    sync(q1_mhd, q1_ion, q1_elec, q1_E, q1_B);
    dg::util::fill(q2_mhd, 0);
    dg::util::fill(q2_ion, 0);
    dg::util::fill(q2_elec, 0);
    dg::util::fill(q2_E, 0);
    dg::util::fill(q2_B, 0);
    {
      auto qmhd = dg::make_view_sequence(
        q1_mhd, q1_B.subarray(
                  std::make_tuple(0, q1_mhd.extent(0)), dg::all{}, dg::all{}));
      auto rmhd = dg::make_view_sequence(
        q2_mhd, q2_B.subarray(
                  std::make_tuple(0, q2_mhd.extent(0)), dg::all{}, dg::all{}));
      auto qtf = dg::make_view_sequence(q1_ion, q1_elec,
        dg::make_view_sequence(
          q1_E, q1_B.subarray(std::make_tuple(q1_mhd.extent(0), q1_B.extent(0)),
                  dg::all{}, dg::all{})));
      auto rtf = dg::make_view_sequence(q2_ion, q2_elec,
        dg::make_view_sequence(
          q2_E, q2_B.subarray(std::make_tuple(q2_mhd.extent(0), q2_B.extent(0)),
                  dg::all{}, dg::all{})));
      rhs_mhd(qmhd, rmhd, qtf);
      rhs_tf(qtf, rtf, qmhd);
      // time advance
      stage1(q0_mhd, q1_mhd, q2_mhd, dt);
      stage1(q0_ion, q1_ion, q2_ion, dt);
      stage1(q0_elec, q1_elec, q2_elec, dt);
      stage1(q0_E, q1_E, q2_E, dt);
      stage1(q0_B, q1_B, q2_B, dt);
    }
    // swap solution into q0
    std::swap(q0_mhd.data(), q2_mhd.data());
    std::swap(q0_ion.data(), q2_ion.data());
    std::swap(q0_elec.data(), q2_elec.data());
    std::swap(q0_E.data(), q2_E.data());
    std::swap(q0_B.data(), q2_B.data());
  }
};

int main(int argc, char** argv)
{
  // enables FP exceptions
  // Note: need to include <xmmintrin.h>
  //_MM_SET_EXCEPTION_MASK(_MM_GET_EXCEPTION_MASK() & ~_MM_MASK_INVALID);

  dg::log::logger log(dg::log::severity_level::info);

  // simulation parameters
  constexpr double Ai = 100;
  constexpr double Ae = 1;
  constexpr double Zi = 1;
  constexpr double Ze = -1;

  constexpr double gamma = 5. / 3.;

#ifdef GOOD_MATCHING
  constexpr double omega_p_norm = 1000;
  constexpr double skin_depth_norm = 1e-1;
#else
  constexpr double omega_p_norm = 100;
  constexpr double skin_depth_norm = 1;
#endif

  constexpr double n_L = 0.995;
  constexpr double n_R = 1.005;
  constexpr double T = 1;

  constexpr size_t nelems = 2000;
  constexpr size_t Degree = 1;
  constexpr double xmin = -2;
  constexpr double xmax = 2;
  constexpr double dx = (xmax - xmin) / nelems;

  constexpr double dt = 100e-7;

  constexpr size_t nout = 400;

  constexpr double t_end = 1e-4;

  size_t writeout_period = std::max<size_t>(round(t_end / (nout * dt)), 1);

  dg::context mctx(argc, argv);
  int rank, nprocs;
  MPI_Comm_rank(mctx.comm, &rank);
  MPI_Comm_size(mctx.comm, &nprocs);

  // not using PETSc
  PetscPopSignalHandler();

  DG_LOG(log, info) << "Generating element and face partitions...";
  partition part(rank, nprocs, nelems, Degree);

  // construct simulation context
  DG_LOG(log, info) << "Setting up simulation context...";

  erk2<Degree> ctx(log, mctx, part, omega_p_norm, skin_depth_norm, gamma, Ai,
    Zi, Ae, Ze, xmin, xmax, n_L, n_R, T);

#ifdef USE_VAR_TRANSLATION
#ifdef GOOD_MATCHING
  dg::hdf5::file f("mixed_langmuir_good_raw.h5", H5F_ACC_TRUNC,
    dg::hdf5::plists::file_access(mctx.comm));
#else
  dg::hdf5::file f("mixed_langmuir_raw.h5", H5F_ACC_TRUNC,
    dg::hdf5::plists::file_access(mctx.comm));
#endif
#else
#ifdef GOOD_MATCHING
  dg::hdf5::file f("mixed_langmuir_good.h5", H5F_ACC_TRUNC,
    dg::hdf5::plists::file_access(mctx.comm));
#else
  dg::hdf5::file f("mixed_langmuir.h5", H5F_ACC_TRUNC,
    dg::hdf5::plists::file_access(mctx.comm));
#endif
#endif
  dg::io::structured<1> out(f);

  std::vector<std::pair<std::string, size_t>> mhd_comps = {
    {"rho", 0}, {"px", 1}, {"py", 2}, {"pz", 3}, {"e", 4}};
  std::vector<std::pair<std::string, size_t>> ion_comps = {
    {"ion rho", 0}, {"ion px", 1}, {"ion py", 2}, {"ion pz", 3}, {"ion e", 4}};
  std::vector<std::pair<std::string, size_t>> elec_comps = {{"elec rho", 0},
    {"elec px", 1}, {"elec py", 2}, {"elec pz", 3}, {"elec e", 4}};

  std::vector<std::pair<std::string, size_t>> E_comps = {
    {"Ex", 0}, {"Ey", 1}, {"Ez", 2}};
  std::vector<std::pair<std::string, size_t>> mhd_B_comps = {
    {"mhd Bx", 0}, {"mhd By", 1}, {"mhd Bz", 2}};
  std::vector<std::pair<std::string, size_t>> tf_B_comps = {
    {"tf Bx", 0}, {"tf By", 1}, {"tf Bz", 2}};

  DG_LOG(log, info) << "Writing out mesh and initial conditions...";

  // write out mesh
  out.write_uniform_mesh<Degree>(xmin, xmax, nelems);
  // write out initial conditions
  // TODO: make work in parallel
  out.write_time(0, 0);

  auto nmhd_elems =
    part.left_bounds[1] + part.right_bounds[1] - part.right_bounds[0];
  auto ntf_elems = part.mid_bounds[1] - part.mid_bounds[0];

  // TODO: calculate write offsets
  auto mhd_offset = part.mhd_elems.size() ? part.mhd_elems[0] : 0;
  auto tf_offset =
    part.tf_elems.size()
      ? (part.tf_elems[0] - (part.left_bounds[1] - part.left_bounds[0]))
      : 0;
  if (part.element_left >= part.left_bounds[1])
  {
    mhd_offset -= ntf_elems;
  }

  out.write_solution(0, ctx.q0_ion, ntf_elems, tf_offset, ion_comps);
  out.write_solution(0, ctx.q0_elec, ntf_elems, tf_offset, elec_comps);
  out.write_solution(0, ctx.q0_E, ntf_elems, tf_offset, E_comps);
  out.write_solution(0,
    ctx.q0_B.subarray(std::make_tuple(ctx.q0_mhd.extent(0), ctx.q0_B.extent(0)),
      dg::all{}, dg::all{}),
    ntf_elems, tf_offset, tf_B_comps);

  out.write_solution(0, ctx.q0_mhd, nmhd_elems, mhd_offset, mhd_comps);
  out.write_solution(0,
    ctx.q0_B.subarray(
      std::make_tuple(ctx.q0_mhd.extent(0)), dg::all{}, dg::all{}),
    nmhd_elems, mhd_offset, mhd_B_comps);

  DG_LOG(log, info) << "Simulation start";
  try
  {
    for (size_t frame = 0; frame < nout; ++frame)
    {
      auto t = frame * writeout_period * dt;
      if (rank == 0)
      {
        DG_LOG(log, info) << "frame " << frame + 1 << ", t = [" << t << ", "
                          << (frame + 1) * writeout_period * dt << "]";
        // std::cin.ignore();
      }
      for (size_t step = 0; step < writeout_period; ++step)
      {
        ctx.step(dt);
      }
      // write out current state of solution
      t = (frame + 1) * writeout_period * dt;
      out.write_time(frame + 1, t);
      out.write_solution(
        frame + 1, ctx.q0_ion, ntf_elems, tf_offset, ion_comps);
      out.write_solution(
        frame + 1, ctx.q0_elec, ntf_elems, tf_offset, elec_comps);
      out.write_solution(frame + 1, ctx.q0_E, ntf_elems, tf_offset, E_comps);
      out.write_solution(frame + 1,
        ctx.q0_B.subarray(
          std::make_tuple(ctx.q0_mhd.extent(0), ctx.q0_B.extent(0)), dg::all{},
          dg::all{}),
        ntf_elems, tf_offset, tf_B_comps);

      out.write_solution(
        frame + 1, ctx.q0_mhd, nmhd_elems, mhd_offset, mhd_comps);
      out.write_solution(frame + 1,
        ctx.q0_B.subarray(
          std::make_tuple(ctx.q0_mhd.extent(0)), dg::all{}, dg::all{}),
        nmhd_elems, mhd_offset, mhd_B_comps);
    }
  }
  catch (...)
  {
  }

  DG_LOG(log, info) << "Done";

  return 0;
}
