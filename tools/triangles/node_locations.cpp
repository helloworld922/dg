#include "dg/math/quadrature/triangle.hpp"
#include "dg/math/quadrature/lobatto.hpp"

#include <cppoptlib/boundedproblem.h>
#include <cppoptlib/solver/lbfgsbsolver.h>
#include <cppoptlib/solver/lbfgssolver.h>

#include <iterator>
#include <array>

#include <iostream>

typedef std::array<size_t, 4> orbits_type;

///
/// Number of nodes for the given orbits
/// S0: central node
/// S1: side of bisector closest to vertex orbit (3 nodes)
/// S2: side of bisector further from vertex orbit (3 nodes)
/// S3: off-bisector orbits (6 nodes)
///
size_t num_nodes(const orbits_type& orbits)
{
  return orbits[0] + 3 * (orbits[1] + orbits[2]) + 6 * orbits[3];
}

orbits_type lobatto_orbits(size_t degree)
{
  orbits_type res = {0, 0, 0, 0};
  if (degree == 0)
  {
    return res;
  }
  else if (degree == 1)
  {
    res[2] = 1;
  }
  else if (degree % 2 == 0)
  {
    res[2] = 2;
    res[3] = (degree - 2) / 2;
  }
  else
  {
    res[2] = 1;
    res[3] = (degree - 1) / 2;
  }
  return res;
}

///
/// Iterates through all possible orbits for a given number of nodes
///
struct orbit_iterator
  : public std::iterator<std::forward_iterator_tag, orbits_type>
{
private:
  orbits_type val = {0, 0, 0, 0};

public:
  orbit_iterator(size_t nnodes, bool end = false)
  {
    // begin iterator
    if (nnodes % 3 == 1)
    {
      val[0] = 1;
      --nnodes;
    }
    else if (nnodes % 3 == 2)
    {
      // error
      throw std::runtime_error("impossible number of nodes");
    }

    val[1] = nnodes / 3;

    if (end)
    {
      val[3] = val[1] / 2;
      val[2] = val[1] % 2;
      val[1] = 0;
    }
  }

  const orbits_type& operator*() const
  {
    return val;
  }

  const orbits_type* operator->() const
  {
    return &val;
  }

  orbit_iterator& operator=(const orbit_iterator& o)
  {
    val = o.val;
    return *this;
  }

  bool operator==(const orbit_iterator& o) const
  {
    return val == o.val;
  }

  bool operator!=(const orbit_iterator& o) const
  {
    return !((*this) == o);
  }

  orbit_iterator& operator++()
  {
    if (val[1])
    {
      --val[1];
      ++val[2];
    }
    else if (val[2] > 1)
    {
      ++val[3];
      val[1] = val[2] - 2;
      val[2] = 0;
    }
    return *this;
  }

  orbit_iterator operator++(int)
  {
    orbit_iterator cpy(*this);
    ++(*this);
    return cpy;
  }
};

///
/// Converts scaled coordinates [0,1], [0,1] to [-1,1],[-1,1] (bounds y to be in
/// the triangle)
///
template <class Real>
std::pair<Real, Real> scale_coordinates(const Real& x, const Real& y)
{
  std::pair<Real, Real> res = {2 * x - 1, 2 * (1 - x) * y - 1};
  return res;
}

template <class Real>
struct constant_problem : public cppoptlib::BoundedProblem<Real>
{
  typedef cppoptlib::BoundedProblem<Real> super_type;

  typedef typename super_type::TVector TVector;
  typedef typename super_type::TCriteria TCriteria;

  const dg::triangle_nodes<Real>& quad;
  Eigen::Matrix<Real, Eigen::Dynamic, 1> rhs;

  constant_problem(const dg::triangle_nodes<Real>& quad)
    : super_type(2), quad(quad), rhs(quad.vander.cols())
  {
    typename super_type::TVector lower(2);
    lower(0,0) = 0;
    lower(1,0) = 0;
    typename super_type::TVector upper(2);
    upper(0,0) = 0.9999999999;
    upper(1,0) = 1;
    super_type::setLowerBound(lower);
    super_type::setUpperBound(upper);
  }

  bool callback(const TCriteria& state, const TVector &x) override
  {
    return state.iterations < 100;
  }

  // computes local negative lebesgue constant
  Real value(const typename super_type::TVector& x) override
  {
    constexpr static dg::triangle_modes<Real> modes{};
    auto coord = scale_coordinates(x(0, 0), x(1, 0));
    Real leb{0};
    {
      size_t i_ = 0, j_ = 0;
      for (size_t row = 0; row < quad.vander.cols(); ++row)
      {
        rhs(row, 0) = modes(coord.first, coord.second, i_, j_);
        ++j_;
        if (j_ > quad.degree - i_)
        {
          j_ = 0;
          ++i_;
        }
      }
    }
    rhs = quad.vander_lu.solve(rhs);

    // calculate local lebesgue number
    for (size_t i_ = 0; i_ < rhs.rows(); ++i_)
    {
      leb += fabs(rhs(i_, 0));
    }
    return -leb;
  }
};

///
/// @param width grid to start optimization at
///
template <class Real>
Real lebesgue_constant(const dg::triangle_nodes<Real>& quad, size_t width)
{
  constexpr static dg::triangle_modes<Real> modes{};
  Real res{0};
  constant_problem<Real> p(quad);
  typename constant_problem<Real>::TVector x0(2);

  // TODO: use optimization algorithm to find local maxima of lebesgue constant
  #if 1
  cppoptlib::LbfgsbSolver<constant_problem<Real>> solver;
  x0(0, 0) = 0.5;
  x0(1, 0) = 0.5;
  solver.minimize(p, x0);
  res = -p.value(x0);
  auto pos = scale_coordinates(x0(0), x0(1));
  std::cout << pos.first << ", " << pos.second << ": " << res << '\n';
  #endif
  #if 0
  for (size_t i = 0; i < width; ++i)
  {
    auto x = (Real{1} * i + 0.5) / width;
    for (size_t j = 0; j < width - i; ++j)
    {
      auto y = (Real{1} * j + 0.5) / width;
      // do optimization starting from x/y
      x0(0, 0) = x;
      x0(1, 0) = y;
      std::cout << x0.transpose() << "; ";
      cppoptlib::LbfgsbSolver<constant_problem<Real>> solver;
      solver.minimize(p, x0);

      std::cout << x0.transpose() << ": " << -p.value(x0) << '\n';

      res = std::max(res, -p.value(x0));
    }
  }
#endif
  return res;
}

///
/// @brief Simplex Lebesgue minimization problem
///
template <class Real>
struct lebesgue_problem : public cppoptlib::BoundedProblem<Real>
{
  typedef cppoptlib::BoundedProblem<Real> super_type;

  orbits_type internal_orbit;
  orbits_type boundary_orbit;

  std::vector<Real> absc_;
  dg::view<Real, dg::extents<2>> absc;
  size_t internal_start;

  lebesgue_problem(const orbits_type& boundary_orbits,
    const orbits_type& internal_orbits, size_t degree, Real tol)
    : super_type(
        internal_orbits[1] + internal_orbits[2] + internal_orbits[3] * 2),
      internal_orbit(internal_orbits), boundary_orbit(boundary_orbits),
      absc_((degree + 1) * (degree + 2)),
      absc(absc_.data(), dg::extents<2>{(degree + 1) * (degree + 2) / 2, 2}),
      internal_start(num_nodes(boundary_orbits) + internal_orbits[0])
  {
    // compute boundary abscissa
    dg::lobatto<Real> lob;
    auto edge = lob.abscissas(degree);
    // bottom edge
    for (size_t i = 0; i < edge.size(); ++i)
    {
      absc(i, 0) = edge[i];
      absc(i, 1) = -1;
    }
    // left edge
    for (size_t i = 0; i < edge.size(); ++i)
    {
      absc(i + edge.size(), 0) = -1;
      absc(i + edge.size(), 1) = edge[i];
    }
    // diagonal
    for (size_t i = 0; i < edge.size(); ++i)
    {
      absc(i + 2 * edge.size(), 0) = edge[i];
      absc(i + 2 * edge.size(), 1) = -edge[i];
    }
    // central orbit (if any)
    if (internal_orbits[0])
    {
      absc(internal_start - 1, 0) = Real{2} / 3 - 1;
      absc(internal_start - 1, 1) = Real{2} / 3 - 1;
    }
    // compute bounds
    {
      auto& b = super_type::lowerBound();
      for (size_t i = 0; i < b.rows(); ++i)
      {
        b(i, 0) = 0;
      }
    }
    {
      auto& b = super_type::upperBound();
      for (size_t i = 0; i < b.rows(); ++i)
      {
        b(i, 0) = 1;
      }
    }
  }

  Real value(const typename super_type::TVector& x) override
  {
    // compute internal abscissa
    dg::triangle_nodes<dg::real128> quad(absc);
    return lebesgue_constant(quad, 2);
  }
};

int main(int argc, char** argv)
{
  typedef dg::real128 real_type;
  std::vector<real_type> absc_ = {-1, -1, -sqrt(real_type{1} / 5), -1,
    sqrt(real_type{1} / 5), -1, 1, -1,

    -1, -sqrt(real_type{1} / 5), -1, sqrt(real_type{1} / 5), -1, 1,

    -1 + real_type{2} / 3, -1 + real_type{2} / 3, -sqrt(real_type{1} / 5),
    sqrt(real_type{1} / 5), sqrt(real_type{1} / 5),
    -sqrt(real_type{1} / 5)};
  dg::view<real_type, dg::extents<2>> absc(
    absc_.data(), dg::extents<2>{10, 2});
  dg::triangle_nodes<real_type> quad(absc);

  std::cout << lebesgue_constant(quad, 2) << '\n';
}
