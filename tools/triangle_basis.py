from numpy import *

degrees = linspace(0, 64, 65, dtype=int)

orbit_sizes = array([1,3,6], dtype=int)

def num_nodes(num_orbits):
    global orbit_sizes
    return num_orbits.dot(orbit_sizes)

def lobatto_orbits(degree):
    # @param degree polynomial basis degree
    # returns the number of nodes in each orbit for the edge lobatto nodes
    if(degree == 0):
        return array([0,0,0],dtype=int)
    if(degree == 1):
        return array([0,1,0],dtype=int)
    if(degree % 2 == 0):
        return array([0, 2, (degree-2)//2], dtype=int)
    else:
        return array([0, 1, (degree-1)//2], dtype=int)

# notes:
#  - if num_nodes is not divisible by 3, then 0th central orbit is required
#  - combinations: choosing between 3 or 6

def orbit_combinations(nnodes):
    orbits = array([0,0,0],dtype=int)
    if(nnodes % 3 == 1):
        orbits[0] = 1
        nnodes -= 1
    elif(nnodes % 3 == 2):
        raise RuntimeError('impossible nnodes')
    
    if(nnodes % 6 == 3):
        # require at least one 1st orbit
        orbits[1] = 1
        nnodes -= 3

    ns2_orbits = nnodes//6

    for i in range(0,ns2_orbits+1):
        orbits[2] = i
        orbits[1] += (ns2_orbits-i)*2
        yield copy(orbits)
        orbits[1] -= (ns2_orbits-i)*2

num_probs = 0
for deg in degrees[:10]:
    nnodes = (deg+1)*(deg+2)//2-num_nodes(lobatto_orbits(deg))
    #num_probs += len(list(orbit_combinations(nnodes)))
    print(deg,nnodes)
    for o in orbit_combinations(nnodes):
        print(o)
    print()
