#include "dg/dg_config.hpp"

#include <string>

#include <fstream>
#include <iostream>

int main(int argn, char** argv)
{
  if(argn < 2)
  {
    std::cerr << "converts a file with floating point numbers in base 10 to base 62";
    std::cerr << "usage:\n";
    std::cerr << "mpfr_base_converter in_file [out_file]\n";
    return -1;
  }
  std::ifstream in(argv[1]);
  std::unique_ptr<std::ofstream> fout;
  if(argn == 3)
  {
    fout.reset(new std::ofstream(argv[2]));
  }
  std::ostream& out = argn == 3 ? *fout : std::cout;
  std::string line;
  
  while(in >> line)
  {
    dg::real256 val(line);
    std::array<char,72> buf{0};
    auto& raw = val.backend().data();
    // exponent
    mpfr_exp_t e;
    mpfr_get_str(buf.data(), &e, 62, 0, raw, MPFR_RNDN);
    std::string num(buf.data());
    num.insert(e, ".");
    //dg::real256 val2;
    //auto& raw2 = val2.backend().data();
    //mpfr_set_str(raw2, num.c_str(), 62, MPFR_RNDN);
    out << "\"" << num << "\", ";
  }
}
